import 'dart:ui';
const my_shadowcolor = Color(0X95E9EBF0);
const my_white = Color(0xFFffffff);
const my_grey = Color(0xFFfdf9fa);
const my_greydark = Color(0xffcbc8c8);
const my_greydark10 = Color(0xffbdb8b8);
const my_greymid = Color(0xffd4d4d4);
const my_greydark20 = Color(0xffaaa9a9);
const my_black = Color(0xFF000000);
const my_blackL = Color(0xFF3E3E3E);
const my_blackL2 = Color(0xFF797979);
const my_red = Color(0xFFff5950);
const appbar_color = Color(0xFFfe7240);
const table_yellow = Color(0xFFffd45a);
const selectedtable_color = Color(0xFFfce4db);
const my_green = Color(0xFF00ba58);
const success_snack = Color(0xFF09eb33);
const timeout_snack = Color(0xFFe61a0b);
const my_light_red = Color(0xFFfde7e6);

const mynew_blue = Color(0xFFbb2c3c);
const mynew_dblue = Color(0xFFbb2c3c);
const mynew_dred = Color(0xFFbb2c3c);
const mynew_yellow = Color(0xFFffb135);
const mynew_table = Color(0xFF2b2c43);

const cc_green = Color(0xffe3fff0);
const cc_blueL = Color(0xff657dad);
const cc_blue = Color(0xffe4ecff);
const cc_vilot = Color(0xffede4ff);
const cc_vilotD = Color(0xff8d68d8);
const cc_yellow= Color(0xfff4d9bc);
const cc_yellowDark= Color(0xffe9ab61);
const cc_pink= Color(0xfff37576);
const cashColor = Color(0xFFfe7e68);
const upiColor = Color(0xFF58aafe);
const cardColor = Color(0xFF00a650);
const creditColor = Color(0xFFf0f169);
const cardBgColor = Color(0xFFf6f6f6);
const headerColor = Color(0xFF7f7f7f);
const annotationColor = Color(0xFF00a651);
const taxTextColor = Color(0xFF7f7f7f);
const paymentColor = Color(0xFF818181);
const progressColor = Color(0xFF089d98);
const cardlightpinkColor = Color(0xFFf2cfd3);
const cardlinepinkColor = Color(0xFFf2bed4);
const regenerartedColor = Color(0xFFd3defc);
const lineColor = Color(0xffd9aaaa);
const counterBackYellow = Color(0xfff4fdea);
const counterBackRed= Color(0xfffdeafc);
const mismatchBackRed= Color(0xfff9e8e8);
const counterButton= Color(0xfff37575);











