import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import 'my_colors.dart';
import 'my_text.dart';

const app_name = "IYC";
const Order_item = "Item";
const Order_qty = "Qty";
const Order_total = "Total";
const AppVersion = "2";

TextStyle hederText = TextStyle(
    fontFamily: fontBold,
    fontSize: textSize18,
    color: my_white,
    fontWeight: FontWeight.bold);
TextStyle snackbarStyle = TextStyle(
    fontFamily: fontBold,
    fontSize: textSize13,
    color: my_white,
    fontWeight: FontWeight.bold);
TextStyle Textstyle13 = TextStyle(
    fontFamily: fontBold,
    fontSize: textSize13,
    color: my_black,
    fontWeight: FontWeight.bold);
TextStyle Textstyle14 = TextStyle(
    fontFamily: fontBold,
    fontSize: textSize14,
    color: my_black,
    fontWeight: FontWeight.bold);
TextStyle Textstyle16 = TextStyle(
    fontFamily: fontBold,
    fontSize: textSize16,
    color: my_black,
    fontWeight: FontWeight.bold);
TextStyle Textstyle16_white = TextStyle(
    fontFamily: fontBold,
    fontSize: textSize16,
    color: my_white,
    fontWeight: FontWeight.bold);
TextStyle Textstyle18 = TextStyle(
    fontFamily: fontBold,
    fontSize: textSize18,
    color: my_black,
    fontWeight: FontWeight.bold);

TextStyle btnTextwhite = TextStyle(
    fontFamily: fontBold,
    fontSize: textSize22,
    color: my_white,
    fontWeight: FontWeight.bold);
TextStyle hintText = TextStyle(
    color: my_greydark,
    );
TextStyle errorText = TextStyle(
    color: mynew_dred,
    fontSize: 12,
);
TextStyle prBlackL = const TextStyle(
    color: my_black,
    fontSize: 20,
    fontWeight: FontWeight.w900,
    fontFamily: fontBold,

);



