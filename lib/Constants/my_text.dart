import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'my_colors.dart';

const fontRegular = 'Regular';
const fontMedium = 'Medium';
const fontSemibold = 'Semibold';
const fontBold = 'Bold';
/* font sizes*/
const textSize12 = 12.0;
const textSize13 = 13.0;
const textSize14 = 14.0;
const textSize15 = 15.0;
const textSize16 = 16.0;
const textSize18 = 18.0;
const textSize20 = 20.0;
const textSize22 = 22.0;
const textSize24 = 24.0;
const textSize28 = 28.0;
const textSize30 = 30.0;


InputDecoration phoneTextField = const InputDecoration(
  hintText: "Phone Number",
  labelText: "Phone Number",
  errorText: "Value Can't be Null",

  labelStyle: TextStyle(
    color: appbar_color,
  ),
  enabledBorder: OutlineInputBorder(
    // width: 0.0 produces a thin "hairline" border
    borderSide: BorderSide(color: Colors.grey, width: 0.0),
  ),
  focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: my_greydark, width: 0.0)),
  border: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.grey, width: 0.0),

  ),
);
InputDecoration nameTextField = const InputDecoration(
  hintText: "Name",
  labelText: "Name",
  errorText: "Value Can't be Null",

  labelStyle: TextStyle(
    color: appbar_color,
  ),

  enabledBorder: OutlineInputBorder(
    // width: 0.0 produces a thin "hairline" border
    borderSide: BorderSide(color: Colors.grey, width: 0.0),
  ),
  focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: my_greydark, width: 0.0)),
  border: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.grey, width: 0.0),

  ),
);
TextStyle paymentText = const TextStyle(
    fontFamily: fontBold,
    fontSize: textSize15,
    color: paymentColor,
    fontWeight: FontWeight.bold);
TextStyle paymentAmountText = const TextStyle(
    fontFamily: fontBold,
    fontSize: textSize15,
    color: my_black,
    fontWeight: FontWeight.bold);
TextStyle amountText = const TextStyle(
    fontFamily: fontBold,
    fontSize: textSize15,
    color: my_black,
    fontWeight: FontWeight.bold);
TextStyle taxText = const TextStyle(
    fontFamily: fontBold,
    fontSize: textSize16,
    color: taxTextColor,
    fontWeight: FontWeight.bold);
TextStyle annotationText = const TextStyle(
    fontFamily: fontBold,
    fontSize: textSize16,
    color: annotationColor,
    fontWeight: FontWeight.bold);
TextStyle smallHeaderText = const TextStyle(
  fontFamily: fontBold,
  fontSize: textSize16,
  color: headerColor,
);
TextStyle headerText = const TextStyle(
    fontFamily: fontBold,
    fontSize: textSize18,
    color: my_black,
    fontWeight: FontWeight.bold);
TextStyle counterHead = const TextStyle(fontSize: 18,
    color: my_black,
    fontWeight: FontWeight.w300);

TextStyle counterMainRed = const TextStyle(fontSize: 26,
    color: mynew_blue,
    fontWeight: FontWeight.w900,
  fontFamily: 'Gilroy',);
TextStyle counterValue = const TextStyle(
    fontSize: 22, fontWeight: FontWeight.w800, fontFamily: 'Gilroy');
TextStyle counterMainGreen = const TextStyle(fontSize: 26,
  color: my_green,
  fontWeight: FontWeight.w900,
  fontFamily: 'Gilroy',);
