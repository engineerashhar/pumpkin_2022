import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences{
  static String USER_ID = "USER_ID"; //use case: setString(UserPreferences.USER_ID, "Adhnan");
}
setString(String key,String Value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString(key, Value);
}
setInt(String key,int value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt(key, value);
}
setDouble(String key,double value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setDouble(key, value);
}setBool(String key,bool value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(key, value);
}
getString(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? stringValue = prefs.getString(key);
  return stringValue;
}
getInt(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int? stringValue = prefs.getInt(key);
  return stringValue;
}
getBool(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool? stringValue = prefs.getBool(key);
  return stringValue??false;
}
getDouble(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  double? stringValue = prefs.getDouble(key);
  return stringValue;
}