import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Views/counter_close_gs.dart';

class CounterBillsListScreen extends StatelessWidget {
  List<CounterCloseGS> list= [];
  String title;
   CounterBillsListScreen({Key? key,required this.list,required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true, // this is all you need
          title: Text(title),
      ),
      body: SafeArea(

        child: list.isEmpty?Container(child: const Center(child: Text('No Data')),):ListView.builder(
            itemCount: list.length,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                child: Container(
                  height: 40,
                    padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                    decoration: const BoxDecoration(
                        color: my_white
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(list[index].name,style: const TextStyle(
                            fontSize: 20
                        ),),
                        Text(list[index].value,style: const TextStyle(
                            fontSize: 20
                        ),),
                      ],
                    )),
              );
            })
      ),

    );
  }
}
