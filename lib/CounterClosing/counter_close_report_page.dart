import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/Provider/counter_close_report_provider.dart';
import 'package:flutter_pumpkin/Provider/counter_provider.dart';
import 'package:provider/provider.dart';

import 'counter_bill_list_screen.dart';

class CounterCloseReportPage extends StatelessWidget {
  String counter;
   CounterCloseReportPage({Key? key,required this.counter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CounterClosingReportProvider counterProvider = Provider.of<CounterClosingReportProvider>(context,listen: false);


    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: my_white,
        title:  Consumer<CounterClosingReportProvider>(
            builder: (context,value,child) {
              return Text(value.displayDate,style: const TextStyle(color: my_black),);
            }
        ),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.calendar_today,
              color: Colors.black,
            ),
            onPressed: () {
              counterProvider.selectDate(context,counter);
              // do something
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [

            Card(
              margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 15),

              color: counterBackYellow,
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                        child: const Text('IN',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,fontFamily: 'Gilroy',color: my_green
                        ),)),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Opening Balance',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text(value.openingBalance.toStringAsFixed(2),style: counterValue,);
                              }
                          ),
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        callNext(CounterBillsListScreen(list: counterProvider.saleBillsList,title: 'Sales',), context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Sales',style: counterHead),

                            Consumer<CounterClosingReportProvider>(
                                builder: (context,value,child) {
                                  return Text(value.settledAmount.toStringAsFixed(2),style: counterValue,);
                                }
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: (){
                        callNext(CounterBillsListScreen(list: counterProvider.totalReceiptList,title: 'Receipt',), context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('Receipt',style: counterHead),

                            Consumer<CounterClosingReportProvider>(
                                builder: (context,value,child) {
                                  return Text(value.rTotal.toStringAsFixed(2),style: counterValue,);
                                }
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Divider(thickness: 2,color: my_blackL2,),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: [
                          Text('Total',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text((value.settledAmount+value.rTotal+value.openingBalance).toStringAsFixed(2),style: counterMainGreen,);
                              }
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),




            Card(
              margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 15),

              color: counterBackRed,
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              child: Column(
                children: [
                  Container(
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                      alignment: Alignment.centerLeft,
                      child: const Text('OUT',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,fontFamily: 'Gilroy',color: mynew_blue
                      ),)),
                  InkWell(
                    onTap: (){
                      callNext(CounterBillsListScreen(list: counterProvider.cardBillsList,title: 'Sales Card',), context);

                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Sales Card',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text(value.sCard.toStringAsFixed(2),style: counterValue,);
                              }
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      callNext(CounterBillsListScreen(list: counterProvider.upiBillsList,title: 'Sales UPI',), context);

                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Sales UPI',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text(value.sUpi.toStringAsFixed(2),style: counterValue,);
                              }
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      callNext(CounterBillsListScreen(list: counterProvider.cardReceiptList,title: 'Receipt Card',), context);

                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Receipt Card',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text(value.rCard.toStringAsFixed(2),style: counterValue,);
                              }
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      callNext(CounterBillsListScreen(list: counterProvider.upiReceiptList,title:'Receipt UPI' ,), context);

                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Receipt UPI',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text(value.rUpi.toStringAsFixed(2),style: counterValue,);
                              }
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      callNext(CounterBillsListScreen(list: counterProvider.creditBillsList,title: 'Credit',), context);

                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Credit',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text(value.credit.toStringAsFixed(2),style: counterValue,);
                              }
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      callNext(CounterBillsListScreen(list: counterProvider.paymentList,title: 'Payment',), context);

                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Payment',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text(((value.payment).abs()).toStringAsFixed(2),style: counterValue,);
                              }
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      callNext(CounterBillsListScreen(list: counterProvider.discountList,title: 'Discount',), context);

                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('Discount',style: counterHead),

                          Consumer<CounterClosingReportProvider>(
                              builder: (context,value,child) {
                                return Text(value.discount.toStringAsFixed(2),style: counterValue,);
                              }
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Counter Balance',style: counterHead),

                        Consumer<CounterClosingReportProvider>(
                            builder: (context,value,child) {
                              return Text(value.actualBalance.toStringAsFixed(2),style: counterValue,);
                            }
                        ),
                      ],
                    ),
                  ),

                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                    child: Divider(thickness: 2,color: my_blackL2,),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Total',style: counterHead),

                        Consumer<CounterClosingReportProvider>(
                            builder: (context,value,child) {
                              return Text(((value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount)+value.actualBalance).toStringAsFixed(2),style: counterMainRed,);
                            }
                        ),
                      ],
                    ),
                  ),


                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
              child: Text('Expected Counter Balance',style: counterHead),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
              child: Consumer<CounterClosingReportProvider>(
                  builder: (context,value,child) {
                    return Text((value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount)).toStringAsFixed(2)+' INR', style: counterMainGreen,);
                  }
              ),
            ),
            Container(
              height: 60,
              margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 15),
              child: Consumer<CounterClosingReportProvider>(
                  builder: (context,value,child) {

                    return value.counterOpen?Text('Counter Still Open',style: counterValue,):Text(value.actualBalance.toString(),style: counterValue,);

                  }
              ),
            ),
            InkWell(
              onTap: (){
                callNext(CounterBillsListScreen(list: counterProvider.counterCloseLog,title: 'Discount',), context);

              },
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 5),
                child: Consumer<CounterClosingReportProvider>(
                    builder: (context,value,child) {

                      return Text('Total Entry ${value.counterCloseLog.length.toString()}');
                    }
                ),
              ),
            ),
            Container(
              decoration:  BoxDecoration(
                  color:mismatchBackRed,
                  borderRadius: BorderRadius.circular(5)
              ),
              padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
              margin: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('*Mismatch : ',style: TextStyle(fontSize: 16,
                          color: mynew_blue,
                          fontWeight: FontWeight.w300),),
                      Consumer<CounterClosingReportProvider>(
                          builder: (context,value,child) {
                            double amount=((value.actualBalance)-(value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount)));

                            return Text(value.actualBalance!=0?((value.actualBalance)-(value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount))).toStringAsFixed(2):'0.0',style:  TextStyle(
                                color: value.actualBalance!=0?amount>0?my_green:amount<0?mynew_blue:my_black:my_black,
                                fontSize: 22, fontWeight: FontWeight.w800, fontFamily: 'Gilroy'),);
                          }
                      ),
                    ],
                  ),
                  Consumer<CounterClosingReportProvider>(
                    builder: (context,value,child) {
                      double amount=0.0;
                       amount=((value.actualBalance)-(value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount)));

                      return Text(value.actualBalance!=0?amount>0?'Excess':amount<0?'Less':'':'',style:  TextStyle(

                          color: value.actualBalance!=0?amount>0?my_green:amount<0?mynew_blue:my_black:my_black,
                          fontSize: 22, fontWeight: FontWeight.w800, fontFamily: 'Gilroy'));
                    }
                  )
                ],
              ),
            ),




          ],
        ),
      ),
    );
  }
}
