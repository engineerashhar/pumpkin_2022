import 'dart:collection';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/CounterClosing/counter_bill_list_screen.dart';
import 'package:flutter_pumpkin/Provider/counter_provider.dart';
import 'package:provider/provider.dart';

class CounterClosingPage extends StatelessWidget {
  String counter;
  String counterId;
  final TextEditingController amountTC =  TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final DatabaseReference mRootReference =
  FirebaseDatabase.instance.reference();


   CounterClosingPage({Key? key,required this.counter,required this.counterId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CounterProvider counterProvider = Provider.of<CounterProvider>(context,listen: false);


  return Scaffold(
    resizeToAvoidBottomInset: true,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            // Card(
            //   margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 15),
            //
            //   color: counterBackYellow,
            //   elevation: 0,
            //   shape: RoundedRectangleBorder(
            //     borderRadius: BorderRadius.circular(30.0),
            //   ),
            //   child: Padding(
            //     padding: const EdgeInsets.all(10.0),
            //     child: Column(
            //       children: [
            //         Container(
            //             width: double.infinity,
            //             padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
            //             child: const Text('IN',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,fontFamily: 'Gilroy',color: my_green
            //             ),)),
            //         Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Text('Opening Balance',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text(value.openingBalance.toStringAsFixed(2),style: counterValue,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //         InkWell(
            //           onTap: (){
            //             callNext(CounterBillsListScreen(list: counterProvider.saleBillsList,title: 'Sales',), context);
            //           },
            //           child: Padding(
            //             padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //             child: Row(
            //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //               children: [
            //                 Text('Sales',style: counterHead),
            //
            //                 Consumer<CounterProvider>(
            //                     builder: (context,value,child) {
            //                       return Text(value.settledAmount.toStringAsFixed(2),style: counterValue,);
            //                     }
            //                 ),
            //               ],
            //             ),
            //           ),
            //         ),
            //         InkWell(
            //           onTap: (){
            //             callNext(CounterBillsListScreen(list: counterProvider.totalReceiptList,title: 'Receipt',), context);
            //           },
            //           child: Padding(
            //             padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //             child: Row(
            //               mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //               children: [
            //                 Text('Receipt',style: counterHead),
            //
            //                 Consumer<CounterProvider>(
            //                     builder: (context,value,child) {
            //                       return Text(value.rTotal.toStringAsFixed(2),style: counterValue,);
            //                     }
            //                 ),
            //               ],
            //             ),
            //           ),
            //         ),
            //         const Padding(
            //           padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Divider(thickness: 2,color: my_blackL2,),
            //         ),
            //         Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //
            //             children: [
            //               Text('Total',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text((value.settledAmount+value.rTotal+value.openingBalance).toStringAsFixed(2),style: counterMainGreen,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
            //
            //
            //
            //
            // Card(
            //   margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 15),
            //
            //   color: counterBackRed,
            //   elevation: 0,
            //   shape: RoundedRectangleBorder(
            //     borderRadius: BorderRadius.circular(30.0),
            //   ),
            //   child: Column(
            //     children: [
            //       Container(
            //           width: double.infinity,
            //           padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
            //           alignment: Alignment.centerLeft,
            //           child: const Text('OUT',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,fontFamily: 'Gilroy',color: mynew_blue
            //           ),)),
            //       InkWell(
            //         onTap: (){
            //           callNext(CounterBillsListScreen(list: counterProvider.cardBillsList,title: 'Sales Card',), context);
            //
            //         },
            //         child: Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Text('Sales Card',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text(value.sCard.toStringAsFixed(2),style: counterValue,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       InkWell(
            //         onTap: (){
            //           callNext(CounterBillsListScreen(list: counterProvider.upiBillsList,title: 'Sales UPI',), context);
            //
            //         },
            //         child: Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Text('Sales UPI',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text(value.sUpi.toStringAsFixed(2),style: counterValue,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       InkWell(
            //         onTap: (){
            //           callNext(CounterBillsListScreen(list: counterProvider.cardReceiptList,title: 'Receipt Card',), context);
            //
            //         },
            //         child: Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Text('Receipt Card',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text(value.rCard.toStringAsFixed(2),style: counterValue,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       InkWell(
            //         onTap: (){
            //           callNext(CounterBillsListScreen(list: counterProvider.upiReceiptList,title:'Receipt UPI' ,), context);
            //
            //         },
            //         child: Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Text('Receipt UPI',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text(value.rUpi.toStringAsFixed(2),style: counterValue,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       InkWell(
            //         onTap: (){
            //           callNext(CounterBillsListScreen(list: counterProvider.creditBillsList,title: 'Credit',), context);
            //
            //         },
            //         child: Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Text('Credit',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text(value.credit.toStringAsFixed(2),style: counterValue,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       InkWell(
            //         onTap: (){
            //           callNext(CounterBillsListScreen(list: counterProvider.paymentList,title: 'Payment',), context);
            //
            //         },
            //         child: Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Text('Payment',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text(((value.payment).abs()).toStringAsFixed(2),style: counterValue,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       InkWell(
            //         onTap: (){
            //           callNext(CounterBillsListScreen(list: counterProvider.discountList,title: 'Discount',), context);
            //
            //         },
            //         child: Padding(
            //           padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //           child: Row(
            //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //             children: [
            //               Text('Discount',style: counterHead),
            //
            //               Consumer<CounterProvider>(
            //                   builder: (context,value,child) {
            //                     return Text(value.discount.toStringAsFixed(2),style: counterValue,);
            //                   }
            //               ),
            //             ],
            //           ),
            //         ),
            //       ),
            //       Padding(
            //         padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //         child: Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [
            //             Text('Counter Balance',style: counterHead),
            //
            //             Consumer<CounterProvider>(
            //                 builder: (context,value,child) {
            //                   return Text((value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount)).toStringAsFixed(2),style: counterValue,);
            //                 }
            //             ),
            //           ],
            //         ),
            //       ),
            //
            //       const Padding(
            //         padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //         child: Divider(thickness: 2,color: my_blackL2,),
            //       ),
            //       Padding(
            //         padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //         child: Row(
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: [
            //             Text('Total',style: counterHead),
            //
            //             Consumer<CounterProvider>(
            //                 builder: (context,value,child) {
            //                   return Text(((value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount)+(value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount))).toStringAsFixed(2),style: counterMainRed,);
            //                 }
            //             ),
            //           ],
            //         ),
            //       ),
            //
            //
            //     ],
            //   ),
            // ),
            //
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //   child: Text('Expected Counter Balance',style: counterHead),
            // ),
            //
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 4),
            //   child: Consumer<CounterProvider>(
            //       builder: (context,value,child) {
            //         return Text((value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount)).toStringAsFixed(2)+' INR', style: counterMainGreen,);
            //       }
            //   ),
            // ),
            Container(
              height: 60,
              margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 15),
              child: Consumer<CounterProvider>(
                builder: (context,value,child) {
                  return value.fromDate==getToday()?TextField(
                    controller: amountTC,
                    keyboardType: TextInputType.number,
                    decoration:  InputDecoration(
                      labelText: 'Enter Actual Counter Balance',

                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),),
                    onChanged: (text){
                      if(text.toString()!=''){
                        counterProvider.getMismatch(double.parse(text));
                      }
                    },

                  ):Text(value.actualBalance.toString(),style: counterValue,);

                }
              ),
            ),

            // Container(
            //   decoration:  BoxDecoration(
            //     color:mismatchBackRed,
            //     borderRadius: BorderRadius.circular(5)
            //   ),
            //   padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 20),
            //   margin: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     children: [
            //       const Text('*Mismatch : ',style: TextStyle(fontSize: 16,
            //           color: mynew_blue,
            //           fontWeight: FontWeight.w300),),
            //       Consumer<CounterProvider>(
            //           builder: (context,value,child) {
            //             double amount=((value.actualBalance)-(value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount)));
            //
            //             return Text(value.actualBalance!=0?((value.actualBalance)-(value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount))).toStringAsFixed(2):'0.0',style:  TextStyle(
            //               color: value.actualBalance!=0?amount>0?my_green:amount<0?mynew_blue:my_black:my_black,
            //                 fontSize: 22, fontWeight: FontWeight.w800, fontFamily: 'Gilroy'),);
            //           }
            //       ),
            //     ],
            //   ),
            // ),

            Consumer<CounterProvider>(
              builder: (context,value,child) {
                return InkWell(
                  onTap: () {

                      if(amountTC.text!=''){
                        DateTime now =  DateTime.now();

                        String dayNode = now.year.toString() +
                            "/" +
                            now.month.toString() +
                            "/" +
                            (now.day).toString();
                        double closingBalance=(value.openingBalance+(value.settledAmount+value.rTotal)-(value.sCard+value.sUpi+value.rCard+value.rUpi+value.credit+(-1*value.payment)+value.discount));
                        mRootReference.child('CashCountersCloseLog').child(counter).child(dayNode).child(DateTime.now().millisecondsSinceEpoch.toString()).set(amountTC.text);
                        if(double.parse(amountTC.text)<(closingBalance+500)&&double.parse(amountTC.text)>(closingBalance-500)){


                          mRootReference.child('Closing_Balances').child(counter).child(dayNode).set(int.parse(amountTC.text));
                          Map<String, Object> data =  HashMap();
                          data['ClosingStatus']='Closed';
                          data['ClosedTime']=DateTime.now().millisecondsSinceEpoch;
                          data['User']='Closed';
                          data['OpeningBalance']=int.parse(amountTC.text);
                          mRootReference.child('CashCounters').child(counterId).update(data);
                          finish(context);
                          finish(context);
                        }else{
                          failureAlertDialog(context,counter,amountTC.text,counterId,dayNode);
                        }

                      }




                  },
                  child: Container(
                      height: 60,
                      width: double.infinity,
                      margin: const EdgeInsets.symmetric(horizontal: 15,vertical: 15),
                      decoration: const BoxDecoration(
                        color: counterButton,
                        borderRadius: BorderRadius.all(Radius.circular(
                            30.0) //                 <--- border radius here
                        ),
                      ),
                      child: const Center(
                        child: Text(
                          "Submit",

                          style: TextStyle(color: my_white,fontSize: 16),
                        ),
                      )),
                );
              }
            ),



          ],
        ),
      ),
    );
  }
  failureAlertDialog(BuildContext context,String counter,String closingBalance,String counterId,String dayNode) {

    // set up the button
    Widget okButton = TextButton(
      child: const Text("OK"),
      onPressed: () {
        mRootReference.child('Closing_Balances').child(counter).child(dayNode).set(int.parse(amountTC.text));
        Map<String, Object> data =  HashMap();
        data['ClosingStatus']='Closed';
        data['ClosedTime']=DateTime.now().millisecondsSinceEpoch;
        mRootReference.child('CashCounters').child(counterId).update(data);

        finish(context);
        finish(context);
        finish(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Alert"),
      content: const Text("Closing Balance did not match . Are you Sure You Want  to Close"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  getToday(){
    DateTime now = DateTime.now();
    return DateTime(now.year,now.month,now.day,0,0,0,0,0);
  }
}
