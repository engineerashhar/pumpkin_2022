import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/CounterClosing/counter_close_report_page.dart';
import 'package:flutter_pumpkin/CounterClosing/counter_closing_page.dart';
import 'package:flutter_pumpkin/Provider/counter_close_report_provider.dart';
import 'package:flutter_pumpkin/Provider/counter_provider.dart';
import 'package:provider/provider.dart';

class CounterListingPage extends StatelessWidget {
  String from;
  final DatabaseReference mRootReference =
  FirebaseDatabase.instance.reference();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController userNameTEC =  TextEditingController();
  TextEditingController passwordTEC =  TextEditingController();


  CounterListingPage({Key? key,required this.from}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SafeArea(
        child: Consumer<CounterProvider>(
          builder: (context,value,child) {
            return value.counterList.isEmpty?Container():ListView.builder(
            itemCount: value.counterList.length,
            scrollDirection: Axis.vertical,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: (){

                  CounterClosingReportProvider counterProvider = Provider.of<CounterClosingReportProvider>(context,listen: false);

                  // callNext(CounterClosingPage(counter: value.counterList[index],counterId: '3bbf9a535d58ce40',), context);
                  if(from=='Report'){

                    counterProvider.funCounterData(value.counterList[index],DateTime.now());

                    callNext(CounterCloseReportPage(counter: value.counterList[index]), context);

                  }else{
                    _showDialogLogIn(context,value.counterList[index]);

                  }

                },
                child: Card(
                  child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                        decoration: BoxDecoration(
                          color: my_white
                        ),
                      child: Text(value.counterList[index],style: const TextStyle(
                        fontSize: 20
                      ),)),
                ),
              );
            });
          }
        ),
      ),

    );
  }
  _showDialogLogIn(context,String counter) {
    CounterProvider counterProvider =
    Provider.of<CounterProvider>(context, listen: false);

    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return Consumer<CounterProvider>(
          builder: (context, value, child) {
            return AlertDialog(
              title: Container(
                  child: const Center(
                      child: Text(
                        'Cash Counter Log in',
                        style: TextStyle(color: my_white),
                      ))),
              backgroundColor: mynew_dred,
              contentPadding: const EdgeInsets.only(
                top: 15.0,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              content: SingleChildScrollView(
                child: Container(
                  width: double.maxFinite,
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
                  decoration: BoxDecoration(
                      color: my_white,
                      borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(10))),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: userNameTEC,
                            keyboardType: TextInputType.number,
                            decoration: const InputDecoration(hintText: 'User Name'),
                            validator: (text) =>
                            text!.isEmpty ? 'Username cannot be blank' : null,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: passwordTEC,
                            keyboardType: TextInputType.number,
                            decoration: const InputDecoration(hintText: 'Password'),
                            validator: (text) =>
                            text!.isEmpty ? 'Password cannot be blank' : null,
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            final FormState? form = _formKey.currentState;

                            if (form!.validate()) {
                              loginUser(counter,userNameTEC.text,passwordTEC.text,context);
                          }



                          },
                          child: Container(
                              height: 50,
                              width: 150,
                              decoration: const BoxDecoration(
                                color: mynew_yellow,
                                borderRadius: BorderRadius.all(Radius.circular(
                                    10.0) //                 <--- border radius here
                                ),
                              ),
                              child: const Center(
                                child: Text(
                                  "Save",
                                  style: TextStyle(color: Colors.black),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
  loginUser(String counter,String user,String password,BuildContext context){
    CounterProvider counterProvider = Provider.of<CounterProvider>(context, listen: false);

    mRootReference.child('CashCounters').once().then((DatabaseEvent databaseEvent) {
      Map<dynamic, dynamic>  map = databaseEvent.snapshot.value as Map;
      map.forEach((key, value) {
        if(value['Counter']!=null&&value['Counter'].toString()==counter){
          if(value['User']!=null&& value['User'].toString()==user){
            mRootReference.child('Users').child(user).child('Password').once().then((DatabaseEvent databaseEvent) {
              if(databaseEvent.snapshot.value!=null&&databaseEvent.snapshot.value==password){
                finish(context);
                counterProvider.funCounterData(counter,DateTime.now());
                callNext(CounterClosingPage(counter: counter,counterId: key,), context);
              }else{

              }
            });
          }else{

          }
        }else{
        }

      });

    });
  }
  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Processing...")),
        ],),
    );
    showDialog(barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,

            child: alert);
      },
    );
  }

}
