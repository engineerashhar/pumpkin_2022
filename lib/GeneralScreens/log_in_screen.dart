import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Provider/main_provider.dart';
import 'package:provider/provider.dart';
class LogInScreen extends StatelessWidget {
  FocusNode inputNode = FocusNode();

  LogInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        return showAlertDialog(context);
      },
      child: Scaffold(

        backgroundColor: my_red,
        body: Consumer<MainProvider>(
          builder: (context,value,child) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [

                value.isPhone?Padding(

                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: TextField(
                    controller: value.employId,
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.center,


                    decoration: const InputDecoration(
                      hintText: "Employ ID",
                      filled: true,

                      fillColor: my_greymid,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)), //                 <--- border radius here
                        borderSide:  BorderSide(color: Colors.grey,),

                      ),
                    ),

                    onChanged: (text){
                      if(text.length==4){
                        inputNode.requestFocus();
                        MainProvider mainProvider = Provider.of<MainProvider>(context, listen: false);
                        mainProvider.changeController();

                      }
                    },
                  ),
                ):
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: TextField(
                    focusNode: inputNode,
                    controller: value.password,
                    keyboardType: TextInputType.number,
                    textAlign: TextAlign.center,

                    autofocus: true,
                    decoration: const InputDecoration(
                      hintText: "Password",
                      filled: true,


                      fillColor: my_greymid,
                      border: OutlineInputBorder(
                                              borderRadius: BorderRadius.all(Radius.circular(10.0)), //                 <--- border radius here
                        borderSide:  BorderSide(color: Colors.grey,),

                      ),
                    ),
                    onChanged: (text){
                      if(text.length==4){
                        showLoaderDialog(context);
                        MainProvider mainProvider = Provider.of<MainProvider>(context, listen: false);
                        mainProvider.loginCheck(value.employId.text,value.password.text,context);
                      }
                    },
                  ),
                )

              ],
            );
          }
        ),

      ),
    );
  }
   showLoaderDialog(BuildContext context){
     AlertDialog alert=AlertDialog(
       content: new Row(
         children: [
           CircularProgressIndicator(),
           Container(margin: EdgeInsets.only(left: 7),child:Text("Processing..." )),
         ],),
     );
     showDialog(barrierDismissible: false,
       context:context,
       builder:(BuildContext context){
         return alert;
       },
     );
   }
  showAlertDialog(BuildContext context) {

    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        exit(0);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Exit"),
      content: Text("Do You Want Exit Application"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
