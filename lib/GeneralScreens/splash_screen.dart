import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/GeneralScreens/log_in_screen.dart';
import 'package:flutter_pumpkin/GeneralScreens/token_screen.dart';
import 'package:flutter_pumpkin/OrderSection/table_listing.dart';
import 'package:flutter_pumpkin/Provider/main_provider.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isfine = true;
  String user="";

  @override
  void initState()  {
    getDataFromSharedPreference();
    // TODO: implement initState
    super.initState();

  }

  Future<void> getDataFromSharedPreference()async {


    OrderProvider _orderprovider = Provider.of<OrderProvider>(context,listen: false);

    SharedPreferences userPreference = await SharedPreferences.getInstance();
    user=userPreference.getString("Uid")??"";
    String strSelectedFloor='0';
    String strSelectedFloorName='';
    if(userPreference.getString("SelectedFloor")!=null){
       strSelectedFloor = userPreference.getString("SelectedFloor")!;
       strSelectedFloorName = userPreference.getString("SelectedFloorName")!;
    }
      _orderprovider.funFloorChange(strSelectedFloor,strSelectedFloorName);
    if(user==""){
      Future.delayed(const Duration(milliseconds: 2000), (){
        callNext(LogInScreen(), context);
      });
    }else{

      _orderprovider.fun_tableListing(strSelectedFloor);
      _orderprovider.funcbillData();
      _orderprovider.fun_initPlatformState();
      _orderprovider.funSteward();
      _orderprovider.funCustomers();
      _orderprovider.funLoadReasons();
      _orderprovider.funGetTokens();

      Future.delayed(Duration(milliseconds: 2000), (){
        callNext(TableListing(), context);
      });
    }

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    isfine = false;
  }

  @override
    @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;


    queryData = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: my_red,
      body: Center(
        child: Container(
          child: Text(
            "DELICIA",
            style: GoogleFonts.redHatDisplay(
                fontWeight: FontWeight.bold,
                fontSize: textSize24,
                color: my_white),
          ),
        ),
      ),
    );
  }


}
