import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/GeneralScreens/takeaway_order_details_screen.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:provider/provider.dart';
class TakeawayOldSubTable extends StatelessWidget {
  const TakeawayOldSubTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OrderProvider _orderprovider =
    Provider.of<OrderProvider>(context, listen: false);
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    double width=queryData.size.width/3;
    return Scaffold(

      body: Consumer<OrderProvider>(
          builder: (context,value,child) {
            return GridView.builder(
                gridDelegate:  SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: width,
                    childAspectRatio: 3 / 2,
                    crossAxisSpacing: 0,
                    mainAxisSpacing: 20),
                itemCount: value.oldTokens.length,
                itemBuilder: (BuildContext ctx, index) {
                  return InkWell(
                      onTap: (){
                        _orderprovider
                            .fun_clear();
                        _orderprovider.getTABillDetails( value.oldTokens[index].subTaleId,value.oldTokens[index].tableID);

                        callNext(TakeawayOrderDetailsSceen(floor: '4', tableID: value.oldTokens[index].subTaleId,  SubTableID: value.oldTokens[index].subTaleId,billNo:value.oldTokens[index].billNumber ,customer: value.oldTokens[index].customer,steward: value.oldTokens[index].steward,), context);
                      },
                    child: Card(
                      elevation: 2,
                      color: my_grey,
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(value.oldTokens[index].subTaleId,style: const TextStyle(
                                fontSize: 24
                            ),textAlign: TextAlign.center),
                            Text(value.oldTokens[index].tableID)
                          ],
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15)),
                      ),
                    ),
                  );
                });
          }
      ),

    );
    return Container();
  }
}
