import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:flutter_pumpkin/Views/items_adapter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class TakeawayOrderDetailsSceen extends StatelessWidget {
  String floor, tableID, SubTableID,billNo,customer,steward;
   TakeawayOrderDetailsSceen({Key? key,
    required this.floor,
    required this.tableID,
    required this.SubTableID,required this.billNo,required this.customer,required this.steward})
      : super(key: key);

  @override
  Widget build(BuildContext context) {


    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  mynew_dblue, mynew_blue
                  ,
                ],
                begin: FractionalOffset(0.0, 0.0),
                end: FractionalOffset(0.0, 1.0),
                stops: [1.0, 0.0],
                tileMode: TileMode.clamp),
          ),
        ), title: Text(
        tableID,
        style: GoogleFonts.redHatDisplay(
            fontWeight: FontWeight.bold,
            fontSize: textSize18,
            color: my_white),
      ),
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            }, child: const Icon(Icons.arrow_back_ios, color: my_white)),

      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () {
                    },
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(10, 5, 10, 10),

                      height: queryData.size.height * 0.06,
                      decoration: BoxDecoration(
                        border: Border.all(color: mynew_dblue),
                        color: my_white,
                        borderRadius: const BorderRadius.all(Radius.circular(
                            30.0) //                 <--- border radius here
                        ),
                      ),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 5),
                                child: Image.asset("assets/bowtie.png",
                                  scale: 35,
                                  color: mynew_dred,
                                ),
                              ),
                              Consumer<OrderProvider>(
                                  builder: (context, value, child) {
                                    return Flexible(

                                      child: Text(
                                        steward ,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,

                                      ),
                                    );
                                  }),
                              const Icon(Icons.arrow_forward_ios,
                                  size: 18,
                                  color: my_black),
                            ],
                          ),
                        ),
                      ),
                      // child: const Center(child:  Text("Steward Name")),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: queryData.size.height * 0.06,
                    margin: const EdgeInsets.fromLTRB(10, 5, 10, 10),
                    decoration: BoxDecoration(
                      color: my_white,
                      border: Border.all(color: mynew_dblue),
                      borderRadius: const BorderRadius.all(Radius.circular(
                          30.0) //                 <--- border radius here
                      ),
                    ),
                    child: Center(child: Text("Sub Tabel: " + SubTableID.replaceAll("S", ''))),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: (){
                if(customer!='No Number'){
                  launch("tel://$customer");
                }
              },
              child: Container(
                height: queryData.size.height * 0.06,
                margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                decoration: BoxDecoration(
                  color: mynew_dred,
                  border: Border.all(color: mynew_dblue),
                  borderRadius: const BorderRadius.all(Radius.circular(
                      30.0) //                 <--- border radius here
                  ),
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SizedBox(width: 2),
                      Text(customer,
                          style: const TextStyle(
                              color: my_white, fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
              ),
            ),
            Consumer<OrderProvider>(
                builder: (context, value, child) {
                  return  Container(
                    margin: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                    decoration: BoxDecoration(
                      color: my_white,
                      border: Border.all(color: mynew_dblue),
                      borderRadius: const BorderRadius.all(Radius.circular(
                          10.0) //                 <--- border radius here
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 60,
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(
                                    10.0) //                 <--- border radius here
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,

                            children: [
                              Container(
                                padding: const EdgeInsets.all(10),
                                child: Consumer<OrderProvider>(
                                  builder: (context, value, child) {
                                    return Text(
                                      "Bill Number :"+ billNo,
                                      style: const TextStyle(color: Colors.black,
                                          fontWeight: FontWeight.bold),);
                                  },

                                ),
                              ),
                            ],
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Divider(
                            color: Colors.black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            children: <Widget> [
                              Container(
                                color: my_white,
                                height: 15,
                                width: 15,
                                margin: const EdgeInsets.only(right: 5),
                              ),
                              Expanded(flex: 4,
                                  child: Container(child: Text("Item",
                                      style: TextStyle(color: mynew_dred,
                                          fontStyle: FontStyle.italic)))),
                              Expanded(flex: 2,
                                  child: Container(child: Text("Qty",
                                      style: TextStyle(color: mynew_dred,
                                          fontStyle: FontStyle.italic)))),
                              Expanded(flex: 2,
                                  child: Center(child: Text("Total",
                                      style: TextStyle(color: mynew_dred,
                                          fontStyle: FontStyle.italic)))),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                            child: Consumer<OrderProvider>(
                              builder: (context, value, child) {
                                return ListView.builder(
                                    itemCount: value.itemGS.length,
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder: (BuildContext context,
                                        int index) {
                                      return ItemsAdapter(
                                          value.itemGS[index], index,
                                          context);
                                    });
                              },
                            )),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Divider(
                            color: Colors.black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(flex: 6,
                                  child: Text("Non-Taxable Amount",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 2,
                                  child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Consumer<OrderProvider>(
                                          builder: (context, value, child) {
                                            return Text(
                                                "₹ " + value.ANT.toString(),
                                                style: TextStyle(
                                                    fontWeight: FontWeight
                                                        .bold));
                                          }))),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(flex: 6,
                                  child: Text("Taxable Amount",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 2,
                                  child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Consumer<OrderProvider>(
                                          builder: (context, value, child) {
                                            return Text(
                                                "₹ " +
                                                    value.AT.toStringAsFixed(2),
                                                style: TextStyle(
                                                    fontWeight: FontWeight
                                                        .bold));
                                          }))),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }
            ),
          ],
        ),
      ),
    );
  }
}
