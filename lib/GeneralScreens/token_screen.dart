import 'package:auto_size_text/auto_size_text.dart';
import 'package:confetti/confetti.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Provider/main_provider.dart';
import 'package:provider/provider.dart';
import 'package:wakelock/wakelock.dart';

class TokenScreen extends StatefulWidget {
  const TokenScreen({Key? key}) : super(key: key);

  @override
  _TokenScreenState createState() => _TokenScreenState();
}

class _TokenScreenState extends State<TokenScreen> {

  @override
  void initState() {
    Future.delayed(Duration.zero, () async {
      MainProvider mainProvider = Provider.of<MainProvider>(context,listen: false);

      mainProvider.fetchTokens(context);
      Wakelock.enable();

    });


    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    double width=queryData.size.width/5;
    return Scaffold(

      body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/background_doodle.png"),
                fit: BoxFit.cover,
              ),
             ),
        child: Consumer<MainProvider>(
            builder: (context,value,child) {
              return GridView.builder(
                  gridDelegate:  SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: width,
                      childAspectRatio: 3 / 2,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 20),
                  itemCount: value.tokens.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return Container(
                      margin: EdgeInsets.all(20),
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 25),
                        child: AutoSizeText(value.tokens[index],style: const TextStyle(
                            fontFamily: 'Berlin',
                            fontSize: 500,
                            fontWeight: FontWeight.bold
                        ),),
                      ),
                      decoration: BoxDecoration(
                          image: const DecorationImage(
                            image: AssetImage("assets/singlrtoken.png"),
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(15)),
                    );
                  });
            }
        ),
      ),

    );
    return Container();
  }
}


