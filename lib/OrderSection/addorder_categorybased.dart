import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:provider/provider.dart';
class AddOrderCategoryBased extends StatelessWidget {
  String floor,tableID,OrderID,Uid,SubTable,StewardID,OrderType;

  AddOrderCategoryBased({Key? key,required this.floor,required this.tableID,
    required this.OrderID,required this.Uid,required this.SubTable,
    required this.StewardID,required this.OrderType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OrderProvider _orderprovider = Provider.of<OrderProvider>(context, listen: false);
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    var size = MediaQuery.of(context).size;
    return Scaffold(

      body: SafeArea(
      maintainBottomViewPadding: true,
      child:
      Container(
        height: size.height,
        width: size.width,
        child: Column(
          children: <Widget>[
            Container(
              width: size.width,
              child: Card(
                margin: EdgeInsets.all(10.0),
                color: my_white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: queryData.size.height * 0.05,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              flex: 4,
                              child: Center(
                                  child: Text(
                                    Order_item,
                                    style: Textstyle16,
                                  ))),
                          Expanded(
                              flex: 2,
                              child: Center(
                                  child: Text(
                                    Order_qty,
                                    style: Textstyle16,
                                  ))),
                          Expanded(
                              flex: 2,
                              child: Center(
                                  child: Text(
                                    Order_total,
                                    style: Textstyle16,
                                  ))),
                        ],
                      ),
                    ),
                    Container(
                      height: size.height*0.30,
                      child: Consumer<OrderProvider>(
                        builder: (context,value,child){
                          return ListView.builder(
                              itemCount: value.itemGS.length,
                              scrollDirection: Axis.vertical,
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 20),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                          flex: 4,
                                          child: Center(child: Text(value.itemGS[index].Name))),
                                      Expanded(
                                          flex: 2,
                                          child: Center(child: Text("${value.itemGS[index].Qty}x${value.itemGS[index].Rate}"))),
                                      Expanded(
                                          flex: 2,
                                          child: Center(child: Text("${value.itemGS[index].Total}"))),
                                    ],
                                  ),
                                );
                              });

                        },

                      ),
                    ),
                    Container(
                      height: queryData.size.height * 0.03,
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Center(
                          child: Divider(
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: queryData.size.height * 0.065,
                      color: my_white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                              flex: 6,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 15.0),
                                child: Text(
                                  Order_total,
                                  style: Textstyle18,
                                ),
                              )),
                          Expanded(
                              flex: 2,
                              child: Center(
                                  child: Consumer<OrderProvider>(
                                    builder: (context ,value,child){
                                      return   Text(
                                        value.dOrderTotal.toString(),
                                        style: Textstyle18,
                                      );
                                    },

                                  )
                              )),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                      children:<Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            height:size.height*0.45 ,
                            child: Consumer<OrderProvider>(
                              builder: (context,value,child){
                                return ListView.builder(
                                    itemCount: value.categoryGS.length,
                                    itemBuilder: (context,index){
                                      return Container(
                                        width: size.width*0.20,
                                        color: my_grey,

                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(5.0),
                                          ),
                                          color: value.categoryGS[index].Status=="NOTSELECTED"?my_white:my_light_red,
                                          child: ListTile(
                                            onTap: (){
                                              _orderprovider.fun_categoryItems(context,value.categoryGS[index].CategoryName.toString(),index);
                                            },
                                            title: Text("${value.categoryGS[index].CategoryName}"),
                                          ),
                                        ),
                                      );
                                    });
                              },

)
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Container(
                            height:size.height*0.45 ,

                          ),
                        ),

                      ],
                      ),
                    )
                  ],
                ),
              ),
            ),

          ],
        ),
      ),
    ),

    );
  }
}
