import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:provider/provider.dart';

class AddOrderImageBased extends StatelessWidget {
  const AddOrderImageBased({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    double width=queryData.size.width;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 25,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: Row(

                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                children: [
                    Text("3 items Add",style: TextStyle(
                      color: my_greydark10,
                      fontSize: 18,
                    ),),
                    Text('₹520',style: prBlackL,)
                ],),
              ),
              Consumer<OrderProvider>(
                builder: (context,value,child) {
                  return GridView.builder(
                  gridDelegate:  SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: width/4,
                      childAspectRatio: 3 / 1.4,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 0),
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: value.itemCategory.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return _buildChip(value.itemCategory[index], const Color(0xFFff6666));
                  });
                }
              ),
              Consumer<OrderProvider>(
                builder: (context,value,child) {
                  return GridView.builder(
                  gridDelegate:  SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: width/2,
                      childAspectRatio: 3 / 2,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 20),
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: value.itemCategory.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return Container(

                      alignment: Alignment.center,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Image.asset('assets/alpham.jpg',
                          fit: BoxFit.fill,)

                        ],
                      ),
                      decoration: BoxDecoration(

                          borderRadius: BorderRadius.circular(15)),
                    );
                  });
                }
              )

            ],
          ),
        ),


      ),
    );
  }
  Widget _buildChip(String label, Color color) {
    return Chip(
      labelPadding: EdgeInsets.all(2.0),
      avatar: CircleAvatar(
        backgroundColor: Colors.white70,
        child: Text(label[0].toUpperCase()),
      ),
      label: Text(
        label,
        style: TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: color,
      elevation: 6.0,
      shadowColor: Colors.grey[60],
      padding: EdgeInsets.all(8.0),
    );
  }

}

