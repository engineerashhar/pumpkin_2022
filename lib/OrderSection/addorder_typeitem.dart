// ignore_for_file: unnecessary_new, prefer_const_constructors

import 'dart:collection';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Provider/main_provider.dart';
import 'package:flutter_pumpkin/Views/dish_item_gs.dart';
import 'package:flutter_pumpkin/Views/itemGS.dart';
import 'package:flutter_pumpkin/Views/items_adapter.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:flutter/material.dart' hide Image;
import 'package:flutter/services.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


class AddOrderTypeItem extends StatefulWidget {
  String floor, tableID, OrderID, Uid, SubTable, StewardID, OrderType,kotStatus,AddItemClick;

  AddOrderTypeItem({
    Key? key,
    required this.floor,
    required this.tableID,
    required this.OrderID,
    required this.Uid,
    required this.SubTable,
    required this.StewardID,
    required this.OrderType,
    required this.kotStatus,
    required this.AddItemClick,
  }) : super(key: key);

  @override
  _AddOrderTypeItemState createState() => _AddOrderTypeItemState();
}

class _AddOrderTypeItemState extends State<AddOrderTypeItem> {
  TextEditingController itemTC = new TextEditingController();
  TextEditingController notesTC = new TextEditingController();
  TextEditingController employIdTC = new TextEditingController();
  TextEditingController passwordTC = new TextEditingController();
  TextEditingController qtyTC = new TextEditingController();
  TextEditingController manualRateTC = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey<FormState> _formKeyManualQty = GlobalKey<FormState>();
  GlobalKey<AutoCompleteTextFieldState<String>> keyDish = GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyDishManuel = GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> keyDishEdit = GlobalKey();
  final DatabaseReference mRootReference =
  FirebaseDatabase.instance.reference();
  FocusNode inputNode = FocusNode();




  String currentText = "";
  String reasonTxt = "";
  List<String> added = [];
  bool isBarcode = false;
  PaperSize paper = PaperSize.mm80;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    OrderProvider _orderprovider = Provider.of<OrderProvider>(context, listen: false);
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    var size = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: my_grey,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin:EdgeInsets.all(8),
            decoration:BoxDecoration(
              color: my_white,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5 ),
                  blurRadius: 12,
                  offset: Offset(3, 2), // changes position of shadow
                ),
              ],//                 <--- border radius here
            ),
            constraints:BoxConstraints(minHeight:size.height*1 ),
            child: Column(
              children: [

                Padding(
                  padding: EdgeInsets.only(left: 20,top: 15),
                  child: Align(
                      alignment: Alignment.center,
                      child: Consumer<OrderProvider>(
                          builder: (context,value,child) {
                            return Text(
                              'Total Amount : '+value.kotAmount.toString(),
                              style: TextStyle(
                                  fontFamily: fontBold,
                                  fontSize: textSize20,
                                  color: my_black,
                                  fontStyle:FontStyle.italic),
                            );
                          }
                      )),
                ),
                SizedBox(height: 10),
                widget.kotStatus!="OrderPrinted"?Consumer<OrderProvider>(
                  builder: (context, value, child) {
                    return Container(
                        width: queryData.size.width * 0.90,
                        height: 50,
                        decoration: BoxDecoration(
                          color: my_white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              blurRadius: 4,
                              offset: Offset(3, 2), // changes position of shadow
                            ),
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(10.0)), //                 <--- border radius here
                        ),
                        child: TypeAheadField<DishItemGS>(

                            textFieldConfiguration: TextFieldConfiguration(
                              autofocus: false,
                              focusNode: inputNode,
                              decoration:  InputDecoration(
                                labelText: "Search Items",
                                labelStyle: TextStyle(color:mynew_dred),
                                enabledBorder:  OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)), //                 <--- border radius here
                                  // width: 0.0 produces a thin "hairline" border
                                  borderSide:  BorderSide(color: Colors.grey,),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(10.0)), //                 <--- border radius here
                                    borderSide:  BorderSide(color: mynew_dred,)),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)), //                 <--- border radius here
                                  borderSide:  BorderSide(color: Colors.grey,),
                                ),
                                prefixIcon: Icon(
                                  Icons.search,
                                  color: my_black,

                                ),

                              ),
                              controller: itemTC,
                            ),
                            suggestionsCallback: (pattern) async {
                              return _orderprovider.getSuggestions(pattern);
                            },
                            transitionBuilder:
                                (context, suggestionsBox, controller) {
                              return suggestionsBox;
                            },
                            itemBuilder: (context, suggestion) {
                              return ListTile(
                                title: Text(suggestion.dishID.toString()+" "+suggestion.dishName.toString()),
                              );
                            },
                            onSuggestionSelected: (text) {
                              setState(()  {
                                setState(()  {
                                  if (text != "") {
                                    int count = 1;
                                    String ItemID = text.dishID;
                                    String itemName = text.dishName;
                                    _orderprovider.fun_Dish_addAlert(context, itemName, ItemID, count, widget.floor,notesTC.text);
                                    print('ItemID');
                                    mRootReference
                                        .child("Dish")
                                        .child(ItemID)
                                        .once()
                                        .then((DatabaseEvent databaseEvent) {
                                          if(databaseEvent.snapshot.value!=null){
                                            Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;
                                            double? floorAmount=double.tryParse(map["F"+widget.floor].toString());

                                            if(floorAmount!>0 || map['Item']=='DISCOUNT'){
                                              ShowDishDailog(context);
                                            }else{
                                              _showDialogManualRate(context);
                                            }

                                          }

                                    });
                                    itemTC.text="";

                                  }
                                });
                              });
                            })

                    );
                  },
                ):Container(),


                widget.AddItemClick!="new" ? Card(
                  margin: EdgeInsets.all(10.0),
                  color: my_white,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: mynew_dred, width: 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: queryData.size.height * 0.05,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                                flex: 4,
                                child: Padding(
                                  padding:  EdgeInsets.only(left: 15),
                                  child: Text(
                                    Order_item,
                                    style: Textstyle16,
                                  ),
                                )),
                            Expanded(
                                flex: 2,
                                child: Center(
                                  child: Text(
                                    Order_qty,
                                    style: Textstyle16,
                                  ),
                                )),
                            Expanded(
                                flex: 2,
                                child: Center(
                                  child: Text(
                                    Order_total,
                                    style: Textstyle16,
                                  ),
                                )),
                          ],
                        ),
                      ),
                      Container(
                        child: Consumer<OrderProvider>(
                          builder: (context, value, child) {
                            return ListView.builder(
                                itemCount: value.itemGS.length,
                                scrollDirection: Axis.vertical,
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int index) {
                                  return value.itemGS[index].kotNumber==widget.OrderID?
                                  Row(
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          onTap: (){
                                            if(value.itemGS[index].Status!="OrderPrinted"&&value.itemGS[index].Status!="Cancelled"){
                                              _orderprovider.fun_Dish_addAlert(context, value.itemGS[index].Name, value.itemGS[index].productId, int.parse(value.itemGS[index].Qty), value.itemGS[index].floor, value.itemGS[index].Notes);
                                              showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return Consumer<OrderProvider>(
                                                      builder: (context, value, child) {
                                                        return AlertDialog(
                                                          shape: const RoundedRectangleBorder(
                                                              borderRadius: BorderRadius.all(Radius.circular(12.0))),
                                                          contentPadding: const EdgeInsets.only(top: 10.0),
                                                          content: Container(
                                                            width: 300.0,
                                                            height: 300.0,
                                                            child: Column(
                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                              crossAxisAlignment: CrossAxisAlignment.stretch,
                                                              mainAxisSize: MainAxisSize.min,
                                                              children: <Widget>[
                                                                Column(

                                                                  children: [
                                                                    SizedBox(
                                                                      height: 5.0,
                                                                    ),
                                                                    Text(
                                                                      value.itemGS[index].Name,
                                                                      style: Textstyle16,
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 10.0,
                                                                    ),
                                                                    Container(
                                                                      margin: EdgeInsets.only(left: 10,right: 10),

                                                                      child: Divider(
                                                                        color: Colors.black,
                                                                        height: 5.0,
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 20.0,
                                                                    ),
                                                                    Row(
                                                                      mainAxisAlignment:MainAxisAlignment.start,
                                                                      children: [
                                                                        Container(
                                                                          margin: EdgeInsets.only(left: 10),
                                                                          child: Text(
                                                                            "Rate : ₹ ${value.itemGS[index].Rate}",
                                                                            style: TextStyle(
                                                                              fontFamily: fontBold,
                                                                              fontSize: textSize16,
                                                                              color: my_black,
                                                                            ),
                                                                            textAlign: TextAlign.start,
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 15.0,
                                                                    ),
                                                                    Padding(
                                                                        padding: EdgeInsets.only(left: 30.0, right: 40.0),
                                                                        child: Container(
                                                                          width: 180,
                                                                          height: 45,
                                                                          child: Row(
                                                                            mainAxisAlignment:
                                                                            MainAxisAlignment.spaceBetween,
                                                                            children: <Widget>[
                                                                              IconButton(
                                                                                icon:  const Icon(
                                                                                  Icons.remove_circle,
                                                                                  color: mynew_dred,
                                                                                  size: 45,
                                                                                ),
                                                                                onPressed: () {
                                                                                  OrderProvider _orderprovider =
                                                                                  Provider.of<OrderProvider>(context,
                                                                                      listen: false);
                                                                                  if(value.iItemCount!= 1){
                                                                                    int newCount = value.iItemCount - 1;

                                                                                    _orderprovider.fun_Dish_addAlert(
                                                                                        context,
                                                                                        value.strItemName,
                                                                                        value.strItemID.toString(),
                                                                                        newCount,
                                                                                        widget.floor,notesTC.text);
                                                                                  }
                                                                                },
                                                                              ),
                                                                              SizedBox(width:8),
                                                                              Container(
                                                                                  decoration: const BoxDecoration(
                                                                                      color: my_greydark,
                                                                                      shape: BoxShape.circle),
                                                                                  width: 45,
                                                                                  height: 45,
                                                                                  child: Center(
                                                                                      child:  TextField(
                                                                                          controller: value.qtyTc,
                                                                                          textAlign: TextAlign.center,
                                                                                          keyboardType: TextInputType.number,
                                                                                          decoration: InputDecoration(
                                                                                            border: InputBorder.none,
                                                                                            focusedBorder: InputBorder.none,
                                                                                            enabledBorder: InputBorder.none,
                                                                                            errorBorder: InputBorder.none,
                                                                                            disabledBorder: InputBorder.none,  ),

                                                                                          style: Textstyle14))),
                                                                              IconButton(
                                                                                icon:  const Icon(
                                                                                  Icons.add_circle,
                                                                                  color: mynew_dred,
                                                                                  size: 45,
                                                                                ),
                                                                                onPressed: () {
                                                                                  OrderProvider _orderprovider =
                                                                                  Provider.of<OrderProvider>(context,
                                                                                      listen: false);
                                                                                  int newCount = value.iItemCount + 1;
                                                                                  _orderprovider.fun_Dish_addAlert(
                                                                                      context,
                                                                                      value.strItemName,
                                                                                      value.strItemID.toString(),
                                                                                      newCount,
                                                                                      value.itemGS[index].floor,value.itemGS[index].Notes);

                                                                                },
                                                                              )
                                                                            ],
                                                                          ),
                                                                        )),
                                                                    SizedBox(
                                                                      height: 20.0,
                                                                    ),
                                                                    Padding(
                                                                      padding: const EdgeInsets.only(left: 10, right: 10),
                                                                      child: Container(
                                                                        height: 50,
                                                                        child:SimpleAutoCompleteTextField(

                                                                            key: keyDishEdit,
                                                                            suggestions: value.notes,
                                                                            clearOnSubmit: false,
                                                                            textSubmitted: (text){
                                                                            },
                                                                            decoration: const InputDecoration(
                                                                              labelText: 'NOTE',
                                                                              labelStyle: TextStyle(color: my_black),
                                                                              filled: true,
                                                                              fillColor: my_white,
                                                                              enabledBorder: OutlineInputBorder(
                                                                                borderRadius:
                                                                                BorderRadius.all(Radius.circular(8.0)),
                                                                                borderSide:
                                                                                BorderSide(color: my_black,),
                                                                              ),
                                                                              focusedBorder: OutlineInputBorder(
                                                                                borderRadius:
                                                                                BorderRadius.all(Radius.circular(8.0)),
                                                                                borderSide:
                                                                                BorderSide(color: mynew_dred,),
                                                                              ),
                                                                            ),

                                                                            controller: value.notesTc),

                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                                Padding(
                                                                  padding: const EdgeInsets.all(15.0),
                                                                  child: InkWell(
                                                                    onTap: () {

                                                                      _orderprovider.fun_changeQty(value.itemGS[index],value.qtyTc.text,value.notesTc.text,context);
                                                                    },
                                                                    child: Container(
                                                                      width:120,
                                                                      height: 50,

                                                                      decoration: BoxDecoration(
                                                                        color: mynew_yellow,
                                                                        borderRadius: BorderRadius.all(
                                                                            Radius.circular(12.0)),
                                                                      ),
                                                                      child: Center(
                                                                        child: Text(
                                                                          "Save",
                                                                          style:TextStyle(
                                                                              fontFamily: fontBold,
                                                                              fontSize: textSize16,
                                                                              color: my_black,
                                                                              fontWeight: FontWeight.bold),
                                                                          textAlign: TextAlign.center,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  });
                                            }
                                            else if(value.itemGS[index].Status!="Cancelled"){
                                              if(value.CancelationLogin){
                                                _showDialogCancellationLogin(context,value.itemGS[index]);

                                              }else{

                                                _showDialogCancellation(context,value.itemGS[index]);
                                              }
                                            }

                                          },
                                          onLongPress: (){
                                            if(value.itemGS[index].Status!="OrderPrinted"){
                                              if(value.itemGS.length!=1){
                                                mRootReference.child(value.itemGS[index].dbRef).once().then((DatabaseEvent databaseEvent) {
                                                  if(databaseEvent.snapshot.value!=null){
                                                    Map<dynamic, dynamic> data = databaseEvent.snapshot.value as Map;

                                                    mRootReference
                                                        .child("DishBucket")
                                                        .child(data["Parent"].toString())
                                                        .push()
                                                        .set(int.parse(value.itemGS[index].Qty));
                                                    mRootReference.child(value.itemGS[index].dbRef).remove();
                                                  }


                                                }); 
                                              }else{
                                                mRootReference.child(value.itemGS[index].dbRef).once().then((DatabaseEvent databaseEvent) {
                                  if(databaseEvent.snapshot.value!=null){
                                    Map<dynamic, dynamic> data = databaseEvent.snapshot.value as Map;

                                    mRootReference
                                        .child("DishBucket")
                                        .child(data["Parent"].toString())
                                        .push()
                                        .set(int.parse(value.itemGS[index].Qty));
                                    mRootReference.child(value.itemGS[index].dbRef).remove();
                                  }



                                                });
                                                mRootReference.child("TableOrders").child("Floors").child(widget.floor).child("Tables").child(widget.tableID).child("SubTables").child(widget.SubTable).child("Orders").remove();                                            }
                                            }
                                          },
                                          child: ItemsAdapter(value.itemGS[index],index,context),
                                        ),
                                      ),
                                      value.itemGS[index].Status!="OrderPrinted"? InkWell(
                                        onTap:(){
                                          if(value.itemGS[index].Status!="OrderPrinted"){
                                            if(value.itemGS.length!=1){
                                              mRootReference.child(value.itemGS[index].dbRef).once().then((DatabaseEvent databaseEvent) {
                                  if(databaseEvent.snapshot.value!=null){
                                    Map<dynamic, dynamic> data = databaseEvent.snapshot.value as Map;
                                    mRootReference
                                        .child("DishBucket")
                                        .child(data["Parent"].toString())
                                        .push()
                                        .set(int.parse(value.itemGS[index].Qty));
                                    mRootReference.child(value.itemGS[index].dbRef).remove();
                                  }


                                              });
                                            }
                                            else{
                                              mRootReference.child(value.itemGS[index].dbRef).once().then((DatabaseEvent databaseEvent) {
                                  if(databaseEvent.snapshot.value!=null){
                                    Map<dynamic, dynamic> data = databaseEvent.snapshot.value as Map;
                                    mRootReference
                                        .child("DishBucket")
                                        .child(data["Parent"].toString())
                                        .push()
                                        .set(int.parse(value.itemGS[index].Qty));
                                    mRootReference.child(value.itemGS[index].dbRef).remove();
                                  }



                                              });
                                              mRootReference.child("TableOrders").child("Floors").child(widget.floor).child("Tables").child(widget.tableID).child("SubTables").child(widget.SubTable).child("Orders").remove();                                            }
                                          }

                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Image.asset("assets/cross_icon.png",scale: 40,),
                                        ),
                                      ):Container()

                                    ],
                                  ):Container();
                                });
                          },
                        ),
                      ),
                      // Container(
                      //   height: queryData.size.height * 0.03,
                      //   child: Padding(
                      //     padding: EdgeInsets.symmetric(horizontal: 10),
                      //     child: Center(
                      //       child: Divider(
                      //         color: Colors.black,
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      // Container(
                      //   height: queryData.size.height * 0.065,
                      //   color: my_white,
                      //   // child: Row(
                      //   //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   //   children: <Widget>[
                      //   //     Expanded(
                      //   //         flex: 6,
                      //   //         child: Padding(
                      //   //           padding: const EdgeInsets.only(left: 15.0),
                      //   //           child: Text(
                      //   //             Order_total,
                      //   //             style: Textstyle18,
                      //   //           ),
                      //   //         )),
                      //   //     Expanded(
                      //   //         flex: 2,
                      //   //         child: Center(child: Consumer<OrderProvider>(
                      //   //           builder: (context, value, child) {
                      //   //             return Text(
                      //   //               value.dOrderTotal.toString(),
                      //   //               style: Textstyle18,
                      //   //             );
                      //   //           },
                      //   //         ))),
                      //   //   ],
                      //   // ),
                      // ),
                    ],
                  ),
                ):
                Container(child:Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(height:queryData.size.height*0.04),
                      Image.asset("assets/search_food.png",scale: 4,),
                      SizedBox(height:queryData.size.height*0.07),
                      Image.asset("assets/select_food.png",scale:4,),
                      SizedBox(height:queryData.size.height*0.07),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(width:queryData.size.width*0.05),
                          Container(
                              child: Image.asset("assets/print_kot.png",scale: 4,)),
                        ],
                      ),

                    ],
                  ),
                ))
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
          padding: EdgeInsets.all(8),
          width: size.width * 0.90,
          height: 60,
          decoration:BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(12.0))),

          child: ElevatedButton.icon(
            onPressed: () {
              _orderprovider.printerKitchens(widget.floor,widget.tableID,widget.SubTable,widget.OrderID);

              funShowPrinterDialog(context, widget.OrderID, widget.floor, widget.SubTable, widget.tableID);
            },
            label: Text(
              'KOT',
              style: btnTextwhite,
            ),
            icon: Icon(Icons.print),
            style: ElevatedButton.styleFrom(
              primary: mynew_dred,
              textStyle: TextStyle(
                color: Colors.black,
                fontSize: 22,
              ),
            ),
          )
      ),
    );
  }


  Future<AlertDialog?> ShowDishDailog(BuildContext context) {

    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Consumer<OrderProvider>(
            builder: (context, value, child) {
              return AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0))),
                contentPadding: EdgeInsets.only(top: 10.0),
                content: Container(
                  width: 300.0,
                  height: 300.0,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Column(
                        children: [
                          SizedBox(
                            height: 5.0,
                          ),

                          Text(
                            value.strItemName,
                            style: Textstyle16,
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10,right: 10),

                            child: Divider(
                              color: Colors.black,
                              height: 5.0,
                            ),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          Row(
                            mainAxisAlignment:MainAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Rate    : ₹ ${value.strItemRate}",
                                  style: TextStyle(
                                    fontFamily: fontBold,
                                    fontSize: textSize16,
                                    color: my_black,
                                  ),
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Padding(
                              padding: EdgeInsets.only(left: 30.0, right: 40.0),
                              child: Container(
                                width: 180,
                                height: 45,
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    new IconButton(
                                      icon: new Icon(
                                        Icons.remove_circle,
                                        color: mynew_dred,
                                        size: 45,
                                      ),
                                      onPressed: () {
                                        OrderProvider _orderprovider =
                                        Provider.of<OrderProvider>(context,
                                            listen: false);
                                        if(value.iItemCount!= 1){
                                          int newCount = value.iItemCount - 1;

                                          _orderprovider.fun_Dish_addAlert(
                                              context,
                                              value.strItemName,
                                              value.strItemID.toString(),
                                              newCount,
                                              widget.floor,notesTC.text);
                                        }

                                      },
                                    ),
                                    SizedBox(width:8),
                                    Container(
                                        decoration: BoxDecoration(
                                            color:my_greydark ,
                                            shape: BoxShape.circle),
                                        width: 45,
                                        height: 45,
                                        child: Center(
                                            child: new TextField(
                                                onTap: (){
                                                  value.qtyTc.text="";
                                                },
                                                controller: value.qtyTc,
                                                textAlign: TextAlign.center,
                                                keyboardType: TextInputType.number,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  focusedBorder: InputBorder.none,
                                                  enabledBorder: InputBorder.none,
                                                  errorBorder: InputBorder.none,
                                                  disabledBorder: InputBorder.none,  ),
                                                style: Textstyle16))),
                                    new IconButton(
                                      icon: new Icon(
                                        Icons.add_circle,
                                        color: mynew_dred,
                                        size: 45,
                                      ),
                                      onPressed: () {
                                        OrderProvider _orderprovider =
                                        Provider.of<OrderProvider>(context,
                                            listen: false);
                                        int newCount = value.iItemCount + 1;
                                        _orderprovider.fun_Dish_addAlert(
                                            context,
                                            value.strItemName,
                                            value.strItemID.toString(),
                                            newCount,
                                            widget.floor,notesTC.text);

                                      },
                                    )
                                  ],
                                ),
                              )),
                          SizedBox(
                            height: 20.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Container(
                              height: 50,
                              child:SimpleAutoCompleteTextField(

                                  key: keyDish,
                                  suggestions: value.notes,
                                  clearOnSubmit: false,
                                  textSubmitted: (text){
                                  },
                                  decoration: const InputDecoration(
                                    labelText: 'NOTE',
                                    labelStyle: TextStyle(color: my_black),
                                    filled: true,
                                    fillColor: my_white,
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                      borderSide:
                                      BorderSide(color: my_black,),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(8.0)),
                                      borderSide:
                                      BorderSide(color: mynew_dred,),
                                    ),
                                  ),

                                  controller: notesTC),

                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Row(
                          mainAxisAlignment:MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                value.FinishOrNext="Finish";
                                String DayNode='';
                                if(value.orderDayNode!=''){
                                  DayNode = value.orderDayNode;

                                }else{
                                  DateTime now = new DateTime.now();
                                  DayNode = now.year.toString() +
                                      "/" +
                                      now.month.toString() +
                                      "/" +
                                      now.day.toString();
                                }
                                OrderProvider _orderprovider =
                                Provider.of<OrderProvider>(context,
                                    listen: false);

                                if(value.qtyTc.text!=""){
                                  fun_AddItem(
                                      value.strItemID,
                                      DayNode,
                                      value.strItemName,
                                      int.parse(value.qtyTc.text),
                                      value.strItemRate,
                                      value.strItemCategory);
                                }


                              },
                              child: Container(
                                width:120,
                                height: 50,

                                decoration: BoxDecoration(
                                  color: mynew_yellow,
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(12.0)),
                                ),
                                child: Center(
                                  child: Text(
                                    "Finish",
                                    style:TextStyle(
                                        fontFamily: fontBold,
                                        fontSize: textSize16,
                                        color: my_black,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                value.FinishOrNext="Next";
                                String DayNode='';
                                DateTime noweee = new DateTime.now();
                               String  strDayNode = noweee.year.toString() +
                                    "/" +
                                    noweee.month.toString() +
                                    "/" +
                                    noweee.day.toString();


                                if(value.orderDayNode!=''){
                                  DayNode = value.orderDayNode;

                                }
                                else{
                                  DateTime now = new DateTime.now();
                                  DayNode = now.year.toString() +
                                      "/" +
                                      now.month.toString() +
                                      "/" +
                                      now.day.toString();
                                }
                                print('DayNode  ${DayNode}');


                                OrderProvider _orderprovider = Provider.of<OrderProvider>(context,
                                    listen: false);

                                if(value.qtyTc.text!=""){
                                  fun_AddItem(
                                      value.strItemID,
                                      DayNode,
                                      value.strItemName,
                                      int.parse(value.qtyTc.text),
                                      value.strItemRate,
                                      value.strItemCategory);
                                }



                              },
                              child: Container(
                                width:120,
                                height: 50,

                                decoration: BoxDecoration(
                                  color: mynew_yellow,
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(12.0)),
                                ),
                                child: Center(
                                  child: Text(
                                    "Next",
                                    style:TextStyle(
                                        fontFamily: fontBold,
                                        fontSize: textSize16,
                                        color: my_black,
                                        fontWeight: FontWeight.bold),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ),
                            ),

                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  void fun_AddItem(String strItemID, String dayNode, String strItemName,
      int iItemCount, String strItemRate, String strItemCategory) async {
    setState(() {
      widget.AddItemClick="";
    });
    print(widget.OrderID);
    OrderProvider _orderprovider = Provider.of<OrderProvider>(context, listen: false);

    mRootReference
        .child("Dish")
        .child(strItemID)
        .once()
        .then((DatabaseEvent databaseEvent) async {
      Map<dynamic, dynamic> ItemData = databaseEvent.snapshot.value as Map;
      String strOrderID = widget.OrderID.toString();
      String strTable = widget.tableID.toString();

      mRootReference
          .child("Tables")
          .child(widget.floor)
          .child(widget.tableID)
          .child("Order")
          .child(widget.OrderID)
          .set(widget.SubTable.replaceAll('S', ''));

      mRootReference
          .child("Tables")
          .child(widget.floor)
          .child(widget.tableID)
          .child("SubTableOrder")
          .child(widget.SubTable.replaceAll('S', ''))
          .child(widget.OrderID)
          .set(widget.OrderID);



      HashMap<String, Object> Data = new HashMap();
      Data['ItemID'] = strItemID;
      Data['OrderID'] = strOrderID;
      Data['StatusBill'] = 'NotBilled';
      Data['Status'] = 'OrderTaken';
      if (notesTC.text != "") {
        Data['Note'] = notesTC.text.toString();
      }else{
        Data['Note'] ="";
      }
      Data['Item'] = strItemName;
      Data['Kitchen'] = ItemData['Kitchen'].toString();
      Data['Rate'] = strItemRate.toString();
      Data['Quantity'] = iItemCount.toString();
      Data['TableNumber'] = strTable;
      Data['OrderedBy'] = _orderprovider.stewardID!;
      Data['Floor'] = widget.floor;
      Data['Category'] = strItemCategory;
      Data['ReGenerated'] = 'NO';
      Data['Parent'] = ItemData['Parent'].toString();
      Data['Unit'] = ItemData['Unit'].toString();
      Data['Depend'] = ItemData['Depend'].toString();
      Data['DependUses'] = ItemData['DependUses'].toString();
      String strTime = DateTime.now().millisecondsSinceEpoch.toString();
      Data['Time'] = strTime;
      if (ItemData['Taxablity'].toString() == 'true') {
        Data['Taxablity'] = 'true';
      } else {
        Data['Taxablity'] = 'false';
      }
      Data['NumberOfPerson'] = _orderprovider.stewardID!;
      Data['SubTable'] = widget.SubTable.replaceAll("S", "");


      final String? AutoGenID = mRootReference.push().key;
      String? Token = mRootReference.push().key;
      mRootReference
          .child("DishBucket")
          .child(ItemData['Parent'].toString())
          .child(Token!)
          .set(double.tryParse(iItemCount.toString())! *
          double.tryParse(ItemData['Unit'].toString())!);


      double? dTotal = 0.0;
      dTotal = double.tryParse(iItemCount.toString())! *
          double.tryParse(strItemRate.toString())!;

      HashMap<String, Object> TableData = new HashMap();

      TableData['OrderID'] = strOrderID;
      String dRef = 'Orders/$dayNode/${widget.floor}/${strTable}/${widget.SubTable.replaceAll('S', '')}/${strOrderID}';
      TableData['OrderRef'] = dRef;




      print('value success 9');
      mRootReference
          .child("Orders").child(dayNode).child(widget.floor)
          .child(strTable)
          .child(widget.SubTable.replaceAll('S', ''))
          .child(strOrderID)
          .child(AutoGenID!)
          .set(Data);
      print('AutoGenID  ${AutoGenID}  strOrderID  $strOrderID  dayNode   ${dayNode}');


      mRootReference.child("TableOrders").child("Floors").child(widget.floor).child("Tables").child(strTable).
      child("SubTables").child(widget.SubTable).child("Orders").child(strOrderID).update(TableData);
      finish(context);

      SharedPreferences userPreference = await SharedPreferences.getInstance();

      userPreference.setString('Floor', widget.floor);
      userPreference.setString('Table', strTable);
      userPreference.setString('SubTable', widget.SubTable);
      _orderprovider.funCalculate(strOrderID);
      _orderprovider.fun_orderItems(widget.floor, strTable, widget.SubTable);


      notesTC.clear();
      manualRateTC.clear();
      qtyTC.clear();
      print(_orderprovider.FinishOrNext);
      if(_orderprovider.FinishOrNext=="Next"){
        inputNode.requestFocus();
      }

      // this is where success gets sent

    });


  }

  void funShowPrinterDialog(BuildContext context, String orderID, String floor,
      String subTable, String tableID) {
    OrderProvider _orderprovider = Provider.of<OrderProvider>(context, listen: false);

    Widget setupAlertDialoadContainer() {
      return Container(
          height: 300,
          width: 300,
          decoration: BoxDecoration(
            color: my_white,),
          // Change as per your requirement
          child: SingleChildScrollView(
            child: Consumer<OrderProvider>(
              builder: (context, value, child) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            value.FloorName,
                            style: Textstyle16,
                          ),
                          Text(
                            value.FloorIP,
                            style:TextStyle(
                                fontFamily: fontBold,
                                fontSize: textSize12,
                                color: mynew_dred,
                                fontStyle: FontStyle.italic),
                          ),
                        ],
                      ),
                    ),
                    SwitchListTile(
                        value: value.isFloorConsolidated,

                        title: const Text("Floor Consolidated"),

                        activeColor: mynew_yellow,
                        onChanged: (value) {
                          setState(() {
                            print('value   $value');
                            String status = 'Enable';
                            if (value == true) {
                              status = 'Enable';
                            } else {
                              status = 'Disable';
                            }
                            // mRootReference
                            //     .child('config')
                            //     .child('Floors')
                            //     .child(floor)
                            //     .child('FloorConsolidated')
                            //     .set(status);
                            _orderprovider.floorConsolidated(value);
                          });
                        }),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'NIKALA',
                            style: Textstyle16,
                          ),
                          Text(
                            value.NikalaIP,
                            style: TextStyle(
                                fontFamily: fontBold,
                                fontSize: textSize12,
                                color: mynew_dred,
                                fontStyle: FontStyle.italic),
                          ),
                        ],
                      ),
                    ),
                    SwitchListTile(
                        value: value.isNikalaConsolidated,
                        title: const Text("Nikala Consolidated"),
                        activeColor: mynew_yellow,
                        onChanged: (value) {
                          setState(() {
                            print('value   $value');
                            String status = 'Enable';
                            if (value == true) {
                              status = 'Enable';
                            } else {
                              status = 'Disable';
                            }
                            // mRootReference
                            //     .child('config')
                            //     .child('Floors')
                            //     .child(floor)
                            //     .child('NikalaConsolidated')
                            //     .set(status);
                            _orderprovider.floorNIkConsolidated(value);
                            // Provider.of<OrderProvider>(context, listen: false).isFloorConsolidated==value;
                          });
                        }),
                    SwitchListTile(
                        value: value.isNikalaSeparated,
                        title: const Text("Nikala Separated"),
                        activeColor: mynew_yellow,
                        onChanged: (value) {
                          setState(() {
                            print('value   $value');
                            String status = 'Enable';
                            if (value == true) {
                              status = 'Enable';
                            } else {
                              status = 'Disable';
                            }
                            // mRootReference
                            //     .child('config')
                            //     .child('Floors')
                            //     .child(floor)
                            //     .child('NikalaSeparated')
                            //     .set(status);
                            _orderprovider.floorNIkSeprated(value);

                            // Provider.of<OrderProvider>(context, listen: false).isFloorConsolidated==value;
                          });
                        }),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'KITCHEN PRINTS',
                            style: Textstyle16,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: Consumer<OrderProvider>(
                        builder: (context, value, child) {
                          return ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: value.printerGS.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        margin:EdgeInsets.only(left:15),
                                        child: Text(
                                          value.printerGS[index].Name,
                                          style: Textstyle14,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: SwitchListTile(

                                            value: value
                                                .printerGS[index].Consolidated,
                                            title: Text(
                                              "CS",
                                              style: Textstyle13,
                                            ),
                                            activeColor: mynew_yellow,
                                            onChanged: (changedvalue) {
                                              setState(() {
                                                // isBarcode=
                                                String status = 'Enable';
                                                if (changedvalue == true) {
                                                  status = 'Enable';
                                                } else {
                                                  status = 'Disable';
                                                }
                                                // mRootReference
                                                //     .child('config')
                                                //     .child('Kitchens')
                                                //     .child(value.printerGS[index].ID.toString())
                                                //     .child('KitchenConsolidated')
                                                //     .set(status);
                                                _orderprovider.kichenConsolidatedP("CS",changedvalue,index);
                                                print(value);
                                              });
                                            }),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: SwitchListTile(
                                            value:
                                            value.printerGS[index].Seprated,
                                            title: Text(
                                              "SP",
                                              style: Textstyle13,
                                            ),
                                            activeColor: mynew_yellow,
                                            onChanged: (changedvalue) {
                                              setState(() {
                                                // isBarcode=value;
                                                String status = 'Enable';
                                                if (changedvalue == true) {
                                                  status = 'Enable';
                                                } else {
                                                  status = 'Disable';
                                                }
                                                // mRootReference
                                                //     .child('config')
                                                //     .child('Kitchens')
                                                //     .child(value.printerGS[index].ID.toString())
                                                //     .child('KitchenSeparated')
                                                //     .set(status);
                                                _orderprovider.kichenConsolidatedP("SP",changedvalue,index);


                                              });
                                            }),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ),
                  ],
                );
              },
            ),
          ));
    }


    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor:mynew_dred,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            contentPadding: EdgeInsets.only(
              top: 10.0,
            ),
            title: Container(child: Text('Printers',style:TextStyle(color: my_white))),
            content: setupAlertDialoadContainer(),
            actions: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20))),
                        height: 40,
                        width: 120,
                        margin: EdgeInsets.all(5),
                        child: MaterialButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(12))
                          ),
                          child: Text('Skip', style: TextStyle(fontSize: 20.0)),
                          color: my_white,
                          textColor: mynew_dred,
                          onPressed: () {
                            String DayNode='';
                            if(_orderprovider.orderDayNode!=''){
                              DayNode = _orderprovider.orderDayNode;

                            }else{
                              DateTime now = new DateTime.now();
                              DayNode = now.year.toString() +
                                  "/" +
                                  now.month.toString() +
                                  "/" +
                                  now.day.toString();
                            }

                            String dRef = 'Orders/$DayNode/${widget.floor}/${tableID}/${widget.SubTable}/${orderID}';

                            _orderprovider.fun_UpdateprintData(dRef,widget.floor,orderID,tableID,widget.SubTable);
                            finish(context);
                            finish(context);
                          },
                        )),
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(20))),
                        height: 40,
                        width: 120,
                        margin: EdgeInsets.all(5),
                        child: Consumer<OrderProvider>(
                          builder: (context, value, hild) {
                            return FlatButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12))
                              ),
                              child: Text(
                                'Print',
                                style: TextStyle(fontSize: 20.0,),
                              ),
                              color: my_white,
                              textColor: mynew_dred,
                              onPressed: () async {
                                _orderprovider.funPrintClear();
                                // print('1  ${value.printThermal.toString()}  ${value.itemGS.length}    ');

                                if(value.itemGS.isNotEmpty){



                                  String DayNode ='';
                                  if(_orderprovider.orderDayNode!=''){
                                    DayNode = _orderprovider.orderDayNode;

                                  }else{
                                    DateTime now = new DateTime.now();
                                    DayNode = now.year.toString() +
                                        "/" +
                                        now.month.toString() +
                                        "/" +
                                        now.day.toString();
                                  }

                                  String dRef = 'Orders/$DayNode/${widget.floor}/${tableID}/${widget.SubTable}/${orderID}';
                                  Navigator.pop(context);
                                  alertPrinterStatus();
                                  if(widget.floor!='4'){
                                    if (value.printerGS.isNotEmpty) {
                                      await _orderprovider.fun_kichenPrint(
                                          value.FloorName,
                                          value.NikalaIP,
                                          orderID,
                                          subTable,
                                          tableID,
                                          context,
                                          "Kitchen",dRef,floor,widget.OrderType,1000,"ALL",value.itemGS,value.printerGS,value.Itemkichens);
                                    }
                                    if (value.isNikalaSeparated == true) {
                                      await _orderprovider.fun_nikalaSeprated(
                                          value.FloorName,
                                          value.NikalaIP,
                                          orderID,
                                          subTable,
                                          tableID,
                                          context,
                                          "NikalaS",dRef,floor,widget.OrderType,1000,"ALL",value.itemGS,value.printerGS,value.Itemkichens);
                                    }
                                    if (value.isFloorConsolidated == true) {
                                      print('1234');

                                      await _orderprovider.fun_consolidatedprint(
                                          value.FloorName,
                                          value.FloorIP,
                                          orderID,
                                          subTable,
                                          tableID,
                                          context,
                                          "FloorC"
                                          ,dRef,floor,widget.OrderType,1000,value.itemGS
                                      );
                                    }

                                    if (value.isNikalaConsolidated == true) {
                                      await _orderprovider.fun_floornikalaprint(
                                          value.FloorName,
                                          value.NikalaIP,
                                          orderID,
                                          subTable,
                                          tableID,
                                          context,
                                          "NikalaC",dRef,floor,widget.OrderType,1000,value.itemGS);
                                    }
                                  }else{
                                    if (value.isFloorConsolidated == true) {
                                      print('1234');

                                      await _orderprovider.fun_consolidatedprint(
                                          value.FloorName,
                                          value.FloorIP,
                                          orderID,
                                          subTable,
                                          tableID,
                                          context,
                                          "FloorC"
                                          ,dRef,floor,widget.OrderType,1000,value.itemGS
                                      );
                                    }

                                    if (value.isNikalaConsolidated == true) {
                                      await _orderprovider.fun_floornikalaprint(
                                          value.FloorName,
                                          value.NikalaIP,
                                          orderID,
                                          subTable,
                                          tableID,
                                          context,
                                          "NikalaC",dRef,floor,widget.OrderType,1000,value.itemGS);
                                    }
                                    if (value.isNikalaSeparated == true) {
                                      await _orderprovider.fun_nikalaSeprated(
                                          value.FloorName,
                                          value.NikalaIP,
                                          orderID,
                                          subTable,
                                          tableID,
                                          context,
                                          "NikalaS",dRef,floor,widget.OrderType,1000,"ALL",value.itemGS,value.printerGS,value.Itemkichens);
                                    }
                                    if (value.printerGS.isNotEmpty) {
                                      await _orderprovider.fun_kichenPrint(
                                          value.FloorName,
                                          value.NikalaIP,
                                          orderID,
                                          subTable,
                                          tableID,
                                          context,
                                          "Kitchen",dRef,floor,widget.OrderType,1000,"ALL",value.itemGS,value.printerGS,value.Itemkichens);
                                    }


                                  }





                                  _orderprovider.fun_UpdateprintData(dRef,widget.floor,orderID,tableID,widget.SubTable);


                                }
                                else{

                                  MainProvider _mainProvider= Provider.of<MainProvider>(context, listen: false);
                                  _mainProvider.fun_Snackbar('Items not added',context);

                                }





                              },
                            );
                          },
                        )),
                  ],
                ),
              ),
            ],
          );
        });
  }
  alertPrinterStatus() {
    OrderProvider _orderprovider =
    Provider.of<OrderProvider>(context, listen: false);
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);

    showDialog(
        context: context,
        barrierDismissible: false,

        builder: (BuildContext context) {
          return AlertDialog(

            backgroundColor: mynew_dred,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            contentPadding: EdgeInsets.only(
              top: 10.0,
            ),
            title: Text('Printers'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  height: 300,
                  width: 300,
                  decoration: BoxDecoration(
                      color: my_white,
                      borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(10))),// Change as per your requirement
                  child: Consumer<OrderProvider>(
                      builder: (context,value,child) {
                        return value.printerReGS.isEmpty?
                        Center(
                            child: CircularProgressIndicator()):
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: value.printerReGS.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  value.printerReGS[index].status=="success"?Icon(Icons.check,color: my_green):Icon(Icons.clear,color: my_red),
                                  Flexible(child: Text(value.printerReGS[index].name)),
                                  InkWell(
                                    onTap: (){
                                      if(value.printerReGS[index].status!="success"){
                                        _orderprovider.printerRetryFunction(value.printerReGS[index],index,value.itemGS,value.printerGS,value.Itemkichens);

                                      }
                                    },
                                    child: Container(
                                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                                        decoration: BoxDecoration(
                                            color: value.printerReGS[index].status=="success"?my_green:my_red,
                                            borderRadius: BorderRadius.circular(10)
                                        ),
                                        child: Text(value.printerReGS[index].status=="success"?"Completed":"Retry")),
                                  ),


                                ],
                              ),
                            );
                          },
                        );
                      }
                  ),
                ),
                InkWell(
                  onTap: (){
                    finish(context);
                    finish(context);
                    finish(context);
                  },
                  child: Container(
                    height: 50,
                    width:300,
                    child: Center(child: Consumer<OrderProvider>(
                        builder: (context,value,child) {
                          return Text(value.printSuccess?"OK":"Cancel" ,style: btnTextwhite,);
                        }
                    ),),),
                )
              ],
            ),
          );
        });
  }



  _showDialogCancellationLogin(context,ItemGS itemGS) {
    OrderProvider _orderprovider =
    Provider.of<OrderProvider>(context, listen: false);

    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return Consumer<OrderProvider>(
          builder: (context, value, child) {
            return AlertDialog(
              title: Container(
                  child: Center(
                      child: Text(
                        'Cancellation Details',
                        style: TextStyle(color: my_white),
                      ))),
              backgroundColor: mynew_dred,
              contentPadding: EdgeInsets.only(
                top: 15.0,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              content: SingleChildScrollView(
                child: Container(
                  width: double.maxFinite,
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
                  decoration: BoxDecoration(
                      color: my_white,
                      borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(10))),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Autocomplete<String>(
                          initialValue: TextEditingValue(text:""),


                          optionsBuilder: (TextEditingValue textEditingValue) {
                            if (textEditingValue.text.isEmpty) {
                              return [];
                            }
                            return  value.reasons
                                .where((String text) => text.toLowerCase()
                                .contains(textEditingValue.text.toLowerCase())
                            )
                                .toList();
                          },

                          fieldViewBuilder: (BuildContext context, textEditingController,
                              focusNode, onFieldSubmitted){
                            return TextFormField(
                              focusNode: focusNode,
                              decoration: InputDecoration(hintText: "Enter The Reason of Delete"),
                              controller: textEditingController,//uses fieldViewBuilder TextEditingController
                              validator: (text) =>
                              text!.isEmpty ? 'Reason cannot be blank' : null,
                              onChanged: (text){
                                reasonTxt=text;

                              },
                            );
                          },
                          onSelected: (text){
                          },


                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: employIdTC,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(hintText: "Employ Id"),
                            validator: (text) => text!.isEmpty ? 'Employ Id cannot be blank' :!value.staffs.contains(text)?"Wrong Employ ID" :null,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: passwordTC,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(hintText: "Password"),
                            validator: (text) =>
                            text!.isEmpty ? 'Password cannot be blank':employIdTC.text!=""&&value.staffsPasses.elementAt(value.staffs.indexOf(employIdTC.text)).toString()!=text? "Wrong Employ ID or Password": null,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: qtyTC,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(hintText: "Maximum quantity you can cancel is "+itemGS.Qty),
                            validator: (text) =>
                            text!.isEmpty ? 'Qty cannot be blank' :int.parse(text)>int.parse(itemGS.Qty)?"You cant delete items more than exist": null,
                          ),
                        ),
                        InkWell(
                          onTap: () {

                            final FormState? form = _formKey.currentState;
                            if (form!.validate()) {
                              _orderprovider.funPrintClear();

                              _orderprovider.fun_cancelconsolidatedprint(
                                  value.FloorName,
                                  value.FloorIP,
                                  context,
                                  "FloorC"
                                  ,"","CANCELLED"
                                  ,qtyTC.text,reasonTxt,employIdTC.text,itemGS,1000);

                              _orderprovider.fun_cancelnikalaSeprated(
                                  value.FloorName,
                                  value.NikalaIP,
                                  context,
                                  "NikalaS","","CANCELLED",qtyTC.text,reasonTxt,employIdTC.text,itemGS,1000);
                              _orderprovider.validateAndSave(itemGS,qtyTC.text,employIdTC.text,reasonTxt);

                              finish(context);
                              alertDialoagCancelStatus(itemGS,qtyTC.text,reasonTxt,employIdTC.text);

                            }

                          },
                          child: Container(
                              height: 50,
                              width: 150,
                              decoration: const BoxDecoration(
                                color: mynew_yellow,
                                borderRadius: BorderRadius.all(Radius.circular(
                                    10.0) //                 <--- border radius here
                                ),
                              ),
                              child: const Center(
                                child: Text(
                                  "Save",
                                  style: TextStyle(color: Colors.black),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
  _showDialogCancellation(context,ItemGS itemGS) {
    OrderProvider _orderprovider =
    Provider.of<OrderProvider>(context, listen: false);

    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return Consumer<OrderProvider>(
          builder: (context, value, child) {
            return AlertDialog(
              title: Container(
                  child: Center(
                      child: Text(
                        'Cancellation Details',
                        style: TextStyle(color: my_white),
                      ))),
              backgroundColor: mynew_dred,
              contentPadding: EdgeInsets.only(
                top: 15.0,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              content: SingleChildScrollView(
                child: Container(
                  width: double.maxFinite,
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
                  decoration: BoxDecoration(
                      color: my_white,
                      borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(10))),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: qtyTC,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(hintText: "Maximum quantity you can cancel is "+itemGS.Qty),
                            validator: (text) =>
                            text!.isEmpty ? 'Qty cannot be blank' :int.parse(text)>int.parse(itemGS.Qty)?"You cant delete items more than exist": null,
                          ),
                        ),
                        InkWell(
                          onTap: () {

                            final FormState? form = _formKey.currentState;
                            if (form!.validate()) {
                              _orderprovider.funPrintClear();

                              _orderprovider.fun_cancelconsolidatedprint(
                                  value.FloorName,
                                  value.FloorIP,
                                  context,
                                  "FloorC"
                                  ,"","CANCELLED"
                                  ,qtyTC.text,"",widget.Uid,itemGS,1000);
                              _orderprovider.fun_cancelnikalaSeprated(
                                  value.FloorName,
                                  value.NikalaIP,
                                  context,
                                  "NikalaS","","CANCELLED",qtyTC.text,"",employIdTC.text,itemGS,1000);
                              _orderprovider.validateAndSave(itemGS,qtyTC.text,employIdTC.text,reasonTxt);

                              finish(context);
                              alertDialoagCancelStatus(itemGS,qtyTC.text,"",widget.Uid);

                            }

                          },
                          child: Container(
                              height: 50,
                              width: 150,
                              decoration: const BoxDecoration(
                                color: mynew_yellow,
                                borderRadius: BorderRadius.all(Radius.circular(
                                    10.0) //                 <--- border radius here
                                ),
                              ),
                              child: const Center(
                                child: Text(
                                  "Save",
                                  style: TextStyle(color: Colors.black),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
  alertDialoagCancelStatus(ItemGS itemGS,String qty,String reason,String emloyID) {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: appbar_color,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            contentPadding: EdgeInsets.only(
              top: 10.0,
            ),
            title: Text('Printers'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  height: 300.0, // Change as per your requirement
                  width: 300.0,
                  decoration: BoxDecoration(
                      color: my_white,
                      borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(10))),// Change as per your requirement
                  child: Consumer<OrderProvider>(
                      builder: (context,value,child) {
                        return value.cancelPrinterGS.isEmpty?
                        Center(
                            child: CircularProgressIndicator()):
                        ListView.builder(
                          shrinkWrap: true,
                          itemCount: value.cancelPrinterGS.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  value.cancelPrinterGS[index].status=="success"?Icon(Icons.check,color: my_green):Icon(Icons.clear,color: my_red),
                                  Flexible(child: Text(value.cancelPrinterGS[index].name)),
                                  InkWell(
                                    onTap: (){
                                      OrderProvider _orderprovider =
                                      Provider.of<OrderProvider>(context, listen: false);
                                      _orderprovider.cancelPrinterRetryFunction(value.cancelPrinterGS[index], index);
                                    },
                                    child: Container(
                                        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                                        decoration: BoxDecoration(
                                            color: value.cancelPrinterGS[index].status=="success"?my_green:my_red,
                                            borderRadius: BorderRadius.circular(10)
                                        ),
                                        child: Text(value.cancelPrinterGS[index].status=="success"?"Completed":"Retry")),
                                  ),


                                ],
                              ),
                            );
                          },
                        );
                      }
                  ),
                ),
                InkWell(
                  onTap: (){
                    finish(context);
                  },
                  child: Container(
                    height: 50,
                    width:300,
                    child: Center(child:
                    Consumer<OrderProvider>(
                        builder: (context,value,child) {
                          return Text(value.printSuccess?"OK":"Cancel" ,style: btnTextwhite,);
                        }
                    ),),),
                )
              ],
            ),
          );
        });
  }
  Future<AlertDialog?> _showDialogManualRate(context) {
    OrderProvider _orderprovider =
    Provider.of<OrderProvider>(context, listen: false);

    // flutter defined function
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return Consumer<OrderProvider>(
          builder: (context, value, child) {
            return AlertDialog(
              title: Container(
                  child: Center(
                      child: Text(
                        'Item Details',
                        style: TextStyle(color: my_white),
                      ))),
              backgroundColor: mynew_dred,
              contentPadding: EdgeInsets.only(
                top: 15.0,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              content: SingleChildScrollView(
                child: Container(
                  width: double.maxFinite,
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
                  decoration: BoxDecoration(
                      color: my_white,
                      borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(10))),
                  child:  Form(
                    key: _formKeyManualQty,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: manualRateTC,
                            keyboardType: TextInputType.number,
                            decoration: const InputDecoration(
                              labelText: 'MANUEL RATE',
                              labelStyle: TextStyle(color: my_black),
                              filled: true,
                              fillColor: my_white,
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                                borderSide:
                                BorderSide(color: my_black,),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                                borderSide:
                                BorderSide(color: mynew_dred,),
                              ),
                            ),
                            validator: (text) =>
                            text!.isEmpty ? 'Rate cannot be blank' : null,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: qtyTC,
                            keyboardType: TextInputType.number,
                            decoration: const InputDecoration(
                              labelText: 'QUANTITY',
                              labelStyle: TextStyle(color: my_black),
                              filled: true,
                              fillColor: my_white,
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                                borderSide:
                                BorderSide(color: my_black,),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(8.0)),
                                borderSide:
                                BorderSide(color: mynew_dred,),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child:SimpleAutoCompleteTextField(

                              key: keyDishManuel,
                              suggestions: value.notes,
                              clearOnSubmit: false,
                              textSubmitted: (text){
                              },
                              decoration: const InputDecoration(
                                labelText: 'NOTE',
                                labelStyle: TextStyle(color: my_black),
                                filled: true,
                                fillColor: my_white,
                                enabledBorder: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                                  borderSide:
                                  BorderSide(color: my_black,),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(8.0)),
                                  borderSide:
                                  BorderSide(color: mynew_dred,),
                                ),
                              ),

                              controller: notesTC),

                        ),
                        InkWell(
                          onTap: () {
                            _orderprovider.funPrintClear();

                            final FormState? form = _formKeyManualQty.currentState;
                            if (form!.validate()) {
                              DateTime now = new DateTime.now();

                              String DayNode = '';
                              if(_orderprovider.orderDayNode!=''){
                                DayNode = _orderprovider.orderDayNode;

                              }else{
                                DateTime now = new DateTime.now();
                                DayNode = now.year.toString() +
                                    "/" +
                                    now.month.toString() +
                                    "/" +
                                    now.day.toString();
                              }
                              if(qtyTC.text==""){
                                fun_AddItem(
                                    value.strItemID,
                                    DayNode,
                                    value.strItemName,
                                    1,
                                    manualRateTC.text,
                                    value.strItemCategory);
                              }else{
                                fun_AddItem(
                                    value.strItemID,
                                    DayNode,
                                    value.strItemName,
                                    int.parse(qtyTC.text),
                                    manualRateTC.text,
                                    value.strItemCategory);
                              }



                            }

                          },
                          child: Container(
                              height: 50,
                              width: 150,
                              decoration: const BoxDecoration(
                                color: mynew_yellow,
                                borderRadius: BorderRadius.all(Radius.circular(
                                    10.0) //                 <--- border radius here
                                ),
                              ),
                              child: const Center(
                                child: Text(
                                  "Save",
                                  style: TextStyle(color: Colors.black),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }


}
