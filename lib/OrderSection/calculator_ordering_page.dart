// import 'dart:collection';
//
// import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
// import 'package:firebase_database/firebase_database.dart';
// import 'package:flutter/material.dart';
//
// import 'package:flutter_pumpkin/Constants/my_colors.dart';
// import 'package:flutter_pumpkin/Constants/my_functions.dart';
// import 'package:flutter_pumpkin/Constants/my_string.dart';
//
// import 'package:flutter_pumpkin/Constants/my_text.dart';
// import 'package:flutter_pumpkin/Provider/main_provider.dart';
// import 'package:flutter_pumpkin/Provider/order_provider.dart';
// import 'package:flutter_pumpkin/Views/itemGS.dart';
// import 'package:flutter_pumpkin/Views/items_adapter.dart';
// import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// class CalculatorOrderingPage extends StatelessWidget {
//   final DatabaseReference mRootReference =
//   FirebaseDatabase.instance.reference();
//   String floor, tableID, OrderID, Uid, SubTable, StewardID, OrderType,AddItemClick;
//   final GlobalKey<FormState> _formKeyManualQty = GlobalKey<FormState>();
//   TextEditingController manualRateTC = new TextEditingController();
//   TextEditingController notesTC = new TextEditingController();
//   TextEditingController reasonTC = new TextEditingController();
//   TextEditingController employIdTC = new TextEditingController();
//   TextEditingController passwordTC = new TextEditingController();
//   TextEditingController qtyTC = new TextEditingController();
//   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
//   GlobalKey<AutoCompleteTextFieldState<String>> keyDish = GlobalKey();
//   GlobalKey<AutoCompleteTextFieldState<String>> keyDishManuel = GlobalKey();
//
//
//
//
//   CalculatorOrderingPage({Key? key, required this.floor, required this.tableID,
//     required this.OrderID, required this.Uid, required this.SubTable,
//     required this.StewardID, required this.OrderType,required this.AddItemClick,
//   }) : super(key: key);
//
//   List<String> num = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
//
//   @override
//   Widget build(BuildContext context) {
//     MediaQueryData queryData;
//     queryData = MediaQuery.of(context);
//     print(queryData.orientation.toString());
//     print(queryData.size.width.toString());
//     OrderProvider _orderprovider =
//     Provider.of<OrderProvider>(context, listen: false);
//     return Scaffold(
//         backgroundColor: my_grey,
//         bottomNavigationBar:Container(
//           padding: EdgeInsets.all(8),
//           width: queryData.size.width * 0.90,
//           height: 60,
//           decoration:BoxDecoration(
//               borderRadius: BorderRadius.all(Radius.circular(12.0))),
//           child: ElevatedButton.icon(
//             onPressed: () {
//               _orderprovider.printerKitchens(floor,tableID,SubTable,OrderID);
//
//               funShowPrinterDialog(context, OrderID, floor, SubTable, tableID);
//             },
//             label: Text(
//               'KOT',
//               style: btnTextwhite,
//             ),
//             icon: Icon(Icons.print),
//             style: ElevatedButton.styleFrom(
//               primary: mynew_dred,
//               textStyle: TextStyle(
//                 color: Colors.black,
//                 fontSize: 22,
//               ),
//             ),
//           ),
//         ),
//
//         body: SafeArea(
//           child: queryData.orientation==Orientation.portrait?portrait(context):landscape(context),
//         ));
//
//   }
//
//   ///Calculator button design
//   Widget calculatorButtons(BuildContext context, String text) {
//     OrderProvider _orderprovider =
//     Provider.of<OrderProvider>(context, listen: false);
//     return Expanded(
//       child: InkWell(
//         onTap: (){
//           _orderprovider.onTap(text);
//         },
//         child: Container(
//           decoration: BoxDecoration(
//             // border: Border.all(width: 0),
//           ),
//           child: Center(
//             child: Text(
//               text,
//               style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   ///portrait Calculator Design
//   Widget portrait (BuildContext context){
//     MediaQueryData queryData;
//     queryData = MediaQuery.of(context);
//     OrderProvider _orderprovider =
//     Provider.of<OrderProvider>(context, listen: false);
//     return Column(
//       children: [
//         Consumer<OrderProvider>(
//             builder: (context,value,child) {
//               return Expanded(
//                 child:value.AddItemClick!="new" ?
//                 Card(
//                   margin: EdgeInsets.all(8),
//                   elevation: 0,
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(8),
//                     side: BorderSide(color: mynew_dred, width: 1),
//
//                   ),
//                   child: Column(
//                     children: [
//                       Container(
//                         height:50,
//                         padding:  EdgeInsets.symmetric(horizontal: 10),
//
//                         child: Row(
//                           children: <Widget>[
//                             Expanded(flex: 4, child: Center(child: Text("Item",style: TextStyle(fontStyle: FontStyle.italic,color: mynew_dred),))),
//                             Expanded(flex: 2, child: Center(child: Text("Quantity",style: TextStyle(fontStyle: FontStyle.italic,color: mynew_dred)))),
//                             Expanded(flex: 2, child: Center(child: Text("Total",style: TextStyle(fontStyle: FontStyle.italic,color: mynew_dred)))),
//                           ],
//                         ),
//                       ),
//                       Expanded(
//                         child: Consumer<OrderProvider>(
//                           builder: (context, value,child){
//                             return
//                               ListView.builder(
//                                   itemCount: value.itemGS.length,
//                                   scrollDirection: Axis.vertical,
//                                   shrinkWrap: true,
//                                   itemBuilder: (BuildContext context, int index) {
//                                     return value.itemGS[index].kotNumber==OrderID?InkWell(
//                                       onTap: (){
//                                         if(value.itemGS[index].Status!="OrderPrinted"&&value.itemGS[index].Status!="Cancelled"){
//                                           _orderprovider.fun_Dish_addAlert(context, value.itemGS[index].Name, value.itemGS[index].productId, int.parse(value.itemGS[index].Qty), value.itemGS[index].floor, value.itemGS[index].Notes);
//                                           showDialog(
//                                               context: context,
//                                               builder: (BuildContext context) {
//                                                 return Consumer<OrderProvider>(
//                                                   builder: (context, value, child) {
//                                                     return AlertDialog(
//                                                       shape: const RoundedRectangleBorder(
//                                                           borderRadius: BorderRadius.all(Radius.circular(12.0))),
//                                                       contentPadding: const EdgeInsets.only(top: 10.0),
//                                                       content: Container(
//                                                         width: 300.0,
//                                                         height: 300.0,
//                                                         child: Column(
//                                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                                           crossAxisAlignment: CrossAxisAlignment.stretch,
//                                                           mainAxisSize: MainAxisSize.min,
//                                                           children: <Widget>[
//                                                             Column(
//
//                                                               children: [
//                                                                 SizedBox(
//                                                                   height: 5.0,
//                                                                 ),
//                                                                 Text(
//                                                                   value.itemGS[index].Name,
//                                                                   style: Textstyle16,
//                                                                 ),
//                                                                 const SizedBox(
//                                                                   height: 10.0,
//                                                                 ),
//                                                                 Container(
//                                                                   margin: EdgeInsets.only(left: 10,right: 10),
//
//                                                                   child: Divider(
//                                                                     color: Colors.black,
//                                                                     height: 5.0,
//                                                                   ),
//                                                                 ),
//                                                                 const SizedBox(
//                                                                   height: 20.0,
//                                                                 ),
//                                                                 Row(
//                                                                   mainAxisAlignment:MainAxisAlignment.start,
//                                                                   children: [
//                                                                     Container(
//                                                                       margin: EdgeInsets.only(left: 10),
//                                                                       child: Text(
//                                                                         "Rate : ₹ ${value.itemGS[index].Rate}",
//                                                                         style: TextStyle(
//                                                                           fontFamily: fontBold,
//                                                                           fontSize: textSize16,
//                                                                           color: my_black,
//                                                                         ),
//                                                                         textAlign: TextAlign.start,
//                                                                       ),
//                                                                     ),
//                                                                   ],
//                                                                 ),
//                                                                 const SizedBox(
//                                                                   height: 15.0,
//                                                                 ),
//                                                                 Padding(
//                                                                     padding: EdgeInsets.only(left: 30.0, right: 40.0),
//                                                                     child: Container(
//                                                                       width: 180,
//                                                                       height: 45,
//                                                                       child: Row(
//                                                                         mainAxisAlignment:
//                                                                         MainAxisAlignment.spaceBetween,
//                                                                         children: <Widget>[
//                                                                           IconButton(
//                                                                             icon:  const Icon(
//                                                                               Icons.remove_circle,
//                                                                               color: mynew_dred,
//                                                                               size: 45,
//                                                                             ),
//                                                                             onPressed: () {
//                                                                               OrderProvider _orderprovider =
//                                                                               Provider.of<OrderProvider>(context,
//                                                                                   listen: false);
//                                                                               if(value.iItemCount!= 1){
//                                                                                 int newCount = value.iItemCount - 1;
//
//                                                                                 _orderprovider.fun_Dish_addAlert(
//                                                                                     context,
//                                                                                     value.strItemName,
//                                                                                     value.strItemID.toString(),
//                                                                                     newCount,
//                                                                                     floor,notesTC.text);
//                                                                               }
//                                                                             },
//                                                                           ),
//                                                                           SizedBox(width:8),
//                                                                           Container(
//                                                                               decoration: const BoxDecoration(
//                                                                                   color: my_greydark,
//                                                                                   shape: BoxShape.circle),
//                                                                               width: 45,
//                                                                               height: 45,
//                                                                               child: Center(
//                                                                                   child:  TextField(
//                                                                                       controller: value.qtyTc,
//                                                                                       textAlign: TextAlign.center,
//                                                                                       keyboardType: TextInputType.number,
//                                                                                       decoration: InputDecoration(
//                                                                                         border: InputBorder.none,
//                                                                                         focusedBorder: InputBorder.none,
//                                                                                         enabledBorder: InputBorder.none,
//                                                                                         errorBorder: InputBorder.none,
//                                                                                         disabledBorder: InputBorder.none,  ),
//
//                                                                                       style: Textstyle14))),
//                                                                           IconButton(
//                                                                             icon:  const Icon(
//                                                                               Icons.add_circle,
//                                                                               color: mynew_dred,
//                                                                               size: 45,
//                                                                             ),
//                                                                             onPressed: () {
//                                                                               OrderProvider _orderprovider =
//                                                                               Provider.of<OrderProvider>(context,
//                                                                                   listen: false);
//                                                                               int newCount = value.iItemCount + 1;
//                                                                               _orderprovider.fun_Dish_addAlert(
//                                                                                   context,
//                                                                                   value.strItemName,
//                                                                                   value.strItemID.toString(),
//                                                                                   newCount,
//                                                                                   value.itemGS[index].floor,value.itemGS[index].Notes);
//
//                                                                             },
//                                                                           )
//                                                                         ],
//                                                                       ),
//                                                                     )),
//                                                                 SizedBox(
//                                                                   height: 20.0,
//                                                                 ),
//                                                                 Padding(
//                                                                   padding: const EdgeInsets.only(left: 10, right: 10),
//                                                                   child: Container(
//                                                                     height: 50,
//                                                                     child:SimpleAutoCompleteTextField(
//
//                                                                         key: keyDish,
//                                                                         suggestions: value.notes,
//                                                                         clearOnSubmit: false,
//                                                                         textSubmitted: (text){
//                                                                         },
//                                                                         decoration: const InputDecoration(
//                                                                           labelText: 'NOTE',
//                                                                           labelStyle: TextStyle(color: my_black),
//                                                                           filled: true,
//                                                                           fillColor: my_white,
//                                                                           enabledBorder: OutlineInputBorder(
//                                                                             borderRadius:
//                                                                             BorderRadius.all(Radius.circular(8.0)),
//                                                                             borderSide:
//                                                                             BorderSide(color: my_black,),
//                                                                           ),
//                                                                           focusedBorder: OutlineInputBorder(
//                                                                             borderRadius:
//                                                                             BorderRadius.all(Radius.circular(8.0)),
//                                                                             borderSide:
//                                                                             BorderSide(color: mynew_dred,),
//                                                                           ),
//                                                                         ),
//
//                                                                         controller: value.notesTc),
//
//                                                                   ),
//                                                                 ),
//                                                               ],
//                                                             ),
//                                                             Padding(
//                                                               padding: const EdgeInsets.all(15.0),
//                                                               child: InkWell(
//                                                                 onTap: () {
//
//                                                                   _orderprovider.fun_changeQty(value.itemGS[index],value.qtyTc.text,value.notesTc.text,context);
//                                                                 },
//                                                                 child: Container(
//                                                                   width:120,
//                                                                   height: 50,
//
//                                                                   decoration: BoxDecoration(
//                                                                     color: mynew_yellow,
//                                                                     borderRadius: BorderRadius.all(
//                                                                         Radius.circular(12.0)),
//                                                                   ),
//                                                                   child: Center(
//                                                                     child: Text(
//                                                                       "Save",
//                                                                       style:TextStyle(
//                                                                           fontFamily: fontBold,
//                                                                           fontSize: textSize16,
//                                                                           color: my_black,
//                                                                           fontWeight: FontWeight.bold),
//                                                                       textAlign: TextAlign.center,
//                                                                     ),
//                                                                   ),
//                                                                 ),
//                                                               ),
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       ),
//                                                     );
//                                                   },
//                                                 );
//                                               });
//                                         }
//                                         else if(value.itemGS[index].Status!="Cancelled"){
//                                           if(value.CancelationLogin){
//                                             _showDialogCancellationLogin(context,value.itemGS[index]);
//
//                                           }else{
//
//                                             _showDialogCancellation(context,value.itemGS[index]);
//                                           }
//                                         }
//
//                                       },
//                                       onLongPress: (){
//                                         if(value.itemGS[index].Status!="OrderPrinted"){
//                                           mRootReference.child(value.itemGS[index].dbRef).once().then((snapshot) {
//                                             mRootReference
//                                                 .child("DishBucket")
//                                                 .child(snapshot.value["Parent"].toString())
//                                                 .push()
//                                                 .set(int.parse(value.itemGS[index].Qty));
//                                             mRootReference.child(value.itemGS[index].dbRef).remove();
//
//                                           });
//
//                                         }
//                                       },
//                                       child: ItemsAdapter(value.itemGS[index],index,context),
//                                     ):Container();
//                                   });
//                           },
//                         ),
//                       ),
//                       // Container(
//                       //   padding: const EdgeInsets.fromLTRB(10,20,10,20),
//                       //   child: Row(
//                       //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       //     children: const [
//                       //       Expanded(flex: 6, child: Text("Total")),
//                       //       Expanded(flex: 2, child: Center(child: Text("0"))),
//                       //     ],
//                       //   ),
//                       // ),
//                     ],
//                   ),
//                 ):Container(
//                   width: queryData.size.width*0.98,
//                   child: Card(
//                       margin: EdgeInsets.all(8),
//                       elevation: 2,
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(8),
//
//                       ),
//                       child:Image.asset("assets/add_calculator.png",scale:2)
//                   ),
//                 ),
//               );
//             }
//         ),
//         Container(
//           height: queryData.size.height / 2,
//           color: my_grey,
//           child: Column(
//             children: <Widget>[
//               Expanded(
//                 flex: 6,
//                 child: Padding(
//                   padding:EdgeInsets.fromLTRB(5.0,0,5,0),
//                   child: Card(
//                     elevation: 12,
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(12),
//                     ),
//                     child: Padding(
//                       padding: const EdgeInsets.all(0.0),
//                       child: Column(
//                         mainAxisSize: MainAxisSize.max,
//                         children: [
//
//                           Row(
//                             children: [
//                               Expanded(
//                                 child: Consumer<OrderProvider>(
//                                   builder: (context,value,child) {
//                                     return InkWell(
//                                       onTap: (){
//                                       _orderprovider.selectedItem();
//                                       },
//                                       child: Container(
//                                           padding: EdgeInsets.symmetric(vertical: 10,horizontal: 5),
//                                           margin: EdgeInsets.symmetric(vertical: 10,horizontal: 5),
//                                           height: 60,
//                                           decoration:  BoxDecoration(
//                                             borderRadius: const BorderRadius.all(
//                                                 Radius.circular(
//                                                     5.0) //                 <--- border radius here
//                                             ),
//                                             border: Border.all(
//                                               color:  value.isQty?my_grey:mynew_dred, //                   <--- border color
//                                               width: 1.0,
//                                             ),
//                                             color:my_grey,
//                                           ),
//                                           child:  Row(
//                                             children: [
//                                               Text(value.cItem.toString()),
//                                               Flexible(
//                                                 child: Text(value.cItemHint,
//                                                   style: hintText,
//                                                 ),
//                                               )
//
//
//                                             ],
//                                           )),
//                                     );
//                                   }
//                                 ),
//                               ),
//                               Expanded(
//                                 child: Consumer<OrderProvider>(
//                                   builder: (context,value,child) {
//                                     return InkWell(
//                                       onTap: (){
//                                         _orderprovider.selectedQty();
//
//                                       },
//                                       child: Container(
//                                           padding: EdgeInsets.symmetric(vertical: 10,horizontal: 5),
//                                           margin: EdgeInsets.symmetric(vertical: 10,horizontal: 5),
//                                           height: 60,
//                                           decoration:  BoxDecoration(
//                                             borderRadius: const BorderRadius.all(
//                                                 Radius.circular(
//                                                     5.0) //                 <--- border radius here
//                                             ),
//                                             border: Border.all(
//                                               color:  value.isQty?mynew_dred:my_grey, //                   <--- border color
//                                               width: 1.0,
//                                             ),
//                                             color: my_grey,
//                                           ),
//                                           child:  Row(
//                                             children: [
//                                               Consumer<OrderProvider>(
//                                                   builder: (context,value,child) {
//                                                     return Text(value.cQty,
//                                                     );
//                                                   }
//                                               ),
//                                               Consumer<OrderProvider>(
//                                                   builder: (context,value,child) {
//                                                     return Text(value.cQtyHint,
//                                                       style: hintText,
//                                                     );
//                                                   }
//                                               ),
//                                             ],
//                                           )),
//                                     );
//                                   }
//                                 ),
//                               ),
//                             ],
//                           ),
//                           Consumer<OrderProvider>(
//                               builder: (context,value,child) {
//                                 return Text(value.cErrorText.toString(),style: errorText,);
//                               }
//                           ),
//
//                           Expanded(
//                             child: Row(
//                               children: [
//                                 calculatorButtons(
//                                     context, num[1].toString()),
//                                 calculatorButtons(
//                                     context, num[2].toString()),
//                                 calculatorButtons(
//                                     context, num[3].toString()),
//                               ],
//                             ),
//                           ),
//                           Padding(
//                             padding: const EdgeInsets.fromLTRB(12,0,12,0),
//                             child: Divider(
//                                color: Colors.grey,
//                                height: 4.0,
//                             ),
//                           ),
//                           Expanded(
//                             child: Row(
//                               children: [
//                                 calculatorButtons(
//                                     context, num[4].toString()),
//                                 calculatorButtons(
//                                     context, num[5].toString()),
//                                 calculatorButtons(
//                                     context, num[6].toString()),
//                               ],
//                             ),
//                           ),
//                           Padding(
//                             padding: const EdgeInsets.fromLTRB(12,0,12,0),
//                             child: Divider(
//                               color: Colors.grey,
//                               height: 4.0,
//                             ),
//                           ),
//                           Expanded(
//                             child: Row(
//                               children: [
//                                 calculatorButtons(
//                                     context, num[7].toString()),
//                                 calculatorButtons(
//                                     context, num[8].toString()),
//                                 calculatorButtons(
//                                     context, num[9].toString()),
//                               ],
//                             ),
//                           ),
//                           Padding(
//                             padding: const EdgeInsets.fromLTRB(12,0,12,0),
//                             child: Divider(
//                               color: Colors.grey,
//                               height: 4.0,
//                             ),
//                           ),
//                           Expanded(
//                             child: Row(
//                               children: [
//                                 Expanded(
//                                   child: InkWell(
//                                     onTap:(){
//                                       _orderprovider.onDel();
//                                      },
//                                     onLongPress: (){
//                                       _orderprovider.onClear();
//
//                                     },
//                                     child: Container(
//                                       decoration: BoxDecoration(
//                                         border: Border.all(
//                                             color: my_grey, width: 0),
//                                         color: mynew_yellow,
//                                         borderRadius: BorderRadius.only(bottomLeft: Radius.circular(12))
//                                       ),
//                                       child: const Center(
//                                         child:  Icon(
//                                         Icons.backspace_outlined,
//                                         color: my_black,
//                                       ),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                                 calculatorButtons(context, num[0].toString()),
//                                 Expanded(
//                                   child: Consumer<OrderProvider>(
//                                     builder: (context,value,child) {
//                                       return InkWell(
//                                         onTap: (){
//                                           print("sdhdsdsdsdsss");
//                                           _orderprovider.onEnter();
//                                           if(value.cQty!=""&&value.cItem!=""&&value.dishesID_list.contains(value.cItem)&&int.parse(value.cQty)>0){
//                                             print("sdhdsdsdsdsss");
//
//                                             String DayNode = '';
//                                             if(_orderprovider.orderDayNode!=''){
//                                               DayNode = _orderprovider.orderDayNode;
//
//                                             }else{
//                                               DateTime now = new DateTime.now();
//                                               DayNode = now.year.toString() +
//                                                   "/" +
//                                                   now.month.toString() +
//                                                   "/" +
//                                                   now.day.toString();
//                                             }
//                                             mRootReference
//                                                 .child("Dish")
//                                                 .child(value.cItem)
//                                                 .once()
//                                                 .then((DataSnapshot snapshot) {
//                                               Map<dynamic, dynamic> ItemData = snapshot.value;
//                                               if(double.parse(ItemData["F"+floor].toString())>0){
//                                                 fun_AddItem(value.cItem, DayNode,value.cItemHint, int.tryParse(value.cQty)!, ItemData["F"+floor], ItemData['Category'], context,"");
//                                                 _orderprovider.onClear();
//
//                                               }else{
//                                                 _showDialogManualRate(value.cItem, DayNode,value.cItemHint, int.tryParse(value.cQty)!, ItemData['Category'],context);
//
//                                               }
//
//                                             });
//
//                                           }
//
//                                         },
//                                         child: Container(
//                                           decoration: BoxDecoration(
//                                             border: Border.all(
//                                                 color: my_grey, width: 0),
//                                             color: mynew_yellow,
//                                               borderRadius: BorderRadius.only(bottomRight:Radius.circular(12))
//                                           ),
//                                           child: const Center(
//                                             child: Icon(
//                                               Icons.arrow_forward,
//                                               color: my_black,
//                                             ),
//                                           ),
//                                         ),
//                                       );
//                                     }
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//               // const Expanded(
//               //   child: SizedBox(),
//               // ),
//               // Container(
//               //   width: double.maxFinite,
//               //   padding: EdgeInsets.all(5),
//               //   child: ElevatedButton.icon(
//               //     onPressed: () {
//               //       _orderprovider.printerKitchens(floor,tableID,SubTable,OrderID);
//               //
//               //       funShowPrinterDialog(context, OrderID, floor, SubTable, tableID);
//               //     },
//               //     label: Text(
//               //       'KOT',
//               //       style: btnTextwhite,
//               //     ),
//               //     icon: Icon(Icons.print),
//               //     style: ElevatedButton.styleFrom(
//               //       primary: mynew_dred,
//               //       textStyle: TextStyle(
//               //         color: Colors.black,
//               //         fontSize: 22,
//               //       ),
//               //     ),
//               //   ),
//               // ),
//               // const Expanded(
//               //   child: SizedBox(),
//               // ),
//             ],
//           ),
//         ),
//       ],
//     );
//   }
//
//   ///Landscape Calculator Design
//   Widget landscape (BuildContext context){
//     MediaQueryData queryData;
//     queryData = MediaQuery.of(context);
//     OrderProvider _orderprovider =
//     Provider.of<OrderProvider>(context, listen: false);
//     return Row(
//       children: [
//         Consumer<OrderProvider>(
//             builder: (context,value,child) {
//               return Expanded(
//                 child:value.AddItemClick!="new" ?
//                 Card(
//                   margin: EdgeInsets.all(8),
//                   elevation: 0,
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(8),
//                     side: BorderSide(color: mynew_dred, width: 1),
//
//                   ),
//                   child: Column(
//                     children: [
//                       Container(
//                         height:50,
//                         padding:  EdgeInsets.symmetric(horizontal: 10),
//
//                         child: Row(
//                           children: <Widget>[
//                             Expanded(flex: 4, child: Center(child: Text("Item",style: TextStyle(fontStyle: FontStyle.italic,color: mynew_dred),))),
//                             Expanded(flex: 2, child: Center(child: Text("Quantity",style: TextStyle(fontStyle: FontStyle.italic,color: mynew_dred)))),
//                             Expanded(flex: 2, child: Center(child: Text("Total",style: TextStyle(fontStyle: FontStyle.italic,color: mynew_dred)))),
//                           ],
//                         ),
//                       ),
//                       Expanded(
//                         child: Consumer<OrderProvider>(
//                           builder: (context, value,child){
//                             return
//                               ListView.builder(
//                                   itemCount: value.itemGS.length,
//                                   scrollDirection: Axis.vertical,
//                                   shrinkWrap: true,
//                                   itemBuilder: (BuildContext context, int index) {
//                                     return value.itemGS[index].kotNumber==OrderID?InkWell(
//                                       onTap: (){
//                                         if(value.itemGS[index].Status!="OrderPrinted"&&value.itemGS[index].Status!="Cancelled"){
//                                           _orderprovider.fun_Dish_addAlert(context, value.itemGS[index].Name, value.itemGS[index].productId, int.parse(value.itemGS[index].Qty), value.itemGS[index].floor, value.itemGS[index].Notes);
//                                           showDialog(
//                                               context: context,
//                                               builder: (BuildContext context) {
//                                                 return Consumer<OrderProvider>(
//                                                   builder: (context, value, child) {
//                                                     return AlertDialog(
//                                                       shape: const RoundedRectangleBorder(
//                                                           borderRadius: BorderRadius.all(Radius.circular(32.0))),
//                                                       contentPadding: const EdgeInsets.only(top: 10.0),
//                                                       content: Container(
//                                                         width: 300.0,
//                                                         height: 300.0,
//                                                         child: Column(
//                                                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                                           crossAxisAlignment: CrossAxisAlignment.stretch,
//                                                           mainAxisSize: MainAxisSize.min,
//                                                           children: <Widget>[
//                                                             Column(
//                                                               children: [
//                                                                 Text(
//                                                                   value.itemGS[index].Name,
//                                                                   style: Textstyle16,
//                                                                 ),
//                                                                 const SizedBox(
//                                                                   height: 5.0,
//                                                                 ),
//                                                                 // const Divider(
//                                                                 //   color: Colors.grey,
//                                                                 //   height: 4.0,
//                                                                 // ),
//                                                                 const SizedBox(
//                                                                   height: 10.0,
//                                                                 ),
//                                                                 Text(
//                                                                   "Rate : ${value.itemGS[index].Rate}",
//                                                                   style: Textstyle18,
//                                                                   textAlign: TextAlign.start,
//                                                                 ),
//                                                                 const SizedBox(
//                                                                   height: 15.0,
//                                                                 ),
//                                                                 Padding(
//                                                                     padding: EdgeInsets.only(left: 30.0, right: 30.0),
//                                                                     child: Container(
//                                                                       width: 180,
//                                                                       height: 45,
//                                                                       child: Row(
//                                                                         mainAxisAlignment:
//                                                                         MainAxisAlignment.spaceBetween,
//                                                                         children: <Widget>[
//                                                                           IconButton(
//                                                                             icon:  const Icon(
//                                                                               Icons.remove_circle_outline,
//                                                                               color: appbar_color,
//                                                                               size: 45,
//                                                                             ),
//                                                                             onPressed: () {
//                                                                               OrderProvider _orderprovider =
//                                                                               Provider.of<OrderProvider>(context,
//                                                                                   listen: false);
//                                                                               int newCount = value.iItemCount - 1;
//
//                                                                               _orderprovider.fun_Dish_addAlert(
//                                                                                   context,
//                                                                                   value.strItemName,
//                                                                                   value.strItemID.toString(),
//                                                                                   newCount,
//                                                                                   value.itemGS[index].floor,value.itemGS[index].Notes);
//                                                                             },
//                                                                           ),
//                                                                           Container(
//                                                                               decoration: const BoxDecoration(
//                                                                                   color: my_grey,
//                                                                                   shape: BoxShape.circle),
//                                                                               width: 45,
//                                                                               height: 45,
//                                                                               child: Center(
//                                                                                   child:  TextField(
//                                                                                       controller: value.qtyTc,
//                                                                                       textAlign: TextAlign.center,
//                                                                                       keyboardType: TextInputType.number,
//
//                                                                                       style: Textstyle14))),
//                                                                           IconButton(
//                                                                             icon:  const Icon(
//                                                                               Icons.add_circle_outline,
//                                                                               color: appbar_color,
//                                                                               size: 45,
//                                                                             ),
//                                                                             onPressed: () {
//                                                                               OrderProvider _orderprovider =
//                                                                               Provider.of<OrderProvider>(context,
//                                                                                   listen: false);
//                                                                               int newCount = value.iItemCount + 1;
//                                                                               _orderprovider.fun_Dish_addAlert(
//                                                                                   context,
//                                                                                   value.strItemName,
//                                                                                   value.strItemID.toString(),
//                                                                                   newCount,
//                                                                                   value.itemGS[index].floor,value.itemGS[index].Notes);
//
//                                                                             },
//                                                                           )
//                                                                         ],
//                                                                       ),
//                                                                     )),
//                                                                 const SizedBox(
//                                                                   height: 10.0,
//                                                                 ),
//                                                                 Padding(
//                                                                   padding: const EdgeInsets.only(left: 10, right: 10),
//                                                                   child: TextFormField(
//                                                                     decoration: const InputDecoration(
//                                                                       labelText: 'NOTE',
//                                                                       labelStyle: TextStyle(color: my_black),
//                                                                       filled: true,
//                                                                       fillColor: my_white,
//                                                                       enabledBorder: OutlineInputBorder(
//                                                                         borderRadius:
//                                                                         BorderRadius.all(Radius.circular(8.0)),
//                                                                         borderSide:
//                                                                         BorderSide(color: my_black, width: 0.8),
//                                                                       ),
//                                                                       focusedBorder: OutlineInputBorder(
//                                                                         borderRadius:
//                                                                         BorderRadius.all(Radius.circular(8.0)),
//                                                                         borderSide:
//                                                                         BorderSide(color: my_black, width: 1.0),
//                                                                       ),
//                                                                     ),
//                                                                     controller: value.notesTc,
//                                                                     autofocus: false,
//                                                                   ),
//                                                                 ),
//                                                               ],
//                                                             ),
//                                                             InkWell(
//                                                               onTap: () {
//
//                                                                 _orderprovider.fun_changeQty(value.itemGS[index],value.qtyTc.text,value.notesTc.text,context);
//                                                               },
//                                                               child: Container(
//                                                                 padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
//                                                                 decoration: const BoxDecoration(
//                                                                   color: appbar_color,
//                                                                   borderRadius: BorderRadius.only(
//                                                                       bottomLeft: Radius.circular(32.0),
//                                                                       bottomRight: Radius.circular(32.0)),
//                                                                 ),
//                                                                 child: Text(
//                                                                   "SAVE",
//                                                                   style: Textstyle16_white,
//                                                                   textAlign: TextAlign.center,
//                                                                 ),
//                                                               ),
//                                                             ),
//                                                           ],
//                                                         ),
//                                                       ),
//                                                     );
//                                                   },
//                                                 );
//                                               });
//                                         }
//                                         else if(value.itemGS[index].Status!="Cancelled"){
//                                           if(value.CancelationLogin){
//                                             _showDialogCancellationLogin(context,value.itemGS[index]);
//
//                                           }else{
//
//                                             _showDialogCancellation(context,value.itemGS[index]);
//                                           }
//                                         }
//
//                                       },
//                                       onLongPress: (){
//                                         if(value.itemGS[index].Status!="OrderPrinted"){
//                                           mRootReference.child(value.itemGS[index].dbRef).remove();
//
//                                         }
//                                       },
//                                       child: ItemsAdapter(value.itemGS[index],index,context),
//                                     ):Container();
//                                   });
//                           },
//                         ),
//                       ),
//                       // Container(
//                       //   padding: const EdgeInsets.fromLTRB(10,20,10,20),
//                       //   child: Row(
//                       //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       //     children: const [
//                       //       Expanded(flex: 6, child: Text("Total")),
//                       //       Expanded(flex: 2, child: Center(child: Text("0"))),
//                       //     ],
//                       //   ),
//                       // ),
//                     ],
//                   ),
//                 ):Container(
//                   width: queryData.size.width*0.98,
//                   child: Card(
//                       margin: EdgeInsets.all(8),
//                       elevation: 2,
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(8),
//
//                       ),
//                       child:Image.asset("assets/add_calculator.png",scale:2)
//                   ),
//                 ),
//               );
//             }
//         ),
//         Expanded(
//           child: Container(
//             height: queryData.size.height*.9,
//             color: my_grey,
//             child: Column(
//               children: <Widget>[
//                 Expanded(
//                   flex: 6,
//                   child: Padding(
//                     padding:EdgeInsets.fromLTRB(5.0,0,5,0),
//                     child: Card(
//                       elevation: 12,
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(12),
//                       ),
//                       child: Padding(
//                         padding: const EdgeInsets.all(0.0),
//                         child: Column(
//                           mainAxisSize: MainAxisSize.max,
//                           children: [
//
//                             Row(
//                               children: [
//                                 Expanded(
//                                   child: Consumer<OrderProvider>(
//                                       builder: (context,value,child) {
//                                         return InkWell(
//                                           onTap: (){
//                                             _orderprovider.selectedItem();
//                                           },
//                                           child: Container(
//                                               padding: EdgeInsets.symmetric(vertical: 10,horizontal: 5),
//                                               margin: EdgeInsets.symmetric(vertical: 10,horizontal: 5),
//                                               height: 60,
//                                               decoration:  BoxDecoration(
//                                                 borderRadius: const BorderRadius.all(
//                                                     Radius.circular(
//                                                         5.0) //                 <--- border radius here
//                                                 ),
//                                                 border: Border.all(
//                                                   color:  value.isQty?my_grey:mynew_dred, //                   <--- border color
//                                                   width: 1.0,
//                                                 ),
//                                                 color:my_grey,
//                                               ),
//                                               child:  Row(
//                                                 children: [
//                                                   Text(value.cItem.toString()),
//                                                   Flexible(
//                                                     child: Text(value.cItemHint,
//                                                       style: hintText,
//                                                     ),
//                                                   )
//
//
//                                                 ],
//                                               )),
//                                         );
//                                       }
//                                   ),
//                                 ),
//                                 Expanded(
//                                   child: Consumer<OrderProvider>(
//                                       builder: (context,value,child) {
//                                         return InkWell(
//                                           onTap: (){
//                                             _orderprovider.selectedQty();
//
//                                           },
//                                           child: Container(
//                                               padding: EdgeInsets.symmetric(vertical: 10,horizontal: 5),
//                                               margin: EdgeInsets.symmetric(vertical: 10,horizontal: 5),
//                                               height: 60,
//                                               decoration:  BoxDecoration(
//                                                 borderRadius: const BorderRadius.all(
//                                                     Radius.circular(
//                                                         5.0) //                 <--- border radius here
//                                                 ),
//                                                 border: Border.all(
//                                                   color:  value.isQty?mynew_dred:my_grey, //                   <--- border color
//                                                   width: 1.0,
//                                                 ),
//                                                 color: my_grey,
//                                               ),
//                                               child:  Row(
//                                                 children: [
//                                                   Consumer<OrderProvider>(
//                                                       builder: (context,value,child) {
//                                                         return Text(value.cQty,
//                                                         );
//                                                       }
//                                                   ),
//                                                   Consumer<OrderProvider>(
//                                                       builder: (context,value,child) {
//                                                         return Text(value.cQtyHint,
//                                                           style: hintText,
//                                                         );
//                                                       }
//                                                   ),
//                                                 ],
//                                               )),
//                                         );
//                                       }
//                                   ),
//                                 ),
//                               ],
//                             ),
//                             Consumer<OrderProvider>(
//                                 builder: (context,value,child) {
//                                   return Text(value.cErrorText.toString(),style: errorText,);
//                                 }
//                             ),
//
//                             Expanded(
//                               child: Row(
//                                 children: [
//                                   calculatorButtons(
//                                       context, num[1].toString()),
//                                   calculatorButtons(
//                                       context, num[2].toString()),
//                                   calculatorButtons(
//                                       context, num[3].toString()),
//                                 ],
//                               ),
//                             ),
//                             Padding(
//                               padding: const EdgeInsets.fromLTRB(12,0,12,0),
//                               child: Divider(
//                                 color: Colors.grey,
//                                 height: 4.0,
//                               ),
//                             ),
//                             Expanded(
//                               child: Row(
//                                 children: [
//                                   calculatorButtons(
//                                       context, num[4].toString()),
//                                   calculatorButtons(
//                                       context, num[5].toString()),
//                                   calculatorButtons(
//                                       context, num[6].toString()),
//                                 ],
//                               ),
//                             ),
//                             Padding(
//                               padding: const EdgeInsets.fromLTRB(12,0,12,0),
//                               child: Divider(
//                                 color: Colors.grey,
//                                 height: 4.0,
//                               ),
//                             ),
//                             Expanded(
//                               child: Row(
//                                 children: [
//                                   calculatorButtons(
//                                       context, num[7].toString()),
//                                   calculatorButtons(
//                                       context, num[8].toString()),
//                                   calculatorButtons(
//                                       context, num[9].toString()),
//                                 ],
//                               ),
//                             ),
//                             Padding(
//                               padding: const EdgeInsets.fromLTRB(12,0,12,0),
//                               child: Divider(
//                                 color: Colors.grey,
//                                 height: 4.0,
//                               ),
//                             ),
//                             Expanded(
//                               child: Row(
//                                 children: [
//                                   Expanded(
//                                     child: InkWell(
//                                       onTap:(){
//                                         _orderprovider.onDel();
//                                       },
//                                       onLongPress: (){
//                                         _orderprovider.onClear();
//
//                                       },
//                                       child: Container(
//                                         decoration: BoxDecoration(
//                                             border: Border.all(
//                                                 color: my_grey, width: 0),
//                                             color: mynew_yellow,
//                                             borderRadius: BorderRadius.only(bottomLeft: Radius.circular(12))
//                                         ),
//                                         child: const Center(
//                                           child:  Icon(
//                                             Icons.backspace_outlined,
//                                             color: my_black,
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                   calculatorButtons(context, num[0].toString()),
//                                   Expanded(
//                                     child: Consumer<OrderProvider>(
//                                         builder: (context,value,child) {
//                                           return InkWell(
//                                             onTap: (){
//                                               print("object");
//                                               _orderprovider.onEnter();
//                                               if(value.cQty!=""&&value.cItem!=""&&value.dishesID_list.contains(value.cItem)&&int.parse(value.cQty)>0){
//                                                 print("object2");
//
//                                                 String DayNode = '';
//                                                 if(_orderprovider.orderDayNode!=''){
//                                                   DayNode = _orderprovider.orderDayNode;
//
//                                                 }else{
//                                                   DateTime now = new DateTime.now();
//                                                   DayNode = now.year.toString() +
//                                                       "/" +
//                                                       now.month.toString() +
//                                                       "/" +
//                                                       now.day.toString();
//                                                 }
//
//                                                 mRootReference
//                                                     .child("Dish")
//                                                     .child(value.cItem)
//                                                     .once()
//                                                     .then((DataSnapshot snapshot) {
//                                                   Map<dynamic, dynamic> ItemData = snapshot.value;
//                                                   if(double.parse(ItemData["F"+floor].toString())>0){
//                                                     fun_AddItem(value.cItem, DayNode,value.cItemHint, int.tryParse(value.cQty)!, ItemData["F"+floor], ItemData['Category'], context,"");
//                                                     _orderprovider.onClear();
//
//                                                   }else{
//                                                     _showDialogManualRate(value.cItem, DayNode,value.cItemHint, int.tryParse(value.cQty)!, ItemData['Category'],context);
//
//                                                   }
//
//                                                 });
//
//                                               }
//
//                                             },
//                                             child: Container(
//                                               decoration: BoxDecoration(
//                                                   border: Border.all(
//                                                       color: my_grey, width: 0),
//                                                   color: mynew_yellow,
//                                                   borderRadius: BorderRadius.only(bottomRight:Radius.circular(12))
//                                               ),
//                                               child: const Center(
//                                                 child: Icon(
//                                                   Icons.arrow_forward,
//                                                   color: my_black,
//                                                 ),
//                                               ),
//                                             ),
//                                           );
//                                         }
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//                 // const Expanded(
//                 //   child: SizedBox(),
//                 // ),
//                 // Container(
//                 //   width: double.maxFinite,
//                 //   padding: EdgeInsets.all(5),
//                 //   child: ElevatedButton.icon(
//                 //     onPressed: () {
//                 //       _orderprovider.printerKitchens(floor,tableID,SubTable,OrderID);
//                 //
//                 //       funShowPrinterDialog(context, OrderID, floor, SubTable, tableID);
//                 //     },
//                 //     label: Text(
//                 //       'KOT',
//                 //       style: btnTextwhite,
//                 //     ),
//                 //     icon: Icon(Icons.print),
//                 //     style: ElevatedButton.styleFrom(
//                 //       primary: mynew_dred,
//                 //       textStyle: TextStyle(
//                 //         color: Colors.black,
//                 //         fontSize: 22,
//                 //       ),
//                 //     ),
//                 //   ),
//                 // ),
//                 // const Expanded(
//                 //   child: SizedBox(),
//                 // ),
//               ],
//             ),
//           ),
//         ),      ],
//     );
//   }
//
//   ///Uploading Dish
//   void fun_AddItem(String strItemID, String dayNode, String strItemName,
//       int iItemCount, String strItemRate, String strItemCategory,BuildContext context,String note) async {
//     OrderProvider _orderprovider = Provider.of<OrderProvider>(context, listen: false);
//     _orderprovider.funAddItemClick();
//     mRootReference
//         .child("Dish")
//         .child(strItemID)
//         .once()
//         .then((DataSnapshot snapshot) async {
//       Map<dynamic, dynamic> ItemData = snapshot.value;
//
//       String strOrderID = OrderID.toString();
//       String strTable = tableID.toString();
//
//       mRootReference
//           .child("Tables")
//           .child(floor)
//           .child(tableID)
//           .child("Order")
//           .child(OrderID)
//           .set(SubTable.replaceAll('S', ''));
//       mRootReference
//           .child("Tables")
//           .child(floor)
//           .child(tableID)
//           .child("SubTableOrder")
//           .child(SubTable.replaceAll('S', ''))
//           .child(OrderID)
//           .set(OrderID);
//
//       HashMap<String, Object> Data = new HashMap();
//       Data['ItemID'] = strItemID;
//       Data['OrderID'] = strOrderID;
//       Data['StatusBill'] = 'NotBilled';
//       Data['Status'] = 'OrderTaken';
//
//       Data['Note'] =note;
//       Data['Item'] = strItemName;
//       Data['Kitchen'] = ItemData['Kitchen'].toString();
//       Data['Rate'] = strItemRate.toString();
//       Data['Quantity'] = iItemCount.toString();
//       Data['TableNumber'] = strTable;
//       Data['OrderedBy'] = _orderprovider.stewardID!;
//       Data['Floor'] = floor;
//       Data['Category'] = strItemCategory;
//       Data['ReGenerated'] = 'NO';
//       Data['Parent'] = ItemData['Parent'].toString();
//       Data['Unit'] = ItemData['Unit'].toString();
//       Data['Depend'] = ItemData['Depend'].toString();
//       Data['DependUses'] = ItemData['DependUses'].toString();
//       String strTime = DateTime.now().millisecondsSinceEpoch.toString();
//       Data['Time'] = strTime;
//       if (ItemData['Taxablity'].toString() == 'true') {
//         Data['Taxablity'] = 'true';
//       } else {
//         Data['Taxablity'] = 'false';
//       }
//       Data['NumberOfPerson'] = _orderprovider.stewardID!;
//       Data['SubTable'] = SubTable;
//       final String AutoGenID = mRootReference.push().key;
//       String Token = mRootReference.push().key;
//       mRootReference
//           .child("DishBucket")
//           .child(ItemData['Parent'].toString())
//           .child(Token)
//           .set(double.tryParse(iItemCount.toString())! *
//           double.tryParse(ItemData['Unit'].toString())!);
//
//         HashMap<String, Object> TableData = new HashMap();
//
//         double? dTotal = 0.0;
//         dTotal = double.tryParse(iItemCount.toString())! *
//             double.tryParse(strItemRate.toString())!;
//
//
//         TableData['OrderID'] = strOrderID;
//         String dRef = 'Orders/$dayNode/${floor}/${strTable}/${SubTable.replaceAll("S", "")}/${strOrderID}';
//         TableData['OrderRef'] = dRef;
//
//
//
//       mRootReference
//           .child("Orders").child(dayNode).child(floor)
//           .child(strTable)
//           .child(SubTable.replaceAll("S", ""))
//           .child(strOrderID)
//           .child(AutoGenID)
//           .set(Data);
//       mRootReference.child("TableOrders").child("Floors").child(floor).child("Tables").child(strTable).child("SubTables").child(SubTable).child('Orders').child(strOrderID).update(TableData);
//       notesTC.clear();
//       manualRateTC.clear();
//         SharedPreferences userPreference = await SharedPreferences.getInstance();
//
//         userPreference.setString('Floor', floor);
//         userPreference.setString('Table', strTable);
//         userPreference.setString('SubTable', SubTable);
//         _orderprovider.fun_orderItems(floor, strTable, SubTable);
//
//
//
//
//       // this is where success gets sent
//
//
//     });
//
//
//   }
//
//   ///Alert Dialog for Manual Rate When Price is 0
//   _showDialogManualRate(String strItemID, String dayNode, String strItemName,
//       int iItemCount, String strItemCategory,BuildContext context) {
//     OrderProvider _orderprovider =
//     Provider.of<OrderProvider>(context, listen: false);
//
//     // flutter defined function
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         // return alert dialog object
//         return Consumer<OrderProvider>(
//           builder: (context, value, child) {
//             return AlertDialog(
//               title: Container(
//                   child: Center(
//                       child: Text(
//                         'Item Details',
//                         style: TextStyle(color: my_white),
//                       ))),
//               backgroundColor: mynew_dred,
//               contentPadding: EdgeInsets.only(
//                 top: 15.0,
//               ),
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.all(Radius.circular(10.0))),
//               content: SingleChildScrollView(
//                 child: Container(
//                   width: double.maxFinite,
//                   padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
//                   decoration: BoxDecoration(
//                       color: my_white,
//                       borderRadius:
//                       BorderRadius.vertical(bottom: Radius.circular(10))),
//                   child: Form(
//                     key: _formKeyManualQty,
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.symmetric(vertical: 10),
//                           child: TextFormField(
//                             controller: manualRateTC,
//                             keyboardType: TextInputType.number,
//                             decoration: const InputDecoration(
//                               labelText: 'MANUEL RATE',
//                               labelStyle: TextStyle(color: my_black),
//                               filled: true,
//                               fillColor: my_white,
//                               enabledBorder: OutlineInputBorder(
//                                 borderRadius:
//                                 BorderRadius.all(Radius.circular(8.0)),
//                                 borderSide:
//                                 BorderSide(color: my_black,),
//                               ),
//                               focusedBorder: OutlineInputBorder(
//                                 borderRadius:
//                                 BorderRadius.all(Radius.circular(8.0)),
//                                 borderSide:
//                                 BorderSide(color: mynew_dred,),
//                               ),
//                             ),                            validator: (text) =>
//                             text!.isEmpty ? 'Rate cannot be blank' : null,
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.symmetric(vertical: 10),
//                           child:SimpleAutoCompleteTextField(
//
//                             key: keyDishManuel,
//                             suggestions: value.notes,
//                             clearOnSubmit: false,
//                             textSubmitted: (text){
//                             },
//                             decoration: const InputDecoration(
//                               labelText: 'NOTE',
//                               labelStyle: TextStyle(color: my_black),
//                               filled: true,
//                               fillColor: my_white,
//                               enabledBorder: OutlineInputBorder(
//                                 borderRadius:
//                                 BorderRadius.all(Radius.circular(8.0)),
//                                 borderSide:
//                                 BorderSide(color: my_black,),
//                               ),
//                               focusedBorder: OutlineInputBorder(
//                                 borderRadius:
//                                 BorderRadius.all(Radius.circular(8.0)),
//                                 borderSide:
//                                 BorderSide(color: mynew_dred,),
//                               ),
//                             ),
//
//                             controller: notesTC),
//
//                 ),
//                         InkWell(
//                           onTap: () {
//                             _orderprovider.funPrintClear();
//
//                             final FormState? form = _formKeyManualQty.currentState;
//                             if (form!.validate()) {
//                               String DayNode ='';
//                               if(_orderprovider.orderDayNode!=''){
//                                 DayNode = _orderprovider.orderDayNode;
//
//                               }else{
//                                 DateTime now = new DateTime.now();
//                                 DayNode = now.year.toString() +
//                                     "/" +
//                                     now.month.toString() +
//                                     "/" +
//                                     now.day.toString();
//                               }
//                                 fun_AddItem(strItemID, dayNode, strItemName, iItemCount, manualRateTC.text, strItemCategory, context,notesTC.text);
//                               finish(context);
//                               _orderprovider.onClear();
//
//
//
//                             }
//
//                           },
//                           child: Container(
//                               height: 50,
//                               width: 150,
//                               decoration: const BoxDecoration(
//                                 color: mynew_yellow,
//                                 borderRadius: BorderRadius.all(Radius.circular(
//                                     10.0) //                 <--- border radius here
//                                 ),
//                               ),
//                               child: const Center(
//                                 child: Text(
//                                   "Save",
//                                   style: TextStyle(color: Colors.black),
//                                 ),
//                               )),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             );
//           },
//         );
//       },
//     );
//   }
//
//   ///Alert Dialog for Listing Printers When Press Kot Button
//   void funShowPrinterDialog(BuildContext context, String orderID, String floor,
//       String subTable, String tableID) {
//     OrderProvider _orderprovider = Provider.of<OrderProvider>(context, listen: false);
//
//     Widget setupAlertDialoadContainer() {
//       return Container(
//           height: 300,
//           width: 300,
//           decoration: BoxDecoration(
//             color: my_white,),
//           // Change as per your requirement
//           child: SingleChildScrollView(
//             child: Consumer<OrderProvider>(
//               builder: (context, value, child) {
//                 return Column(
//                   crossAxisAlignment: CrossAxisAlignment.stretch,
//                   children: <Widget>[
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(left: 10.0, right: 10.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Text(
//                             value.FloorName,
//                             style: Textstyle16,
//                           ),
//                           Text(
//                             value.FloorIP,
//                             style:TextStyle(
//                                 fontFamily: fontBold,
//                                 fontSize: textSize12,
//                                 color: mynew_dred,
//                                 fontStyle: FontStyle.italic),
//                           ),
//                         ],
//                       ),
//                     ),
//                     SwitchListTile(
//                         value: value.isFloorConsolidated,
//                         title: const Text("Floor Consolidated"),
//
//                         activeColor: mynew_yellow,
//                         onChanged: (value) {
//                             print('value   $value');
//                             String status = 'Enable';
//                             if (value == true) {
//                               status = 'Enable';
//                             } else {
//                               status = 'Disable';
//                             }
//                             // mRootReference
//                             //     .child('config')
//                             //     .child('Floors')
//                             //     .child(floor)
//                             //     .child('FloorConsolidated')
//                             //     .set(status);
//                             _orderprovider.floorConsolidated(value);
//
//                           // Provider.of<OrderProvider>(context, listen: false).isFloorConsolidated==value;
//                         }),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(left: 10.0, right: 10.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Text(
//                             'NIKALA',
//                             style: Textstyle16,
//                           ),
//                           Text(
//                             value.NikalaIP,
//                             style: TextStyle(
//                                 fontFamily: fontBold,
//                                 fontSize: textSize12,
//                                 color: mynew_dred,
//                                 fontStyle: FontStyle.italic),
//                           ),
//                         ],
//                       ),
//                     ),
//                     SwitchListTile(
//                         value: value.isNikalaConsolidated,
//                         title: const Text("Nikala Consolidated"),
//                         activeColor: mynew_yellow,
//                         onChanged: (value) {
//                             print('value   $value');
//                             String status = 'Enable';
//                             if (value == true) {
//                               status = 'Enable';
//                             } else {
//                               status = 'Disable';
//                             }
//                             // mRootReference
//                             //     .child('config')
//                             //     .child('Floors')
//                             //     .child(floor)
//                             //     .child('NikalaConsolidated')
//                             //     .set(status);
//                             _orderprovider.floorNIkConsolidated(value);
//
//                           // Provider.of<OrderProvider>(context, listen: false).isFloorConsolidated==value;
//                         }),
//                     SwitchListTile(
//                         value: value.isNikalaSeparated,
//                         title: const Text("Nikala Separated"),
//                         activeColor: mynew_yellow,
//                         onChanged: (value) {
//                             print('value   $value');
//                             String status = 'Enable';
//                             if (value == true) {
//                               status = 'Enable';
//                             } else {
//                               status = 'Disable';
//                             }
//                             // mRootReference
//                             //     .child('config')
//                             //     .child('Floors')
//                             //     .child(floor)
//                             //     .child('NikalaSeparated')
//                             //     .set(status);
//                             // Provider.of<OrderProvider>(context, listen: false).isFloorConsolidated==value;
//                             _orderprovider.floorNIkSeprated(value);
//
//                         }),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Padding(
//                       padding: const EdgeInsets.only(left: 10.0, right: 10.0),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Text(
//                             'KITCHEN PRINTS',
//                             style: Textstyle16,
//                           ),
//                           SizedBox(
//                             width: 10,
//                           ),
//                         ],
//                       ),
//                     ),
//                     SizedBox(
//                       height: 10,
//                     ),
//                     Container(
//                       child: Consumer<OrderProvider>(
//                         builder: (context, value, child) {
//                           return ListView.builder(
//                             physics: NeverScrollableScrollPhysics(),
//                             shrinkWrap: true,
//                             itemCount: value.printerGS.length,
//                             itemBuilder: (BuildContext context, int index) {
//                               return Container(
//                                 child: Row(
//                                   mainAxisAlignment:
//                                   MainAxisAlignment.spaceBetween,
//                                   children: <Widget>[
//                                     Expanded(
//                                       child: Container(
//                                         margin:EdgeInsets.only(left:15),
//                                         child: Text(
//                                           value.printerGS[index].Name,
//                                           style: Textstyle14,
//                                         ),
//                                       ),
//                                     ),
//                                     Expanded(
//                                       child: Container(
//                                         child: SwitchListTile(
//
//                                             value: value
//                                                 .printerGS[index].Consolidated,
//                                             title: Text(
//                                               "CS",
//                                               style: Textstyle13,
//                                             ),
//                                             activeColor: mynew_yellow,
//                                             onChanged: (changedvalue) {
//                                                 // isBarcode=
//                                                 // print('value   $changedvalue');
//                                                 String status = 'Enable';
//                                                 if (changedvalue == true) {
//                                                   status = 'Enable';
//                                                 } else {
//                                                   status = 'Disable';
//                                                 }
//                                                 // mRootReference
//                                                 //     .child('config')
//                                                 //     .child('Kitchens')
//                                                 //     .child(value.printerGS[index].ID.toString())
//                                                 //     .child('KitchenConsolidated')
//                                                 //     .set(status);
//                                                 // print(value);
//                                                 _orderprovider.kichenConsolidatedP("CS",changedvalue,index);
//
//                                             }),
//                                       ),
//                                     ),
//                                     Expanded(
//                                       child: Container(
//                                         child: SwitchListTile(
//                                             value:
//                                             value.printerGS[index].Seprated,
//                                             title: Text(
//                                               "SP",
//                                               style: Textstyle13,
//                                             ),
//                                             activeColor: mynew_yellow,
//                                             onChanged: (changedvalue) {
//                                                 // isBarcode=value;
//                                                 print('value   $changedvalue');
//                                                 String status = 'Enable';
//                                                 if (changedvalue == true) {
//                                                   status = 'Enable';
//                                                 } else {
//                                                   status = 'Disable';
//                                                 }
//                                                 // mRootReference
//                                                 //     .child('config')
//                                                 //     .child('Kitchens')
//                                                 //     .child(value.printerGS[index].ID.toString())
//                                                 //     .child('KitchenSeparated')
//                                                 //     .set(status);
//                                                 _orderprovider.kichenConsolidatedP("SP",changedvalue,index);
//
//
//                                             }),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               );
//                             },
//                           );
//                         },
//                       ),
//                     ),
//                   ],
//                 );
//               },
//             ),
//           ));
//     }
//
//
//     showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             backgroundColor:mynew_dred,
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(Radius.circular(12.0))),
//             contentPadding: EdgeInsets.only(
//               top: 10.0,
//             ),
//             title: Container(child: Text('Printers',style:TextStyle(color: my_white))),
//             content: setupAlertDialoadContainer(),
//             actions: [
//               Container(
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: <Widget>[
//                     Container(
//                         decoration: BoxDecoration(
//                             borderRadius: BorderRadius.all(Radius.circular(20))),
//                         height: 40,
//                         width: 120,
//                         margin: EdgeInsets.all(5),
//                         child: MaterialButton(
//                           shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.all(Radius.circular(12))
//                           ),
//                           child: Text('Skip', style: TextStyle(fontSize: 20.0)),
//                           color: my_white,
//                           textColor: mynew_dred,
//                           onPressed: () {
//
//                             String DayNode = '';
//                             if(_orderprovider.orderDayNode!=''){
//                               DayNode = _orderprovider.orderDayNode;
//
//                             }else{
//                               DateTime now = new DateTime.now();
//                               DayNode = now.year.toString() +
//                                   "/" +
//                                   now.month.toString() +
//                                   "/" +
//                                   now.day.toString();
//                             }
//
//                             String dRef = 'Orders/$DayNode/${floor}/${tableID}/${SubTable}/${orderID}';
//
//                             _orderprovider.fun_UpdateprintData(dRef,floor,orderID,tableID,SubTable);
//                             finish(context);
//                             finish(context);
//                           },
//                         )),
//                     Container(
//                         decoration: BoxDecoration(
//                             borderRadius: BorderRadius.all(Radius.circular(20))),
//                         height: 40,
//                         width: 120,
//                         margin: EdgeInsets.all(5),
//                         child: Consumer<OrderProvider>(
//                           builder: (context, value, hild) {
//                             return FlatButton(
//                               shape: RoundedRectangleBorder(
//                                   borderRadius: BorderRadius.all(Radius.circular(12))
//                               ),
//                               child: Text(
//                                 'Print',
//                                 style: TextStyle(fontSize: 20.0,),
//                               ),
//                               color: my_white,
//                               textColor: mynew_dred,
//                               onPressed: () async {
//                                 _orderprovider.funPrintClear();
//
//                                 if(value.itemGS.isNotEmpty){
//
//                                   String DayNode = '';
//                                   if(_orderprovider.orderDayNode!=''){
//                                     DayNode = _orderprovider.orderDayNode;
//
//                                   }else{
//                                     DateTime now = new DateTime.now();
//                                     DayNode = now.year.toString() +
//                                         "/" +
//                                         now.month.toString() +
//                                         "/" +
//                                         now.day.toString();
//                                   }
//                                   String dRef = 'Orders/$DayNode/${floor}/${tableID}/${SubTable}/${orderID}';
//                                   Navigator.pop(context);
//                                   alertPrinterStatus(context);
//
//                                   if (value.isFloorConsolidated == true) {
//
//                                     await _orderprovider.fun_consolidatedprint(
//                                         value.FloorName,
//                                         value.FloorIP,
//                                         orderID,
//                                         subTable,
//                                         tableID,
//                                         context,
//                                         "FloorC"
//                                         ,dRef,floor,OrderType,1000,value.itemGS
//                                     );
//                                   }
//
//                                   if (value.isNikalaConsolidated == true) {
//                                     await _orderprovider.fun_floornikalaprint(
//                                         value.FloorName,
//                                         value.NikalaIP,
//                                         orderID,
//                                         subTable,
//                                         tableID,
//                                         context,
//                                         "NikalaC",dRef,floor,OrderType,1000,value.itemGS);
//                                   }
//
//                                   // if (value.isNikalaSeparated == true) {
//                                   //   await _orderprovider.fun_nikalaSeprated(
//                                   //       value.FloorName,
//                                   //       value.NikalaIP,
//                                   //       orderID,
//                                   //       subTable,
//                                   //       tableID,
//                                   //       context,
//                                   //       "NikalaS",dRef,floor,OrderType,1000,"ALL",value.itemGS);
//                                   // }
//                                   //
//                                   // if (value.printerGS.length > 0) {
//                                   //   await _orderprovider.fun_kichenPrint(
//                                   //       value.FloorName,
//                                   //       value.NikalaIP,
//                                   //       orderID,
//                                   //       subTable,
//                                   //       tableID,
//                                   //       context,
//                                   //       "Kitchen",dRef,floor,OrderType,1000,"ALL",value.itemGS);
//                                   // }
//                                   _orderprovider.fun_UpdateprintData(dRef,floor,orderID,tableID,SubTable);
//
//
//                                 }else{
//
//                                   MainProvider _mainProvider= Provider.of<MainProvider>(context, listen: false);
//                                   _mainProvider.fun_Snackbar('Items not added',context);
//
//                                 }
//
//
//
//
//                               },
//                             );
//                           },
//                         )),
//                   ],
//                 ),
//               ),
//             ],
//           );
//         });
//   }
//
//   ///Alert Dialog for Status of printers  kot
//   alertPrinterStatus(BuildContext context) {
//     OrderProvider _orderprovider =
//     Provider.of<OrderProvider>(context, listen: false);
//     MediaQueryData queryData;
//     queryData = MediaQuery.of(context);
//
//     showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//
//             backgroundColor: mynew_dred,
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(Radius.circular(15.0))),
//             contentPadding: EdgeInsets.only(
//               top: 10.0,
//             ),
//             title: Text('Printers'),
//             content: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 Container(
//                   padding: EdgeInsets.all(10),
//                   height: 300,
//                   width: 300,
//                   decoration: BoxDecoration(
//                       color: my_white,
//                       borderRadius:
//                       BorderRadius.vertical(bottom: Radius.circular(10))),// Change as per your requirement
//                   child: Consumer<OrderProvider>(
//                       builder: (context,value,child) {
//                         return value.printerReGS.isEmpty?
//                         Center(
//                             child: CircularProgressIndicator()):
//                         ListView.builder(
//                           shrinkWrap: true,
//                           itemCount: value.printerReGS.length,
//                           itemBuilder: (BuildContext context, int index) {
//                             return Padding(
//                               padding: const EdgeInsets.symmetric(vertical: 10),
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                 children: [
//                                   value.printerReGS[index].status=="success"?Icon(Icons.check,color: my_green):Icon(Icons.clear,color: my_red),
//                                   Flexible(child: Text(value.printerReGS[index].name)),
//                                   InkWell(
//                                     onTap: (){
//                                       if(value.printerReGS[index].status!="success"){
//                                         // _orderprovider.printerRetryFunction(value.printerReGS[index],index,value.itemGS);
//
//                                       }
//                                     },
//                                     child: Container(
//                                         padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
//                                         decoration: BoxDecoration(
//                                             color: value.printerReGS[index].status=="success"?my_green:my_red,
//                                             borderRadius: BorderRadius.circular(10)
//                                         ),
//                                         child: Text(value.printerReGS[index].status=="success"?"Completed":"Retry")),
//                                   ),
//
//
//                                 ],
//                               ),
//                             );
//                           },
//                         );
//                       }
//                   ),
//                 ),
//                 InkWell(
//                   onTap: (){
//                     finish(context);
//                     finish(context);
//                   },
//                   child: Container(
//                     height: 50,
//                     width:300,
//                     child: Center(child: Consumer<OrderProvider>(
//                         builder: (context,value,child) {
//                           return Text(value.printSuccess?"OK":"Cancel" ,style: btnTextwhite,);
//                         }
//                     ),),),
//                 )
//               ],
//             ),
//           );
//         });
//   }
//
//   ///Alert Dialog for Cancellation Login Enabled
//   _showDialogCancellationLogin(context,ItemGS itemGS) {
//     OrderProvider _orderprovider =
//     Provider.of<OrderProvider>(context, listen: false);
//
//     // flutter defined function
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         // return alert dialog object
//         return Consumer<OrderProvider>(
//           builder: (context, value, child) {
//             return AlertDialog(
//               title: Container(
//                   child: Center(
//                       child: Text(
//                         'Cancellation Details',
//                         style: TextStyle(color: my_white),
//                       ))),
//               backgroundColor: mynew_dred,
//               contentPadding: EdgeInsets.only(
//                 top: 15.0,
//               ),
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.all(Radius.circular(10.0))),
//               content: SingleChildScrollView(
//                 child: Container(
//                   width: double.maxFinite,
//                   padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
//                   decoration: BoxDecoration(
//                       color: my_white,
//                       borderRadius:
//                       BorderRadius.vertical(bottom: Radius.circular(10))),
//                   child: Form(
//                     key: _formKey,
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.symmetric(vertical: 10),
//                           child: TextFormField(
//                             controller: reasonTC,
//                             decoration: InputDecoration(hintText: "Enter The Reason of Delete"),
//                             validator: (text) =>
//                             text!.isEmpty ? 'Reason cannot be blank' : null,
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.symmetric(vertical: 10),
//                           child: TextFormField(
//                             controller: employIdTC,
//                             keyboardType: TextInputType.number,
//                             decoration: InputDecoration(hintText: "Employ Id"),
//                             validator: (text) => text!.isEmpty ? 'Employ Id cannot be blank' :!value.staffs.contains(text)?"Wrong Employ ID" :null,
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.symmetric(vertical: 10),
//                           child: TextFormField(
//                             controller: passwordTC,
//                             keyboardType: TextInputType.number,
//                             decoration: InputDecoration(hintText: "Password"),
//                             validator: (text) =>
//                             text!.isEmpty ? 'Password cannot be blank':employIdTC.text!=""&&value.staffsPasses.elementAt(value.stewardsIds.indexOf(employIdTC.text)).toString()!=text? "Wrong Employ ID or Password": null,
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.symmetric(vertical: 10),
//                           child: TextFormField(
//                             controller: qtyTC,
//                             keyboardType: TextInputType.number,
//                             decoration: InputDecoration(hintText: "Maximum quantity you can cancel is "+itemGS.Qty),
//                             validator: (text) =>
//                             text!.isEmpty ? 'Qty cannot be blank' :int.parse(text)>int.parse(itemGS.Qty)?"You cant delete items more than exist": null,
//                           ),
//                         ),
//                         InkWell(
//                           onTap: () {
//
//                             final FormState? form = _formKey.currentState;
//                             if (form!.validate()) {
//                               _orderprovider.funPrintClear();
//
//                               _orderprovider.fun_cancelconsolidatedprint(
//                                   value.FloorName,
//                                   value.FloorIP,
//                                   context,
//                                   "FloorC"
//                                   ,"","CANCELLED"
//                                   ,qtyTC.text,reasonTC.text,employIdTC.text,itemGS,1000);
//                               _orderprovider.fun_cancelnikalaSeprated(
//                                   value.FloorName,
//                                   value.NikalaIP,
//                                   context,
//                                   "NikalaS","","CANCELLED",qtyTC.text,reasonTC.text,employIdTC.text,itemGS,1000);
//                               _orderprovider.validateAndSave(itemGS,qtyTC.text,employIdTC.text,reasonTC.text);
//
//                               finish(context);
//                               alertDialoagCancelStatus(itemGS,qtyTC.text,reasonTC.text,employIdTC.text,context);
//
//                             }
//
//                           },
//                           child: Container(
//                               height: 50,
//                               width: 150,
//                               decoration: const BoxDecoration(
//                                 color: mynew_yellow,
//                                 borderRadius: BorderRadius.all(Radius.circular(
//                                     10.0) //                 <--- border radius here
//                                 ),
//                               ),
//                               child: const Center(
//                                 child: Text(
//                                   "Save",
//                                   style: TextStyle(color: Colors.black),
//                                 ),
//                               )),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             );
//           },
//         );
//       },
//     );
//   }
//
//   ///Alert Dialog for Cancellation Login Disabled
//   _showDialogCancellation(context,ItemGS itemGS) {
//     OrderProvider _orderprovider =
//     Provider.of<OrderProvider>(context, listen: false);
//
//     // flutter defined function
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         // return alert dialog object
//         return Consumer<OrderProvider>(
//           builder: (context, value, child) {
//             return AlertDialog(
//               title: Container(
//                   child: Center(
//                       child: Text(
//                         'Cancellation Details',
//                         style: TextStyle(color: my_white),
//                       ))),
//               backgroundColor: mynew_dred,
//               contentPadding: EdgeInsets.only(
//                 top: 15.0,
//               ),
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.all(Radius.circular(10.0))),
//               content: SingleChildScrollView(
//                 child: Container(
//                   width: double.maxFinite,
//                   padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
//                   decoration: BoxDecoration(
//                       color: my_white,
//                       borderRadius:
//                       BorderRadius.vertical(bottom: Radius.circular(10))),
//                   child: Form(
//                     key: _formKey,
//                     child: Column(
//                       mainAxisSize: MainAxisSize.min,
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.symmetric(vertical: 10),
//                           child: TextFormField(
//                             controller: qtyTC,
//                             keyboardType: TextInputType.number,
//                             decoration: InputDecoration(hintText: "Maximum quantity you can cancel is "+itemGS.Qty),
//                             validator: (text) =>
//                             text!.isEmpty ? 'Qty cannot be blank' :int.parse(text)>int.parse(itemGS.Qty)?"You cant delete items more than exist": null,
//                           ),
//                         ),
//                         InkWell(
//                           onTap: () {
//
//                             final FormState? form = _formKey.currentState;
//                             if (form!.validate()) {
//                               _orderprovider.funPrintClear();
//
//                               _orderprovider.fun_cancelconsolidatedprint(
//                                   value.FloorName,
//                                   value.FloorIP,
//                                   context,
//                                   "FloorC"
//                                   ,"","CANCELLED"
//                                   ,qtyTC.text,"",Uid,itemGS,1000);
//                               _orderprovider.fun_cancelnikalaSeprated(
//                                   value.FloorName,
//                                   value.NikalaIP,
//                                   context,
//                                   "NikalaS","","CANCELLED",qtyTC.text,"",employIdTC.text,itemGS,1000);
//                               _orderprovider.validateAndSave(itemGS,qtyTC.text,employIdTC.text,reasonTC.text);
//
//                               finish(context);
//                               alertDialoagCancelStatus(itemGS,qtyTC.text,"",Uid,context);
//
//                             }
//
//                           },
//                           child: Container(
//                               height: 50,
//                               width: 150,
//                               decoration: const BoxDecoration(
//                                 color: mynew_yellow,
//                                 borderRadius: BorderRadius.all(Radius.circular(
//                                     10.0) //                 <--- border radius here
//                                 ),
//                               ),
//                               child: const Center(
//                                 child: Text(
//                                   "Save",
//                                   style: TextStyle(color: Colors.black),
//                                 ),
//                               )),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             );
//           },
//         );
//       },
//     );
//   }
//
//   ///Alert Dialog for Cancel Print Status
//   alertDialoagCancelStatus(ItemGS itemGS,String qty,String reason,String emloyID,BuildContext context) {
//
//     showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             backgroundColor: appbar_color,
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(Radius.circular(15.0))),
//             contentPadding: EdgeInsets.only(
//               top: 10.0,
//             ),
//             title: Text('Printers'),
//             content: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 Container(
//                   padding: EdgeInsets.all(10),
//                   height: 300.0, // Change as per your requirement
//                   width: 300.0,
//                   decoration: BoxDecoration(
//                       color: my_white,
//                       borderRadius:
//                       BorderRadius.vertical(bottom: Radius.circular(10))),// Change as per your requirement
//                   child: Consumer<OrderProvider>(
//                       builder: (context,value,child) {
//                         return value.cancelPrinterGS.isEmpty?
//                         Center(
//                             child: CircularProgressIndicator()):
//                         ListView.builder(
//                           shrinkWrap: true,
//                           itemCount: value.cancelPrinterGS.length,
//                           itemBuilder: (BuildContext context, int index) {
//                             return Padding(
//                               padding: const EdgeInsets.symmetric(vertical: 10),
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                 children: [
//                                   value.cancelPrinterGS[index].status=="success"?Icon(Icons.check,color: my_green):Icon(Icons.clear,color: my_red),
//                                   Flexible(child: Text(value.cancelPrinterGS[index].name)),
//                                   InkWell(
//                                     onTap: (){
//                                       OrderProvider _orderprovider =
//                                       Provider.of<OrderProvider>(context, listen: false);
//                                       _orderprovider.cancelPrinterRetryFunction(value.cancelPrinterGS[index], index);
//                                     },
//                                     child: Container(
//                                         padding: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
//                                         decoration: BoxDecoration(
//                                             color: value.cancelPrinterGS[index].status=="success"?my_green:my_red,
//                                             borderRadius: BorderRadius.circular(10)
//                                         ),
//                                         child: Text(value.cancelPrinterGS[index].status=="success"?"Completed":"Retry")),
//                                   ),
//
//
//                                 ],
//                               ),
//                             );
//                           },
//                         );
//                       }
//                   ),
//                 ),
//                 InkWell(
//                   onTap: (){
//                     finish(context);
//                   },
//                   child: Container(
//                     height: 50,
//                     width:300,
//                     child: Center(child: Consumer<OrderProvider>(
//                         builder: (context,value,child) {
//                           return Text(value.printSuccess?"OK":"Cancel" ,style: btnTextwhite,);
//                         }
//                     ),),),
//                 )
//               ],
//             ),
//           );
//         });
//   }
// }
