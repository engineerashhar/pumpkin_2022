import 'dart:collection';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/OrderSection/table_order_screen.dart';
import 'package:flutter_pumpkin/Provider/main_provider.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
class ChangeTableScreen extends StatelessWidget {
  final DatabaseReference mRootReference = FirebaseDatabase.instance.reference();

  String floor, tableID, tableName, OrderType, SubTableID, OrderID;
  // ignore: non_constant_identifier_names
   ChangeTableScreen({Key? key, required this.floor, required this.tableID,required this.tableName,required this.OrderType ,required this.SubTableID, required this.OrderID,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OrderProvider orderprovider = Provider.of<OrderProvider>(context, listen: false);
    MainProvider mainProvider = Provider.of<MainProvider>(context, listen: false);
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: my_grey,
      appBar: AppBar(
        backgroundColor: mynew_dred,
        title: Text(
          tableName,
          style: GoogleFonts.redHatDisplay(
              fontWeight: FontWeight.bold,
              fontSize: textSize18,
              color: my_white),
        ),
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            }, child: const Icon(Icons.arrow_back_ios, color: my_white)),
        actions:<Widget> [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: InkWell(
              onTap: (){
                callNext(ChangeTableScreen(floor:floor,tableID:tableID,SubTableID:SubTableID,OrderID:OrderID, OrderType: OrderType, tableName: tableName,), context);
                // _showTables(context,  _orderprovider);
              },
              child: const Icon(Icons.swap_horizontal_circle_outlined, color: my_white,size: 30,),
            ),
          )
        ],
      ),

      body:Container(
        margin: EdgeInsets.all(50),
        child: Consumer<OrderProvider>(
          builder: (context, value, child) {
            return GridView.builder(
              itemCount:value.tableGS.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: MediaQuery.of(context).orientation ==
                    Orientation.landscape ? 4: 3,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
              ),
              itemBuilder: (context,index,) {
                return InkWell(
                  onTap: (){

                    List<String> temp=[];
                    value.tableGS[index].subtableGS.forEach((element) {
                      if(int.parse(element.subtable_ID.replaceAll("S", ''))<900){
                        temp.add(element.subtable_ID);
                      }
                    });
                    int inewtableID=1;
                    if(temp.isNotEmpty){
                      inewtableID=int.parse(temp.last.replaceAll('S', '').toString())+1;
                    }
                    if (value.selectedFloorName == "TAKE AWAY") {
                      inewtableID = value.tokenId;
                    }
                    String strFloor=value.tableGS[index].Floor.toString();
                    String strTableID=value.tableGS[index].ID.toString();
                    String strTableName=value.tableGS[index].Name.toString();

                    print(' new table :  $inewtableID');
                    print(' strTableID :  $strTableID');
                    print(' strTableName :  $strTableName');
                    print(' strFloor :  $strFloor');

                    String dayNode=value.orderDayNode;



                    if(strTableID!=tableID){

                      showLoaderDialog(context);

                      mRootReference.child("TableOrders").child('Floors').child(floor).child('Tables').child(tableID).child('SubTables').child(SubTableID).child('Orders').once().then((DatabaseEvent databaseEvent) {
                        Map<dynamic, dynamic> data =databaseEvent.snapshot.value as Map;

                        data.forEach((keyTable, valueTable) {
                          mRootReference.child(valueTable['OrderRef']).once().then((DatabaseEvent databaseEvent2) {
                            if(databaseEvent2.snapshot.value!=null){
                              mRootReference.child('Orders').child(dayNode).child(floor).child(strTableID).child(inewtableID.toString()).child(keyTable).set(databaseEvent2.snapshot.value);
                              mRootReference.child(valueTable['OrderRef']).remove();
                              HashMap<String, Object> tableData =  HashMap();

                              tableData['OrderID']=keyTable.toString();
                              tableData['OrderRef']='Orders/'+dayNode+'/'+strFloor+'/'+strTableID+'/'+inewtableID.toString()+'/'+keyTable;
                              mRootReference.child("TableOrders").child('Floors').child(strFloor).child('Tables').child( strTableID).child('SubTables').child('S'+inewtableID.toString()).child('Orders').child(keyTable.toString()).set(tableData);
                              mRootReference.child("TableOrders").child('Floors').child(floor).child('Tables').child(tableID).child('SubTables').child(SubTableID).child('Orders').child(keyTable.toString()).remove();
                              if(data.keys.last==keyTable){
                                mRootReference.child("TableOrders").child('Floors').child(floor).child('Tables').child(tableID).child('SubTables').child(SubTableID).once().then((DatabaseEvent databaseEvent3) {
                                  if(databaseEvent3.snapshot.value!=null){
                                    Map<dynamic, dynamic> data2 =databaseEvent3.snapshot.value as Map;
                                    if(data2['OrderedBy']!=null){
                                      mRootReference.child("TableOrders").child('Floors').child(strFloor).child('Tables').child( strTableID).child('SubTables').child('S'+inewtableID.toString()).child('OrderedBy').set(data2['OrderedBy']);

                                    }
                                    if(data2['Customer']!=null){
                                      mRootReference.child("TableOrders").child('Floors').child(strFloor).child('Tables').child( strTableID).child('SubTables').child('S'+inewtableID.toString()).child('Customer').set(data2['Customer']);
                                    }
                                  }

                                  mRootReference.child("TableOrders").child('Floors').child(floor).child('Tables').child(tableID).child('SubTables').child(SubTableID).remove();


                                });
                              }

                            }

                          });
                        });



                      });

                      


                      Future.delayed(const Duration(milliseconds: 2000), () async {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        orderprovider.fun_clear();
                        SharedPreferences userPreference = await SharedPreferences.getInstance();

                        userPreference.setString('Floor', strFloor);
                        userPreference.setString('Table', strTableID);
                        userPreference.setString('SubTable', 'S'+inewtableID.toString());
                        orderprovider.fun_orderItems(strFloor, strTableID, 'S'+inewtableID.toString());
                        orderprovider.funOrderDetails("TableOrders/Floors/" +
                            strFloor +
                            "/Tables/" +
                            strTableID+
                            "/SubTables/" +
                            'S'+inewtableID.toString());


                        callNextReplacement(TableOrderScreen(
                          floor:strFloor,
                          tableID:strTableID,
                          tableName:strTableName,
                          OrderType:"LiveOrder",
                          OrderID:OrderID,
                          OrderRef:'NIL',
                          SubTableID:'S'+inewtableID.toString(),
                          subIndex: index, Uid: value.uid,

                        ), context);
                      });
                    }else{
                      mainProvider.fun_Snackbar("Your are selected current table ", context) ;
                    }
                    },
                  child: Container(
                    height: 100,
                    margin: EdgeInsets.all(5),
                    padding: EdgeInsets.all(2.5),
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.3 ),
                            blurRadius: 5,
                            offset: Offset(3, 0), // changes position of shadow
                          ),
                        ],
                        color:tableID==value.tableGS[index].ID? mynew_yellow:my_white,
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: Column(
                      children: [
                        Expanded(child:  Icon(Icons.table_chart,size: 50,color: tableID==value.tableGS[index].ID ? my_white:table_yellow,)),
                        Text(value.tableGS[index].Name.toString(),style: TextStyle(
                            fontFamily: fontBold,
                            fontSize: textSize14,
                            color: tableID==value.tableGS[index].ID ? my_white: my_black,
                            fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),


    );
  }
  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Processing...")),
        ],),
    );
    showDialog(barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,

            child: alert);
      },
    );
  }

}
