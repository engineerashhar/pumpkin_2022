import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
class BillRegenerator extends StatelessWidget {
  const BillRegenerator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OrderProvider _orderprovider = Provider.of<OrderProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  mynew_dblue,mynew_blue
                  ,
                ],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(0.0, 1.0),
                stops: [1.0, 0.0],
                tileMode: TileMode.clamp),
          ),
        ),        title: Text(
        "Regenertor" ,
        style: GoogleFonts.redHatDisplay(
            fontWeight: FontWeight.bold,
            fontSize: textSize18,
            color: my_white),
      ),
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            }, child: const Icon(Icons.arrow_back_ios, color: my_white)),

      ),
      body:       Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.white,
        child: Consumer<OrderProvider>(
          builder: (context,value,child){
            return    ListView.builder(
                shrinkWrap: true,
                itemCount: value.daybillGS.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onLongPress: (){
                      ConfirmationAlert(context,value.daybillGS[index].BILLID.toString(),_orderprovider);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 10, right: 10, bottom: 15),
                      child: Container(
                        decoration: const BoxDecoration(
                          color: my_grey,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, top: 10),
                                  child: Text(value.daybillGS[index].BILLID.toString(),
                                    style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14,color: my_black),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                  value.daybillGS[index].BILLABOUNT.toString()),
                              // print("yes6"+PhoneNumber.toString());
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }
            );
          },
        ),

      ),


    );
  }

  void ConfirmationAlert(BuildContext context, String BillNo, OrderProvider orderprovider) {
    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("No"),
      onPressed:  () {
        finish(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text("Yes"),
      onPressed:  () {
        finish(context);
        orderprovider.funCRegenerate(context,BillNo);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Regenerate Bill"),
      content: Text("Are you sure you want to delete Bill No " + BillNo + " ?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
