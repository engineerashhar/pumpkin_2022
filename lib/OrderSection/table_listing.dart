import 'dart:io';
import 'dart:math';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/CounterClosing/counter_listing_page.dart';
import 'package:flutter_pumpkin/GeneralScreens/log_in_screen.dart';
import 'package:flutter_pumpkin/GeneralScreens/takeaway_old_sub_table.dart';
import 'package:flutter_pumpkin/GeneralScreens/token_screen.dart';
import 'package:flutter_pumpkin/OrderSection/regeneration_screen.dart';
import 'package:flutter_pumpkin/OrderSection/table_order_screen.dart';
import 'package:flutter_pumpkin/Provider/counter_provider.dart';
import 'package:flutter_pumpkin/Provider/main_provider.dart';
import 'package:flutter_pumpkin/Provider/mainsale_provider.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:flutter_pumpkin/Reports/mainsale_report.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TableListing extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final DatabaseReference mRootReference =
      FirebaseDatabase.instance.reference();

  TableListing({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OrderProvider _orderprovider =
        Provider.of<OrderProvider>(context, listen: false);


    return WillPopScope(
      onWillPop: () {
        return showAlertDialog(context);
      },
      child: Scaffold(
        key: _scaffoldKey,
        drawer: SideDrawer(),
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    mynew_dblue,
                    mynew_blue,
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(0.0, 1.0),
                  stops: [1.0, 0.0],
                  tileMode: TileMode.clamp),
            ),
          ),
          title: InkWell(
            onTap: () {},
            child: Consumer<OrderProvider>(builder: (context, value, child) {
              return Text(
                value.selectedFloorName,
                style: GoogleFonts.redHatDisplay(
                    fontWeight: FontWeight.bold,
                    fontSize: textSize18,
                    color: my_white),
              );
            }),
          ),
          leading: InkWell(
              onTap: () {
                _scaffoldKey.currentState!.openDrawer();
              },
              child: Icon(Icons.menu, color: my_white)),
          actions: [
            InkWell(
              onTap: () {
                _showFloorDialog(context, _orderprovider);
              },
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Icon(Icons.settings, color: my_white),
              ),
            ),
            InkWell(
              onTap: () {
                logOutAlert(context);
              },
              child: const Padding(
                padding: EdgeInsets.all(10.0),
                child: Icon(Icons.logout, color: my_white),
              ),
            ),
          ],
        ),
        backgroundColor: my_grey,
        body: Consumer<OrderProvider>(
          builder: (context, value, child) {
            return Container(
                margin: const EdgeInsets.all(8),
                child: value.tableGS.isNotEmpty
                    ? StaggeredGridView.countBuilder(
                        crossAxisCount: MediaQuery.of(context).orientation ==
                                Orientation.landscape
                            ? 8
                            : 4,
                        itemCount: value.tableGS.length,
                        itemBuilder: (BuildContext context, int index) {
                          return value.tableGS[index].Status == "SELECTED"
                              ? InkWell(
                                  onTap: () {},
                                  child: Container(
                                      margin: EdgeInsets.all(4),
                                      decoration: BoxDecoration(
                                          color: my_light_red,
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.7),
                                              blurRadius: 6,
                                              offset: Offset(3,
                                                  0), // changes position of shadow
                                            ),
                                          ],
                                          border: Border.all(color: Colors.primaries[Random().nextInt(Colors.primaries.length)],width: 2),

                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(17))),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          Container(
                                            height: 47,
                                            decoration: const BoxDecoration(
                                                color:Color(0xFF11162B),
                                                gradient: LinearGradient(
                                                  begin: Alignment.bottomCenter,
                                                  end: Alignment.topCenter,
                                                  colors: [
                                                    Colors.black,
                                                    Color(0xFF282111),
                                                  ],
                                                ),
                                                image: DecorationImage(
                                                  fit: BoxFit.fitWidth,
                                                  alignment: Alignment.center,
                                                  image: AssetImage(
                                                    'assets/selected_table.jpeg',
                                                  ),
                                                ),

                                                borderRadius:
                                                    BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(15),
                                                        topRight:
                                                            Radius.circular(
                                                                15))),
                                            child: Row(
                                              children: <Widget>[
                                                Container(
                                                    child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          8, 8, 8, 8),
                                                  // child: Image.asset(
                                                  //   "assets/selected_table.png",
                                                  //   scale: 5,
                                                  // ),
                                                )),
                                                Text(
                                                  value.tableGS[index].ID,
                                                  style:
                                                      GoogleFonts.redHatDisplay(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: textSize20,
                                                          color: my_white),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            child: Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8),
                                                child: GridView.builder(
                                                    scrollDirection:
                                                        Axis.vertical,
                                                    shrinkWrap: true,
                                                    gridDelegate:
                                                        SliverGridDelegateWithFixedCrossAxisCount(
                                                      crossAxisCount: 3,
                                                    ),
                                                    // itemCount: value.tableGS[index].subtableGS.length,
                                                    itemCount: value.tableGS[index].subtableGS.length + 1,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int inde2x) {
                                                      return InkWell(
                                                        onTap: () async {
                                                          _orderprovider.func_floorprinterData(context, value
                                                                      .tableGS[
                                                                          index]
                                                                      .Floor
                                                                      .toString());

                                                          if (value
                                                                  .tableGS[
                                                                      index]
                                                                  .subtableGS[
                                                                      inde2x]
                                                                  .table_Status ==
                                                              "SELECTED") {
                                                            _orderprovider
                                                                .fun_clear();
                                                            String oFloor=value.tableGS[index].Floor.toString();
                                                            String oTable=value.tableGS[index].ID.toString();
                                                            String oSubTable=value.tableGS[index].subtableGS[inde2x].subtable_ID.toString();
                                                            print('dbshdshsdgsdshdss');
                                                            SharedPreferences userPreference = await SharedPreferences.getInstance();

                                                            userPreference.setString('Floor', oFloor);
                                                            userPreference.setString('Table', oTable);
                                                            userPreference.setString('SubTable', oSubTable);
                                                            // _orderprovider.orderDetails(oFloor,oTable,oSubTable);
                                                            _orderprovider.fun_orderItems(oFloor,oTable, oSubTable);
                                                            _orderprovider.funOrderDetails("TableOrders/Floors/" +
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .Floor +
                                                                "/Tables/" +
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .ID +
                                                                "/SubTables/" +
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .subtableGS[
                                                                        inde2x]
                                                                    .subtable_ID
                                                                    .toString());

                                                            callNext(
                                                                TableOrderScreen(
                                                                    floor: value
                                                                        .tableGS[
                                                                            index]
                                                                        .Floor,
                                                                    tableID: value
                                                                        .tableGS[
                                                                            index]
                                                                        .ID,
                                                                    tableName: value
                                                                        .tableGS[
                                                                            index]
                                                                        .Name,
                                                                    OrderType:
                                                                        "RUNNING ORDER",
                                                                    OrderID: value
                                                                        .tableGS[
                                                                            index]
                                                                        .subtableGS[
                                                                            inde2x]
                                                                        .OrderID
                                                                        .toString(),
                                                                    OrderRef: value
                                                                        .tableGS[
                                                                            index]
                                                                        .subtableGS[
                                                                            inde2x]
                                                                        .OrderRef
                                                                        .toString(),
                                                                    SubTableID: value
                                                                        .tableGS[
                                                                            index]
                                                                        .subtableGS[
                                                                            inde2x]
                                                                        .subtable_ID
                                                                        .toString(),
                                                                    subIndex:
                                                                        index,
                                                                    Uid:
                                                                        '1000'),
                                                                context);
                                                          } else {
                                                            _orderprovider
                                                                .fun_clear();
                                                            SharedPreferences userPreference = await SharedPreferences.getInstance();

                                                            userPreference.setString('Floor', value.tableGS[index].Floor.toString());
                                                            userPreference.setString('Table', value.tableGS[index].ID.toString());
                                                            userPreference.setString('SubTable', value.tableGS[index].subtableGS[inde2x].subtable_ID.toString());
                                                            _orderprovider.fun_orderItems(
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .Floor
                                                                    .toString(),
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .ID
                                                                    .toString(),
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .subtableGS[
                                                                        inde2x]
                                                                    .subtable_ID
                                                                    .toString());
                                                            _orderprovider.funOrderDetails("TableOrders/Floors/" +
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .Floor +
                                                                "/Tables/" +
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .ID +
                                                                "/SubTables/" +
                                                                value
                                                                    .tableGS[
                                                                        index]
                                                                    .subtableGS[
                                                                        inde2x]
                                                                    .subtable_ID
                                                                    .toString());

                                                            callNext(
                                                                TableOrderScreen(
                                                                    floor: value
                                                                        .tableGS[
                                                                            index]
                                                                        .Floor,
                                                                    tableID: value
                                                                        .tableGS[
                                                                            index]
                                                                        .ID,
                                                                    tableName: value
                                                                        .tableGS[
                                                                            index]
                                                                        .Name,
                                                                    OrderType:
                                                                        "NEW ORDER",
                                                                    SubTableID: value
                                                                        .tableGS[
                                                                            index]
                                                                        .subtableGS[
                                                                            inde2x]
                                                                        .subtable_ID
                                                                        .toString(),
                                                                    OrderID:
                                                                        'NIL',
                                                                    OrderRef:
                                                                        'NIL',
                                                                    subIndex:
                                                                        index,
                                                                    Uid:
                                                                        '1000'),
                                                                context);
                                                          }
                                                        },
                                                        child: value
                                                                    .tableGS[
                                                                        index]
                                                                    .subtableGS
                                                                    .length ==
                                                                inde2x
                                                            ? InkWell(
                                                                onTap: () {
                                                                  _orderprovider
                                                                      .fun_clear();
                                                                  List<String>
                                                                      temp = [];
                                                                  value.tableGS[index].subtableGS.forEach((element) {
                                                                    if (int.parse(element.subtable_ID.replaceAll(
                                                                            "S",
                                                                            '')) <
                                                                        900) {
                                                                      temp.add(
                                                                          element
                                                                              .subtable_ID);
                                                                    }
                                                                  });
                                                                  int inewtableID = 1;
                                                                  if (temp.isNotEmpty) {
                                                                    inewtableID = int.parse(temp.last.replaceAll('S', '').toString()) + 1;
                                                                  }
                                                                  if (value.selectedFloorName == "TAKE AWAY") {
                                                                    inewtableID = value.tokenId;}

                                                                  print(
                                                                      ' new table :  $inewtableID');
                                                                  _orderprovider.fun_addSubTable(
                                                                      value
                                                                          .tableGS[
                                                                              index]
                                                                          .Floor
                                                                          .toString(),
                                                                      value
                                                                          .tableGS[
                                                                              index]
                                                                          .ID
                                                                          .toString(),
                                                                      inewtableID);
                                                                  _orderprovider.func_floorprinterData(
                                                                      context,
                                                                      value
                                                                          .tableGS[
                                                                              index]
                                                                          .Floor
                                                                          .toString());
                                                                  callNext(
                                                                      TableOrderScreen(
                                                                          floor: value
                                                                              .tableGS[
                                                                                  index]
                                                                              .Floor,
                                                                          tableID: value
                                                                              .tableGS[
                                                                                  index]
                                                                              .ID,
                                                                          tableName: value
                                                                              .tableGS[
                                                                                  index]
                                                                              .Name,
                                                                          OrderType:
                                                                              "NEW ORDER",
                                                                          SubTableID: "S" +
                                                                              inewtableID
                                                                                  .toString(),
                                                                          OrderID:
                                                                              'NIL',
                                                                          OrderRef:
                                                                              'NIL',
                                                                          subIndex:
                                                                              index,
                                                                          Uid:
                                                                              '1000'),
                                                                      context);
                                                                },
                                                                child:
                                                                    Container(
                                                                  height: 50,
                                                                  child: Card(
                                                                    shape:
                                                                        RoundedRectangleBorder(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              15.0),
                                                                    ),
                                                                    color:
                                                                        my_white,
                                                                    child: Center(
                                                                        child: Icon(
                                                                      Icons.add,
                                                                      color:
                                                                          my_black,
                                                                      size: 24,
                                                                    )),
                                                                  ),
                                                                ),
                                                              )
                                                            : Container(
                                                                child: Card(
                                                                  shape:
                                                                      RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            15.0),
                                                                  ),
                                                                  color: value
                                                                              .tableGS[index]
                                                                              .subtableGS[inde2x]
                                                                              .table_Status ==
                                                                          "SELECTED"
                                                                      ? mynew_yellow
                                                                      : mynew_yellow,
                                                                  child: Center(
                                                                      child:
                                                                          Text(
                                                                    value
                                                                        .tableGS[
                                                                            index]
                                                                        .subtableGS[
                                                                            inde2x]
                                                                        .table_Name
                                                                        .replaceAll(
                                                                            'S',
                                                                            ''),
                                                                    style:
                                                                        Textstyle16,
                                                                  )),
                                                                ),
                                                              ),
                                                      );
                                                    }),
                                              ),
                                            ),
                                          )
                                        ],
                                      )),
                                )
                              : InkWell(
                                  onTap: () {
                                    _orderprovider.fun_clear();

                                    int inewtableID = 1;
                                    if (value.selectedFloorName == "TAKE AWAY") {
                                      inewtableID = value.tokenId;
                                    }
                                    print(' new table :  $inewtableID');

                                    _orderprovider.fun_addSubTable(
                                        value.tableGS[index].Floor.toString(),
                                        value.tableGS[index].ID.toString(),
                                        inewtableID);
                                    _orderprovider.func_floorprinterData(
                                        context,
                                        value.tableGS[index].Floor.toString());

                                    callNext(
                                        TableOrderScreen(
                                            floor: value.tableGS[index].Floor,
                                            tableID: value.tableGS[index].ID,
                                            tableName:
                                                value.tableGS[index].Name,
                                            OrderType: "NEW ORDER",
                                            SubTableID:
                                                "S" + inewtableID.toString(),
                                            OrderID: 'NIL',
                                            OrderRef: 'NIL',
                                            subIndex: index,
                                            Uid: '1000'),
                                        context);
                                  },
                                  child: Container(
                                    margin: EdgeInsets.all(4),
                                    decoration: BoxDecoration(
                                        // color:Color(0xFF170D08),

                                        // gradient: LinearGradient(
                                        //   begin: Alignment.bottomCenter,
                                        //   end: Alignment.topCenter,
                                        //   colors: [
                                        //     Colors.black,
                                        //     Color(0xFF4E1717),
                                        //   ],
                                        // ),
                                        image: DecorationImage(
                                          fit: BoxFit.fitWidth,
                                          alignment: Alignment.centerRight,
                                          image: AssetImage(
                                            'assets/blendedtable.png',
                                          ),
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(15))),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          left: 12, top: 10),
                                      child: Text(
                                        value.tableGS[index].ID,
                                        style: GoogleFonts.redHatDisplay(
                                            fontWeight: FontWeight.bold,
                                            fontSize: textSize20,
                                            color: my_black),
                                      ),
                                    ),
                                    // Row(
                                    //   children: <Widget>[
                                    //     Container(
                                    //         child: Padding(
                                    //       padding:
                                    //           const EdgeInsets.fromLTRB(10, 8, 8, 8),
                                    //       child: Image.asset(
                                    //         "assets/avaialble_table.png",
                                    //         scale: 4,
                                    //       ),
                                    //     )),
                                    //     Text(
                                    //       value.tableGS[index].Name,
                                    //       style: GoogleFonts.redHatDisplay(
                                    //           fontWeight: FontWeight.bold,
                                    //           fontSize: textSize14,
                                    //           color: my_black),
                                    //     )
                                    //   ],
                                    // ),
                                  ),
                                );
                        },
                        staggeredTileBuilder: (int index) =>
                            StaggeredTile.count(
                                2,
                                value.tableGS[index].Status == "SELECTED"
                                    ? (((((double.parse(value.tableGS[index]
                                                            .subtableGS.length
                                                            .toString()) +
                                                        1) /
                                                    3)
                                                .ceilToDouble()) *
                                            0.64) +
                                        0.72)
                                    : 0.68),
                        mainAxisSpacing: 2,
                        crossAxisSpacing: 2,
                      )
                    : Container(
                        child: Center(
                          child: Image.asset(
                            "assets/table.jpg",
                            scale: 4,
                          ),
                        ),
                      ));
          },
        ),
      ),
    );
  }

  void _showFloorDialog(BuildContext context, OrderProvider orderprovider) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        MediaQueryData queryData;
        queryData = MediaQuery.of(context);
        // return alert dialog object
        return Material(
          type: MaterialType.transparency,
          borderOnForeground: false,
          child: Container(
            margin: EdgeInsets.all(45),
            child: Consumer<OrderProvider>(
              builder: (context, value, child) {
                return GridView.builder(
                  itemCount: value.floorGS.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: MediaQuery.of(context).orientation ==
                              Orientation.landscape
                          ? 4
                          : 3,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 20,
                      childAspectRatio: 0.9),
                  itemBuilder: (
                    context,
                    index,
                  ) {
                    return InkWell(
                      onTap: () async {
                        orderprovider.funFloorChange(
                            value.floorGS[index].floorID.toString(),
                            value.floorGS[index].floorName.toString());
                        orderprovider.fun_tableListing(value.floorGS[index].floorID.toString());
                        finish(context);
                      },
                      child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: my_grey,
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                child: Image.asset(
                              "assets/floor.png",
                              scale: 8,
                            )),
                            Text(
                              value.floorGS[index].floorName.toString(),
                              style: TextStyle(
                                  fontSize: 12,
                                  color: my_black,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        );
      },
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        exit(0);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Exit"),
      content: Text("Do You Want Exit Application"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  logOutAlert(BuildContext context) {
    // set up the button
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        finish(context);
      },
    );
    Widget continueButton = TextButton(
      child: Text("Ok"),
      onPressed: () async {
        SharedPreferences userPreference =
            await SharedPreferences.getInstance();
        userPreference.clear();


        callNextReplacement(LogInScreen(), context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Logout"),
      content: Text("Are you sure do you want to logout ?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

class SideDrawer extends StatelessWidget {
  SideDrawer();

  @override
  Widget build(BuildContext context) {
    OrderProvider _orderprovider =
        Provider.of<OrderProvider>(context, listen: false);

    return Drawer(

      child: Column(
        mainAxisSize: MainAxisSize.max,
        
        children: <Widget>[
          Expanded(
            child: Column(
              children: [
                const DrawerHeader(
                  child: Center(
                    child: Text(
                      'Pumpkin',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: my_red,
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.supervisor_account_sharp),
                  title: const Text('Take Away Display'),
                  onTap: () {

                    callNext(TokenScreen(), context);
                  },

                ),
                ListTile(
                  leading: Icon(Icons.supervisor_account_sharp),
                  title: const Text('Take Away SubTables'),
                  onTap: () {
                    _orderprovider.getTakeawaySubTable();
                    callNext(TakeawayOldSubTable(), context);
                  },

                ),
                ListTile(
                  leading: Icon(Icons.supervisor_account_sharp),
                  title: const Text('Counter Close'),
                  onTap: () {
                    CounterProvider counterProvider =
                    Provider.of<CounterProvider>(context, listen: false);
                    counterProvider.funCounterList();

                    callNext(CounterListingPage(from: 'CounterClose',), context);
                  },),
                Consumer<OrderProvider>(
                  builder: (context,value,child) {
                    return !value.stewardsIds.contains(value.uid)?ListTile(
                      leading: Icon(Icons.supervisor_account_sharp),
                      title: const Text('Counter Close Report'),
                      onTap: () {
                        CounterProvider counterProvider =
                        Provider.of<CounterProvider>(context, listen: false);
                        counterProvider.funCounterList();

                        callNext(CounterListingPage(from: 'Report',), context);
                      },):Container();
                  }
                ),
                Consumer<OrderProvider>(
                  builder: (context,value,child) {
                    return !value.stewardsIds.contains(value.uid)?ListTile(
                      leading: const Icon(Icons.supervisor_account_sharp),
                      title: const Text('Reports'),
                      onTap: () {
                        // MainSaleProvider mainSaleProvider = Provider.of<MainSaleProvider>(context,listen: false);
                        // DateTime fromDate =  DateTime.now();
                        // //
                        // DateTime toDate =  DateTime.now();
                        // fromDate=DateTime(fromDate.year, fromDate.month, fromDate.day,  1, 59,59);
                        // toDate=DateTime(toDate.year, toDate.month, toDate.day, 23, 59,59);
                        // mainSaleProvider.funGetSaleData(context,fromDate,toDate);
                        // mainSaleProvider.getDateTime(context,fromDate,toDate);
                        callNext(  MainSaleReport(), context);
                      },

                    ):Container();
                  }
                ),
              ],
            ),
          ),

          
          InkWell(
            splashColor: my_white,
            onLongPress: () {
              _orderprovider.funC_RegeneratedBills(context);

              callNext(BillRegenerator(), context);
            },
            child: Container(
              height: 50,
              
            ),
          ),
          

        ],
      ),
    );
  }
}
