import 'dart:collection';

import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:firebase_database/firebase_database.dart';

import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/OrderSection/calculator_ordering_page.dart';
import 'package:flutter_pumpkin/OrderSection/regeneration_screen.dart';
import 'package:flutter_pumpkin/Provider/main_provider.dart';
import 'package:flutter_pumpkin/OrderSection/addorder_categorybased.dart';
import 'package:flutter_pumpkin/Provider/order_provider.dart';
import 'package:flutter_pumpkin/Views/items_adapter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'addorder_typeitem.dart';
import 'change_tables_screen.dart';

class TableOrderScreen extends StatelessWidget {
  final DatabaseReference mRootReference =
  FirebaseDatabase.instance.reference();

  String floor, tableID, tableName, OrderType, SubTableID, OrderID, OrderRef,
      Uid;
  int? subIndex;
  GlobalKey<AutoCompleteTextFieldState<String>> key2 = GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<String>> key1 = GlobalKey();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController crackerTC = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  TextEditingController notesTC = TextEditingController();


  TableOrderScreen({Key? key,
    required this.floor,
    required this.tableID,
    required this.tableName,
    required this.OrderType,
    required this.SubTableID,
    required this.OrderID,
    required this.OrderRef,
    this.subIndex,
    required this.Uid})
      : super(key: key);

  @override
  void dispose() {


  }

  @override
  Widget build(BuildContext context) {
    OrderProvider _orderprovider = Provider.of<OrderProvider>(
        context, listen: false);
    _orderprovider.checkConnection(context);

    MainProvider mainProvider = Provider.of<MainProvider>(
        context, listen: false);

    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  mynew_dblue, mynew_blue
                  ,
                ],
                begin: FractionalOffset(0.0, 0.0),
                end: FractionalOffset(0.0, 1.0),
                stops: [1.0, 0.0],
                tileMode: TileMode.clamp),
          ),
        ), title: Text(
        tableID,
        style: GoogleFonts.redHatDisplay(
            fontWeight: FontWeight.bold,
            fontSize: textSize18,
            color: my_white),
      ),
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            }, child: const Icon(Icons.arrow_back_ios, color: my_white)),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: InkWell(
              onTap: () {
                _showDialogMenus(context, mainProvider);
              }, child: const Icon(Icons.menu, color: my_white, size: 30,),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [

            Container(
              height: 60,
              margin: const EdgeInsets.only(left: 10, top: 10),
              alignment: Alignment.topLeft,
              child: Consumer<OrderProvider>(
                  builder: (context, value, child) {
                    return ListView.builder(
                        itemCount: value.tableGS[subIndex ?? 0].subtableGS
                            .length + 1,
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            height: 60,
                            width: 60,

                            child: InkWell(
                              onTap: () async {
                                _orderprovider.func_floorprinterData(context, value.tableGS[subIndex ?? 0].Floor.toString());

                                if (value.tableGS[subIndex ?? 0].subtableGS[index].table_Status == "SELECTED") {
                                  print('Test 6  SELECTED');
                                  _orderprovider.fun_clear();
                                  SharedPreferences userPreference = await SharedPreferences.getInstance();

                                  userPreference.setString('Floor', value.tableGS[subIndex ?? 0].Floor.toString());
                                  userPreference.setString('Table', value.tableGS[subIndex ?? 0].ID.toString());
                                  userPreference.setString('SubTable', value.tableGS[subIndex ?? 0].subtableGS[index].subtable_ID.toString());
                                  _orderprovider.fun_orderItems(value.tableGS[subIndex ?? 0].Floor.toString(), value.tableGS[subIndex ?? 0].ID.toString(),value.tableGS[subIndex ?? 0].subtableGS[index].subtable_ID.toString());
                                  _orderprovider.funOrderDetails("TableOrders/Floors/"+value.tableGS[subIndex ?? 0].Floor+"/Tables/"+value.tableGS[subIndex ?? 0].ID+"/SubTables/"+ value.tableGS[subIndex ?? 0].subtableGS[index].subtable_ID.toString());

                                  callNextReplacement(TableOrderScreen(
                                    floor: value.tableGS[subIndex ?? 0].Floor,
                                    tableID: value.tableGS[subIndex ?? 0].ID,
                                    tableName: value.tableGS[subIndex ?? 0]
                                        .Name,
                                    OrderType: "RUNNING ORDER",
                                    OrderID: value.tableGS[subIndex ?? 0]
                                        .subtableGS[index].OrderID.toString(),
                                    OrderRef: value.tableGS[subIndex ?? 0]
                                        .subtableGS[index].OrderRef.toString(),
                                    SubTableID: value.tableGS[subIndex ?? 0]
                                        .subtableGS[index].subtable_ID
                                        .toString(),
                                    subIndex: subIndex ?? 0,
                                    Uid: value.uid,

                                  ), context);
                                }
                                else {
                                  print('Test 6  NOTSELECTED');

                                  _orderprovider.fun_clear();
                                  SharedPreferences userPreference = await SharedPreferences.getInstance();
                                  userPreference.setString('Floor', value.tableGS[subIndex ?? 0].Floor.toString());
                                  userPreference.setString('Table', value.tableGS[subIndex ?? 0].ID.toString());
                                  userPreference.setString('SubTable', value.tableGS[subIndex ?? 0].subtableGS[index].subtable_ID.toString());
                                  _orderprovider.fun_orderItems(value.tableGS[subIndex ?? 0].Floor.toString(), value.tableGS[subIndex ?? 0].ID.toString(),value.tableGS[subIndex ?? 0].subtableGS[index].subtable_ID.toString());
                                  _orderprovider.funOrderDetails("TableOrders/Floors/"+value.tableGS[subIndex ?? 0].Floor+"/Tables/"+value.tableGS[subIndex ?? 0].ID+"/SubTables/"+ value.tableGS[subIndex ?? 0].subtableGS[index].subtable_ID.toString());

                                  callNextReplacement(TableOrderScreen(
                                    floor: value.tableGS[subIndex ?? 0].Floor,
                                    tableID: value.tableGS[subIndex ?? 0].ID,
                                    tableName: value.tableGS[subIndex ?? 0]
                                        .Name,
                                    OrderType: "NEW ORDER",
                                    SubTableID: value.tableGS[subIndex ?? 0]
                                        .subtableGS[index].subtable_ID
                                        .toString(),
                                    OrderID: 'NIL',
                                    OrderRef: 'NIL',
                                    subIndex: subIndex ?? 0,
                                    Uid: value.uid,
                                  ), context);
                                }
                              },

                              child: value.tableGS[subIndex ?? 0].subtableGS
                                  .length == index ? InkWell(
                                onTap: () async {
                                  _orderprovider.fun_clear();
                                  List<String> temp=[];
                                  for (var element in value.tableGS[subIndex ?? 0].subtableGS) {
                                    if(int.parse(element.subtable_ID.replaceAll("S", ''))<900){
                                      temp.add(element.subtable_ID);
                                    }
                                  }
                                  int inewtableID=1;
                                  if(temp.isNotEmpty){
                                    inewtableID=int.parse(temp.last.replaceAll('S', '').toString())+1;
                                  }
                                  if (value.selectedFloorName == "TAKE AWAY"){
                                    inewtableID=value.tokenId;
                                  }
                                  _orderprovider.fun_addSubTable(
                                      value.tableGS[subIndex ?? 0].Floor
                                          .toString(),
                                      value.tableGS[subIndex ?? 0].ID
                                          .toString(), inewtableID);
                                  callNextReplacement(TableOrderScreen(
                                      floor: value.tableGS[subIndex ?? 0].Floor,
                                      tableID: value.tableGS[subIndex ?? 0].ID,
                                      tableName: value.tableGS[subIndex ?? 0].Name,
                                      OrderType: "NEW ORDER",
                                      SubTableID: "S" + inewtableID.toString(),
                                      OrderID: 'NIL',
                                      OrderRef: 'NIL',
                                      subIndex: subIndex,
                                      Uid: value.uid
                                  ), context);
                                },
                                child: Card(

                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  color: my_white,
                                  child: Center(child: Icon(
                                    Icons.add, color: my_black, size: 24,
                                  )
                                  ),

                                ),
                              ) : Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                color: value.tableGS[subIndex ?? 0]
                                    .subtableGS[index].table_Name == SubTableID
                                    ? mynew_yellow
                                    : my_white,
                                child: Center(
                                    child: Text(value.tableGS[subIndex ?? 0]
                                        .subtableGS[index].table_Name
                                        .replaceAll('S', ''),
                                      style: Textstyle16,)),
                              ),
                            ),
                          );
                        });
                  }
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: InkWell(
                    onTap: () {
                      _showDialogStewards(context,'Add Steward');
                    },
                    child: Container(
                      margin: const EdgeInsets.fromLTRB(10, 5, 10, 10),

                      height: queryData.size.height * 0.06,
                      decoration: BoxDecoration(
                        border: Border.all(color: mynew_dblue),
                        color: my_white,
                        borderRadius: const BorderRadius.all(Radius.circular(
                            30.0) //                 <--- border radius here
                        ),
                      ),
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 5),
                                child: Image.asset("assets/bowtie.png",
                                  scale: 35,
                                  color: mynew_dred,
                                ),
                              ),
                              Consumer<OrderProvider>(
                                  builder: (context, value, child) {
                                    return Flexible(

                                      child: Text(
                                          value.stewardName==''? 'Add Steward':value.stewardName,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 1,

                                      ),
                                    );
                                  }),
                              const Icon(Icons.arrow_forward_ios,
                                  size: 18,
                                  color: my_black),
                            ],
                          ),
                        ),
                      ),
                      // child: const Center(child:  Text("Steward Name")),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: queryData.size.height * 0.06,
                    margin: const EdgeInsets.fromLTRB(10, 5, 10, 10),
                    decoration: BoxDecoration(
                      color: my_white,
                      border: Border.all(color: mynew_dblue),
                      borderRadius: const BorderRadius.all(Radius.circular(
                          30.0) //                 <--- border radius here
                      ),
                    ),
                    child: Center(child: Text("Sub Tabel: " + SubTableID.replaceAll("S", ''))),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {
                _orderprovider.funMoreButtonFalse();
                _showDialogCustomer(context, _orderprovider);
              },
              child: Container(
                height: queryData.size.height * 0.06,
                margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                decoration: BoxDecoration(
                  color: mynew_dred,
                  border: Border.all(color: mynew_dblue),
                  borderRadius: const BorderRadius.all(Radius.circular(
                      30.0) //                 <--- border radius here
                  ),
                ),
                child: Center(
                  child:
                  Consumer<OrderProvider>(builder: (context, value, child) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        value.customerName == "Customer"
                            ? Icon(Icons.add, color: my_white, size: 18,)
                            : Container(),
                        SizedBox(width: 2),
                        Text(value.customerName ?? "+ Add Customer",
                            style: TextStyle(
                                color: my_white, fontWeight: FontWeight.bold)),
                      ],
                    );
                  }),
                ),
              ),
            ),
            Consumer<OrderProvider>(
                builder: (context, value, child) {
                  return !value.noOrder  ? Container(
                    margin: const EdgeInsets.fromLTRB(15, 15, 15, 0),
                    decoration: BoxDecoration(
                      color: my_white,
                      border: Border.all(color: mynew_dblue),
                      borderRadius: const BorderRadius.all(Radius.circular(
                          10.0) //                 <--- border radius here
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 60,
                          decoration: BoxDecoration(
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(
                                    10.0) //                 <--- border radius here
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,

                            children: [
                              Container(
                                padding: const EdgeInsets.all(10),
                                child: Consumer<OrderProvider>(
                                  builder: (context, value, child) {
                                    return Text(
                                      "Bill Number :"+ value.strBillNo,
                                      style: TextStyle(color: Colors.black,
                                          fontWeight: FontWeight.bold),);
                                  },

                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  if(value.allOrderPrinted){
                                    if (value.New) {
                                      showCrackerAlert(context, "Normal");
                                    }
                                    else {
                                      showLoaderDialog(context);
                                      _orderprovider.fun_finalPrint(
                                          OrderID, SubTableID, tableID, context,
                                          Uid, floor);
                                    }
                                  }else{
                                    showKotAlert(context);
                                  }

                                },
                                child: Container(
                                    height: 40,
                                    width: queryData.size.width * 0.25,
                                    margin: const EdgeInsets.only(
                                        right: 15, top: 5),
                                    decoration: const BoxDecoration(
                                      color: mynew_yellow,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              10.0) //                 <--- border radius here
                                      ),
                                    ),
                                    child: const Center(
                                      child: Text(
                                        "Print Bill",
                                        style: TextStyle(color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Divider(
                            color: Colors.black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: Row(
                            children: <Widget>[
                              Container(
                                color: my_white,
                                height: 15,
                                width: 15,
                                margin: const EdgeInsets.only(right: 5),
                              ),
                              Expanded(flex: 4,
                                  child: Container(child: Text("Item",
                                      style: TextStyle(color: mynew_dred,
                                          fontStyle: FontStyle.italic)))),
                              Expanded(flex: 2,
                                  child: Container(child: Text("Qty",
                                      style: TextStyle(color: mynew_dred,
                                          fontStyle: FontStyle.italic)))),
                              Expanded(flex: 2,
                                  child: Center(child: Text("Total",
                                      style: TextStyle(color: mynew_dred,
                                          fontStyle: FontStyle.italic)))),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                            child: Consumer<OrderProvider>(
                              builder: (context, value, child) {
                                return ListView.builder(
                                    itemCount: value.itemGS.length,
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemBuilder: (BuildContext context,
                                        int index) {
                                      return InkWell(
                                        onTap: () {
                                          _orderprovider.func_floorprinterData(context, floor);
                                          _orderprovider.funCalculate(value.itemGS[index].kotNumber);
                                          callNext(AddOrderTypeItem(
                                            floor: value.itemGS[index].floor,
                                            tableID: value.itemGS[index].table,
                                            OrderID: value.itemGS[index]
                                                .kotNumber,
                                            Uid: "+919633693348",
                                            SubTable: value.itemGS[index]
                                                .subTableId,
                                            StewardID: "123",
                                            OrderType: "RUNNING ORDER",
                                            kotStatus: value.itemGS[index]
                                                .Status,
                                            AddItemClick: "old",
                                          )
                                              , context);
                                        },

                                        child: ItemsAdapter(
                                            value.itemGS[index], index,
                                            context),
                                      );
                                    });
                              },
                            )),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10),
                          child: Divider(
                            color: Colors.black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(flex: 6,
                                  child: Text("Non-Taxable Amount",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 2,
                                  child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Consumer<OrderProvider>(
                                          builder: (context, value, child) {
                                            return Text(
                                                "₹ " + value.ANT.toString(),
                                                style: TextStyle(
                                                    fontWeight: FontWeight
                                                        .bold));
                                          }))),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(flex: 6,
                                  child: Text("Taxable Amount",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold))),
                              Expanded(
                                  flex: 2,
                                  child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Consumer<OrderProvider>(
                                          builder: (context, value, child) {
                                            return Text(
                                                "₹ " +
                                                    value.AT.toStringAsFixed(2),
                                                style: TextStyle(
                                                    fontWeight: FontWeight
                                                        .bold));
                                          }))),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ) :
                  InkWell(
                    onTap: () {
                      _orderprovider.func_floorprinterData(context, floor);

                      if (value.itemGS.isNotEmpty) {
                        OrderType = 'RUNNING ORDER';
                        OrderID = DateTime
                            .now()
                            .millisecondsSinceEpoch
                            .toString();
                      } else {
                        OrderType = 'NEW ORDER';
                        OrderID = DateTime
                            .now()
                            .millisecondsSinceEpoch
                            .toString();
                      }
                      _orderprovider.funCalculate(OrderID);
                      callNext(
                          AddOrderTypeItem(
                            floor: floor,
                            tableID: tableID,
                            OrderID: OrderID,
                            Uid: "+919633693348",
                            SubTable: SubTableID,
                            StewardID: "100",
                            OrderType: OrderType,
                            kotStatus: "",
                            AddItemClick: "new",
                          ),
                          context);
                    },
                    child: Container(
                        width: queryData.size.width * 0.8,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(height: queryData.size.height * 0.1),
                            Image.asset(
                              "assets/no_orders_yet.png", scale: 1.8,),
                            SizedBox(height: queryData.size.height * 0.2),
                          ],
                        )),
                  );
                }
            ),
          ],
        ),
      ),
      bottomNavigationBar: Column(mainAxisSize: MainAxisSize.min, children: [
        // Align(
        //   alignment: Alignment.centerRight,
        //   child: InkWell(
        //     onTap: () {
        //       showLoaderDialog(context);
        //       _orderprovider.fun_finalPrint(OrderID, SubTableID, tableID, context,Uid,floor);
        //     },
        //     child: Container(
        //         height: 50,
        //         width: queryData.size.width * 0.25,
        //         margin: const EdgeInsets.only(right: 15, top: 5),
        //         decoration: const BoxDecoration(
        //           color: appbar_color,
        //           borderRadius: BorderRadius.all(Radius.circular(
        //                   10.0) //                 <--- border radius here
        //               ),
        //         ),
        //         child: const Center(
        //           child: Text(
        //             "Print",
        //             style: TextStyle(color: Colors.white),
        //           ),
        //         )),
        //   ),
        // ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Consumer<OrderProvider>(
                  builder: (context, value, child) {
                    return InkWell(
                      onTap: () {

                        if(value.stewardName!=''){
                          _orderprovider.func_floorprinterData(context, floor);


                          if (value.itemGS.isNotEmpty) {
                            OrderType = 'RUNNING ORDER';
                            OrderID = DateTime
                                .now()
                                .millisecondsSinceEpoch
                                .toString();
                          } else {
                            OrderType = 'NEW ORDER';
                            OrderID = DateTime
                                .now()
                                .millisecondsSinceEpoch
                                .toString();
                          }
                          _orderprovider.funCalculate(OrderID);
                          callNext(
                              AddOrderTypeItem(
                                floor: floor,
                                tableID: tableID,
                                OrderID: OrderID,
                                Uid: "+919633693348",
                                SubTable: SubTableID,
                                StewardID: "100",
                                OrderType: OrderType,
                                kotStatus: "",
                                AddItemClick: "new",
                              ),
                              context);
                        }else{
                          _showDialogStewards(context,'AddOrder');
                        }

                      },
                      child: Container(
                          height: 50,
                          margin: const EdgeInsets.symmetric(horizontal: 4),
                          decoration: const BoxDecoration(
                            color: mynew_dred,
                            borderRadius: BorderRadius.all(Radius.circular(
                                12.0) //                 <--- border radius here
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.widgets_outlined, color: my_white),
                              Text(
                                "Add item",
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          )),
                    );
                  },
                ),
              ),
              // Expanded(
              //   child: Container(
              //       height: 50,
              //       margin: const EdgeInsets.symmetric(horizontal: 4),
              //       decoration: const BoxDecoration(
              //         color: mynew_dred,
              //         borderRadius: BorderRadius.all(Radius.circular(
              //             12.0) //                 <--- border radius here
              //         ),
              //       ),
              //       child: InkWell(
              //         onTap: () {
              //           _orderprovider.func_floorprinterData(context, floor);
              //
              //           _orderprovider.fun_Dishes();
              //           _orderprovider.onClear();
              //           _orderprovider.AddItemClear();
              //
              //           if (OrderType != "LiveOrder") {
              //             OrderID =
              //                 DateTime.now().millisecondsSinceEpoch.toString();
              //           }
              //           callNext(
              //               CalculatorOrderingPage(
              //                 floor: floor,
              //                 tableID: tableID,
              //                 OrderID: OrderID,
              //                 Uid: "+919633693348",
              //                 SubTable: SubTableID,
              //                 StewardID: "123",
              //                 OrderType: OrderType,
              //                 AddItemClick: "new",),
              //               context);
              //         },
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           children: <Widget>[
              //             Icon(Icons.calculate, color: my_white),
              //             Text(
              //               "Add item",
              //               style: TextStyle(color: Colors.white),
              //             ),
              //           ],
              //         ),
              //       )),
              // ),
              // Expanded(
              //   child: Container(
              //       height: 50,
              //       margin: const EdgeInsets.symmetric(horizontal: 4),
              //       decoration: const BoxDecoration(
              //         color: mynew_dred,
              //         borderRadius: BorderRadius.all(Radius.circular(
              //             12.0) //                 <--- border radius here
              //         ),
              //       ),
              //       child: InkWell(
              //         onTap: () {
              //           _orderprovider.func_floorprinterData(context, floor);
              //
              //           _orderprovider.fun_Category(context);
              //           if (OrderType != "LiveOrder") {
              //             OrderID = DateTime
              //                 .now()
              //                 .millisecondsSinceEpoch
              //                 .toString();
              //           }
              //           callNext(AddOrderCategoryBased(floor: floor,
              //               tableID: tableID,
              //               OrderID: OrderID,
              //               Uid: "+919633693348",
              //               SubTable: SubTableID,
              //               StewardID: "1000",
              //               OrderType: OrderType), context);
              //         },
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           children: <Widget>[
              //             Icon(Icons.calculate, color: my_white),
              //             Text(
              //               "Add item T",
              //               style: TextStyle(color: Colors.white),
              //             ),
              //           ],
              //         ),
              //       )),
              // ),
            ],
          ),
        ),
      ]),
    );
  }

  void _showDialogStewards(context,String from) {
    OrderProvider orderProvider = Provider.of<OrderProvider>(
        context, listen: false);
    // flutter defined function
    showDialog(
      context: context,
      builder: (context) {
        // return alert dialog object
        return Material(
          type: MaterialType.transparency,
          child: Container(
            margin: EdgeInsets.all(50),
            child: Consumer<OrderProvider>(
              builder: (context, value, child) {
                return GridView.builder(
                  itemCount: value.stewards.length,

                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: MediaQuery
                        .of(context)
                        .orientation ==
                        Orientation.landscape ? 4 : 3,
                    crossAxisSpacing: 8,
                    mainAxisSpacing: 8,
                  ),
                  itemBuilder: (context, index,) {
                    return InkWell(
                      onTap: () {
                        String path = "TableOrders/Floors/" + floor +
                            "/Tables/" + tableID + "/SubTables/" + SubTableID;

                        orderProvider.funSetSteward(
                            value.stewards[index].toString(), path);
                        Navigator.pop(context);
                        if(from=='AddOrder'){
                          orderProvider.func_floorprinterData(context, floor);

                          if (value.itemGS.isNotEmpty) {
                            OrderType = 'RUNNING ORDER';
                            OrderID = DateTime
                                .now()
                                .millisecondsSinceEpoch
                                .toString();
                          } else {
                            OrderType = 'NEW ORDER';
                            OrderID = DateTime
                                .now()
                                .millisecondsSinceEpoch
                                .toString();
                          }
                          orderProvider.funCalculate(OrderID);
                          callNext(
                              AddOrderTypeItem(
                                floor: floor,
                                tableID: tableID,
                                OrderID: OrderID,
                                Uid: "+919633693348",
                                SubTable: SubTableID,
                                StewardID: "100",
                                OrderType: OrderType,
                                kotStatus: "",
                                AddItemClick: "new",
                              ),
                              context);
                        }
                      },
                      child: Container(
                        height: 100,
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: my_white,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: Column(
                          children: [
                            Expanded(child: Image.asset(
                                "assets/steward_selection.png", scale: 5)),
                            Text(value.stewards[index].toString(),
                              style: TextStyle(fontSize: 12, color: my_black),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        );
      },
    );
  }

  void _showDialogCustomer(context, OrderProvider orderProvider) {
    OrderProvider _orderprovider =
    Provider.of<OrderProvider>(context, listen: false);

    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return Consumer<OrderProvider>(
          builder: (context, value, child) {
            return AlertDialog(
              title: Container(
                  child: const Center(
                      child: Text(
                        'Customer Details',
                        style: TextStyle(color: my_white),
                      ))),
              backgroundColor: mynew_dred,
              contentPadding: const EdgeInsets.only(
                top: 15.0,
              ),
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              content: Container(
                width: double.maxFinite,
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
                decoration: const BoxDecoration(
                    color: my_white,
                    borderRadius:
                    BorderRadius.vertical(bottom: Radius.circular(10))),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Container(
                          height: 50,
                          child: SimpleAutoCompleteTextField(
                              key: key1,
                              suggestions: value.costumersNamePhone,

                              clearOnSubmit: false,
                              textSubmitted: (itemName) {
                                _orderprovider.funCheckCustomer(
                                    itemName, "Name");
                              },

                              decoration: const InputDecoration(
                                labelText: "Name",

                                labelStyle: TextStyle(
                                  color: my_black,
                                  fontSize: 12,
                                ),

                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10.0)),
                                  //                 <--- border radius here
                                  // width: 0.0 produces a thin "hairline" border
                                  borderSide: BorderSide(color: Colors.grey,),
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10.0)),
                                    //                 <--- border radius here
                                    borderSide: BorderSide(color: mynew_dred,)),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10.0)),
                                  //                 <--- border radius here
                                  borderSide: BorderSide(color: Colors.grey,),

                                ),
                              ),
                              controller: value.nameController),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7),
                        child: Container(
                          height: 50,
                          child: SimpleAutoCompleteTextField(
                              key: key2,
                              suggestions: value.costumersNamePhone,

                              clearOnSubmit: false,
                              textSubmitted: (itemName) {
                                _orderprovider.funCheckCustomer(
                                    itemName, "Phone");
                              },

                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                labelText: "Phone Number",

                                labelStyle: TextStyle(
                                  color: my_black,
                                  fontSize: 12,
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10.0)),
                                  //                 <--- border radius here
                                  // width: 0.0 produces a thin "hairline" border
                                  borderSide: BorderSide(color: Colors.grey,),

                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10.0)),
                                    //                 <--- border radius here
                                    borderSide: BorderSide(color: mynew_dred,)),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10.0)),
                                  //                 <--- border radius here
                                  borderSide: BorderSide(color: Colors.grey,),

                                ),
                              ),
                              controller: value.phoneController),
                        ),
                      ),
                      !value.moreOpen ? Align(
                        alignment: Alignment.bottomRight,
                        child: InkWell(
                          onTap: () {
                            _orderprovider.funMoreButtonTrue();
                          },
                          child: const Text(
                            "More...", style: TextStyle(fontSize: 12),),
                        ),
                      ) : Container(),
                      value.moreOpen
                          ? textField(value.gstController, "GST")
                          : Container(),
                      value.moreOpen ? textField(
                          value.addressController, "Address") : Container(),
                      value.moreOpen ? textField(
                          value.landMarkController, "Landmark") : Container(),

                      value.moreOpen ? Padding(
                        padding: const EdgeInsets.symmetric(vertical: 7),
                        child: Container(
                          height: 40,
                          child: TextField(
                            controller: value.dateOfBirthController,
                            onTap: () {
                              _orderprovider.selectDate(context);
                            },
                            decoration: const InputDecoration(
                              labelText: "Date Of Birth",

                              labelStyle: TextStyle(
                                color: my_black,
                                fontSize: 12,
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(10.0)),
                                //                 <--- border radius here
                                // width: 0.0 produces a thin "hairline" border
                                borderSide: BorderSide(color: Colors.grey,),

                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10.0)),
                                  //                 <--- border radius here
                                  borderSide: BorderSide(color: mynew_dred,)),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(10.0)),
                                //                 <--- border radius here
                                borderSide: BorderSide(color: Colors.grey,),

                              ),
                            ),
                          ),
                        ),
                      ) : Container(),
                      value.moreOpen
                          ? textField(value.notesController, "Notes")
                          : Container(),
                      InkWell(
                        onTap: () {
                          if (value.nameController.text.isNotEmpty &&
                              value.phoneController.text.length == 10) {
                            String path = "TableOrders/Floors/" +
                                floor +
                                "/Tables/" +
                                tableID +
                                "/SubTables/" +
                                SubTableID;
                            _orderprovider.uploadCustomerData(
                                value.nameController.text.toString().trim(),
                                value.phoneController.text.toString(),
                                path,
                                value.gstController.text,
                                value.addressController.text,
                                value.landMarkController.text,
                                value.sDateOfBirth,
                                value.notesController.text);
                            mRootReference.child("TempUser").child(floor).child(
                                tableID).child(SubTableID).set(
                                value.phoneController.text);
                            _orderprovider.funMoreButtonFalse();
                            Navigator.pop(context);
                          }
                        },
                        child: Container(
                            height: 40,
                            width: 120,
                            decoration: const BoxDecoration(
                              color: mynew_yellow,
                              borderRadius: BorderRadius.all(Radius.circular(
                                  10.0) //                 <--- border radius here
                              ),
                            ),
                            child: const Center(
                              child: Text(
                                "Save",
                                style: TextStyle(color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,),
                              ),
                            )),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }


  // void _showTables(BuildContext context, OrderProvider orderprovider) {
  //   showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       // return alert dialog object
  //       return Material(
  //         type: MaterialType.transparency,
  //
  //         child:
  //         Container(
  //           margin: EdgeInsets.all(50),
  //           child: Consumer<OrderProvider>(
  //             builder: (context, value, child) {
  //               return GridView.builder(
  //                 itemCount:value.tableGS.length,
  //                 gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
  //                   crossAxisCount: MediaQuery.of(context).orientation ==
  //                       Orientation.landscape ? 4: 3,
  //                   crossAxisSpacing: 8,
  //                   mainAxisSpacing: 8,
  //                 ),
  //                 itemBuilder: (context,index,) {
  //                   return InkWell(
  //                     onTap: (){
  //                       int inewtableID=value.tableGS[index].subtableGS.length+2;
  //                       print(' new table :  $inewtableID');
  //                       String strFloor=value.tableGS[index].Floor.toString();
  //                       String strTableID=value.tableGS[index].ID.toString();
  //                       orderprovider.fun_addSubTable(value.tableGS[index].Floor.toString(), value.tableGS[index].ID.toString(),inewtableID);
  //                       mRootReference.child("TableOrders").child('Floors').child(floor!).child('Tables').child(tableID!).child('SubTables').child(SubTableID!).once().then((DataSnapshot snapshot){
  //                         Map<dynamic, dynamic> MAINDATA = snapshot.value;
  //                         mRootReference.child("TableOrders").child('Floors').child(value.tableGS[index].Floor.toString()).child('Tables')
  //                             .child( value.tableGS[index].ID.toString()).child('SubTables').child(inewtableID.toString()).set(snapshot.value).then((value){
  //
  //                           print('success');
  //                           orderprovider.fun_orderItems(strFloor, strTableID,inewtableID.toString());
  //                           orderprovider.funOrderDetails("TableOrders/Floors/"+strFloor+"/Tables/"+strTableID+"/SubTables/"+inewtableID.toString());
  //                           mRootReference.child("TableOrders").child('Floors').child(floor!).child('Tables').child(tableID!).child('SubTables').child(SubTableID!).remove();
  //                         }).catchError((error){
  //
  //                         });
  //
  //                       });
  //
  //
  //                                            // String path="TableOrders/Floors/"+floor!+"/Tables/"+tableID!+"/SubTables/"+SubTableID!;
  //                       //
  //                       // orderProvider.funSetSteward(value.stewards[index].toString(),path);
  //                       Navigator.pop(context);
  //                     },
  //                     child: Container(
  //                       height: 100,
  //                       padding: EdgeInsets.all(5),
  //                       decoration: BoxDecoration(
  //                           color: my_white,
  //                           borderRadius: BorderRadius.circular(10)
  //                       ),
  //                       child: Column(
  //                         children: [
  //                           Expanded(child:  Icon(Icons.table_chart,size: 50,color: table_yellow,)),
  //                           Text(value.tableGS[index].Name.toString(),style: Textstyle14,
  //                           ),
  //                         ],
  //                       ),
  //                     ),
  //                   );
  //                 },
  //               );
  //             },
  //           ),
  //         ),
  //       );
  //     },
  //   );
  //
  //
  // }

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Processing...")),
        ],),
    );
    showDialog(barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,

            child: alert);
      },
    );
  }

  void _showDialogMenus(BuildContext context, MainProvider mainProvider) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return alert dialog object
        return Material(
          type: MaterialType.transparency,

          child: Container(
            margin: EdgeInsets.fromLTRB(20, 50, 20, 50),
            child: Consumer<MainProvider>(
              builder: (context, value, child) {
                return
                  GridView.builder(
                    itemCount: value.MenuGS.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: MediaQuery
                          .of(context)
                          .orientation ==
                          Orientation.landscape ? 1 : 1,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 8,
                      childAspectRatio: 6,
                    ),
                    itemBuilder: (context, index,) {
                      return InkWell(
                        onTap: () {
                          OrderProvider _orderprovider = Provider.of<
                              OrderProvider>(context, listen: false);

                          if (value.MenuGS[index] == 'CHANGE TABLE') {
                            finish(context);
                            callNext(ChangeTableScreen(floor: floor,
                              tableID: tableID,
                              SubTableID: SubTableID,
                              OrderID: OrderID,
                              OrderType: OrderType,
                              tableName: tableName,), context);
                          } else if (value.MenuGS[index] == 'REFERESH TABLE') {
                            _orderprovider.funC_RefereshTable(
                                floor, tableID, SubTableID, OrderID, OrderType,
                                context);
                          }

                          else if (value.MenuGS[index] ==
                              'Bill WITH CUSTOMER DATA') {
                            if(_orderprovider.allOrderPrinted){
                              if (_orderprovider.New) {
                                showCrackerAlert(
                                    context, "Bill WITH CUSTOMER DATA");
                              } else {
                                if (_orderprovider.nameController.text != ""&&_orderprovider.phoneController.text!="") {
                                  _orderprovider.fun_finalPrintwithCustomerData(
                                      OrderID, SubTableID, tableID, context, Uid,
                                      floor);
                                } else {
                                  showAlertDialog(context);
                                }
                              }
                            }else{
                              showKotAlert(context);
                            }

                          } else
                          if (value.MenuGS[index] == 'FOOD AND SERVICE') {
                            if(_orderprovider.allOrderPrinted){
                              if (_orderprovider.New) {
                                showCrackerAlert(context, "FOOD AND SERVICE");
                              } else {
                                _orderprovider.fun_finalPrintwithFAS(
                                    OrderID, SubTableID, tableID, context, Uid,
                                    floor);
                              }
                            }else{
                              showKotAlert(context);
                            }



                          }
                        },
                        child: Container(
                          height: 50,
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: my_grey,
                              borderRadius: BorderRadius.circular(8)
                          ),
                          child:
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Image.asset(
                                  value.MenuIcon[index].toString(), scale: 7,),
                              ),
                              // Icon(Icons.person,size: 50,color: my_greymid,),
                              SizedBox(width: 20),
                              Text(value.MenuGS[index].toString(),
                                style: TextStyle(fontSize: 12,
                                    color: my_black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  );
              },
            ),
          ),
        );
      },
    );
  }

  /// TextField Design
  Widget textField(TextEditingController controller, String label) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 7),
      child: Container(
        height: 40,
        child: TextField(
          controller: controller,
          decoration: InputDecoration(
            labelText: label,

            labelStyle: const TextStyle(
              color: my_black,
              fontSize: 12,
            ),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              //                 <--- border radius here
              // width: 0.0 produces a thin "hairline" border
              borderSide: BorderSide(color: Colors.grey,),

            ),
            focusedBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                //                 <--- border radius here
                borderSide: BorderSide(color: mynew_dred,)),
            border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              //                 <--- border radius here
              borderSide: BorderSide(color: Colors.grey,),

            ),
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        finish(context);
        finish(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Error"),
      content: Text("Please Enter Costumer Details"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  showCrackerAlert(context, String billType) {
    OrderProvider _orderprovider =
    Provider.of<OrderProvider>(context, listen: false);

    // flutter defined function
    showDialog(
      context: _scaffoldKey.currentContext!,
      builder: (BuildContext contextAlert) {
        // return alert dialog object
        return Consumer<OrderProvider>(
          builder: (context, value, child) {
            return AlertDialog(
              title: Container(
                  child: Center(
                      child: Text(
                        'Cancellation Details',
                        style: TextStyle(color: my_white),
                      ))),
              backgroundColor: mynew_dred,
              contentPadding: EdgeInsets.only(
                top: 15.0,
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              content: SingleChildScrollView(
                child: Container(
                  width: double.maxFinite,
                  padding: EdgeInsets.fromLTRB(15, 20, 15, 20),
                  decoration: BoxDecoration(
                      color: my_white,
                      borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(10))),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: TextFormField(
                            controller: crackerTC,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                hintText: "Enter Cracker Number"),
                            validator: (text) =>
                            text!.isEmpty ? 'INVALID CRACKER' : text !=
                                "9048004320" ? "INVALID CRACKER" : null,
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            final FormState? form = _formKey.currentState;
                            if (form!.validate()) {

                              if (billType == "Normal") {
                                finish(contextAlert);

                                _orderprovider.fun_finalPrint(
                                    OrderID, SubTableID, tableID, context, Uid,
                                    floor);
                              }
                              if (billType == "Bill WITH CUSTOMER DATA") {
                                finish(context);

                                if (_orderprovider.nameController.text != ""&&_orderprovider.phoneController.text!="") {
                                  _orderprovider.fun_finalPrintwithCustomerData(
                                      OrderID, SubTableID, tableID, context,
                                      Uid, floor);
                                } else {
                                  showAlertDialog(context);
                                }
                              }
                              if (billType=="FOOD AND SERVICE") {
                                finish(context);

                                _orderprovider.fun_finalPrintwithFAS(
                                    OrderID, SubTableID, tableID, context, Uid,
                                    floor);
                              }

                            }

                          },
                          child: Container(
                              height: 50,
                              width: 150,
                              decoration: const BoxDecoration(
                                color: mynew_yellow,
                                borderRadius: BorderRadius.all(Radius.circular(
                                    10.0) //                 <--- border radius here
                                ),
                              ),
                              child: const Center(
                                child: Text(
                                  "Save",
                                  style: TextStyle(color: Colors.black),
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        );
      },
    );
  }
  showKotAlert(BuildContext context) {

    // set up the button
    Widget okButton = TextButton(
      child: const Text("OK"),
      onPressed: () {
        finish(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: const Text("Alert"),
      content: const Text("Some kot is note printed"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


}


