import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Views/counter_close_gs.dart';

class CounterProvider with ChangeNotifier {
  final DatabaseReference mRootReference =
      FirebaseDatabase.instance.reference();
  final FirebaseFirestore db = FirebaseFirestore.instance;
  List<String> counterList = [];
  List<CounterCloseGS> saleBillsList = [];
  List<CounterCloseGS> cardBillsList = [];
  List<CounterCloseGS> upiBillsList = [];
  List<CounterCloseGS> creditBillsList = [];
  List<CounterCloseGS> discountList = [];
  List<CounterCloseGS> totalReceiptList = [];
  List<CounterCloseGS> cardReceiptList = [];
  List<CounterCloseGS> upiReceiptList = [];
  List<CounterCloseGS> paymentList = [];
  double sale=0.0;
  double saleTax=0.0;
  double settledAmount=0.0;
  double pendingAmount=0.0;
  double openingBalance=0.0;
  double sCash=0.0;
  double sCard=0.0;
  double sUpi=0.0;
  double rTotal=0.0;
  double rCash=0.0;
  double rCard=0.0;
  double rUpi=0.0;
  double totalInCash=0.0;
  double credit=0.0;
  double payment=0.0;
  double totalOutCash=0.0;
  double discount=0.0;
  double counterBalance=0.0;
  double actualBalance=0.0;
  int dayMines=1;
  DateTime fromDate =  DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day,0,0,0,0,0);
  String displayDate='';
  StreamSubscription? _ledgerStream;
  StreamSubscription? _billsStream;
  StreamSubscription? _counterStream;





  funCounterList(){
    mRootReference.child('config').child('DisplayCounter').once().then((DatabaseEvent databaseEvent) {
      counterList.clear();
      if(databaseEvent.snapshot.value!=null){
        Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;
        map.forEach((key, value) {
          counterList.add(value);
          notifyListeners();

        });
      }
      notifyListeners();

    }

    );
  }
  funCounterData(String counter,DateTime now)async{
    DateTime fromDate =  DateTime(now.year,now.month,now.day,0,0,0,0,0);
    DateTime toDate =  DateTime(now.year,now.month,now.day,23,59,59,59,59);
    String dayNode = now.year.toString() +
        "/" +
        now.month.toString() +
        "/" +
        (now.day).toString();
    displayDate=now.day.toString() +
        "/" +
        now.month.toString() +
        "/" +
        (now.year).toString();
     sale=0.0;
     saleTax=0.0;
     settledAmount=0.0;
     pendingAmount=0.0;
     openingBalance=0.0;
     sCash=0.0;
     sCard=0.0;
     sUpi=0.0;
     rCash=0.0;
     rCard=0.0;
     rUpi=0.0;
     totalInCash=0.0;
     payment=0.0;
     totalOutCash=0.0;
     discount=0.0;
     counterBalance=0.0;
     dayMines=1;
     if(_ledgerStream!=null){
       _ledgerStream!.cancel();
     }
    if(_billsStream!=null){
      _billsStream!.cancel();
    }
    if(_counterStream!=null){
      _counterStream!.cancel();
    }
    _billsStream=db.collection('Bills').where("BillingTime", isGreaterThan:fromDate ).where("BillingTime", isLessThan:toDate).snapshots().listen((event) {
      sale=0.0;
      saleTax=0.0;
      settledAmount=0.0;
      pendingAmount=0.0;
      sCash=0.0;
      sCard=0.0;
      sUpi=0.0;
      discount=0.0;
      credit=0.0;
      saleBillsList.clear();
      cardBillsList.clear();
      upiBillsList.clear();
      creditBillsList.clear();
      discountList.clear();

      if(event.docs.isNotEmpty){
        for (var element in event.docs) {
          if(element.get('Status')!=null&&element.get('Status').toString()=='BillPayed'&&element.get('CashCounter')==counter){
            if(element.get('TaxableAmount')!=null){
              sale=sale+double.parse(element.get('TaxableAmount').toString());
            }
            if(element.get('TotalAmount')!=null){
              saleTax=saleTax+double.parse(element.get('TotalAmount').toString());
            }
            if(element.get('Status')!=null&&element.get('Status').toString()=='BillPayed'&&element.get('TotalAmount')!=null){
              settledAmount=settledAmount+double.parse(element.get('TotalAmount').toString());
              saleBillsList.add(CounterCloseGS(element.get('BillNo').toString(), double.parse(element.get('TotalAmount').toString()).toStringAsFixed(2)));
            }
            if(element.get('Status')!=null&&element.get('Status').toString()=='BillPrinted'&&element.get('TotalAmount')!=null){
              pendingAmount=pendingAmount+double.parse(element.get('TotalAmount').toString());
            }
            if(element.get('Status')!=null&&element.get('Status').toString()=='BillPayed'&&element.get('Cash')!=null){
              sCash=sCash+double.parse(element.get('Cash').toString());

            }
            if(element.get('Status')!=null&&element.get('Status').toString()=='BillPayed'&&element.get('Card')!=null){
              sCard=sCard+double.parse(element.get('Card').toString());
              if(double.parse(element.get('Card').toString())>0){
                cardBillsList.add(CounterCloseGS(element.get('BillNo').toString(), element.get('Card').toString()));
              }

            }
            if(element.get('Status')!=null&&element.get('Status').toString()=='BillPayed'&&element.get('UPI')!=null){
              sUpi=sUpi+double.parse(element.get('UPI').toString());
              if(double.parse(element.get('UPI').toString())>0){
                upiBillsList.add(CounterCloseGS(element.get('BillNo').toString(), element.get('UPI').toString()));

              }


            }
            if(element.get('Status')!=null&&element.get('Status').toString()=='BillPayed'&&element.get('Credit')!=null){
              credit=credit+double.parse(element.get('Credit').toString());
              if(double.parse(element.get('Credit').toString())>0){
                if(element.get('CreditCustomerID')!=null){
                  creditBillsList.add(CounterCloseGS(element.get('CreditCustomerID').toString(), element.get('Credit').toString()));

                }else{
                  creditBillsList.add(CounterCloseGS(element.get('BillNo').toString(), element.get('Credit').toString()));

                }
              }
            }
            if(element.get('Status')!=null&&element.get('Status').toString()=='BillPayed'&&element.get('Discount')!=null){
              discount=discount+double.parse(element.get('Discount').toString());
              if(double.parse(element.get('Discount').toString())>0){
                discountList.add(CounterCloseGS(element.get('BillNo').toString(), element.get('Discount').toString()));

              }


            }
            notifyListeners();

          }


          notifyListeners();

        }
        totalInCash=sCash+sCard+sUpi+rCash+rCard+rUpi;
        counterBalance=sCash+rCash+payment;
      }

      notifyListeners();
    });
    _ledgerStream =
        mRootReference.child('LEDGER').child(dayNode).child('GROUPS').onValue.listen((DatabaseEvent databaseEvent) {
      rCash=0.0;
      rCard=0.0;
      rUpi=0.0;
      payment=0.0;
      totalOutCash=0.0;
      rTotal=0.0;
      totalReceiptList.clear();
      cardReceiptList.clear();
      upiReceiptList.clear();
      paymentList.clear();

      if(databaseEvent.snapshot.value!=null){
        Map<dynamic, dynamic> group = databaseEvent.snapshot.value as Map;
        group.forEach((gKey, gValue) {
          if(gKey!='CUSTOMER'){
            Map<dynamic, dynamic> subGroup = gValue['SUBGROUPS'];
            subGroup.forEach((subKey, subValue) {
              Map<dynamic, dynamic> data = subValue['COUSEENTER'][subKey.toString()]['DATA'];
              data.forEach((dataKey , dataValue) {
                if(dataValue['EnteredFrom'].toString()==counter){
                  if(double.parse(dataValue['Amount'].toString())>0){

                    rTotal=rTotal+ double.parse(dataValue['Amount'].toString());
                    totalReceiptList.add(CounterCloseGS(subKey.toString(),dataValue['Amount'].toString()));

                    if(dataValue['Cash']!=null&&double.parse(dataValue['Cash'].toString())>0){
                      rCash=rCash+double.parse(dataValue['Cash'].toString());
                    }
                    if(dataValue['Card']!=null&&double.parse(dataValue['Card'].toString())>0){
                      rCard=rCard+double.parse(dataValue['Card'].toString());
                      cardReceiptList.add(CounterCloseGS(subKey.toString(),dataValue['Card'].toString()));

                    }
                    if(dataValue['UPI']!=null&&double.parse(dataValue['UPI'].toString())>0){
                      rUpi=rUpi+double.parse(dataValue['UPI'].toString());
                      upiReceiptList.add(CounterCloseGS(subKey.toString(),dataValue['UPI'].toString()));

                    }

                  }

                  if(double.parse(dataValue['Amount'].toString())<0){
                    if(dataValue['Type'].toString()=='COUNTER_PAYMENT'){
                      payment=payment+double.parse(dataValue['Amount'].toString());
                      paymentList.add(CounterCloseGS(subKey.toString(),dataValue['Amount'].toString()));
                    }
                    totalOutCash=totalOutCash+double.parse(dataValue['Amount'].toString());

                  }

                }
                notifyListeners();

              });
              notifyListeners();

            });


            notifyListeners();
          }


        });
        totalInCash=sCash+sCard+sUpi+rCash+rCard+rUpi;
        counterBalance=sCash+rCash+payment;
        notifyListeners();


      }
      notifyListeners();

    });

    dayMines=1;
    getCounterBalance(counter,now);






  }
  
  getCounterBalance(String counter,DateTime now){

   now = now.subtract(Duration(days: (dayMines)));

    String dayNode = now.year.toString() +
        "/" +
        now.month.toString() +
        "/" +
        (now.day).toString();
   openingBalance=0.0;

  if(_counterStream!=null){
    _counterStream!.cancel();
  }
   _counterStream=mRootReference.child('Closing_Balances').child(counter).child(dayNode).onValue.listen((DatabaseEvent databaseEvent) {

      if(databaseEvent.snapshot.value!=null){

        openingBalance=double.parse(databaseEvent.snapshot.value.toString());
        notifyListeners();

      }else{
        dayMines++;

        if(dayMines<31){
          getCounterBalance(counter,fromDate);

        }
      }
    });
  }
  getMismatch(double txt){
    actualBalance=txt;
    notifyListeners();

  }



}
