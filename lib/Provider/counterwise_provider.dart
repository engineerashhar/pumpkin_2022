import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Views/counter_pie_model.dart';

import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class CounterWiseProvider with ChangeNotifier {
  // Option 2

  String counterDropDownValue = 'Select Counter';
  final List<String> counterListArray = [];
  List<CounterPieGS> counterWisePieGS = [];
  List<String> counterCheckArray = [];

  List<String> stewardCheck = [];
  final DatabaseReference mRootReference =
      FirebaseDatabase.instance.reference();
  final FirebaseFirestore db = FirebaseFirestore.instance;
  DateRangePickerController dateRangePickerController =
      DateRangePickerController();
  DateTime fromDate = DateTime.now();
  DateTime toDate = DateTime.now();

  double dTotalAmount = 0.0;
  double dTaxbleAmount = 0.0;
  double dNonTaxableAmount = 0.0;

  double rCash=0.0;
  double rCard=0.0;
  double rUpi=0.0;
  double totalInCash=0.0;
  double credit=0.0;
  double payment=0.0;
  double totalOutCash=0.0;

  Future<void> selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2101));
    if (picked != null) {
      fromDate = DateTime(picked.year, picked.month, picked.day, 1, 59, 59);
      toDate = DateTime(picked.year, picked.month, picked.day, 23, 59, 59);

      funGetCounterData(context, fromDate, toDate);
    }
    notifyListeners();
  }

  void funGetConfigCounter(BuildContext context) {
    counterDropDownValue='Select Counter';

    mRootReference
        .child('config')
        .child('DisplayCounter')
        .once()
        .then((DatabaseEvent databaseEvent) {
      counterListArray.clear();
      counterListArray.add('Select Counter');
      databaseEvent.snapshot.children.forEach((element) {
        counterListArray.add(element.key.toString());
        notifyListeners();
      });
    });
    notifyListeners();
  }

  void setDropdown(BuildContext context, String newValue) {
    counterDropDownValue = newValue;
    fromDate = DateTime(fromDate.year, fromDate.month, fromDate.day, 1, 59, 59);
    toDate = DateTime(toDate.year, toDate.month, toDate.day, 23, 59, 59);
    funGetCounterData(context, fromDate, toDate);
    notifyListeners();
  }

  void funGetCounterData(BuildContext context, DateTime fromDate, DateTime toDate) {
    db.collection("Bills")
        .where("BillingTime", isGreaterThan: fromDate)
        .where("BillingTime", isLessThan: toDate)
        .snapshots()
        .listen((event) async {
      dTotalAmount = 0.0;
      dTaxbleAmount = 0.0;
      dNonTaxableAmount = 0.0;

      counterWisePieGS.clear();
      counterCheckArray.clear();
      if (event.docs.isNotEmpty) {
        for (var element in event.docs) {
          if (element.get("Status") != null) {
            if (element.get("Status") == 'BillPayed') {
              if (element.get("CashCounter") != null) {
                if (element.get('CashCounter').toString() == counterDropDownValue) {
                  double taxAmount = 0.0,
                      nonTaxAmount = 0.0,
                      totalAmount = 0.0;
                  double cashAmount = 0.0,
                      upiAmount = 0.0,
                      cardAmount = 0.0,
                      creditAmount = 0.0;
                  if (element.get("TaxableAmount") != null) {
                    taxAmount =
                    double.tryParse(element.get("TaxableAmount").toString())!;
                  }
                  if (element.get("NontaxableAmount") != null) {
                    nonTaxAmount = double.tryParse(
                        element.get("NontaxableAmount").toString())!;
                  }
                  if (element.get("TotalAmount") != null) {
                    totalAmount =
                    double.tryParse(element.get("TotalAmount").toString())!;
                  }
                  dTotalAmount = dTotalAmount + totalAmount;
                  dTaxbleAmount = dTaxbleAmount + taxAmount;
                  dNonTaxableAmount = dNonTaxableAmount + nonTaxAmount;
                  // print('dTotalAmount  ${dTotalAmount}');
                  // print('dTaxbleAmount  ${dTaxbleAmount}');
                  // print('dNonTaxableAmount  ${dNonTaxableAmount}');


                  if (element.get("Cash") != null) {
                    cashAmount = double.tryParse(element.get("Cash").toString())!;
                    // print('cashAmount  ${cashAmount}');
                    if (!counterCheckArray.contains('Cash')) {counterWisePieGS.add(CounterPieGS(
                          "Cash", cashAmount, cashColor, "Cash",
                          "Cash : ${cashAmount}"));
                      counterCheckArray.add('Cash');
                    }
                    else {
                      var index = counterWisePieGS.indexWhere((user) =>
                      user.type == 'Cash');
                      if (index > -1) {
                        double itemCount = counterWisePieGS[index].total;
                        double newCount = itemCount + cashAmount;
                        counterWisePieGS[index].total = newCount;
                        counterWisePieGS[index].labelData =
                        "Cash : ${newCount}";
                      }
                    }
                  }

                  if (element.get("Card") != null) {
                    cardAmount =
                    double.tryParse(element.get("Card").toString())!;

                    if (!counterCheckArray.contains('Card')) {
                      counterWisePieGS.add(
                          CounterPieGS("Card", cardAmount, cardColor, "Card",
                              "Card : ${cardAmount}"));
                      counterCheckArray.add('Card');
                    }
                    else {
                      var index = counterWisePieGS.indexWhere((user) =>
                      user.type == 'Card');
                      if (index > -1) {
                        double itemCount = counterWisePieGS[index].total;
                        double newCount = itemCount + cardAmount;
                        counterWisePieGS[index].total = newCount;
                        counterWisePieGS[index].labelData =
                        "Card : ${newCount}";
                      }
                    }
                  }

                  if (element.get("Credit") != null) {
                    creditAmount =
                    double.tryParse(element.get("Credit").toString())!;

                    if (!counterCheckArray.contains('Credit')) {
                      counterWisePieGS.add(CounterPieGS(
                          "Credit", creditAmount, creditColor, "Credit",
                          "Credit : ${creditAmount}"));
                      counterCheckArray.add('Credit');
                    }
                    else {
                      var index = counterWisePieGS.indexWhere((user) =>
                      user.type == 'Credit');
                      if (index > -1) {
                        double itemCount = counterWisePieGS[index].total;
                        double newCount = itemCount + creditAmount;
                        counterWisePieGS[index].total = newCount;
                        counterWisePieGS[index].labelData =
                        "Credit : ${newCount}";
                      }
                    }
                  }

                  if (element.get("UPI") != null) {
                    upiAmount = double.tryParse(element.get("UPI").toString())!;
                    if (!counterCheckArray.contains('UPI')) {
                      counterWisePieGS.add(
                          CounterPieGS("UPI", upiAmount, upiColor, "UPI",
                              "UPI : ${upiAmount}"));
                      counterCheckArray.add('UPI');
                    }
                    else {
                      var index = counterWisePieGS.indexWhere((user) =>
                      user.type == 'UPI');
                      if (index > -1) {
                        double itemCount = counterWisePieGS[index].total;
                        double newCount = itemCount + upiAmount;
                        counterWisePieGS[index].total = newCount;
                        counterWisePieGS[index].labelData = "UPI : ${newCount}";
                      }
                    }
                  }
                  notifyListeners();
                }
              }
            }
          }
        }
        // print('counterWisePieGS  ${counterWisePieGS.length}');
        // counterWisePieGS.forEach((element2) {
        //   print(
        //       'type  ${element2.type} total  ${element2.total}  color ${element2
        //           .color}  label  ${element2.label} labelData ${element2
        //           .labelData}');
        // });


        notifyListeners();
      }
      notifyListeners();
    });


    String dayNode = fromDate.year.toString() + "/" +
        fromDate.month.toString() + "/" + (fromDate.day).toString();
    Stream<DatabaseEvent> stream = mRootReference
        .child('LEDGER')
        .child(dayNode)
        .child('GROUPS')
        .onValue;


    stream.listen((DatabaseEvent event) {
      rCash = 0.0;
      rCard = 0.0;
      rUpi = 0.0;
      payment = 0.0;
      credit = 0.0;
      totalOutCash = 0.0;
      if (event.snapshot.value != null) {
        Map<dynamic, dynamic>? group = event.snapshot.value as Map;
        group.forEach((gKey, gValue) {
          Map<dynamic, dynamic> subGroup = gValue['SUBGROUPS'];
          subGroup.forEach((subKey, subValue) {
            Map<dynamic, dynamic> data = subValue['COUSEENTER'][subKey.toString()]['DATA'];
            data.forEach((dataKey, dataValue) {
              if (dataValue['EnteredFrom'].toString() == counterDropDownValue) {
                // print('EnteredFrom  ${counterDropDownValue}');

                if (double.parse(dataValue['Amount'].toString()) > 0) {
                  rCash = rCash + double.parse(dataValue['Amount'].toString());


                  // if(dataValue['Cash']!=null&&double.parse(dataValue['Cash'].toString())>0){
                  //   rCash=rCash+double.parse(dataValue['Cash'].toString());
                  // }
                  // if(dataValue['Card']!=null&&double.parse(dataValue['Card'].toString())>0){
                  //   rCard=rCard+double.parse(dataValue['Card'].toString());
                  // }
                  // if(dataValue['Upi']!=null&&double.parse(dataValue['Upi'].toString())>0){
                  //   rUpi=rUpi+double.parse(dataValue['Upi'].toString());
                  // }

                }

                if (double.parse(dataValue['Amount'].toString()) < 0) {
                  if (dataValue['Type'].toString() == 'COUNTER_PAYMENT') {
                    payment = payment + double.parse(
                        dataValue['Amount'].toString());
                  }
                  if (dataValue['Type'].toString() == 'CREDIT') {
                    credit = credit + double.parse(
                        dataValue['Amount'].toString());
                  }
                  totalOutCash = totalOutCash + double.parse(dataValue['Amount']
                      .toString());
                }
              }
              notifyListeners();
            });
            notifyListeners();
          });


          notifyListeners();
        });
        // totalInCash=sCash+rCash;
        // counterBalance=sCash+rCash+payment;
        notifyListeners();
      }
      notifyListeners();
    });
  }
}
