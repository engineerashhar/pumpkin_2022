import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Views/itemwise_master.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import 'order_provider.dart';

class FloorWiseProvider with ChangeNotifier {
  String floorDropDownValue = '0';
  final List<String> floorListArray = [];
  final List<String> floorListNameArray = [];
  List<ItemWiseGS> itemWiseGS = [];
  List<ItemWiseGS> stewarWiseGS = [];
  List<String> itemCheck = [];
  List<String> stewardCheck = [];
  final DatabaseReference mRootReference =
      FirebaseDatabase.instance.reference();
  final FirebaseFirestore db = FirebaseFirestore.instance;
  double dTotalSaleAmount = 0.0;
  double dTotalSettledAmount = 0.0;
  double dTotalPendingAmount = 0.0;
  double dTotalBilledAmount = 0.0;
  double notBillPrinted = 0.0;
  double dSetteledCount = 0.0;
  double dSetteledPerc = 0.0;
  double dPendingCount = 0.0;
  double dPendingPerc = 0.0;
  double dTotalCount = 0.0;
  DateRangePickerController dateRangePickerController =
      DateRangePickerController();
  DateTime fromDate = DateTime.now();
  DateTime toDate = DateTime.now();
  StreamSubscription? orderStream;
  StreamSubscription? billsDbStream;

  void funGetFloorData(
      BuildContext context, DateTime fromDate, DateTime toDate) {
    if(orderStream!=null){
      orderStream!.cancel();
    }
    String dayNode = fromDate.year.toString() +
        "/" +
        fromDate.month.toString() +
        "/" +
        fromDate.day.toString();

    // print('floorDropDownValue  ${floorDropDownValue} dayNode ${dayNode}');
    if (floorListNameArray.contains(floorDropDownValue)) {
      String floor =
          floorListArray[floorListNameArray.indexOf(floorDropDownValue)];
      orderStream= mRootReference
          .child("Orders")
          .child(dayNode)
          .child(floor)
          .onValue
          .listen((DatabaseEvent databaseEvent) {
        dTotalSaleAmount = 0.0;
        notBillPrinted = 0.0;
        // dTotalCount=0;
        itemWiseGS.clear();
        itemCheck.clear();
        databaseEvent.snapshot.children.forEach((element0) {
          // Stream<DatabaseEvent> stream2 = element0.ref.onValue;
          element0.ref.once().then((DatabaseEvent event2) {
            event2.snapshot.children.forEach((element2) {
              Map<dynamic, dynamic> tableMap = element2.value as Map;
              tableMap.forEach((orderID, value) {
                // dTotalCount=dTotalCount+1;
                value.forEach((itemID, itemValue) {
                  double dQty = 0.0;
                  double dRate = 0.0;
                  if (itemValue['Rate'] != null &&
                      itemValue['Rate'].toString() != '') {
                    dRate = double.tryParse(itemValue['Rate'].toString())!;
                  }
                  if (itemValue['Quantity'] != null &&
                      itemValue['Quantity'].toString() != '') {
                    dQty = double.tryParse(itemValue['Quantity'].toString())!;
                  }
                  dTotalSaleAmount = dTotalSaleAmount + (dQty * dRate);
                  // print('dTotalSaleAmount 1  ${dTotalSaleAmount}');
                  if (itemValue['ItemID'] != null) {
                    if (!itemCheck.contains(itemValue['ItemID'].toString())) {
                      double itemPrice = double.parse(itemValue['Quantity']) *
                          double.parse(itemValue['Rate']);

                      itemWiseGS.add(ItemWiseGS(
                          itemValue['ItemID'].toString(),
                          itemValue['Item'].toString(),
                          itemValue['Quantity'].toString(),
                          itemPrice.toString()));
                      itemCheck.add(itemValue['ItemID'].toString());
                    } else {
                      var index = itemWiseGS.indexWhere((user) =>
                          user.itemID == itemValue['ItemID'].toString());
                      if (index > -1) {
                        int itemCount = int.tryParse(
                            itemWiseGS[index].itemCount.toString())!;
                        int newCount = itemCount + 1;
                        double itemPrice = double.parse(
                            itemWiseGS[index].itemPrice.toString());
                        double newPrice = itemPrice +
                            (double.parse(itemValue['Quantity']) *
                                double.parse(itemValue['Rate']));
                        itemWiseGS[index].itemCount = newCount.toString();
                        itemWiseGS[index].itemPrice = newPrice.toString();
                      }
                    }
                  }
                  itemWiseGS.sort((a, b) => double.parse(b.itemPrice)
                      .compareTo(double.parse(a.itemPrice)));
                  notifyListeners();
                });
              });
            });
            // DataSnapshot
          });
        });

        notifyListeners();
        // print('dTotalSaleAmount  ${dTotalSaleAmount} ');
          if(billsDbStream!=null){
             billsDbStream!.cancel();
            }
        billsDbStream=db
            .collection("Bills")
            .where("BillingTime", isGreaterThan: fromDate)
            .where("BillingTime", isLessThan: toDate)
            .snapshots()
            .listen((event) async {
          dSetteledCount = 0.0;
          dPendingCount = 0.0;
          dSetteledPerc = 0.0;
          dTotalSettledAmount = 0.0;
          dTotalBilledAmount = 0.0;
          dTotalPendingAmount = 0.0;
          dPendingPerc = 0.0;
          dTotalCount = 0;
          stewarWiseGS.clear();
          stewardCheck.clear();

          if (event.docs.isNotEmpty) {
            int i = 0;

            for (var element in event.docs) {
              if (element.get('Floor') == floor) {
                i++;
                dTotalCount = dTotalCount + 1;
                // try{
                if (element.get('Steward') != null) {
                  if (!stewardCheck
                      .contains(element.get('Steward').toString())) {
                    if (Provider.of<OrderProvider>(context, listen: false)
                        .stewardsIds
                        .contains(element.get('Steward').toString())) {
                      String stewardName = '';
                      int count = 1;

                      // print('${Provider.of<OrderProvider>(context,listen: false).stewards}');
                      // print('${Provider.of<OrderProvider>(context,listen: false).stewardsIds}');
                      stewardName =
                          Provider.of<OrderProvider>(context, listen: false)
                                  .stewards[
                              Provider.of<OrderProvider>(context, listen: false)
                                  .stewardsIds
                                  .indexOf(element.get('Steward').toString())];
                      stewarWiseGS.add(ItemWiseGS(
                          element.get('Steward').toString(),
                          stewardName,
                          count.toString(),
                          '0'));
                      stewardCheck.add(element.get('Steward').toString());
                    }
                  } else {
                    var index = stewarWiseGS.indexWhere((user) =>
                        user.itemID == element.get('Steward').toString());
                    if (index > -1) {
                      int itemCount = int.tryParse(
                          stewarWiseGS[index].itemCount.toString())!;
                      int newCount = itemCount + 1;
                      stewarWiseGS[index].itemCount = newCount.toString();
                    }
                  }
                }
                stewarWiseGS.sort((a, b) => double.parse(b.itemCount)
                    .compareTo(double.parse(a.itemCount)));

                // }catch(e){
                //
                // }

                double amount = 0.0;
                double taxAm = 0.0,
                    nonTaxm = 0.0,
                    cash = 0.0,
                    card = 0.0,
                    payBack = 0.0,
                    credit = 0.0,
                    discount = 0.0,
                    roundOff = 0.0,
                    cgst = 0.0,
                    sgst = 0.0;
                if (element.get("TaxableAmount") != null) {
                  taxAm =
                      double.tryParse(element.get("TaxableAmount").toString())!;
                }
                if (element.get("NontaxableAmount") != null) {
                  nonTaxm = double.tryParse(
                      element.get("NontaxableAmount").toString())!;
                }

                if (element.get("TotalAmount") != null) {
                  amount =
                      double.tryParse(element.get("TotalAmount").toString())!;
                }

                if (element.get("Status") != null) {
                  if (element.get("Status") == 'BillPayed') {
                    dSetteledCount = dSetteledCount + 1;

                    // if (element.get("Cash")!=null) {
                    //   cash = double.tryParse(element.get("Cash").toString())!;
                    // }
                    // if (element.get("Card") != null) {
                    //   card = double.tryParse(element.get("Card").toString())!;
                    // }
                    // if (element.get("Credit") != null) {
                    //   credit = double.tryParse(element.get("Credit").toString())!;
                    // }
                    // if (element.get("Discount") != null) {
                    //   discount = double.tryParse(element.get("Discount").toString())!;
                    // }
                    // if (element.get("RoundOf") != null) {
                    //   roundOff = double.tryParse(element.get("RoundOf").toString())!;
                    // }
                    // if (element.get("SGST") != null) {
                    //   sgst = double.tryParse(element.get("SGST").toString())!;
                    // }
                    // if (element.get("CGST") != null) {
                    //   cgst = double.tryParse(element.get("CGST").toString())!;
                    // }
                    // if (element.get("PayBack") != null) {
                    //   payBack = double.tryParse(element.get("PayBack").toString())!;
                    // }
                    dTotalSettledAmount = dTotalSettledAmount + taxAm + nonTaxm;
                  }
                  // if (element.get("Status") == 'BillPrinted') {
                  //   dTotalBilledAmount=dTotalBilledAmount+taxAm + nonTaxm;
                  // }
                }

                Timestamp timestamp = element.get('BillingTime');
                DateTime time = DateTime.parse(timestamp.toDate().toString());
                notifyListeners();
              } else {
                i++;
              }
            }

            dSetteledPerc = (dSetteledCount * 100) / dTotalCount;
            if (dSetteledPerc > 0.0) {
              dSetteledPerc = dSetteledPerc / 100;
            } else {
              dSetteledPerc = 0.0;
            }
            // print('dTotalCount Navaf  ${dTotalCount}');
            // print('dSetteledCount Navaf  ${dSetteledCount}');
            dPendingCount = dTotalCount - dSetteledCount;
            dPendingPerc = (dPendingCount * 100) / dTotalCount;
            // print('dPendingCount Navaf  ${dPendingCount}');
            // print('dPendingPerc Navaf  ${dPendingPerc}');

            if (dPendingPerc > 0.0) {
              dPendingPerc = dPendingPerc / 100;
              // print('dPendingPerc  above 0.0 Navaf  ${dPendingPerc}');

            } else {
              dPendingPerc = 0.0;
              // print('dPendingPerc  below 0.0 Navaf  ${dPendingPerc}');

            }
            dTotalPendingAmount = dTotalSaleAmount - dTotalSettledAmount;

            // print('dSetteledPerc  ${dSetteledPerc}');
            // notifyListeners();
            //
            // print('dTotalSaleAmount ${dTotalSaleAmount}');
            // print('dTotalSettledAmount ${dTotalSettledAmount}');
            // print('dTotalPendingAmount ${dTotalPendingAmount}');
            notifyListeners();
          } else {
            dSetteledPerc = (dSetteledCount * 100) / dTotalCount;
            if (dSetteledPerc > 0.0) {
              dSetteledPerc = dSetteledPerc / 100;
            } else {
              dSetteledPerc = 0.0;
            }

            dPendingCount = dTotalCount - dSetteledCount;
            dPendingPerc = (dPendingCount * 100) / dTotalCount;
            if (dPendingPerc > 0.0) {
              dPendingPerc = dPendingPerc / 100;
            } else {
              dPendingPerc = 0.0;
            }

            // print('dSetteledCount  ${dSetteledCount}');
            // print('dTotalCount  ${dTotalCount}');
            // print('dPendingCount  ${dPendingCount}');
            // print('dPendingPerc  ${dPendingPerc}');
            // print('dSetteledPerc  ${dSetteledPerc}');
            // print('no data');
            notifyListeners();
          }
        });
      });
    }
  }

  Future<void> selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2101));
    if (picked != null) {
      fromDate = DateTime(picked.year, picked.month, picked.day, 1, 59, 59);
      toDate = DateTime(picked.year, picked.month, picked.day, 23, 59, 59);
      itemWiseGS.clear();
      itemCheck.clear();
      funGetFloorData(context, fromDate, toDate);
    }
    notifyListeners();
  }

  void funGetConfig(BuildContext context) {
    mRootReference
        .child('config')
        .child('Floors')
        .once()
        .then((DatabaseEvent databaseEvent) {
      floorListArray.clear();
      floorListNameArray.clear();
      floorListArray.add('SELECT FLOOR');
      floorListNameArray.add('SELECT FLOOR');
      databaseEvent.snapshot.children.forEach((element) {
        if (element.key != 'NIL') {
          element.ref.once().then((DatabaseEvent data) {
            if (data.snapshot.value != null) {
              Map<dynamic, dynamic>? ItemData = data.snapshot.value as Map;
              floorListArray.add(element.key.toString());
              floorListNameArray.add(ItemData['Name'].toString());
              notifyListeners();
            }
          });
        }
      });
      floorDropDownValue = 'SELECT FLOOR';
      notifyListeners();
    });
  }

  void setDropdown(BuildContext context, String newValue) {
    floorDropDownValue = newValue;
    fromDate = DateTime(fromDate.year, fromDate.month, fromDate.day, 1, 59, 59);
    toDate = DateTime(toDate.year, toDate.month, toDate.day, 23, 59, 59);
    funGetFloorData(context, fromDate, toDate);
    notifyListeners();
  }
}
