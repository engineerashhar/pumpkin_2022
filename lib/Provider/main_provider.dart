import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:confetti/confetti.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/OrderSection/table_listing.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:image/image.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'order_provider.dart';



class MainProvider with ChangeNotifier{

  final FirebaseFirestore db = FirebaseFirestore.instance;
  List<String> tokenDisplays= [];
  List<String> tokens= [];
  final FlutterTts flutterTts = FlutterTts();
  AssetsAudioPlayer audioPlayer = AssetsAudioPlayer();
   ConfettiController controllerCenter=ConfettiController();










  // late Canvas canvas;

  bool isPhone=true;
  List<String> MenuGS = ["CHANGE TABLE","REFERESH TABLE","Bill WITH CUSTOMER DATA","FOOD AND SERVICE"];
  List<String> MenuIcon =  ["assets/swap.png","assets/refresh.png","assets/bill_customer.png","assets/food_service.png"];

  TextEditingController employId =  TextEditingController();
  TextEditingController password =  TextEditingController();
  GlobalKey globalKey = GlobalKey();

MainProvider(){
}

  void fun_Snackbar(String msg, BuildContext context) {
    final snackBar = SnackBar(
        backgroundColor:timeout_snack ,
        duration: Duration(milliseconds: 3000),
        content: Text('${msg}', textAlign: TextAlign.center,softWrap: true,
          style: snackbarStyle,
        ));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    notifyListeners();

  }

  changeController(){
    isPhone=false;
    notifyListeners();
  }

loginCheck(String uid,String passWord,BuildContext context){
    db.collection("Users").doc(uid).get().then((DocumentSnapshot snapshot) async {
      if(snapshot.exists&&snapshot.get("Password").toString()==passWord){
        SharedPreferences userPreference = await SharedPreferences.getInstance();
        userPreference.setString("Uid", uid);
        userPreference.setString("PassWord", passWord);
        userPreference.setString("Name", snapshot.get("Name").toString());
        String strSelectedFloor='0';
        String strSelectedFloorName='';
        if(userPreference.getString("SelectedFloor")!=null){
          strSelectedFloor = userPreference.getString("SelectedFloor")!;
          strSelectedFloorName = userPreference.getString("SelectedFloorName")!;
        }
        OrderProvider _orderprovider = Provider.of<OrderProvider>(context,listen: false);
        _orderprovider.funFloorChange(strSelectedFloor,strSelectedFloorName);
        _orderprovider.fun_tableListing(strSelectedFloor);
        _orderprovider.funcbillData();
        _orderprovider.fun_initPlatformState();
        _orderprovider.funSteward();
        _orderprovider.funCustomers();
        _orderprovider.funGetTokens();

        finish(context);
        callNext(TableListing(), context);
      }else{
        finish(context);

        Widget okButton = TextButton(
          child: Text("OK"),
          onPressed: () {
            finish(context);
          },
        );

        // set up the AlertDialog
        AlertDialog alert = AlertDialog(
          title: Text("Error"),
          content: Text("Wrong EmbloyID Is or PassWord"),
          actions: [
            okButton,
          ],
        );

        // show the dialog
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );
      }
    });
}



  Future<void> printUsingWifi(List<int> bytes) async {
    const PaperSize paper = PaperSize.mm80;
    final profile = await CapabilityProfile.load();
    final printer = NetworkPrinter(paper, profile);
    final PosPrintResult res = await printer.connect('192.168.1.100', port: 9100);
    printer.beep(n: 1, duration: PosBeepDuration.beep150ms);

    if (res == PosPrintResult.success) {

      Duration timeout = const Duration(seconds: 5);
      var result = await Socket.connect('192.168.1.100', 9100, timeout: timeout).then((Socket socket) {
        socket.add(bytes);

        socket.destroy();
        printer.disconnect();
        notifyListeners();
        return 'Data sent Success';
      }).catchError((dynamic e) {
        return 'Error sending Data';
      });
      print(result);
    }

    print(' res  ${res.msg}');
  }
  @override
  void paint(Canvas canvas, Size size) {
    final textStyle = TextStyle(
      color: Colors.black,
      fontSize: 30,
    );
    final textSpan = TextSpan(
      text: 'Hello, world.',
      style: textStyle,
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
    );
    textPainter.layout(
      minWidth: 0,
      maxWidth: size.width,
    );
    final xCenter = (size.width - textPainter.width) / 2;
    final yCenter = (size.height - textPainter.height) / 2;
    final offset = Offset(xCenter, yCenter);
    textPainter.paint(canvas, offset);
  }

  void printtestData(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    // final p1 = Offset(50, 50);
    // final p2 = Offset(250, 150);
    //
    // final paint = Paint();
    // paint.color=Colors.black;
    // paint.strokeWidth=4;
    // canvas.drawLine(p1, p2, paint);


 
    print('ok');
    const esc = '\x1B';
    const gs = '\x1D';
    const fs = '\x1C';

    const reset = '$esc@'; // Initialize printer
    const underline = '$esc-1';
    const noUnderline = '$esc-0';
    const whiteOnBlack = '${esc}4';
    const noWhiteOnBlack = '${esc}5';
    const doubleWide = '${esc}W1';
    const doubleHeight = '${esc}h1';
    const tripleWide = '${esc}W2';
    const tripleHeight = '${esc}h2';
    const cutPaper = '${esc}d3';

    List<int> bytes = [];
    bytes += reset.codeUnits;

    bytes += tripleWide.codeUnits;
    bytes += tripleHeight.codeUnits;
    bytes += utf8.encode('നമസ്കാരം സർ  \n');
    bytes += doubleWide.codeUnits;
    bytes += doubleHeight.codeUnits;
    bytes += utf8.encode('Subtitle');
    bytes += reset.codeUnits;


    bytes += utf8.encode('Normal Text \n');
    bytes += whiteOnBlack.codeUnits;
    bytes += utf8.encode('White Text with black background\n');
    bytes += reset.codeUnits;

    bytes += underline.codeUnits;
    bytes += noUnderline.codeUnits;
    bytes += utf8.encode('Underlined Text \n');

    bytes += cutPaper.codeUnits;



    printTicketViaLan(bytes);

    notifyListeners();
  }
  void printTicketViaLan(List<int> bytes) async {
    // Duration timeout = const Duration(seconds: 5);
    // var result = await Socket.connect('192.168.1.100', 9100, timeout: timeout)
    //     .then((Socket socket) {
    //
    //   socket.;
    //   socket.add(bytes);
    //   socket.destroy();
    //   return 'Data sent Success';
    // }).catchError((dynamic e) {
    //   return 'Error sending Data';
    // });
    // print(result);


    const PaperSize paper = PaperSize.mm80;
    final profile = await CapabilityProfile.load();
    final printer = NetworkPrinter(paper, profile);
    final PosPrintResult res = await printer.connect('192.168.1.100', port: 9100);

    if (res == PosPrintResult.success) {
      await printFloorC(printer);
    }
    else {

    }

  }

  printFloorC(NetworkPrinter printer) async {
    int _width = 558;
    int _height = 40;
    ui.PictureRecorder recorder = new ui.PictureRecorder();

    Paint _paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4.0;

    Canvas c = new Canvas(recorder);
    TextSpan span = new TextSpan(style: new TextStyle( fontSize: 40, color: my_black,fontFamily: 'Roboto',fontWeight: FontWeight.bold), text: "നമസ്കാരം");
    TextPainter tp = new TextPainter(text: span, textDirection: TextDirection.rtl,textAlign: TextAlign.center);
    tp.layout();
    tp.paint(c, new Offset(180.0, 0.0));

    ui.Picture p = recorder.endRecording();
    ui.Image _uiImg = await p.toImage(_width, _height); //.toByteData(format: ImageByteFormat.png);

    // ByteData? _byteData = await _uiImg.toByteData(format: ui.ImageByteFormat.png);
    final ByteData? data = await _uiImg.toByteData(format: ui.ImageByteFormat.png);
    final Uint8List bytes = data!.buffer.asUint8List();
    var image = decodeImage(bytes);
    printer.image(image!);
    printer.text(
      '${"08/11/2021 8:00 AM"}',
      styles: PosStyles(
        align: PosAlign.center,
        height: PosTextSize.size1,
        width: PosTextSize.size1,
      ),
    );
    printer.hr();
    printer.feed(2);
    printer.text('**<#>..${"ddd"}..<#>**',
        styles: PosStyles(align: PosAlign.center, bold: true));
    printer.feed(1);
    printer.cut();
    printer.disconnect();
  }





  Future<ByteData?> getImageByteData() async {
    int _width = 100;
    int _height = 500;
    ui.PictureRecorder recorder = new ui.PictureRecorder();

    Paint _paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4.0;

    Canvas c = new Canvas(recorder);
    // c.drawRRect(RRect.fromLTRBAndCorners(20, 30, 40, 50), _paint);
    // _paint.color = Colors.red;
    // c.drawRect(Rect.fromLTWH(10, 10, 10, 10), _paint);
    // _paint.color = Colors.blue;
    // c.drawRect(
    //     Rect.fromCenter(center: Offset(50, 50), height: 50, width: 50), _paint);
    // _paint.color = Colors.black;
    // c.drawRect(
    //     Rect.fromPoints(
    //         Offset(0, 0), Offset(_width.toDouble(), _height.toDouble())),
    //     _paint);
    
    // c.drawPaint(Paint());
    //   final offset = Offset(xCenter, yCenter);etc
   // final Size size=Size(300, 300);
   //  final textStyle = TextStyle(
   //    color: Colors.black,
   //    fontSize: 30,
   //  );
   //  final textSpan = TextSpan(
   //    text: 'Hello, world.',
   //    style: textStyle,
   //  );
   //  final textPainter = TextPainter(
   //    text: textSpan,
   //    textDirection: TextDirection.ltr,
   //  );
   //  textPainter.layout(
   //    minWidth: 0,
   //    maxWidth: size.width,
   //  );
   //  final xCenter = (size.width - textPainter.width) / 2;
   //  final yCenter = (size.height - textPainter.height) / 2;
   //  final offset = Offset(xCenter, yCenter);
   //  textPainter.paint(c, offset);

    ui.Picture p = recorder.endRecording();
    ui.Image _uiImg = await p.toImage(_width, _height); //.toByteData(format: ImageByteFormat.png);

    ByteData? _byteData = await _uiImg.toByteData(format: ui.ImageByteFormat.png);
    return _byteData;
  }

  fetchTokens(BuildContext context) async {

    db.collection('Bills').where("Floor",isEqualTo: "4").where("TableNumber",isEqualTo:'TA' ).where('Status',isEqualTo: 'BillPrinted').snapshots().listen((event) async {
      tokens.clear();
      if(event.docs.isNotEmpty){

        if(tokenDisplays.isNotEmpty){
          for (var element in event.docs) {

            if(!tokenDisplays.contains(element.get('SubTable').toString())){

              await AssetsAudioPlayer.newPlayer().open(
                Audio("assets/airoplane.m4a"),

                showNotification: true,
              );
              await flutterTts.setLanguage("en-US");
              await flutterTts.setSpeechRate(0.4);
              controllerCenter = ConfettiController(duration: const Duration(seconds: 10));

              Future.delayed(Duration(milliseconds: 2500), () async {
                await flutterTts.speak("Token Number "+element.get('SubTable').toString());
                _openCustomDialog(context,element.get('SubTable').toString());
                controllerCenter.play();
              });

              Future.delayed(Duration(milliseconds: 6000), () async {
                await flutterTts.speak("Token Number "+element.get('SubTable').toString());
              });

              tokenDisplays.add(element.get('SubTable').toString());

            }

            notifyListeners();

          }
        }
        else{
          if(event.docs.length!=1){
            for (var element in event.docs) {

              tokenDisplays.add(element.get('SubTable').toString());

              notifyListeners();

            }
          }else{
            for (var element in event.docs) {

              AssetsAudioPlayer.newPlayer().open(
                Audio("assets/airoplane.m4a"),
                showNotification: true,
              );
              await flutterTts.setLanguage("en-US");
              await flutterTts.setSpeechRate(0.4);

              Future.delayed(Duration(milliseconds: 2500), () async {
                await flutterTts.speak("Token Number "+element.get('SubTable').toString());
                _openCustomDialog(context,element.get('SubTable').toString());

              });

              Future.delayed(Duration(milliseconds: 4500), () async {
                await flutterTts.speak("Token Number "+element.get('SubTable').toString());
              });

              tokenDisplays.add(element.get('SubTable').toString());

              notifyListeners();

            }
          }

        }


        tokens.clear();

        for (var element in event.docs) {
          tokens.add(element.get('SubTable').toString());
          notifyListeners();

        }

        notifyListeners();


      }
      notifyListeners();

    });
    notifyListeners();
  }




  void _openCustomDialog(BuildContext context,String text) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    showGeneralDialog(barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: AlertDialog(

                shape: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(16.0)),
                content: Container(
                  height: queryData.size.height,
                  width: queryData.size.width,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/singlrtoken.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Consumer<MainProvider>(
                            builder: (context,value,child) {

                              return ConfettiWidget(
                                confettiController: controllerCenter,
                                blastDirectionality: BlastDirectionality
                                    .explosive, // don't specify a direction, blast randomly
                                shouldLoop:
                                true, // start again as soon as the animation is finished
                                colors: const [
                                  Colors.green,
                                  Colors.blue,
                                  Colors.pink,
                                  Colors.orange,
                                  Colors.purple
                                ], // manually specify the colors to be used
                                createParticlePath: drawStar, // define a custom shape/path.
                              );
                            }
                        ),
                      ),
                      Container(

                          child: Center(
                            child: AutoSizeText(text,style: TextStyle(
                                fontFamily: 'Berlin',
                                fontSize: 500,
                                fontWeight: FontWeight.bold
                            ),
                            maxLines: 1,
                            ),
                          )),

                    ],

                  ),
                ),
              ),
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 500),
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return Text('PAGE BUILDER');

        });
    Future.delayed(Duration(milliseconds: 6000), (){
      finish(context);
    });
  }
  Path drawStar(Size size) {
    // Method to convert degree to radians
    double degToRad(double deg) => deg * (pi / 180.0);

    const numberOfPoints = 5;
    final halfWidth = size.width / 2;
    final externalRadius = halfWidth;
    final internalRadius = halfWidth / 2.5;
    final degreesPerStep = degToRad(360 / numberOfPoints);
    final halfDegreesPerStep = degreesPerStep / 2;
    final path = Path();
    final fullAngle = degToRad(360);
    path.moveTo(size.width, halfWidth);

    for (double step = 0; step < fullAngle; step += degreesPerStep) {
      path.lineTo(halfWidth + externalRadius * cos(step),
          halfWidth + externalRadius * sin(step));
      path.lineTo(halfWidth + internalRadius * cos(step + halfDegreesPerStep),
          halfWidth + internalRadius * sin(step + halfDegreesPerStep));
    }
    path.close();
    return path;
  }

}
class LabelPainter {
}