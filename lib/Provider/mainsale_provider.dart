import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Views/barchart_model.dart';
import 'package:flutter_pumpkin/Views/cancelled_gs.dart';
import 'package:flutter_pumpkin/Views/counter_pie_model.dart';
import 'package:flutter_pumpkin/Views/itemwise_master.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:charts_flutter/flutter.dart' as charts;


import 'order_provider.dart';

class MainSaleProvider with ChangeNotifier{
  DateRangePickerController dateRangePickerController = DateRangePickerController();
  DateTime fromDate =  DateTime.now();
  DateTime toDate =  DateTime.now();
  final List<ChartGS> data = [];


  double dTotalSaleAmount=0.0;
  double dTotalSettledAmount=0.0;
  double dTotalPendingAmount=0.0;
  double dTotalBilledAmount=0.0;
  double notBillPrinted=0.0;
  double dSetteledCount=0.0;
  double dSetteledPerc=0.0;
  double dPendingCount=0.0;
  double dPendingPerc=0.0;
  double dTotalCount=0.0;
  double dRegeneratedCount=0.0;
  List<CounterPieGS> paymentWisePieGS = [];
  List<ItemWiseGS>itemWiseMainGS=[];
  List<ItemWiseGS>stewardMainWiseGS=[];
  List<String>itemCheck=[];
  List<String>stewardCheck=[];
  List<String>regeneratedBills=[];
  List<CancelledGS>cancelledGS=[];
  final DatabaseReference mRootReference = FirebaseDatabase.instance.reference();
  final FirebaseFirestore db = FirebaseFirestore.instance;


  double dTotalAmount = 0.0;
  double dTaxbleAmount = 0.0;
  double dNonTaxableAmount = 0.0;
  List<String> paymentCheckArray = [];
  List<DateTime> fromDateTime = [];
  List<String>timeCheck=[];

  List<DateTime> toDateTime = [];
  List<charts.Series<ChartGS, String>> series = [];
  int cancelledBillsCount=0;
  StreamSubscription? orderStream;
  StreamSubscription? reGeneratedStream;
  StreamSubscription? billsDbStream;



  MainSaleProvider(){


  }
  Future<void> selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2101));
    if (picked != null) {
      fromDate=DateTime(picked.year, picked.month, picked.day, 1, 59,59);
      toDate=DateTime(picked.year, picked.month, picked.day, 23, 59,59);

      funGetSaleData(context,fromDate, toDate);

    }
    notifyListeners();
  }

  void funGetSaleData(BuildContext context,DateTime fromDate, DateTime toDate) {
    showLoaderDialog(context);


    String dayNode = fromDate.year.toString() + "/" + fromDate.month.toString() + "/" + fromDate.day.toString();
    if(orderStream!=null){
      orderStream!.cancel();
    }
    orderStream =mRootReference.child("Orders").child(dayNode).onValue.listen((DatabaseEvent databaseEvent){
      dTotalSaleAmount=0.0;
      notBillPrinted=0.0;
      itemWiseMainGS.clear();
      itemCheck.clear();
      cancelledBillsCount=0;

      databaseEvent.snapshot.children.forEach((element) {
        Map<dynamic, dynamic>floorMap =element.value as Map;
        floorMap.forEach((floor, floorvalue) {
          // Stream<DatabaseEvent> floorStream = element.ref.child(floor).onValue;
          element.ref.child(floor).once().then((DatabaseEvent eventfloor) {
            eventfloor.snapshot.children.forEach((tableelement) {
              Map<dynamic, dynamic>tableMap =tableelement.value as Map;
              tableMap.forEach((table, tablevalue) {

                tablevalue.forEach((itemID,itemValue){

                  // print(' floor  ${floor} tableelement  ${tableelement.key} table  ${table} itemID  ${itemID}');
                  double dQty=0.0;
                  double dRate=0.0;
                  if(itemValue['Rate']!=null && itemValue['Rate'].toString()!=''){
                    dRate=double.tryParse(itemValue['Rate'].toString())!;
                  }
                  if(itemValue['Quantity']!=null && itemValue['Quantity'].toString()!=''){
                    dQty=double.tryParse(itemValue['Quantity'].toString())!;
                  }
                  dTotalSaleAmount=dTotalSaleAmount+(dQty*dRate);
                  // print('dTotalSaleAmount 1  ${dTotalSaleAmount}');
                  if(itemValue['ItemID']!=null){
                    if(!itemCheck.contains(itemValue['ItemID'].toString())){
                      double itemPrice=double.parse(itemValue['Quantity'])*double.parse(itemValue['Rate']);
                      itemWiseMainGS.add(ItemWiseGS(itemValue['ItemID'].toString(),itemValue['Item'].toString(),
                          itemValue['Quantity'].toString(),itemPrice.toString()));
                      itemCheck.add(itemValue['ItemID'].toString());
                    }else{
                      var index = itemWiseMainGS.indexWhere((user) => user.itemID == itemValue['ItemID'].toString());
                      if(index > -1){
                        int itemCount=int.tryParse(itemWiseMainGS[index].itemCount.toString())!;
                        int newCount=itemCount+1;
                        double itemPrice=double.parse(itemWiseMainGS[index].itemPrice.toString());
                        double newPrice=itemPrice+(double.parse(itemValue['Quantity'])*double.parse(itemValue['Rate']));
                        itemWiseMainGS[index].itemCount = newCount.toString();
                        itemWiseMainGS[index].itemPrice = newPrice.toString();
                      }
                    }
                  }
                  itemWiseMainGS.sort(
                          (a, b) => double.parse(b.itemPrice).compareTo(double.parse(a.itemPrice)));
                  if(itemValue['CancelledQuantity'] !=null ){
                    cancelledBillsCount++;
                    cancelledGS.add(CancelledGS(itemValue['Item'].toString(), itemValue['CancelledQuantity'].toString()));
                  }
                  notifyListeners();
                });
              });
            });
          });
        });
      });

      getBillsData(context,fromDate,toDate);
      getRegeneratedBills(context,fromDate,toDate);
      getDateTime(context,fromDate,toDate);





    });
    Future.delayed(const Duration(milliseconds: 7000), (){
      finish(context);
    });

  }

  void getBillsData(BuildContext context, DateTime fromDate, DateTime toDate) {
    if(billsDbStream!=null){
      billsDbStream!.cancel();
    }
    billsDbStream=db.collection("Bills").where("BillingTime", isGreaterThan:fromDate).where("BillingTime", isLessThan:toDate ).snapshots().listen((event) async {
      dSetteledCount=0.0;
      dPendingCount=0.0;
      dSetteledPerc=0.0;
      dTotalSettledAmount=0.0;
      dTotalBilledAmount=0.0;
      dTotalPendingAmount=0.0;
      dPendingPerc=0.0;
      stewardMainWiseGS.clear();
      stewardCheck.clear();
      dTotalAmount = 0.0;
      dNonTaxableAmount = 0.0;
      paymentWisePieGS.clear();
      paymentCheckArray.clear();
      dTotalCount=0.0;

      if(event.docs.isNotEmpty){
        int i=0;


        for (var element in event.docs) {
          i++;
          dTotalCount=dTotalCount+1;


          if(element.get('Steward')!=null){

            if(!stewardCheck.contains(element.get('Steward').toString())){
              if( Provider.of<OrderProvider>(context,listen: false).stewardsIds.contains(element.get('Steward').toString())){
                String stewardName='';
                int count=1;

                // print('${Provider.of<OrderProvider>(context,listen: false).stewards}');
                // print('${Provider.of<OrderProvider>(context,listen: false).stewardsIds}');
                stewardName= Provider.of<OrderProvider>(context,listen: false).stewards[Provider.of<OrderProvider>(context,listen: false).stewardsIds.indexOf(element.get('Steward').toString())];
                stewardMainWiseGS.add(ItemWiseGS(element.get('Steward').toString(), stewardName, count.toString(),'0'));
                stewardCheck.add(element.get('Steward').toString());
              }
            }else{
              var index = stewardMainWiseGS.indexWhere((user) => user.itemID == element.get('Steward').toString());
              if(index > -1){
                int itemCount=int.tryParse(stewardMainWiseGS[index].itemCount.toString())!;
                int newCount=itemCount+1;
                stewardMainWiseGS[index].itemCount = newCount.toString();
              }
            }
          }
          stewardMainWiseGS.sort(
                  (a, b) => double.parse(b.itemCount).compareTo(double.parse(a.itemCount)));
          double amount = 0.0;
          double taxAm = 0.0, nonTaxm = 0.0, cash = 0.0, card = 0.0, payBack = 0.0, credit = 0.0, discount = 0.0, roundOff = 0.0, cgst = 0.0, sgst = 0.0;
          if (element.get("TaxableAmount")!= null) {
            taxAm = double.tryParse(element.get("TaxableAmount").toString())!;
          }
          if (element.get("NontaxableAmount") != null) {
            nonTaxm = double.tryParse(element.get("NontaxableAmount").toString())!;
          }


          if (element.get("TotalAmount") != null) {
            amount=double.tryParse(element.get("TotalAmount").toString())!;
          }

          if (element.get("Status") != null) {
            if (element.get("Status") == 'BillPayed') {
              dSetteledCount=dSetteledCount+1;
              dTotalSettledAmount = dTotalSettledAmount + taxAm + nonTaxm;


              if (element.get("CashCounter") != null) {
                double taxAmount = 0.0,
                    nonTaxAmount = 0.0,
                    totalAmount = 0.0;
                double cashAmount = 0.0,
                    upiAmount = 0.0,
                    cardAmount = 0.0,
                    creditAmount = 0.0;
                if (element.get("TaxableAmount") != null) {
                  taxAmount = double.tryParse(element.get("TaxableAmount").toString())!;
                }
                if (element.get("NontaxableAmount") != null) {
                  nonTaxAmount = double.tryParse(
                      element.get("NontaxableAmount").toString())!;
                }
                if (element.get("TotalAmount") != null) {
                  totalAmount =
                  double.tryParse(element.get("TotalAmount").toString())!;
                }
                dTotalAmount = dTotalAmount + totalAmount;
                dTaxbleAmount = dTaxbleAmount + taxAmount;
                dNonTaxableAmount = dNonTaxableAmount + nonTaxAmount;
                // print('dTaxbleAmount  ${dTaxbleAmount}');
                // print('dNonTaxableAmount  ${dNonTaxableAmount}');

                if (element.get("Cash") != null) {
                  cashAmount = double.tryParse(element.get("Cash").toString())!;
                  // print('cashAmount  ${cashAmount}');
                  if (!paymentCheckArray.contains('Cash')) {paymentWisePieGS.add(CounterPieGS(
                      "Cash", cashAmount, cashColor, "Cash",
                      "Cash : ${cashAmount}"));
                  paymentCheckArray.add('Cash');
                  }
                  else {
                    var index = paymentWisePieGS.indexWhere((user) =>
                    user.type == 'Cash');
                    if (index > -1) {
                      double itemCount = paymentWisePieGS[index].total;
                      double newCount = itemCount + cashAmount;
                      paymentWisePieGS[index].total = newCount;paymentWisePieGS[index].labelData =
                      "Cash : ${newCount}";
                    }
                  }
                }

                if (element.get("Card") != null) {
                  cardAmount =
                  double.tryParse(element.get("Card").toString())!;

                  if (!paymentCheckArray.contains('Card')) {
                    paymentWisePieGS.add(
                        CounterPieGS("Card", cardAmount, cardColor, "Card",
                            "Card : ${cardAmount}"));
                    paymentCheckArray.add('Card');
                  }
                  else {
                    var index = paymentWisePieGS.indexWhere((user) =>
                    user.type == 'Card');
                    if (index > -1) {
                      double itemCount = paymentWisePieGS[index].total;
                      double newCount = itemCount + cardAmount;
                      paymentWisePieGS[index].total = newCount;
                      paymentWisePieGS[index].labelData =
                      "Card : ${newCount}";
                    }
                  }
                }

                if (element.get("Credit") != null) {
                  creditAmount =
                  double.tryParse(element.get("Credit").toString())!;

                  if (!paymentCheckArray.contains('Credit')) {
                    paymentWisePieGS.add(CounterPieGS(
                        "Credit", creditAmount, creditColor, "Credit",
                        "Credit : ${creditAmount}"));
                    paymentCheckArray.add('Credit');
                  }
                  else {
                    var index = paymentWisePieGS.indexWhere((user) =>
                    user.type == 'Credit');
                    if (index > -1) {
                      double itemCount = paymentWisePieGS[index].total;
                      double newCount = itemCount + creditAmount;
                      paymentWisePieGS[index].total = newCount;
                      paymentWisePieGS[index].labelData =
                      "Credit : ${newCount}";
                    }
                  }
                }

                if (element.get("UPI") != null) {
                  upiAmount = double.tryParse(element.get("UPI").toString())!;
                  if (!paymentCheckArray.contains('UPI')) {
                    paymentWisePieGS.add(
                        CounterPieGS("UPI", upiAmount, upiColor, "UPI",
                            "UPI : ${upiAmount}"));
                    paymentCheckArray.add('UPI');
                  }
                  else {
                    var index = paymentWisePieGS.indexWhere((user) =>
                    user.type == 'UPI');
                    if (index > -1) {
                      double itemCount = paymentWisePieGS[index].total;
                      double newCount = itemCount + upiAmount;
                      paymentWisePieGS[index].total = newCount;
                      paymentWisePieGS[index].labelData = "UPI : ${newCount}";
                    }
                  }
                }
                notifyListeners();
              }
            }
            // if (element.get("Status") == 'BillPrinted') {
            //   dTotalBilledAmount=dTotalBilledAmount+taxAm + nonTaxm;
            // }
          }


          Timestamp timestamp = element.get('BillingTime');
          DateTime time = DateTime.parse(timestamp.toDate().toString());
          notifyListeners();
        }

        dSetteledPerc=(dSetteledCount*100)/dTotalCount;
        if(dSetteledPerc >0.0){
          dSetteledPerc=dSetteledPerc/100;
        }
        else{
          dSetteledPerc=0.0;
        }
        dPendingCount=dTotalCount-dSetteledCount;
        dPendingPerc=(dPendingCount*100)/dTotalCount;


        if(dPendingPerc >0.0){
          dPendingPerc=dPendingPerc/100;
        }else{
          dPendingPerc=0.0;
        }
        dTotalPendingAmount=dTotalSaleAmount-dTotalSettledAmount;
        notifyListeners();

      }
      else{


        dSetteledPerc=(dSetteledCount*100)/dTotalCount;
        if(dSetteledPerc >0.0){
          dSetteledPerc=dSetteledPerc/100;
        }else{
          dSetteledPerc=0.0;
        }

        dPendingCount=dTotalCount-dSetteledCount;
        dPendingPerc=(dPendingCount*100)/dTotalCount;
        if(dPendingPerc >0.0){
          dPendingPerc=dPendingPerc/100;
        }else{
          dPendingPerc=0.0;
        }

        notifyListeners();
      }

    });

  }

  void getDateTime(BuildContext context, DateTime fromDate11, DateTime toDate) {
    DateTime nineAmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 9, 0,0);
    DateTime nineAmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 9, 59,59);
    DateTime tenAmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 10, 0,0);
    DateTime tenAmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 10, 59,59);
    DateTime elevenAmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 11, 0,0);
    DateTime elevenAmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 11, 59,59);
    DateTime twelevePmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 12, 0,0);
    DateTime twelevePmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 12, 59,59);
    DateTime onePmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 13, 0,0);  //1pm
    DateTime onePmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 13, 59,59);
    DateTime twoPmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 14, 0,0);  //2pm
    DateTime twoPmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 14, 59,59);
    DateTime threePmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 15, 0,0);  //3pm
    DateTime threePmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 15, 59,59);
    DateTime fourPmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 16, 0,0);  //4pm
    DateTime fourPmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 16, 59,59);
    DateTime fivePmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 17, 0,0);  //5pm
    DateTime fivePmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 17, 59,59);
    DateTime sixPmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 18, 0,0);  //6pm
    DateTime sixPmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 18, 59,59);
    DateTime sevenPmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 19, 0,0);  //7pm
    DateTime sevenPmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 19, 59,59);
    DateTime eightPmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 20, 0,0);  //8pm
    DateTime eightPmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 20, 59,59,);
    DateTime ninePmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 21, 0,0);  //9pm
    DateTime ninePmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 21, 59,59,);
    DateTime tenPmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 22, 0,0);  //10pm
    DateTime tenPmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 22, 59,59,);
    DateTime elevenPmFrom=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 23, 0,0);  //11pm
    DateTime elevenPmTo=DateTime(fromDate11.year, fromDate11.month, fromDate11.day, 23, 59,59,);
    fromDateTime.clear();
    toDateTime.clear();
    fromDateTime=[nineAmFrom,tenAmFrom,elevenAmFrom,twelevePmFrom,onePmFrom,twoPmFrom, threePmFrom,fourPmFrom,fivePmFrom,sixPmFrom,sevenPmFrom,eightPmFrom,ninePmFrom,tenPmFrom,elevenPmFrom];
    toDateTime=[nineAmTo,tenAmTo,elevenAmTo,twelevePmTo,onePmTo,twoPmTo,threePmTo,fourPmTo,fivePmTo,sixPmTo,sevenPmTo,eightPmTo,ninePmTo,tenPmTo,elevenPmTo];

    data.clear();
    timeCheck.clear();
    int jj=0;
    fromDateTime.forEach((element) {
      // print('element  ${element}');

      DateTime toDate11=toDateTime[fromDateTime.indexOf(element)];
       db.collection("Bills").where("BillingTime", isGreaterThan:element).where("BillingTime", isLessThan:toDate11 ).snapshots().listen((event) async {
        if(event.docs.isNotEmpty){
          int mm=0;
           double dcalcAmount=0.0;
          // print('  fromDate11  ${fromDate11}   toDate11  ${toDate11} docs ${ event.docs.length}');

          for (var element2 in event.docs) {
            double amount=0.0;
            if (element2.get("TotalAmount") != null) {
              amount=double.tryParse(element2.get("TotalAmount").toString())!;
            }
            dcalcAmount=dcalcAmount+amount;
            mm++;

            if(event.docs.length==mm){
              data.add(ChartGS(element.hour.toString(),dcalcAmount,element.hour));
              jj++;
              // print('  if hour ${element.hour.toString()} dcalcAmount  ${dcalcAmount} event.docs.length  ${event.docs.length}');
            }
          }

        }
        else{
          data.add(ChartGS(element.hour.toString(), 0.0,element.hour));
          // print('else hour  ${element.hour.toString()} dcalcAmount  ${0.0}');
          jj++;

        }



        // if(fromDateTime.length==jj){
        // }
        data.sort((a, b) => a.hour.compareTo(b.hour));

        series=[
          charts.Series(
              id: "Subscribers",
              data:  Provider.of<MainSaleProvider>(context,listen: false).data,
              domainFn: (ChartGS series, _) => series.year.toString(),
              measureFn: (ChartGS series, _) => series.subscribers,
              colorFn: (_, __) => charts.Color.fromHex(code:'#099f9b'),
              labelAccessorFn: (ChartGS row, _) => '${row.subscribers.toString()}')
        ];
        notifyListeners();



       });



      notifyListeners();

    });

    // List<charts.Series<ChartGS, String>> series = [
    //   charts.Series(
    //       id: "Subscribers",
    //       data:  Provider.of<MainSaleProvider>(context,listen: false).data,
    //       domainFn: (ChartGS series, _) => series.year.toString(),
    //       measureFn: (ChartGS series, _) => series.subscribers,
    //       colorFn: (_, __) => charts.Color.fromHex(code:'#099f9b'),
    //       labelAccessorFn: (ChartGS row, _) => '${row.subscribers.toString()}')
    // ];
    notifyListeners();







  }

  void getRegeneratedBills(BuildContext context, DateTime fromDate, DateTime toDate) {
    print('ok');
    String dayNode = fromDate.year.toString() + "/" + fromDate.month.toString() + "/" + fromDate.day.toString();
    if(reGeneratedStream!=null){
      reGeneratedStream!.cancel();
    }
    reGeneratedStream=mRootReference.child("ReGeneratedBills").child(dayNode).onValue.listen((DatabaseEvent databaseEvent){
      regeneratedBills.clear();
      if(databaseEvent.snapshot.value!=null){
        Map<dynamic, dynamic>floorMap =databaseEvent.snapshot.value as Map;
        dRegeneratedCount=0.0;
        floorMap.forEach((key, value) {
          dRegeneratedCount++;
          regeneratedBills.add(key);
            print('key  ${key}  value  ${value}');
        });

      }else{
        dRegeneratedCount=0.0;

      }
      notifyListeners();



    });


  }






}