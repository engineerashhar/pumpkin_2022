// ignore_for_file: deprecated_member_use

import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/OrderSection/table_listing.dart';
import 'package:flutter_pumpkin/Views/cancel_printer_gs.dart';
import 'package:flutter_pumpkin/Views/categoryGS.dart';
import 'package:flutter_pumpkin/Views/daybillGS.dart';
import 'package:flutter_pumpkin/Views/dishGS.dart';
import 'package:flutter_pumpkin/Views/dish_item_gs.dart';
import 'package:flutter_pumpkin/Views/final_itemGS.dart';
import 'package:flutter_pumpkin/Views/floorGS.dart';
import 'package:flutter_pumpkin/Views/itemGS.dart';
import 'package:flutter_pumpkin/Views/old_token_gs.dart';
import 'package:flutter_pumpkin/Views/printerGS.dart';
import 'package:flutter_pumpkin/Views/printer_re_gs.dart';
import 'package:flutter_pumpkin/Views/printer_status_gs.dart';
import 'package:flutter_pumpkin/Views/subtableGS.dart';
import 'package:flutter_pumpkin/Views/tableGS.dart';
import 'package:image/image.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'main_provider.dart';

class OrderProvider with ChangeNotifier {
  final DatabaseReference mRootReference =
      FirebaseDatabase.instance.reference();
  final FirebaseFirestore db = FirebaseFirestore.instance;
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};

  String SelectedFloor = '0';
  String selectedFloorName = '';
  List<TablesGS> tableGS = [];
  List<FloorGS> floorGS = [];
  List<DaybillGS> daybillGS = [];
  List<PrinterGS> printerGS = [];
  List<ItemGS> itemGS = [];
  List<FinalItemsGS> finalPrintitemG = [];
  List<DishGS> dishGS = [];
  List<CategoryGS> categoryGS = [];
  List<PrinterReGS> printerReGS = [];
  List<CancelPrinterReGS> cancelPrinterGS = [];
  List<String> categories = [];
  List<String> Itemkichens = [];
  List<String> reasons = [];

  List<String> costumersName = [];
  List<String> costumersPhone = [];
  List<String> costumersNamePhone = [];
  List<String> stewards = [];
  List<String> staffs = [];
  List<String> stewardsIds = [];
  List<String> staffsPasses = [];
  List<PrinterStatusGs> printerStatus = [];

  String stewardName = '';
  String? stewardID;
  String? customerName = "Customer";
  String? customerPhone = "0000000000";
  String uid = "";
  String loginEmbloy = "";
  String tmpTable = "";
  String tmpSubTable = "";

  String AddItemClick = "new";

  double? SubtableCount;
  double? halfSubtableCount;

//Add Order Item
  String FinishOrNext = "Next";

  TextEditingController qtyTc = TextEditingController();
  TextEditingController notesTc = TextEditingController();

  ///add customer
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController gstController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController landMarkController = TextEditingController();
  TextEditingController dateOfBirthController = TextEditingController();
  TextEditingController notesController = TextEditingController();
  bool errorText = true;
  bool moreOpen = false;
  String sDateOfBirth = "";
  String orderDayNode = '';
  bool noOrder = true;

  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  bool hasConnection = false;

  //This is how we'll allow subscribing to connection changes
  StreamController connectionChangeController = StreamController.broadcast();

  //flutter_connectivity

  //printing
  String FloorName = '';
  String FloorIP = '';
  bool isFloorConsolidated = true;
  String NikalaIP = '';
  bool isNikalaSeparated = false;
  bool isNikalaConsolidated = false;
  bool CancelationLogin = false;
  String cItem = "";
  String cItemHint = "Item";
  String cQty = "";
  String cQtyHint = "Qty";
  String cErrorText = "";
  bool isQty = false;

  String networkStatus = '';

  //printing

  //mainbillconfig

  String strLine1 = '';
  String strLine2 = '';
  String strLine3 = '';
  String strLine4 = '';
  String strLine5 = '';
  String strHotelName = '';
  String strDeviceID = '';

  bool printSuccess = true;
  bool isDisposed = true;
  List<int> printerBytes = [0x1b, 0x61, 0x1];
  List<int> printerBytesLeft = [0x1b, 0x61, 0x00];
  List<int> printerBytesRight = [0x1b, 0x61, 0x02];
  List<int> printerFeed = [10];
  List<int> printerBold = [0x1b, 0x45, 0x01];
  List<int> printerCancelBold = [0x1b, 0x45, 0x00];
  List<OldTokenGs> oldTokens = [];
  int tokenId = 1;
  double kotAmount = 0.0;
  String kotNumber = '';
  StreamSubscription? orderStream;
  StreamSubscription? orderStream2;
  StreamSubscription? dishStream;
  List<Map<String, Object>> NITEMS = [];
  List<String> NITEMCODE = [];

  //mainbillconfig

  OrderProvider() {
    DateTime now = DateTime.now();
    String DayNode = now.year.toString() +
        "/" +
        now.month.toString() +
        "/" +
        now.day.toString();
    mRootReference.child("Orders").child(DayNode).keepSynced(true);
    mRootReference.child("TableOrders").keepSynced(true);
    mRootReference.child("config").keepSynced(true);
    mRootReference.child("Devices").keepSynced(true);
    fun_Dishes();
    initConnectivity();
    fetchCategory();

    // checkConnection();
    notifyListeners();
  }

  @override
  void dispose() {
    isDisposed = false;

    print(' isDisposed ${isDisposed}');
    super.dispose();
  }

  //AlertData
  List<String> dishesname_list = [];
  List<String> dishesID_list = [];
  List<DishItemGS> dishesID_Name_list = [];

  List<String> finalItemIDarray = [];
  List<String> finalqty = [];
  List<String> finalID = [];

  String strItemID = "";
  String strItemName = "";
  String strItemRate = "";
  String strItemCategory = "";
  int iItemCount = 0;

  // bool printThermal = false;

  double dOrderTotal = 0.0;
  double dOrderTaxTotal = 0.0;
  int TableItemCoount = 0;
  int BillItemCountNT = 0,
      BillITemCountT = 0,
      LastUsedNumber = 0; //nontaxable count   //taxable count
  double ANT = 0.0, AT = 0.0; //Amount  non taxable , Amount taxable
  String strBillNo = '', Series = '';
  bool Regeneate = false, New = false;
  bool allOrderPrinted=false;
  int BillID = 0;
  Map<String, Object> Device = new HashMap();
  String kotID='';

  //finalPrint

  double dDiscount = 0.0;
  List<String> notes = ["PARCEL"];
  List<String> itemCategory = [];

  //finalPrint

  //AlertData
  void fun_Dish_addAlert(BuildContext context, String itemName, String itemID,
      int count, floor, String Notes) async {
    mRootReference
        .child("Dish")
        .child(itemID)
        .once()
        .then((DatabaseEvent databaseEvent) {
      Map<dynamic, dynamic>? ItemData = databaseEvent.snapshot.value as Map;
      strItemID = itemID;
      strItemName = itemName;
      strItemRate = ItemData["F" + floor];
      iItemCount = count;
      strItemCategory = ItemData['Category'];
      qtyTc.text = count.toString();
      notesTc.text = Notes;
      notifyListeners();
    });
  }

  //AlertData

  //OrderItemList
  void funPrintClear() {
    printerReGS.clear();
    cancelPrinterGS.clear();
    printSuccess = true;
  }

  void fun_orderItems(String floorId, String tableId, String subtableID) {
    tmpTable = tableId;
    tmpSubTable = subtableID;
    New = false;
    DDFirebase();
    getTokenID();
    noOrder = true;
    allOrderPrinted=true;
    if (orderStream != null) {
      orderStream!.cancel();
    }
    if (orderStream2 != null) {
      orderStream2!.cancel();
    }
    orderStream = mRootReference
        .child('TableOrders')
        .child('Floors')
        .child(floorId)
        .child('Tables')
        .child(tableId)
        .child('SubTables')
        .child(subtableID)
        .child('Orders')
        .onValue
        .listen((DatabaseEvent databaseEvent) async {
      if (databaseEvent.snapshot.value != null) {
        noOrder = false;

        final Map<dynamic, dynamic> MAINDATA =
            databaseEvent.snapshot.value as Map;

        if (MAINDATA.values.first['OrderRef'] != null) {
          orderStream2 = mRootReference
              .child(MAINDATA.values.first['OrderRef'])
              .parent!
              .onValue
              .listen((DatabaseEvent eventOrder) async {
            SharedPreferences userPreference =
                await SharedPreferences.getInstance();

            if (userPreference.getString('Floor') == floorId &&
                userPreference.getString('Table') == tableId &&
                userPreference.getString('SubTable') == subtableID) {
              try {
                print('MAINDATA.keys.first  ${MAINDATA.keys.first}');
                orderDayNode = funDateC(MAINDATA.keys.first);
              } catch (error) {}

              itemGS.clear();
              NITEMCODE.clear();
              NITEMS.clear();
              dOrderTotal = 0.0;
              dOrderTaxTotal = 0.0;
              ANT = 0;
              BillItemCountNT = 0;
              BillITemCountT = 0;
              AT = 0;
              String BN = '';
              bool ReGenerate = false;
              Regeneate = false;
              allOrderPrinted=true;


              int O = 0;
              funCalculate(kotNumber);

              if (eventOrder.snapshot.value != null) {
                final Map<dynamic, dynamic> orderData =
                    eventOrder.snapshot.value as Map;

                MAINDATA.forEach((orderID, orderValue) {
                  O++;
                  if (orderData[orderID] != null) {
                    final Map<dynamic, dynamic> DISHDATA = orderData[orderID];

                    int I = 0;

                    DISHDATA.forEach((id, value) {
                      if (value["Status"].toString() != "Cancelled") {
                        I++;
                        String Id = '';
                        String Name = '';
                        String Qty = '';
                        String Rate = '';
                        String Total = '';
                        String kitchen = '';
                        String Status = '';
                        String dbRef = '';
                        String floor = '';
                        String table = '';
                        String subTableId = '';
                        String productId = '';
                        String kotNumber = '';
                        String Notes = '';
                        String time = '';

                        double? dqty = 0.0;
                        double? dRate = 0.0;
                        double? dTotal = 0.0;

                        if (value['ReGenerated'] != 'null' &&
                            value['ReGenerated'] != 'NO') {
                          ReGenerate = true;
                          BN = value['ReGenerated'].toString();
                        }
                        print("Order " +
                            DISHDATA.keys.length.toString() +
                            " = " +
                            I.toString() +
                            " / " +
                            MAINDATA.keys.length.toString() +
                            " = " +
                            O.toString());
                        if (ReGenerate) {
                          strBillNo = BN;
                          Regeneate = true;
                        } else {
                          Regeneate = false;
                        }

                        if (value['Quantity'] != null) {
                          dqty = double.tryParse(value['Quantity']);
                        }
                        if (value['Rate'] != null) {
                          dRate = double.tryParse(value['Rate']);
                        }
                        if (value['Status'] == 'OrderPrinted' ||
                            value['Status'] == 'OrderTaken') {
                          TableItemCoount++;
                        }
                        if(value['Status'] == 'OrderTaken' ){
                          allOrderPrinted=false;
                        }
                        if (value['Taxablity'] == 'false') {
                          ANT = ANT + dqty! * dRate!;
                        } else {
                          BillITemCountT++;
                          AT = AT + dqty! * dRate!;
                        }

                        dTotal = dqty * dRate;
                        dOrderTotal = dOrderTotal + dTotal;

                        String path = orderValue['OrderRef'].toString() +
                            "/" +
                            id.toString();
                        // String path='Orders/'+funDateC(MAINDATA.keys.first)+'/'+floorId+'/'+tableId+'/'+subtableID.replaceAll('S', '')+'/'+orderID+'/'+id.toString();

                        Id = id.toString();
                        Name = value['Item'].toString();
                        Qty = value['Quantity'].toString();
                        Rate = value['Rate'].toString();
                        Total = dTotal.toString();
                        kitchen = value['Kitchen'].toString();
                        Status = value['Status'].toString();
                        dbRef = path;
                        floor = floorId;
                        table = tableId;
                        subTableId = subtableID;
                        productId = value['ItemID'].toString();
                        kotNumber = value['OrderID'].toString();
                        Notes = value["Note"].toString();
                        time = value["Time"].toString();

                        if (value['Quantity'].toString() != '0') {
                          if (value['Item'].toString() != 'DISCOUNT') {
                            if (!NITEMCODE.contains(value['ItemID'].toString() + value['Rate'].toString())) {

                              Map<String, Object> Item = HashMap();
                              Item['Item'] = Name;
                              Item['ItemID'] = productId;
                              Item['Quantity'] = Qty;
                              Item['Rate'] = Rate;
                              Item['Total'] = dTotal.toString();

                              NITEMCODE.add(productId + Rate);
                              NITEMS.add(Item);
                              print('Not contain ${productId + Rate} ');
                            }
                            else {
                              if (NITEMS.isNotEmpty) {
                                Map<String, Object> data = HashMap();
                                if (NITEMCODE.contains(productId + Rate)) {
                                  int index = NITEMCODE.indexOf(productId + Rate);
                                  print(' contain ${productId + Rate} ');


                                  data = NITEMS[NITEMCODE.indexOf(productId + Rate)];

                                  int tempQty = int.tryParse(data["Quantity"].toString())! + int.tryParse(Qty)!;
                                  data['Quantity'] = tempQty.toString();
                                  double? dQTY = double.tryParse(Qty);
                                  double? dRate = double.tryParse(Rate);
                                  double dTotal = double.tryParse(
                                          data["Total"].toString())! +
                                      (dQTY! * dRate!);
                                  data['Total'] = dTotal.toStringAsFixed(2);
                                  data['Item'] = Name;
                                  data['ItemID'] = productId;
                                  data['Rate'] = Rate;

                                  NITEMS[index] = data;
                                }
                              }
                            }
                            print('NITEMS SIZE  ${NITEMS.length}');
                            print('NITEMCODE SIZE  ${NITEMCODE.length}');
                          } else {
                            double? dq = double.tryParse(Qty.toString());
                            double? dR = double.tryParse(Rate);
                            dDiscount = (dq! * dR!);
                          }
                        }

                        itemGS.add(
                          ItemGS(
                            Id,
                            Name,
                            Qty,
                            Rate,
                            Total,
                            kitchen,
                            Status,
                            dbRef,
                            floor,
                            table,
                            subTableId,
                            productId,
                            kotNumber,
                            Notes,
                            time,
                          ),
                        );

                        notifyListeners();
                      } else {
                        I++;
                      }
                      itemGS.sort((a, b) => a.time.compareTo(b.time));
                    });
                    notifyListeners();
                  }

                  double CGST = (dOrderTotal * 2.5) / 100;
                  double SGST = (dOrderTotal * 2.5) / 100;
                  dOrderTaxTotal = dOrderTotal + CGST + SGST;
                  notifyListeners();
                });
              } else {
                itemGS.clear();
                notifyListeners();
              }
              funCalculate(kotNumber);
            }
          });
        } else {
          itemGS.clear();
          notifyListeners();
        }
      } else {
        SharedPreferences userPreference =
            await SharedPreferences.getInstance();
        if (userPreference.getString('Floor') == floorId &&
            userPreference.getString('Table') == tableId &&
            userPreference.getString('SubTable') == subtableID) {
          noOrder = true;
          notifyListeners();
        }
      }
    });
  }

  ///new Order Details Code

  // void orderDetails(String floorId, String tableId, String subtableID){
  //   DateTime now = new DateTime.now();
  //   String DayNode = now.year.toString() +
  //       "/" +
  //       now.month.toString() +
  //       "/" +
  //       now.day.toString();
  //   mRootReference.child('Orders').child(DayNode).child(floorId).child(tableId).child(subtableID.replaceAll('S', '')).onValue.listen((event) {
  //     if(event.snapshot.value!=null){
  //       fun_orderItems2(floorId,tableId,subtableID);
  //     }
  //   });
  // }

  // void fun_orderItems2(String floorId, String tableId, String subtableID) {
  //   print('check is   1  ${tableId}     ${subtableID}');
  //   tmpTable=tableId;
  //   tmpSubTable=subtableID;
  //   New = false;
  //   DDFirebase();
  //   noOrder = true;
  //
  //   mRootReference
  //       .child('TableOrders')
  //       .child('Floors')
  //       .child(floorId)
  //       .child('Tables')
  //       .child(tableId)
  //       .child('SubTables')
  //       .child(subtableID)
  //       .child('Orders')
  //       .onValue
  //       .listen((Event event) {
  //
  //     if (event.snapshot.value != null) {
  //       noOrder = false;
  //       final Map<dynamic, dynamic> MAINDATA = event.snapshot.value;
  //       print("MAINDATA  "+event.snapshot.value.toString());
  //
  //       if (MAINDATA.values.first['OrderRef'] != null) {
  //         try {
  //           orderDayNode = funDateC(MAINDATA.keys.first);
  //         } catch (error){
  //
  //         }
  //
  //         mRootReference.child(MAINDATA.values.first['OrderRef']).parent()!
  //             .once()
  //             .then((snapShot)  {
  //
  //
  //
  //             itemGS.clear();
  //             dOrderTotal = 0.0;
  //             dOrderTaxTotal = 0.0;
  //             ANT = 0;
  //             BillItemCountNT = 0;
  //             BillITemCountT = 0;
  //             AT = 0;
  //             String BN = '';
  //             bool ReGenerate = false;
  //             Regeneate = false;
  //
  //             int O = 0;
  //
  //             if(snapShot.value!=null){
  //               final Map<dynamic, dynamic> orderData = snapShot.value;
  //
  //               MAINDATA.forEach((orderID, orderValue) {
  //                 print(orderData.keys);
  //                 O++;
  //                 if (orderData[orderID] != null) {
  //
  //                   final Map<dynamic, dynamic> DISHDATA = orderData[orderID];
  //
  //                   int I = 0;
  //
  //                   DISHDATA.forEach((id, value) {
  //
  //                     if (value["Status"].toString() != "Cancelled") {
  //                       I++;
  //                       String Id          ='';
  //                       String Name        ='';
  //                       String Qty         ='';
  //                       String Rate        ='';
  //                       String Total       ='';
  //                       String kitchen     ='';
  //                       String Status      ='';
  //                       String dbRef       ='';
  //                       String floor       ='';
  //                       String table       ='';
  //                       String subTableId  ='';
  //                       String productId   ='';
  //                       String kotNumber   ='';
  //                       String Notes       ='';
  //                       String time        ='';
  //
  //
  //                       double? dqty = 0.0;
  //                       double? dRate = 0.0;
  //                       double? dTotal = 0.0;
  //
  //                       if (value['ReGenerated'] != 'null' &&
  //                           value['ReGenerated'] != 'NO') {
  //                         ReGenerate = true;
  //                         BN = value['ReGenerated'].toString();
  //                       }
  //                       print("Order " +
  //                           DISHDATA.keys.length.toString() +
  //                           " = " +
  //                           I.toString() +
  //                           " / " +
  //                           MAINDATA.keys.length.toString() +
  //                           " = " +
  //                           O.toString());
  //                       if (ReGenerate) {
  //                         strBillNo = BN;
  //                         Regeneate = true;
  //                       } else {
  //                         Regeneate = false;
  //                       }
  //
  //                       if (value['Quantity'] != null) {
  //                         dqty = double.tryParse(value['Quantity']);
  //                       }
  //                       if (value['Rate'] != null) {
  //                         dRate = double.tryParse(value['Rate']);
  //                       }
  //                       if (value['Status'] == 'OrderPrinted' ||
  //                           value['Status'] == 'OrderTaken') {
  //                         TableItemCoount++;
  //                       }
  //                       if (value['Taxablity'] == 'false') {
  //                         ANT = ANT + dqty! * dRate!;
  //                       } else {
  //                         BillITemCountT++;
  //                         AT = AT + dqty! * dRate!;
  //                       }
  //
  //                       dTotal = dqty * dRate;
  //                       dOrderTotal = dOrderTotal + dTotal;
  //
  //                       String path = orderValue['OrderRef'].toString() + "/" + id.toString();
  //
  //                       Id          =id.toString();
  //                       Name        =value['Item'].toString()   ;
  //                       Qty         =value['Quantity'].toString() ;
  //                       Rate        =value['Rate'].toString()  ;
  //                       Total       =dTotal.toString();
  //                       kitchen     =value['Kitchen'].toString();
  //                       Status      =value['Status'].toString();
  //                       dbRef       =path;
  //                       floor       =floorId  ;
  //                       table       =tableId   ;
  //                       subTableId  =subtableID;
  //                       productId   =value['ItemID'].toString();
  //                       kotNumber   =value['OrderID'].toString();
  //                       Notes       =value["Note"].toString();
  //                       time        =value["Time"].toString();
  //                       print(subtableID.toString()+"   equel   "+value['SubTable'].toString());
  //
  //                       itemGS.add(ItemGS(
  //                         Id,
  //                         Name,
  //                         Qty  ,
  //                         Rate  ,
  //                         Total  ,
  //                         kitchen ,
  //                         Status  ,
  //                         dbRef  ,
  //                         floor,
  //                         table,
  //                         subTableId,
  //                         productId,
  //                         kotNumber,
  //                         Notes,
  //                         time,
  //                       ),
  //                       );
  //
  //
  //                       notifyListeners();
  //                     } else {
  //                       I++;
  //                     }
  //                     itemGS.sort((a, b) => a.time.compareTo(b.time));
  //                   });
  //                 }
  //
  //                 double CGST = (dOrderTotal * 2.5) / 100;
  //                 double SGST = (dOrderTotal * 2.5) / 100;
  //                 dOrderTaxTotal = dOrderTotal + CGST + SGST;
  //                 notifyListeners();
  //               });
  //             }
  //
  //
  //
  //
  //           notifyListeners();
  //
  //         });
  //       }
  //     } else {
  //       noOrder = true;
  //     }
  //     notifyListeners();
  //
  //   });
  //   notifyListeners();
  // }

  void printerKitchens(
      String floor, String table, String subtableID, String kotNumber) {
    Itemkichens.clear();

    mRootReference
        .child('TableOrders')
        .child('Floors')
        .child(floor)
        .child('Tables')
        .child(table)
        .child('SubTables')
        .child(subtableID)
        .child('Orders')
        .once()
        .then((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> MAINDATA = databaseEvent.snapshot.value as Map;

        MAINDATA.forEach((orderID, orderValue) {
          mRootReference
              .child(orderValue['OrderRef'].toString())
              .once()
              .then((DatabaseEvent databaseEvent) {
            if (databaseEvent.snapshot.value != null) {
              Map<dynamic, dynamic> DISHDATA =
                  databaseEvent.snapshot.value as Map;

              DISHDATA.forEach((id, value) {
                if (value["Status"].toString() != "Cancelled") {
                  if (value["OrderID"].toString() == kotNumber) {
                    Itemkichens.add(value['Kitchen'].toString());
                    notifyListeners();
                  }
                }
              });
            }
            notifyListeners();
          });
        });
      }
    });
  }

  void fun_clear() {
    print('Test 5   clear');
    itemGS.clear();
    customerName = "Customer";
    customerPhone = "0000000000";
    stewardName = '';
    stewardID = uid;
    dOrderTotal = 0.0;
    dOrderTaxTotal = 0.0;
    nameController.text = "";
    phoneController.text = "";
    strBillNo = "";
    Series = '';
    gstController.text = "";
    addressController.text = "";
    landMarkController.text = "";
    dateOfBirthController.text = "";
    notesController.text = "";
    orderDayNode = '';
    noOrder = true;
    allOrderPrinted=true;

    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  void remove(ItemGS item) {
    itemGS.remove(item);
    // This call tells the widgets that are listening to this model to rebuild.
    notifyListeners();
  }

  //OrderItemList

  fun_tableListing(String floor) {
    mRootReference
        .child("TableOrders")
        .child("Floors")
        .child(floor)
        .onValue
        .listen((DatabaseEvent databaseEvent) async {
      if (databaseEvent.snapshot.value != null) {
        SharedPreferences userPreference =
            await SharedPreferences.getInstance();

        if (userPreference.getString("SelectedFloor") == floor) {
          var KEYS = databaseEvent.snapshot.key;
          // var DATA = event.snapshot.value;

          Map<dynamic, dynamic> DATA = databaseEvent.snapshot.value as Map;
          tableGS.clear();

          if (KEYS != 'Empty') {
            if (DATA["Tables"] != null) {
              DATA["Tables"].forEach((tableID, tableData) {
                int shortNum = 1000;
                try {
                  shortNum = int.tryParse(tableID)!;
                } catch (error) {
                  shortNum = 1000;
                }
                String status = "NOTSELECTED";
                String capacity = "0";
                List<SubtableGS> subtableGS = [];
                if (tableData['SubTables'] != null) {
                  tableData['SubTables'].forEach((subtableID, subtableData) {
                    if (subtableID.toString() != 'NIL') {
                      String Substatus = "NOTSELECTED";
                      String Subcapacity = "0";
                      String strOrderID = 'NIL';
                      String strOrderRef = 'NIL';
                      if (subtableData['Orders'] != null) {
                        status = 'SELECTED';
                        Substatus = "SELECTED";
                        subtableData['Orders'].forEach((ID, OrderData) {
                          strOrderID = OrderData['OrderID'].toString();
                          strOrderRef = OrderData['OrderRef'].toString();
                        });
                        subtableGS.add( SubtableGS(
                            tableID.toString(),
                            subtableID.toString(),
                            subtableID.toString(),
                            Substatus,
                            SelectedFloor.toString(),
                            Subcapacity,
                            strOrderID,
                            strOrderRef));
                      }

                      subtableGS.sort((a, b) => int.parse(
                              a.subtable_ID.replaceAll('S', ''))
                          .compareTo(
                              int.parse(b.subtable_ID.replaceAll('S', ''))));
                    }
                  });

                  if (subtableGS.isNotEmpty) {
                    tableGS.add(TablesGS(
                        tableID.toString(),
                        tableData['Name'].toString(),
                        status,
                        SelectedFloor.toString(),
                        capacity,
                        subtableGS,
                        shortNum.toString()));

                    notifyListeners();
                  } else {
                    tableGS.add(TablesGS(
                        tableID.toString(),
                        tableData['Name'].toString(),
                        status,
                        SelectedFloor.toString(),
                        capacity,
                        subtableGS,
                        shortNum.toString()));
                    notifyListeners();
                  }
                } else {
                  tableGS.add(TablesGS(
                      tableID.toString(),
                      tableData['Name'].toString(),
                      status,
                      SelectedFloor.toString(),
                      capacity,
                      subtableGS,
                      shortNum.toString()));
                  notifyListeners();
                }
              });
            }
          }
        }
      } else {
        SharedPreferences userPreference =
            await SharedPreferences.getInstance();

        if (userPreference.getString("SelectedFloor") == floor) {
          tableGS.clear();
        }
        notifyListeners();
      }
      tableGS.sort(
          (a, b) => int.parse(a.sortNumber).compareTo(int.parse(b.sortNumber)));
      // DATA.forEach((floor, value) {
      //
      // });
    });
  }

  void fun_Dishes() {
    if(dishStream!=null){
      dishStream!.cancel();
    }
    dishStream=mRootReference.child("Dish").onValue.listen((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> DishMap = databaseEvent.snapshot.value as Map;
        dishesname_list.clear();
        dishesID_list.clear();
        dishesID_Name_list.clear();
        DishMap.forEach((ID, value) {
          if (!dishesID_list.contains(ID.toString())) {
            int shortNum = 1000;
            try {
              shortNum = int.tryParse(ID.toString())!;
            } catch (error) {
              shortNum = 1000;
            }
            dishesname_list.add(value['Item'].toString());
            dishesID_list.add(ID.toString());
            dishesID_Name_list.add(DishItemGS(ID.toString(),value['Item'].toString(),shortNum.toString()));
          }
        });
        if (isDisposed) {
          notifyListeners();
        }
      }
    });
  }

  void fun_Category(BuildContext context) {
    mRootReference.child("Dish").once().then((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> DishMap = databaseEvent.snapshot.value as Map;
        categories.clear();
        categoryGS.clear();
        DishMap.forEach((ID, value) {
          if (!categories.contains(value['Category'].toString())) {
            categoryGS
                .add(CategoryGS(value['Category'].toString(), "NOTSELECTED"));
            categories.add(value['Category'].toString());
          }
        });
        notifyListeners();
      }
    });
  }

  void fun_categoryItems(BuildContext context, String strcategory, int index) {
    categoryGS[index].Status == "SELECTED";
    mRootReference.child("Dish").once().then((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> DishMap = databaseEvent.snapshot.value as Map;
        dishGS.clear();
        DishMap.forEach((ID, value) {
          if (value['Deactivated'] != 'true') {
            if (value['Category'] == strcategory &&
                value['SubCategory'] == strcategory) {
              mRootReference
                  .child('Dish')
                  .child(value['Parent'].toString())
                  .once()
                  .then((DatabaseEvent databaseEvent) {
                if (databaseEvent.snapshot.value != null) {
                  Map<dynamic, dynamic> ParentMap =
                      databaseEvent.snapshot.value as Map;
                  double? STK = 0.0;
                  double? USD = 0.0;
                  String strTaxability = 'false';

                  if (ParentMap['Stock'].toString() != null) {
                    STK = double.tryParse(ParentMap['Stock'].toString());
                  }
                  double FNL = STK! - USD;

                  if (ParentMap['Taxablity'].toString() == 'true') {
                    strTaxability = 'true';
                  } else {
                    strTaxability = 'false';
                  }

                  dishGS.add(new DishGS(
                      ID.toString(),
                      value['Item'],
                      "dishCategory",
                      "dishRate",
                      "dishStock",
                      "dishUnit",
                      "dishDepend",
                      "dishDependUses",
                      "dishKitchen",
                      "dishFloor",
                      "dishParent"));
                  notifyListeners();
                }
              });
            }
          }
        });
      }
    });
  }

  funSteward() async {
    SharedPreferences userPreference = await SharedPreferences.getInstance();

    uid = userPreference.getString("Uid") ?? "";
    loginEmbloy = userPreference.getString("Name") ?? "";

    mRootReference.child("Users").onValue.listen((DatabaseEvent databaseEvent) {
      Map<dynamic, dynamic> stewardsMap = databaseEvent.snapshot.value as Map;
      stewards.clear();
      staffs.clear();
      stewardsIds.clear();
      staffsPasses.clear();
      stewardsMap.forEach((key, value) {
        if (value["WaiterStatus"].toString() == "ON") {
          stewards.add(value["Name"].toString());
          stewardsIds.add(key.toString());
          notifyListeners();
        }
        staffs.add(key.toString());
        staffsPasses.add(value["Password"].toString());
        notifyListeners();
      });
      if (isDisposed) {
        notifyListeners();
      }
    });
  }

  funSetSteward(String name, String path) {
    stewardName = name;
    stewardID = stewardsIds.elementAt(stewards.indexOf(name));
    mRootReference.child(path).child("OrderedBy").child("ID").set(stewardID);
    notifyListeners();
  }

  funCustomers() {
    mRootReference
        .child("CustomerTemp")
        .onValue
        .listen((DatabaseEvent databaseEvent) {
      Map<dynamic, dynamic> customersMap = databaseEvent.snapshot.value as Map;
      costumersPhone.clear();
      costumersNamePhone.clear();
      costumersName.clear();
      customersMap.forEach((key, value) {
        if (!costumersPhone.contains(key.toString())) {
          costumersPhone.add(key.toString());
          costumersName.add(value.toString());
          costumersNamePhone.add(value.toString() + " " + key.toString());
        }
      });
    });
  }

  funCheckCustomer(String s, String ctrl) {
    if (costumersNamePhone.contains(s)) {
      nameController.text =
          costumersName.elementAt(costumersNamePhone.indexOf(s));
      phoneController.text =
          costumersPhone.elementAt(costumersNamePhone.indexOf(s));
    } else {
      if (ctrl == "Name") {
        nameController.text = s;
      }
      if (ctrl == "Phone") {
        phoneController.text = s;
      }
    }
  }

  funOrderDetails(String path) {
    mRootReference.child(path).once().then((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> data = databaseEvent.snapshot.value as Map;
        if (data["Customer"] != null && data["Customer"]["Number"] != null) {
          mRootReference
              .child("CustomerTemp")
              .child(data["Customer"]["Number"].toString())
              .once()
              .then((DatabaseEvent databaseEvent) {
            customerName =
                (databaseEvent.snapshot.value ?? "Customer") as String;
            customerPhone = databaseEvent.snapshot.key ?? "0000000000";
            nameController.text =
                (databaseEvent.snapshot.value ?? "") as String;
            phoneController.text = databaseEvent.snapshot.key.toString();
            mRootReference
                .child("CustomerData")
                .child(databaseEvent.snapshot.key.toString())
                .once()
                .then((DatabaseEvent databaseEvent) {
              Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;

              gstController.text = map["CustomerGST"] ?? "";
              addressController.text = map["CustomerAddress"] ?? "";
              landMarkController.text = map["CustomerLandMark"] ?? "";
              if (map["CustomerDOB"] != null &&
                  map["CustomerDOB"]["time"] != null) {
                int millis = int.parse(map["CustomerDOB"]["time"].toString());
                var dt = DateTime.fromMillisecondsSinceEpoch(millis);
                var d24 = DateFormat('dd/MM/yyyy').format(dt);
                dateOfBirthController.text = d24.toString();
              }
              notesController.text = map["CustomerNotes"] ?? "";
            });
            notifyListeners();
          });
        } else {
          customerName = "Customer";
          customerPhone = "0000000000";
          notifyListeners();
        }
        if (data["OrderedBy"] != null && data["OrderedBy"]["ID"] != null) {
          mRootReference
              .child("Users")
              .child(data["OrderedBy"]["ID"].toString())
              .once()
              .then((DatabaseEvent databaseEvent) {
            if (databaseEvent.snapshot.value != null) {
              Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;

              stewardName = map["Name"] ?? '';
              stewardID = databaseEvent.snapshot.key ?? uid;
            }

            notifyListeners();
          });
        } else {
          stewardName = '';
          stewardID = uid;
          notifyListeners();
        }
      }
    });
  }

  Future<void> selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2101));
    if (picked != null) {
      dateOfBirthController.text = picked.day.toString() +
          "/" +
          picked.month.toString() +
          "/" +
          picked.year.toString();
      sDateOfBirth = picked.millisecondsSinceEpoch.toString();
    }
    notifyListeners();
  }

  uploadCustomerData(String name, String phoneNumber, String path, String gst,
      String address, String landmark, String dob, String notes) {
    customerName = name;
    customerPhone = phoneNumber;
    HashMap<String, Object> customer = HashMap();
    customer["CustomerName"] = name;
    customer["CustomerMobile"] = phoneNumber;
    customer["CustomerAddress"] = address;
    customer["CustomerGST"] = gst;
    customer["CustomerLandMark"] = landmark;
    customer["CustomerNotes"] = notes;
    DateTime now = DateTime.now();

    if (dob == "") {
      customer["CustomerDOB/time"] = now.millisecondsSinceEpoch;
    } else {
      customer["CustomerDOB/time"] = dob;
    }

    mRootReference.child("CustomerData").child(phoneNumber).update(customer);
    mRootReference.child("CustomerTemp").child(phoneNumber).set(name);

    mRootReference
        .child(path)
        .child("Customer")
        .child("Number")
        .set(phoneNumber);
    notifyListeners();
  }

  AddItemClear() {
    AddItemClick = "new";
  }

  onClear() {
    cItem = "";
    cItemHint = "Item";
    cQty = "";
    cQtyHint = "Qty";
    isQty = false;
    cErrorText = "";
    notifyListeners();
  }

  onDel() {
    if (isQty) {
      if (cQty != "" && cQty.isNotEmpty) {
        cQty = cQty.substring(0, cQty.length - 1);
      }
    } else {
      if (cItem != "" && cItem.isNotEmpty) {
        cItem = cItem.substring(0, cItem.length - 1);
        if (dishesID_list.contains(cItem)) {
          cItemHint = dishesname_list.elementAt(dishesID_list.indexOf(cItem));
        } else {
          cItemHint = "";
        }
      }
    }
    notifyListeners();
  }

  onTap(String num) {
    if (isQty) {
      cQty = cQty + num;
      cQtyHint = "QTY";
      notifyListeners();
    } else {
      cItem = cItem + num;
      if (dishesID_list.contains(cItem)) {
        cItemHint = dishesname_list.elementAt(dishesID_list.indexOf(cItem));
      } else {
        cItemHint = "";
      }
      notifyListeners();
    }
  }

  onEnter() {
    if (cItem == "" || !dishesID_list.contains(cItem)) {
      cErrorText = "Please Enter valid item ID";
    } else {
      if (isQty) {
        if (cQty == "") {
          cErrorText = "Please Enter Qty";
        } else if (int.parse(cQty) < 1) {
          cErrorText = "Please Enter valid Qty";
        } else {
          isQty = false;
        }
      } else {
        isQty = true;
      }
    }

    notifyListeners();
  }

  selectedItem() {
    isQty = false;

    notifyListeners();
  }

  selectedQty() {
    isQty = true;
    notifyListeners();
  }

  void fun_addSubTable(String floor, String table, int newsubtable) {
    print("         " + floor);
    HashMap<String, Object> Data = new HashMap();
    Data['Name'] = "S" + newsubtable.toString();
    if (uid != "") {
      mRootReference
          .child("Tables")
          .child(floor)
          .child(table)
          .child("SubTableNP")
          .child(newsubtable.toString().replaceAll('S', ''))
          .set(uid);
    } else {
      mRootReference
          .child("Tables")
          .child(floor)
          .child(table)
          .child("SubTableNP")
          .child(newsubtable.toString().replaceAll('S', ''))
          .set("9999");
    }

    mRootReference
        .child('TableOrders')
        .child('Floors')
        .child(floor)
        .child('Tables')
        .child(table)
        .child('SubTables')
        .child("S" + newsubtable.toString())
        .update(Data);
    if (selectedFloorName == "TAKE AWAY") {
      DateTime now = new DateTime.now();
      String DayNode = now.year.toString() +
          "/" +
          now.month.toString() +
          "/" +
          now.day.toString();
      HashMap<String, Object> map = new HashMap();
      map["S" + newsubtable.toString()] = "S" + newsubtable.toString();

      mRootReference.child("TOKENS").child(DayNode).update(map);
    }

    fun_tableListing(floor);
    funGetTokens();

    notifyListeners();

    // notifyListeners();
  }

  void func_floorprinterData(BuildContext context, String floor) async {
    mRootReference
        .child('config')
        .child('Floors')
        .child(floor)
        .onValue
        .listen((DatabaseEvent databaseEvent) {
      Map<dynamic, dynamic> floorData = databaseEvent.snapshot.value as Map;
      if (floorData['Name'] != null) {
        FloorName = floorData['Name'].toString().toUpperCase();
      } else {
        FloorName = floor.toString().toUpperCase();
      }
      FloorIP = floorData['IP'].toString();

      if (floorData['FloorConsolidated'].toString().trim() == 'Enable') {
        isFloorConsolidated = true;
      } else {
        isFloorConsolidated = false;
      }

      NikalaIP = floorData['NIP'].toString();

      if (floorData['NikalaConsolidated'] == 'Enable') {
        isNikalaConsolidated = true;
      } else {
        isNikalaConsolidated = false;
      }
      if (floorData['NikalaSeparated'] == 'Enable') {
        isNikalaSeparated = true;
      } else {
        isNikalaSeparated = false;
      }
      notifyListeners();
    });

    mRootReference
        .child('config')
        .child('Kitchens')
        .onValue
        .listen((DatabaseEvent databaseEvent) {
      printerGS.clear();
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> KitchenData = databaseEvent.snapshot.value as Map;
        KitchenData.forEach((key, value) {
          if (value['Name'] != 'NIL') {
            bool isCS = false;
            bool isSP = false;
            if (value['KitchenConsolidated'] == 'Enable') {
              isCS = true;
            } else {
              isCS = false;
            }
            if (value['KitchenSeparated'] == 'Enable') {
              isSP = true;
            } else {
              isSP = false;
            }
            printerGS.add(PrinterGS(
                key.toString(), value['Name'], value['IP'], isCS, isSP));
            notifyListeners();
          }
        });
      }
    });
  }

  //floor consolidated----------------------------------------------------------
  Future<void> fun_consolidatedprint(
      String floorName,
      String floorIP,
      String kot,
      String subTable,
      String tableID,
      BuildContext context,
      String s,
      String dRef,
      String floor,
      String orderType,
      int index,
      List<ItemGS> _itemGs) async {
    int count = 1;
    bool isSuccess = false;
    while (count < 50) {
      count++;
      const PaperSize paper = PaperSize.mm80;
      final profile = await CapabilityProfile.load();
      final printer = NetworkPrinter(paper, profile);

      final PosPrintResult res = await printer.connect(floorIP, port: 9100);

      if (res == PosPrintResult.success) {
        isSuccess = true;
        List<int> items = await getItemListCon(_itemGs, kot);

        await printFloorConsolidated(
            printer, kot, subTable, floorName, tableID, orderType, items);
        printer.disconnect();

        if (index == 1000) {
          printerReGS.add(PrinterReGS(
              floorName,
              floorIP,
              kot,
              subTable,
              tableID,
              context,
              s,
              dRef,
              floor,
              orderType,
              floorName + " C",
              "success",
              ""));
        } else {
          printerReGS[index] = PrinterReGS(
              floorName,
              floorIP,
              kot,
              subTable,
              tableID,
              context,
              s,
              dRef,
              floor,
              orderType,
              floorName + " C",
              "success",
              "");
        }
        notifyListeners();
        break;
      }
    }
    if (count == 50) {
      if (!isSuccess) {
        if (index == 1000) {
          printSuccess = false;

          printerReGS.add(PrinterReGS(
              floorName,
              floorIP,
              kot,
              subTable,
              tableID,
              context,
              s,
              dRef,
              floor,
              orderType,
              floorName + " C",
              "Failed",
              ""));
        } else {
          printerReGS[index] = PrinterReGS(
              floorName,
              floorIP,
              kot,
              subTable,
              tableID,
              context,
              s,
              dRef,
              floor,
              orderType,
              floorName + " C",
              "Failed",
              "");
        }
        notifyListeners();
      }
    }
    notifyListeners();
  }

  Future<void> printFloorConsolidated(
      NetworkPrinter printer,
      String kot,
      String subTable,
      String floorName,
      String tableID,
      String orderType,
      List<int> _itemGs)  async {
    printer.rawBytes(printCustom('Consolidated', 3, 1, 0));
    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('KOT : ${kot}', 3, 1, 0));
    printer.rawBytes(printCustom(orderType, 3, 1, 0));

    if (floorName == 'TAKE AWAY') {
      final ByteData data = await rootBundle.load('assets/takeaway.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    } else {
      if (orderType == 'RUNNING ORDER') {
        final ByteData data = await rootBundle.load('assets/running.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      } else if (orderType == 'NEW ORDER') {
        final ByteData data = await rootBundle.load('assets/neworder.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      }
    }
    printer.rawBytes(printCustom(floorName, 2, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom(timestamp, 3, 1, 0));
    printer.rawBytes(printerFeed);

    printer.rawBytes(printCustom('TABLE : ${tableID}', 2, 1, 1));

    printer.rawBytes(printCustom('SUB TABLE : ${subTable}', 2, 1, 1));

    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('ORDERED BY : ${stewardName}', 1, 0, 0));

    printer.hr();

    printer.rawBytes(IndexColumnPrint('Qty', 'Item'));
    printer.hr();
    printer.rawBytes(_itemGs);

    printer.hr();
    printer.feed(2);
    printer.rawBytes(printerBytes);
    printer.text('**<#>..${kotAmount}..<#>**',
        styles: PosStyles(align: PosAlign.center, bold: true));
    printer.feed(1);
    printer.cut();

    notifyListeners();
  }

  //floor consolidated-------------------c---------------------------------------

// nikala consolidated----------------------------------------------------------
  Future<void> fun_floornikalaprint(
      String floorName,
      String nikalaIP,
      String kot,
      String subTable,
      String tableID,
      BuildContext context,
      String s,
      String dRef,
      String floor,
      String orderType,
      int index,
      List<ItemGS> _itemGs) async {
    int count = 1;
    bool isSuccess = false;
    while (count < 50) {
      count++;
      const PaperSize paper = PaperSize.mm80;
      final profile = await CapabilityProfile.load();
      final printer = NetworkPrinter(paper, profile);

      final PosPrintResult res = await printer.connect(nikalaIP, port: 9100);
      // printer.beep(n: 1, duration: PosBeepDuration.beep150ms);

      if (res == PosPrintResult.success) {
        isSuccess = true;
        List<int> items = await getItemListCon(_itemGs, kot);

        await printFloorNikalaCS(
            printer, kot, subTable, floorName, tableID, orderType, items);
        printer.disconnect();

        if (index == 1000) {
          printerReGS.add(PrinterReGS(
              floorName,
              nikalaIP,
              kot,
              subTable,
              tableID,
              context,
              s,
              dRef,
              floor,
              orderType,
              floorName + " Nikala C",
              "success",
              ""));
        } else {
          printerReGS[index] = PrinterReGS(
              floorName,
              nikalaIP,
              kot,
              subTable,
              tableID,
              context,
              s,
              dRef,
              floor,
              orderType,
              floorName + " Nikala C",
              "success",
              "");
        }
        notifyListeners();
        break;
      }
    }
    if (count == 50) {
      if (!isSuccess) {
        if (index == 1000) {
          printSuccess = false;

          printerReGS.add(PrinterReGS(
              floorName,
              nikalaIP,
              kot,
              subTable,
              tableID,
              context,
              s,
              dRef,
              floor,
              orderType,
              floorName + " Nikala C",
              "Failed",
              ""));
        } else {
          printerReGS[index] = PrinterReGS(
              floorName,
              nikalaIP,
              kot,
              subTable,
              tableID,
              context,
              s,
              dRef,
              floor,
              orderType,
              floorName + " Nikala C",
              "Failed",
              "");
        }
        notifyListeners();
      }
    }

    notifyListeners();
  }

  printFloorNikalaCS(
      NetworkPrinter printer,
      String kot,
      String subTable,
      String floorName,
      String tableID,
      String orderType,
      List<int> _itemGs) async {
    printer.rawBytes(printCustom('Consolidated', 3, 1, 0));
    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('KOT : ${kot}', 3, 1, 0));
    printer.rawBytes(printCustom(orderType, 3, 1, 0));

    if (floorName == 'TAKE AWAY') {
      final ByteData data = await rootBundle.load('assets/takeaway.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    } else {
      if (orderType == 'RUNNING ORDER') {
        final ByteData data = await rootBundle.load('assets/running.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      } else if (orderType == 'NEW ORDER') {
        final ByteData data = await rootBundle.load('assets/neworder.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      }
    }
    printer.rawBytes(printCustom(floorName, 2, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom(timestamp, 3, 1, 0));
    printer.rawBytes(printerFeed);

    printer.rawBytes(printCustom('TABLE : ${tableID}', 2, 1, 1));

    printer.rawBytes(printCustom('SUB TABLE : ${subTable}', 2, 1, 1));

    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('ORDERED BY : ${stewardName}', 1, 0, 0));

    printer.hr();

    printer.rawBytes(IndexColumnPrint('Qty', 'Item'));
    printer.hr();
    printer.rawBytes(_itemGs);
    printer.hr();
    printer.feed(2);
    printer.rawBytes(printerBytes);

    printer.text('**<#>..${kotAmount}..<#>**',
        styles: PosStyles(align: PosAlign.center, bold: true));
    printer.feed(1);
    printer.cut();

    notifyListeners();
  }

// nikala consolidated----------------------------------------------------------

//nikala seperated--------------------------------------------------------------

  Future<void> fun_nikalaSeprated(
      String floorName,
      String nikalaIP,
      String kot,
      String subTable,
      String tableID,
      BuildContext context,
      String s,
      String dRef,
      String floor,
      String orderType,
      int index,
      String kitchenId,
      List<ItemGS> _itemGs,
      List<PrinterGS> _printerGs,
      List<String> _itemKichens) async {
    // DEMO RECEIPT
    int i = 0;

    for (var element in _printerGs) {
      i++;
      if (kitchenId == "ALL") {
        if (_itemKichens.contains(element.ID)) {
          int count = 1;
          bool isSuccess = false;
          while (count < 50) {
            count++;
            const PaperSize paper = PaperSize.mm80;
            final profile = await CapabilityProfile.load();
            final printer = NetworkPrinter(paper, profile);
            final PosPrintResult res = await printer.connect(
              nikalaIP,
              port: 9100,
            );

            if (res == PosPrintResult.success) {
              print('contain  ID${res.msg}   ${element.ID}  ${element.Name}');

              isSuccess = true;
              List<int> items = await getItemsList(_itemGs, kot, element.ID.toString());
// Here you can write your code
              await printFloorNikalaSeprated(
                  printer,
                  kot,
                  subTable,
                  floorName,
                  tableID,
                  element.ID.toString(),
                  element.Name.toString(),
                  orderType,
                  items);
              printer.disconnect();

              if (index == 1000) {
                printerReGS.add(PrinterReGS(
                    floorName,
                    nikalaIP,
                    kot,
                    subTable,
                    tableID,
                    context,
                    s,
                    dRef,
                    floor,
                    orderType,
                    "Nikala S " + element.Name,
                    "success",
                    element.ID));
              } else {
                printerReGS[index] = PrinterReGS(
                    floorName,
                    nikalaIP,
                    kot,
                    subTable,
                    tableID,
                    context,
                    s,
                    dRef,
                    floor,
                    orderType,
                    "Nikala S " + element.Name,
                    "success",
                    element.ID);
              }

              notifyListeners();
              break;
            }
          }
          if (count == 50) {
            if (!isSuccess) {
              if (index == 1000) {
                printSuccess = false;

                printerReGS.add(PrinterReGS(
                    floorName,
                    nikalaIP,
                    kot,
                    subTable,
                    tableID,
                    context,
                    s,
                    dRef,
                    floor,
                    orderType,
                    "Nikala S " + element.Name,
                    "Failed",
                    element.ID));
              } else {
                printerReGS[index] = PrinterReGS(
                    floorName,
                    nikalaIP,
                    kot,
                    subTable,
                    tableID,
                    context,
                    s,
                    dRef,
                    floor,
                    orderType,
                    "Nikala S " + element.Name,
                    "Failed",
                    element.ID);
              }
            }
          }

          // Duration timeout = const Duration(seconds: 5);

        }
      } else {
        print('ELSE');

        if (element.ID == kitchenId) {
          if (Itemkichens.contains(element.ID)) {
            int count = 1;
            bool isSuccess = false;
            while (count < 50) {
              count++;
              const PaperSize paper = PaperSize.mm80;
              final profile = await CapabilityProfile.load();
              final printer = NetworkPrinter(paper, profile);
              final PosPrintResult res =
                  await printer.connect(nikalaIP, port: 9100);
              if (res == PosPrintResult.success) {
                isSuccess = true;
                List<int> items = await getItemsList(_itemGs, kot, element.ID.toString());
                await printFloorNikalaSeprated(
                    printer,
                    kot,
                    subTable,
                    floorName,
                    tableID,
                    element.ID.toString(),
                    element.Name.toString(),
                    orderType,
                    items);
                printer.disconnect();

                if (index == 1000) {
                  printerReGS.add(PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      "Nikala S " + element.Name,
                      "success",
                      element.ID));
                } else {
                  printerReGS[index] = PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      "Nikala S " + element.Name,
                      "success",
                      element.ID);
                }
                notifyListeners();
                break;
              }
            }
            if (count == 50) {
              if (!isSuccess) {
                if (index == 1000) {
                  printerReGS.add(PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      "Nikala S " + element.Name,
                      "Failed",
                      element.ID));
                } else {
                  printerReGS[index] = PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      "Nikala S " + element.Name,
                      "Failed",
                      element.ID);
                }
                notifyListeners();
              }
            }
          }
        }
      }

      notifyListeners();
    }
  }

  printFloorNikalaSeprated(
      NetworkPrinter printer,
      String _kot,
      String subTable,
      String floorName,
      String tableID,
      String _kitenID,
      String kitenName,
      String orderType,
      List<int> _itemGs) async {
    printer.rawBytes(printerBytes);

    printer.rawBytes(printCustom('KOT : ${_kot}', 3, 1, 0));
    printer.rawBytes(printCustom(orderType, 3, 1, 0));

    if (floorName == 'TAKE AWAY') {
      final ByteData data = await rootBundle.load('assets/takeaway.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    } else {
      if (orderType == 'RUNNING ORDER') {
        final ByteData data = await rootBundle.load('assets/running.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      } else if (orderType == 'NEW ORDER') {
        final ByteData data = await rootBundle.load('assets/neworder.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      }
    }
    printer.rawBytes(printCustom(floorName, 2, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom(timestamp, 3, 1, 0));
    printer.rawBytes(printerFeed);

    printer.rawBytes(printCustom('TABLE : ${tableID}', 2, 1, 1));

    printer.rawBytes(printCustom('SUB TABLE : ${subTable}', 2, 1, 1));

    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('ORDERED BY : ${stewardName}', 1, 0, 0));

    printer.hr();

    printer.rawBytes(IndexColumnPrint('Qty', 'Item'));
    printer.hr();
    printer.rawBytes(_itemGs);

    printer.hr();
    printer.feed(2);
    printer.rawBytes(printerBytes);
    printer.text('**<#>..${kotAmount}..<#>**',
        styles: PosStyles(align: PosAlign.center, bold: true));
    printer.feed(1);
    printer.cut();
    notifyListeners();
  }

  //nikala seperated------------------------------------------------------------

//Kichen Print------------------------------------------------------------------
  Future<void> fun_kichenPrint(
      String floorName,
      String nikalaIP,
      String kot,
      String subTable,
      String tableID,
      BuildContext context,
      String s,
      String dRef,
      String floor,
      String orderType,
      int index,
      String kitchenId,
      List<ItemGS> _itemGs,
      List<PrinterGS> _printerGs,
      List<String> _itemKichens) async {
    // DEMO RECEIPT
    int i = 0;
    for (var _element in _printerGs) {
      if (kitchenId == "ALL") {
        if (_element.Seprated == true) {
          if (_itemKichens.contains(_element.ID)) {
            int count = 1;
            bool isSuccess = false;
            while (count < 50) {
              count++;
              const PaperSize paper = PaperSize.mm80;
              final profile = await CapabilityProfile.load();
              final printer = NetworkPrinter(paper, profile);
              final PosPrintResult res =
                  await printer.connect(_element.IP.toString(), port: 9100);
              if (res == PosPrintResult.success) {
                isSuccess = true;
                List<int> items = await getItemsList(_itemGs, kot, _element.ID.toString());
                await printKichenSeprated(
                    printer,
                    kot,
                    subTable,
                    floorName,
                    tableID,
                    _element.ID.toString(),
                    _element.Name.toString(),
                    "SP",
                    _element.IP.toString(),
                    orderType,
                    items);
                printer.disconnect();
                if (index == 1000) {
                  printerReGS.add(PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " S",
                      "success",
                      _element.ID));
                } else {
                  printerReGS[index] = PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " S",
                      "success",
                      _element.ID);
                }
                notifyListeners();
                break;
              }
            }
            if (count == 50) {
              if (!isSuccess) {
                if (index == 1000) {
                  printSuccess = false;

                  printerReGS.add(PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " S",
                      "Failed",
                      _element.ID));
                } else {
                  printerReGS[index] = PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " S",
                      "Failed",
                      _element.ID);
                }
              }
            }
          }
        }

        if (_element.Consolidated == true) {
          if (_itemKichens.contains(_element.ID)) {
            int count = 1;
            bool isSuccess = false;
            while (count < 50) {
              count++;
              const PaperSize paper = PaperSize.mm80;
              final profile = await CapabilityProfile.load();
              final printer = NetworkPrinter(paper, profile);
              final PosPrintResult res =
                  await printer.connect(_element.IP.toString(), port: 9100);
              if (res == PosPrintResult.success) {
                isSuccess = true;
                List<int> items = await getItemListCon(_itemGs, kot);

                await printKichenConsolidated(
                    printer,
                    kot,
                    subTable,
                    floorName,
                    tableID,
                    _element.ID.toString(),
                    _element.Name.toString(),
                    "CS",
                    _element.IP.toString(),
                    orderType,
                    items);
                printer.disconnect();
                if (index == 1000) {
                  printerReGS.add(PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " C",
                      "success",
                      ""));
                } else {
                  printerReGS[index] = PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " C",
                      "success",
                      "");
                }
                notifyListeners();

                break;
              }
            }
            if (count == 50) {
              if (!isSuccess) {
                if (index == 1000) {
                  printerReGS.add(PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " C",
                      "Failed",
                      ""));
                } else {
                  printerReGS[index] = PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " C",
                      "Failed",
                      "");
                }
              }
            }
          }
        }
      } else {
        if (_element.ID == kitchenId) {
          if (_element.Seprated == true) {
            if (Itemkichens.contains(_element.ID)) {
              int count = 1;
              bool isSuccess = false;
              while (count < 50) {
                count++;

                const PaperSize paper = PaperSize.mm80;
                final profile = await CapabilityProfile.load();
                final printer = NetworkPrinter(paper, profile);
                final PosPrintResult res =
                    await printer.connect(_element.IP.toString(), port: 9100);
                if (res == PosPrintResult.success) {
                  isSuccess = true;
                  List<int> items = await getItemsList(_itemGs, kot, _element.ID.toString());
                  await printKichenSeprated(
                      printer,
                      kot,
                      subTable,
                      floorName,
                      tableID,
                      _element.ID.toString(),
                      _element.Name.toString(),
                      "SP",
                      _element.IP.toString(),
                      orderType,
                      items);
                  printer.disconnect();
                  if (index == 1000) {
                    printerReGS.add(PrinterReGS(
                        floorName,
                        nikalaIP,
                        kot,
                        subTable,
                        tableID,
                        context,
                        s,
                        dRef,
                        floor,
                        orderType,
                        _element.Name + " S",
                        "success",
                        _element.ID));
                  } else {
                    printerReGS[index] = PrinterReGS(
                        floorName,
                        nikalaIP,
                        kot,
                        subTable,
                        tableID,
                        context,
                        s,
                        dRef,
                        floor,
                        orderType,
                        _element.Name + " S",
                        "success",
                        _element.ID);
                  }
                  notifyListeners();
                  break;
                }
              }
              if (count == 50) {
                if (!isSuccess) {
                  if (index == 1000) {
                    printSuccess = false;

                    printerReGS.add(PrinterReGS(
                        floorName,
                        nikalaIP,
                        kot,
                        subTable,
                        tableID,
                        context,
                        s,
                        dRef,
                        floor,
                        orderType,
                        _element.Name + " S",
                        "Failed",
                        _element.ID));
                  } else {
                    printerReGS[index] = PrinterReGS(
                        floorName,
                        nikalaIP,
                        kot,
                        subTable,
                        tableID,
                        context,
                        s,
                        dRef,
                        floor,
                        orderType,
                        _element.Name + " S",
                        "Failed",
                        _element.ID);
                  }
                  notifyListeners();
                }
              }
            }
          }

          if (_element.Consolidated == true) {
            if (Itemkichens.contains(_element.ID)) {
              const PaperSize paper = PaperSize.mm80;
              final profile = await CapabilityProfile.load();
              final printer = NetworkPrinter(paper, profile);
              final PosPrintResult res =
                  await printer.connect(_element.IP.toString(), port: 9100);
              if (res == PosPrintResult.success) {
                List<int> items = await getItemListCon(_itemGs, kot);

                await printKichenConsolidated(
                    printer,
                    kot,
                    subTable,
                    floorName,
                    tableID,
                    _element.ID.toString(),
                    _element.Name.toString(),
                    "CS",
                    _element.IP.toString(),
                    orderType,
                    items);
                printer.disconnect();
                if (index == 1000) {
                  printerReGS.add(PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " C",
                      "success",
                      ""));
                } else {
                  printerReGS[index] = PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " C",
                      "success",
                      "");
                }
                notifyListeners();
              } else {
                if (index == 1000) {
                  printerReGS.add(PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " C",
                      res.msg,
                      ""));
                } else {
                  printerReGS[index] = PrinterReGS(
                      floorName,
                      nikalaIP,
                      kot,
                      subTable,
                      tableID,
                      context,
                      s,
                      dRef,
                      floor,
                      orderType,
                      _element.Name + " C",
                      res.msg,
                      "");
                }
                notifyListeners();
              }
            }
          }
        }
      }
    }

    notifyListeners();
  }

  printKichenConsolidated(
      NetworkPrinter printer,
      String kot,
      String subTable,
      String floorName,
      String tableID,
      String kichenID,
      String kichenName,
      String type,
      String IP,
      String orderType,
      List<int> _itemGs) async {
    printer.rawBytes(printCustom('Consolidated', 3, 1, 0));
    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('KOT : ${kot}', 3, 1, 0));
    printer.rawBytes(printCustom(orderType, 3, 1, 0));

    if (floorName == 'TAKE AWAY') {
      final ByteData data = await rootBundle.load('assets/takeaway.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    } else {
      if (orderType == 'RUNNING ORDER') {
        final ByteData data = await rootBundle.load('assets/running.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      } else if (orderType == 'NEW ORDER') {
        final ByteData data = await rootBundle.load('assets/neworder.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      }
    }
    printer.rawBytes(printCustom(floorName, 2, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom(timestamp, 3, 1, 0));
    printer.rawBytes(printerFeed);

    printer.rawBytes(printCustom('TABLE : ${tableID}', 2, 1, 1));

    printer.rawBytes(printCustom('SUB TABLE : ${subTable}', 2, 1, 1));

    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('ORDERED BY : ${stewardName}', 1, 0, 0));
    printer.rawBytes(printCustom('KICHEN: ${kichenName}', 1, 0, 0));

    printer.hr();

    printer.rawBytes(IndexColumnPrint('Qty', 'Item'));
    printer.hr();
    printer.rawBytes(_itemGs);
    printer.hr();
    printer.feed(2);
    printer.rawBytes(printerBytes);

    printer.text('**<#>..${kotAmount}..<#>**',
        styles: PosStyles(align: PosAlign.center, bold: true));
    printer.feed(1);
    printer.cut();
  }

  printKichenSeprated(
      NetworkPrinter printer,
      String kot,
      String subTable,
      String floorName,
      String tableID,
      String kichenID,
      String kichenName,
      String type,
      String IP,
      String orderType,
      List<int> _itemGs) async {
    printer.rawBytes(printerBytes);

    printer.rawBytes(printCustom('KOT : ${kot}', 3, 1, 0));

    printer.rawBytes(printCustom(orderType, 3, 1, 0));

    if (floorName == 'TAKE AWAY') {
      final ByteData data = await rootBundle.load('assets/takeaway.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    } else {
      if (orderType == 'RUNNING ORDER') {
        final ByteData data = await rootBundle.load('assets/running.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      } else if (orderType == 'NEW ORDER') {
        final ByteData data = await rootBundle.load('assets/neworder.jpg');
        final Uint8List bytes = data.buffer.asUint8List();
        var image = decodeImage(bytes);
        printer.rawBytes(printerBytes);
        printer.image(image!);
      }
    }
    printer.rawBytes(printCustom(floorName, 2, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom(timestamp, 3, 1, 0));
    printer.rawBytes(printerFeed);

    printer.rawBytes(printCustom('TABLE : ${tableID}', 2, 1, 1));

    printer.rawBytes(printCustom('SUB TABLE : ${subTable}', 2, 1, 1));

    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('ORDERED BY : ${stewardName}', 1, 0, 0));
    printer.rawBytes(printCustom('KICHEN: ${kichenName}', 1, 0, 0));

    printer.hr();

    printer.rawBytes(IndexColumnPrint('Qty', 'Item'));
    printer.hr();
    printer.rawBytes(_itemGs);
    printer.hr();
    printer.feed(2);
    printer.rawBytes(printerBytes);
    printer.text('**<#>..${kotAmount}..<#>**',
        styles: PosStyles(align: PosAlign.center, bold: true));
    printer.feed(1);
    printer.cut();
  }

//Kichen Print------------------------------------------------------------------

  //Final Print-----------------------------------------------------------------

  Future<void> fun_finalPrint(String orderID, String subTableID, String tableID,
      BuildContext context, String uid, String floor) async {
    int count = 1;
    bool isSuccess = false;
    while (count < 50) {
      count++;
      print(count);
      const PaperSize paper = PaperSize.mm80;
      final profile = await CapabilityProfile.load();
      final printer = NetworkPrinter(paper, profile);

      final PosPrintResult res = await printer.connect(FloorIP, port: 9100);
      if (res == PosPrintResult.success) {
        isSuccess = false;
        // List<Map<String, Object>> NITEMS = [];
        // List<String> NITEMCODE = [];
        // int i = 0;

        // for (var element in itemGS) {
        //   if (element.Qty.toString() != '0') {
        //     if (element.Name.toString() != 'DISCOUNT') {
        //       if (!NITEMCODE.contains(
        //           element.productId.toString() + element.Rate.toString())) {
        //         print("check   1");
        //
        //         Map<String, Object> Item = HashMap();
        //         Item['Item'] = element.Name.toString();
        //         Item['ItemID'] = element.productId.toString();
        //         Item['Quantity'] = element.Qty.toString();
        //         Item['Rate'] = element.Rate.toString();
        //         Item['Total'] = element.Total.toString();
        //
        //         NITEMCODE.add(
        //             element.productId.toString() + element.Rate.toString());
        //         NITEMS.add(Item);
        //       } else {
        //         if (NITEMS.isNotEmpty) {
        //           Map<String, Object> data = HashMap();
        //           if (NITEMCODE.contains(
        //               element.productId.toString() + element.Rate.toString())) {
        //             int index = NITEMCODE.indexOf(
        //                 element.productId.toString() + element.Rate.toString());
        //
        //             data = NITEMS[NITEMCODE.indexOf(
        //                 element.productId.toString() +
        //                     element.Rate.toString())];
        //
        //             int tempQty = int.tryParse(data["Quantity"].toString())! +
        //                 int.tryParse(element.Qty.toString())!;
        //             data['Quantity'] = tempQty.toString();
        //             double? dQTY = double.tryParse(element.Qty.toString());
        //             double? dRate = double.tryParse(element.Rate.toString());
        //             double dTotal = double.tryParse(data["Total"].toString())! +
        //                 (dQTY! * dRate!);
        //             data['Total'] = dTotal.toStringAsFixed(2);
        //             data['Item'] = element.Name.toString();
        //             data['ItemID'] = element.productId.toString();
        //             data['Rate'] = element.Rate.toString();
        //
        //             NITEMS[index] = data;
        //           }
        //         }
        //       }
        //     } else {
        //       double? dq = double.tryParse(element.Qty.toString());
        //       double? dR = double.tryParse(element.Rate.toString());
        //       dDiscount = (dq! * dR!);
        //     }
        //   }
        //
        //   i++;
        // }
        if (strBillNo != 'NIL' && strBillNo != "") {
          Future.delayed(const Duration(milliseconds: 2000), () async {
            await FinalprintReceipt(printer, orderID, tableID, subTableID,
                context, uid, floor, res.msg, NITEMS);
            notifyListeners();
            printer.disconnect();
          });
        }

        break;
      }
    }
    if (count == 50) {
      if (!isSuccess) {
        finish(context);
        final snackBar = SnackBar(
            backgroundColor: timeout_snack,
            content: Text(
              'Failed',
              textAlign: TextAlign.center,
              softWrap: true,
              style: snackbarStyle,
            ));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    }

    notifyListeners();
  }

  Future<void> FinalprintReceipt(
      NetworkPrinter printer,
      String orderID,
      String tableID,
      String subTableID,
      BuildContext context,
      String uid,
      String floor,
      String msg,
      List<Map<String, Object>> NITEMS) async {
    double CGST = (AT * 2.5) / 100;
    double SGST = (AT * 2.5) / 100;
    double dbDiscount = dDiscount;
    final double NewAT = AT;
    final double NewANT = ANT;
    final int NewItemCount = BillItemCountNT + BillITemCountT;
    double dFinalAmt = NewAT + NewANT + CGST + SGST;

    double dAmt = NewAT + NewANT + CGST + SGST;

    // double RoundOf = NewAT + NewANT + CGST + SGST - Math.round(NewAT + NewANT + CGST + SGST);
    double RoundOf = dAmt - dAmt.round();

    // Print image
    final ByteData data = await rootBundle.load('assets/Delicia.jpg');
    final Uint8List bytes = data.buffer.asUint8List();
    var image = decodeImage(bytes);
    printer.rawBytes(printerBytes);

    printer.image(image!);

    printer.rawBytes(printCustom('${strLine1}', 5, 1, 1));
    printer.rawBytes(printCustom('${strLine2}', 5, 1, 0));
    printer.rawBytes(printCustom('${strLine3}', 5, 1, 0));
    printer.rawBytes(printCustom('${strLine4}', 5, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom('${timestamp}', 5, 1, 0));

    printer.rawBytes(printCustom(
        '${FloorName}    TABLE : ${tableID}  SUB ORDER : ${subTableID}',
        1,
        1,
        1));
    printer.rawBytes(printCustom('BILL ID : ${strBillNo}', 5, 1, 0));
    printer.rawBytes(printCustom('STEWARD ID : ${stewardName}', 5, 1, 0));

    printer.hr(ch: '-', linesAfter: 0);

    printer.rawBytes(printCustom('TABLE ITEMS', 5, 1, 1));

    printer.hr(
      ch: '-',
    );
    printer.rawBytes(printerBytesLeft);
    printer.setStyles(const PosStyles.defaults());

    printer.row([
      PosColumn(text: 'PRODUCT', width: 7),
      PosColumn(text: 'QTY', width: 1),
      PosColumn(
          text: 'RATE', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: 'TOTAL', width: 2, styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.hr(
      ch: '=',
    );
    for (var element in NITEMS) {
      printer.rawBytes(printerBytesLeft);
      printer.row([
        PosColumn(text: element['Item'].toString(), width: 7),
        PosColumn(text: element['Quantity'].toString(), width: 1),
        PosColumn(
            text: element['Rate'].toString(),
            width: 2,
            styles: PosStyles(align: PosAlign.right)),
        PosColumn(
            text: element['Total'].toString(),
            width: 2,
            styles: PosStyles(align: PosAlign.right)),
      ]);
    }

    printer.hr(
      ch: '-',
    );
    if (dbDiscount < 0.0) {
      double Tot = NewAT + dbDiscount.abs();
      printer.rawBytes(printerBytes);
      printer.row([
        PosColumn(
            text: 'TOTAL', width: 6, styles: PosStyles(align: PosAlign.left)),
        PosColumn(
            text: Tot.toStringAsFixed(2),
            width: 6,
            styles: PosStyles(align: PosAlign.right)),
      ]);
      printer.rawBytes(printerBytes);
      printer.row([
        PosColumn(
            text: 'DISCOUNT',
            width: 6,
            styles: PosStyles(align: PosAlign.left)),
        PosColumn(
            text: dbDiscount.toStringAsFixed(2),
            width: 6,
            styles: PosStyles(align: PosAlign.right)),
      ]);
    }
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'TAXABLE ITEM TOTAL',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: NewAT.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'SGST (2.50%)',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: SGST.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'CGST (2.50%)',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: CGST.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);

    printer.hr(ch: '=', linesAfter: 1);
    printer.rawBytes(printerBytes);

    printer.row([
      PosColumn(
          text: 'RoundOf  : ${RoundOf.toStringAsFixed(2)}',
          width: 8,
          styles: PosStyles(align: PosAlign.left, width: PosTextSize.size1)),
      PosColumn(
          text: '',
          width: 4,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    ]);
    printer.rawBytes(printerBytes);

    printer.row([
      PosColumn(
          text: 'Grand Total',
          width: 6,
          styles: PosStyles(align: PosAlign.left, width: PosTextSize.size2)),
      PosColumn(
          text: dFinalAmt.round().toStringAsFixed(2),
          width: 6,
          styles: PosStyles(
              align: PosAlign.right,
              width: PosTextSize.size2,
              height: PosTextSize.size3)),
    ]);
    printer.hr(ch: '-', linesAfter: 1);

    printer.feed(1);
    printer.rawBytes(printerBytes);

    printer.text('>>..THANK YOU..<<',
        styles: PosStyles(align: PosAlign.center, bold: true), linesAfter: 1);
    printer.rawBytes(printCustom('Powered By : Spine', 5, 1, 0));

    printer.feed(1);
    printer.cut();
    billPrintAfterSetDevice(
        orderID, tableID, subTableID, context, uid, floor, msg);

    notifyListeners();
  }

  //Final Print-----------------------------------------------------------------
  void funcbillData() {
    mRootReference.child('config').once().then((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> confiData = databaseEvent.snapshot.value as Map;
        floorGS.clear();
        if (confiData['Floors'] != null) {
          confiData['Floors'].forEach((key, value) {
            if (key.toString() != 'NIL') {
              floorGS.add(FloorGS(key.toString(), value['Name'].toString()));
              floorGS.sort((a, b) => a.floorID.compareTo(b.floorID));
            }
            notifyListeners();
          });
        }
        if (confiData['HotalName'] != null) {
          strHotelName = confiData['HotalName'];
        }
        if (confiData['BillingAddress']['Line1'] != null) {
          strLine1 = confiData['BillingAddress']['Line1'];
        }
        if (confiData['BillingAddress']['Line2'] != null) {
          strLine2 = confiData['BillingAddress']['Line2'];
        }
        if (confiData['BillingAddress']['Line3'] != null) {
          strLine3 = confiData['BillingAddress']['Line3'];
        }
        if (confiData['BillingAddress']['Line4'] != null) {
          strLine4 = confiData['BillingAddress']['Line4'];
        }
        if (confiData['BillingAddress']['Line5'] != null) {
          strLine5 = confiData['BillingAddress']['Line5'];
        }
        if (confiData['CancellationLogin'] != null &&
            confiData['CancellationLogin'].toString() == "Enable") {
          print("fdfd");
          CancelationLogin = true;
        }
        notifyListeners();
      }
    });
  }

  void fun_UpdateprintData(
      String dRef, String floor, String kot, String tableID, String subTable) {
    print("dadna");

    mRootReference
        .child('TableOrders')
        .child('Floors')
        .child(floor)
        .child('Tables')
        .child(tableID)
        .child('SubTables')
        .child(subTable)
        .child('Orders')
        .once()
        .then((DatabaseEvent databaseEvent) {
      Map<dynamic, dynamic> MAINDATA = databaseEvent.snapshot.value as Map;
      MAINDATA.forEach((key, value) {
        mRootReference
            .child(value['OrderRef'].toString())
            .once()
            .then((DatabaseEvent databaseEvent) {
          Map<dynamic, dynamic> reference = databaseEvent.snapshot.value as Map;
          int ExpType = 1;
          int ExpTime = DateTime.now().millisecondsSinceEpoch;

          reference.forEach((ItemID, itemValue) {
            if (kot == value['OrderID'].toString()) {
              if (itemValue['StatusBill'] == 'NotBilled') {
                mRootReference
                    .child(value['OrderRef'].toString())
                    .child(ItemID)
                    .child('Status')
                    .set('OrderPrinted');
              }
              if (ExpType == 0) {
                mRootReference
                    .child(value['OrderRef'].toString())
                    .child(ItemID)
                    .child('DETime')
                    .set("-1");
              } else {
                mRootReference
                    .child(value['OrderRef'].toString())
                    .child(ItemID)
                    .child('DETime')
                    .set(ExpTime.toString());
              }
            }
            notifyListeners();
          });
        });
      });
    });

    DateTime now = DateTime.now();

    String DayNode = now.year.toString() +
        "/" +
        now.month.toString() +
        "/" +
        now.day.toString();
    mRootReference.child('KotToken').child(strDeviceID).child(DayNode).child('kotID').set(kotID);
  }

  Future<void> fun_initPlatformState() async {
    try {
      if (Platform.isAndroid) {
        _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {}
  }

  _readAndroidBuildData(AndroidDeviceInfo build) {
    strDeviceID = build.androidId!;
    print('strDeviceID:   ${strDeviceID}');

    notifyListeners();
  }

  _readIosDeviceInfo(IosDeviceInfo data) {
    strDeviceID = data.utsname.version!;

    print('strDeviceID:   ${strDeviceID}');
    notifyListeners();
  }

  void DDFirebase() {
    mRootReference
        .child("Devices")
        .child(strDeviceID)
        .once()
        .then((DatabaseEvent databaseEvent) {
      final DateTime now = DateTime.now();
      final DateFormat formatter = DateFormat('yyyy');
      final String YEAR = formatter.format(now);
      BillID = now.millisecondsSinceEpoch;

      if (databaseEvent.snapshot.value == null) {
        New = true;
        mRootReference
            .child("Devices")
            .once()
            .then((DatabaseEvent databaseEvent) {
          int? Number = 0;
          if (databaseEvent.snapshot.value != null) {
            Map<dynamic, dynamic> Device_Data =
                databaseEvent.snapshot.value as Map;
            Device_Data.forEach((key, value) {
              if (int.tryParse(value['Number'].toString())! > Number!) {
                Number = int.tryParse(value['Number'].toString());
              }
            });
          }
          String Code = colArray(100)[Number! + 1];
          print('Code   :  $Code');
          int inumber = Number! + 1;
          Device['DeviceID'] = strDeviceID;
          Device['Number'] = inumber.toString();
          Device['Code'] = Code;
          Device['LastUsedNumber'] = "1";
          Series = Code;

          String BillNO = (Code + "-" + YEAR + "-1");
          strBillNo = BillNO;
          notifyListeners();
        });
      } else {
        Map<dynamic, dynamic>? dData = databaseEvent.snapshot.value as Map;

        New = false;
        LastUsedNumber = (int.tryParse(dData['LastUsedNumber']))! + 1;
        Series = dData['Code'].toString();
        Device['DeviceID'] = strDeviceID;
        Device['Number'] = dData['Number'].toString();
        Device['Code'] = dData['Code'].toString();
        Device['LastUsedNumber'] = LastUsedNumber.toString();

        String BillNO = (Series + "-" + YEAR + "-" + LastUsedNumber.toString());
        strBillNo = BillNO;

        notifyListeners();
      }
      print(strBillNo);
      notifyListeners();
    });
  }

  static List<String> colArray(int length) {
    List<String> result = List<String>.filled(length, "0", growable: false);
    // ignore: deprecated_member_use
    String colName = "";
    for (int i = 0; i < length; i++) {
      int value = 'A'.codeUnitAt(0) + (i % 26); //get unicode for semicolon

      String c = String.fromCharCode(value);

      print('c   ${c}'); //get the semicolon string ;

      colName = (c.toString());
      if (i > 25) {
        colName = ((result[(i ~/ 26) - 1] + "") + c.toString());
      }
      result[i] = colName;
    }
    return result;
  }

  Future<void> billPrintAfterSetDevice(
      String orderID,
      String tableID,
      String subTableID,
      BuildContext context,
      String uid,
      String floor,
      String msg) async {
    print("sdsdssddsddsdddds");
    Map<String, Object> data = new HashMap();
    Map<String, Object> data2 = new HashMap();
    double CGST = (AT * 2.5) / 100;
    double SGST = (AT * 2.5) / 100;
    double dbDiscount = 0.0;
    final double NewAT = AT;
    final double NewANT = ANT;
    final int NewItemCount = BillItemCountNT + BillITemCountT;
    double dFinalAmt = NewAT + NewANT + CGST + SGST;

    double dAmt = NewAT + NewANT + CGST + SGST;

    // double RoundOf = NewAT + NewANT + CGST + SGST - Math.round(NewAT + NewANT + CGST + SGST);
    double RoundOf = dAmt - dAmt.round();

    data["TaxableAmount"] = NewAT;
    data["TaxableAmountWithTax"] = NewAT + CGST + SGST;
    data["NontaxableAmount"] = NewANT;
    data["ItemCount"] = NewItemCount;
    data["Series"] = Series;
    data["CGST"] = CGST;
    data["SGST"] = SGST;
    data["BillNo"] = strBillNo;
    data["BUI"] = BillID;
    data["ReGenerated"] = "NO";
    data["Status"] = "BillPrinted";
    data["TableNumber"] = tableID;
    data["SubTable"] = subTableID.replaceAll("S", "");
    final DateTime date = DateTime.now();
    int output = (date.millisecondsSinceEpoch ~/ 1000);
    String str = output.toString();
    int timestamp = (int.tryParse(str)! * 1000);
    data["Time"] = timestamp;
    data["Biller"] = uid;
    data["Floor"] = floor;
    data["Steward"] = stewardID.toString();
    data["CustomerName"] = customerName.toString();
    data["CustomerPhone"] = customerPhone.toString();
    data["TotalAmount"] = dFinalAmt.round();
    data["BillingTime"] = date;
    data["RoundOf"] = RoundOf;

    data2["TaxableAmount"] = NewAT;
    data2["TaxableAmountWithTax"] = NewAT + CGST + SGST;
    data2["NontaxableAmount"] = NewANT;
    data2["ItemCount"] = NewItemCount;
    data2["Series"] = Series;
    data2["CGST"] = CGST;
    data2["SGST"] = SGST;
    data2["BillNo"] = strBillNo;
    data2["BUI"] = BillID;
    data2["ReGenerated"] = "NO";
    data2["Status"] = "BillPrinted";
    data2["TableNumber"] = tableID;
    data2["SubTable"] = subTableID.replaceAll("S", "");

    data2["Time"] = timestamp.toString();
    data2["Biller"] = uid;
    data2["Floor"] = floor;
    data2["Steward"] = stewardID.toString();
    data2["CustomerName"] = customerName.toString();
    data2["CustomerPhone"] = customerPhone.toString();
    data2["TotalAmount"] = dFinalAmt.round();
    data2["BillingTime"] = "123456";
    // data2["BillingTime/time"] =  date.millisecondsSinceEpoch;
    data2["RoundOf"] = RoundOf;

    DateTime now =  DateTime.now();
    String Today = now.year.toString() +
        "/" +
        now.month.toString() +
        "/" +
        now.day.toString();
    DatabaseEvent event = await mRootReference
        .child('TableOrders')
        .child('Floors')
        .child(floor)
        .child('Tables')
        .child(tableID)
        .child('SubTables')
        .child(subTableID)
        .child('Orders')
        .once();
    Map<dynamic, dynamic>? MAINDATA = event.snapshot.value as Map;

    MAINDATA.forEach((orderID, orderValue) async {
      DatabaseEvent event2 =
          await mRootReference.child(orderValue['OrderRef'].toString()).once();
      if (event2.snapshot.value != null) {
        Map<dynamic, dynamic>? DISHDATA = event2.snapshot.value as Map;
        int I = 0;
        DISHDATA.forEach((id, value) {
          mRootReference
              .child("EditedOrders")
              .child(Today)
              .child(floor)
              .child(tableID)
              .child(subTableID)
              .child(orderID)
              .child(id.toString())
              .set(value);
          if (value['Status'] == 'OrderPrinted' ||
              value['Status'] == 'OrderTaken') {
            Map<String, Object> dataset = HashMap();
            dataset['DETime'] = '-1';
            dataset['ReGenerated'] = 'NO';
            dataset['BillNo'] = strBillNo;
            dataset['Status'] = 'BillPrinted';
            dataset['StatusBill'] = 'BillPrinted';
            mRootReference
                .child(orderValue['OrderRef'].toString())
                .child(id)
                .update(dataset);
            mRootReference
                .child("EditedOrders")
                .child(Today)
                .child(floor)
                .child(tableID)
                .child(subTableID)
                .child(orderID)
                .child(id.toString())
                .update(dataset);
          }
        });
      }
    });
    db.collection("Bills").doc(strBillNo).set(data);
    db.collection("BillsEdited").doc(strBillNo).set(data);
    mRootReference.child("ActiveBills").child(strBillNo).set(data2);
    if (!Regeneate) {
      mRootReference.child("Devices").child(strDeviceID).set(Device);
    }
    mRootReference
        .child('TableOrders')
        .child('Floors')
        .child(floor)
        .child('Tables')
        .child(tableID)
        .child('SubTables')
        .child(subTableID)
        .remove();

    DatabaseEvent event3 = await mRootReference
        .child("Tables")
        .child(floor)
        .child(tableID)
        .child("Order")
        .once();
    Map<dynamic, dynamic>? tableData = event3.snapshot.value as Map;
    tableData.forEach((key, value) {
      if (value.toString() == subTableID) {
        mRootReference
            .child("Tables")
            .child(floor)
            .child(tableID)
            .child("Order")
            .child(key.toString())
            .remove();
      }
    });

    mRootReference
        .child("Tables")
        .child(floor)
        .child(tableID)
        .child("SubTableNP")
        .child(subTableID.replaceAll('S', ''))
        .remove();
    mRootReference
        .child("Tables")
        .child(floor)
        .child(tableID)
        .child("SubTableOrder")
        .child(subTableID.replaceAll('S', ''))
        .remove();
    final snackBar = SnackBar(
        backgroundColor: success_snack,
        content: Text(
          '${msg}',
          textAlign: TextAlign.center,
          softWrap: true,
          style: snackbarStyle,
        ));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    Future.delayed(const Duration(seconds: 1), () {
      finish(context);

      finish(context);
// Here you can write your code
    });

    notifyListeners();
  }

  Future<void> fun_cancelconsolidatedprint(
      String floorName,
      String floorIP,
      BuildContext context,
      String s,
      String dRef,
      String orderType,
      String Qty,
      String reason,
      String steward,
      ItemGS itemGS,
      int index) async {
    // printer.beep(n: 1, duration: PosBeepDuration.beep150ms);
    int count = 1;
    bool isSuccess = false;
    while (count < 50) {
      count++;
      const PaperSize paper = PaperSize.mm80;
      final profile = await CapabilityProfile.load();
      final printer = NetworkPrinter(paper, profile);
      final PosPrintResult res = await printer.connect(floorIP, port: 9100);
      if (res == PosPrintResult.success) {
        isSuccess = true;
        // DEMO RECEIPT
        await printCancelFloorConsolidated(
            printer,
            itemGS.kotNumber,
            itemGS.subTableId,
            floorName,
            itemGS.table,
            orderType,
            itemGS.Name,
            Qty,
            reason);
        printer.disconnect();
        if (index == 1000) {
          cancelPrinterGS.add(CancelPrinterReGS(
              floorName,
              floorIP,
              context,
              s,
              dRef,
              orderType,
              Qty,
              reason,
              steward,
              itemGS,
              "success",
              floorName));
        } else {
          cancelPrinterGS[index] = CancelPrinterReGS(
              floorName,
              floorIP,
              context,
              s,
              dRef,
              orderType,
              Qty,
              reason,
              steward,
              itemGS,
              "success",
              floorName);
        }
        notifyListeners();
        break;
      }
    }
    if (count == 50) {
      if (!isSuccess) {
        if (index == 1000) {
          printSuccess = false;

          cancelPrinterGS.add(CancelPrinterReGS(
              floorName,
              floorIP,
              context,
              s,
              dRef,
              orderType,
              Qty,
              reason,
              steward,
              itemGS,
              "Failed",
              floorName));
        } else {
          cancelPrinterGS[index] = CancelPrinterReGS(
              floorName,
              floorIP,
              context,
              s,
              dRef,
              orderType,
              Qty,
              reason,
              steward,
              itemGS,
              "Failed",
              floorName);
        }
        notifyListeners();
      }
    }

    notifyListeners();
  }

  Future<void> printCancelFloorConsolidated(
      NetworkPrinter printer,
      String kot,
      String subTable,
      String floorName,
      String tableID,
      String orderType,
      String item,
      String Qty,
      String Resone) async {
    printer.rawBytes(printCustom('Consolidated', 3, 1, 0));
    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('KOT : ${kot}', 3, 1, 0));
    if (orderType == 'RUNNING ORDER') {
      final ByteData data = await rootBundle.load('assets/running.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);

      printer.image(image!);
    } else if (orderType == 'NEW ORDER') {
      final ByteData data = await rootBundle.load('assets/neworder.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);

      printer.image(image!);
    } else if (orderType == 'TAKE AWAY') {
      final ByteData data = await rootBundle.load('assets/takeaway.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);

      printer.image(image!);
    } else if (orderType == 'CANCELLED') {
      final ByteData data = await rootBundle.load('assets/stop.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);

      printer.image(image!);
    }
    printer.rawBytes(printCustom(orderType, 3, 1, 0));

    printer.rawBytes(printCustom(floorName, 2, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom(timestamp, 3, 1, 0));
    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('TABLE : ${tableID}', 2, 1, 1));

    printer.rawBytes(printCustom('SUB TABLE : ${subTable}', 2, 1, 1));

    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('ORDERED BY : ${stewardName}', 1, 0, 0));

    printer.hr();

    printer.rawBytes(IndexColumnPrint('Qty', 'Item'));
    printer.hr();
    printer.rawBytes(IndexColumnPrint(Qty, item));
    if (Resone != '') {
      printer.rawBytes(IndexColumnPrint('', '(${Resone})'));
    }

    printer.rawBytes(printCustom("ORDER CANCELLED", 2, 1, 0));

    printer.hr();
    printer.feed(2);
    printer.rawBytes(printerBytes);

    printer.text('**<#>..${dOrderTotal}..<#>**',
        styles: PosStyles(align: PosAlign.center, bold: true));
    printer.feed(1);
    printer.cut();
    notifyListeners();
  }

  Future<void> fun_cancelnikalaSeprated(
      String floorName,
      String nikalaIP,
      BuildContext context,
      String s,
      String dRef,
      String orderType,
      String Qty,
      String reason,
      String steward,
      ItemGS itemGS,
      int index) async {
    // DEMO RECEIPT
    printerGS.forEach((element) async {
      if (itemGS.kitchen == element.ID) {
        int count = 1;
        bool isSuccess = false;
        while (count < 50) {
          count++;
          const PaperSize paper = PaperSize.mm80;
          final profile = await CapabilityProfile.load();
          final printer = NetworkPrinter(paper, profile);
          final PosPrintResult res =
              await printer.connect(element.IP.toString(), port: 9100);
          if (res == PosPrintResult.success) {
            isSuccess = true;
            await printCancelFloorNikalaSeprated(
                printer,
                itemGS.kotNumber,
                itemGS.subTableId,
                floorName,
                itemGS.table,
                element.ID.toString(),
                element.Name.toString(),
                orderType,
                itemGS.Name,
                Qty,
                reason);
            printer.disconnect();
            if (index == 1000) {
              cancelPrinterGS.add(CancelPrinterReGS(
                  floorName,
                  nikalaIP,
                  context,
                  s,
                  dRef,
                  orderType,
                  Qty,
                  reason,
                  steward,
                  itemGS,
                  "success",
                  element.Name));
            } else {
              cancelPrinterGS[index] = CancelPrinterReGS(
                  floorName,
                  nikalaIP,
                  context,
                  s,
                  dRef,
                  orderType,
                  Qty,
                  reason,
                  steward,
                  itemGS,
                  "success",
                  element.Name);
            }
            notifyListeners();
            break;
          }
        }
        if (count == 50) {
          if (!isSuccess) {
            if (index == 1000) {
              printSuccess = false;

              cancelPrinterGS.add(CancelPrinterReGS(
                  floorName,
                  nikalaIP,
                  context,
                  s,
                  dRef,
                  orderType,
                  Qty,
                  reason,
                  steward,
                  itemGS,
                  "Failed",
                  element.Name));
            } else {
              cancelPrinterGS[index] = CancelPrinterReGS(
                  floorName,
                  nikalaIP,
                  context,
                  s,
                  dRef,
                  orderType,
                  Qty,
                  reason,
                  steward,
                  itemGS,
                  "Failed",
                  element.Name);
            }
            notifyListeners();
          }
        }
      }

      notifyListeners();
    });
  }

  printCancelFloorNikalaSeprated(
      NetworkPrinter printer,
      String kot,
      String subTable,
      String floorName,
      String tableID,
      String kitenID,
      String kitenName,
      String orderType,
      String item,
      String Qty,
      String reason) async {
    printer.rawBytes(printCustom('KOT : ${kot}', 3, 1, 0));

    printer.rawBytes(printCustom(orderType, 3, 1, 0));

    if (orderType == 'RUNNING ORDER') {
      final ByteData data = await rootBundle.load('assets/running.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    } else if (orderType == 'NEW ORDER') {
      final ByteData data = await rootBundle.load('assets/neworder.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    } else if (orderType == 'TAKE AWAY') {
      final ByteData data = await rootBundle.load('assets/takeaway.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    } else if (orderType == 'CANCELLED') {
      final ByteData data = await rootBundle.load('assets/stop.jpg');
      final Uint8List bytes = data.buffer.asUint8List();
      var image = decodeImage(bytes);
      printer.rawBytes(printerBytes);
      printer.image(image!);
    }
    printer.rawBytes(printCustom(floorName, 2, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom(timestamp, 3, 1, 0));

    printer.rawBytes(printCustom('TABLE : ${tableID}', 2, 1, 1));

    printer.rawBytes(printCustom('SUB TABLE : ${subTable}', 2, 1, 1));

    printer.rawBytes(printerFeed);
    printer.rawBytes(printCustom('ORDERED BY : ${stewardName}', 2, 0, 0));
    printer.rawBytes(printCustom('KICHEN: ${kitenName}', 2, 0, 0));

    printer.hr();

    printer.rawBytes(IndexColumnPrint('Qty', 'Item'));
    printer.hr();
    printer.rawBytes(IndexColumnPrint(Qty, item));
    if (reason != '') {
      printer.rawBytes(IndexColumnPrint('', reason));
    }
    printer.rawBytes(printCustom("ORDER CANCELLED", 2, 1, 0));

    printer.hr();
    printer.feed(2);
    printer.rawBytes(printerBytes);
    printer.text('**<#>..${dOrderTotal}..<#>**',
        styles: PosStyles(align: PosAlign.center, bold: true));
    printer.feed(1);
    printer.cut();
  }

  void validateAndSave(
      ItemGS itemGs, String newQty, String stewardId, String reason) {
    HashMap<String, Object> data = HashMap();
    data["Quantity"] = (int.parse(itemGs.Qty) - int.parse(newQty)).toString();
    data["CancelledBy"] = stewardId;
    data["CancelledReason"] = reason;
    String parent = "";
    double unit = 0.0;
    String? token = mRootReference.push().key;

    data["CancelledStatus"] = "Open";

    if (int.parse(itemGs.Qty) - int.parse(newQty) == 0) {
      data["Status"] = "Cancelled";
      data["ReGenerated"] = "NO";
    }

    mRootReference
        .child(itemGs.dbRef)
        .once()
        .then((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic>? dData = databaseEvent.snapshot.value as Map;

        if (dData["CancelledQuantity"] == null) {
          data["CancelledQuantity"] = newQty;
        } else {
          parent = dData["Parent"];
          unit = double.tryParse(dData["Unit"].toString())!;
          data["CancelledQuantity"] =
              (int.parse(dData["CancelledQuantity"].toString()) +
                      int.parse(newQty))
                  .toString();
        }
        mRootReference.child(itemGs.dbRef).update(data);
      }
    });
    mRootReference
        .child("DishBucket")
        .child(parent)
        .child(token!)
        .set((-int.parse(newQty)) * unit);
  }

  fun_changeQty(ItemGS itemGS, String Qty, String Note, BuildContext context) {
    mRootReference
        .child(itemGS.dbRef)
        .once()
        .then((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic>? dData = databaseEvent.snapshot.value as Map;

        mRootReference
            .child("DishBucket")
            .child(dData["Parent"].toString())
            .push()
            .set(int.parse(qtyTc.text) -
                (int.parse(itemGS.Qty)) *
                    double.tryParse(dData["Unit"].toString())!);
      }
    });
    HashMap<String, Object> data = HashMap();

    data["Quantity"] = Qty;
    data["Note"] = Note;
    mRootReference.child(itemGS.dbRef).update(data);

    finish(context);
  }

  printerRetryFunction(PrinterReGS printerReGS, int index, List<ItemGS> itemGs,
      List<PrinterGS> printerGs, List<String> itemKichens) {
    if (printerReGS.s == "FloorC") {
      fun_consolidatedprint(
          printerReGS.floorName,
          printerReGS.floorIP,
          printerReGS.kot,
          printerReGS.subTable,
          printerReGS.tableID,
          printerReGS.context,
          "FloorC",
          printerReGS.dRef,
          printerReGS.floor,
          printerReGS.orderType,
          index,
          itemGs);
    } else if (printerReGS.s == "NikalaC") {
      fun_floornikalaprint(
          printerReGS.floorName,
          printerReGS.floorIP,
          printerReGS.kot,
          printerReGS.subTable,
          printerReGS.tableID,
          printerReGS.context,
          "NikalaC",
          printerReGS.dRef,
          printerReGS.floor,
          printerReGS.orderType,
          index,
          itemGs);
    } else if (printerReGS.s == "NikalaS") {
      fun_nikalaSeprated(
          printerReGS.floorName,
          printerReGS.floorIP,
          printerReGS.kot,
          printerReGS.subTable,
          printerReGS.tableID,
          printerReGS.context,
          "NikalaS",
          printerReGS.dRef,
          printerReGS.floor,
          printerReGS.orderType,
          index,
          printerReGS.kitchenId,
          itemGs,
          printerGs,
          itemKichens);
    } else if (printerReGS.s == "Kitchen") {
      fun_kichenPrint(
          printerReGS.floorName,
          printerReGS.floorIP,
          printerReGS.kot,
          printerReGS.subTable,
          printerReGS.tableID,
          printerReGS.context,
          "Kitchen",
          printerReGS.dRef,
          printerReGS.floor,
          printerReGS.orderType,
          index,
          printerReGS.kitchenId,
          itemGs,
          printerGs,
          itemKichens);
    }
  }

  cancelPrinterRetryFunction(CancelPrinterReGS printerReGS, int index) {
    if (printerReGS.s == "FloorC") {
      fun_cancelconsolidatedprint(
          printerReGS.floorName,
          printerReGS.nikalaIP,
          printerReGS.context,
          printerReGS.s,
          printerReGS.dRef,
          printerReGS.orderType,
          printerReGS.Qty,
          printerReGS.reason,
          printerReGS.steward,
          printerReGS.itemGS,
          index);
    } else if (printerReGS.s == "NikalaS") {
      fun_cancelnikalaSeprated(
          printerReGS.floorName,
          printerReGS.nikalaIP,
          printerReGS.context,
          printerReGS.s,
          printerReGS.dRef,
          printerReGS.orderType,
          printerReGS.Qty,
          printerReGS.reason,
          printerReGS.steward,
          printerReGS.itemGS,
          index);
    }
  }

  SubTableCountRound(int Subtable) {
    if (Subtable != null) {
      SubtableCount = (Subtable / 3).ceilToDouble();
      halfSubtableCount = SubtableCount! / 2;
    }
  }

  void funC_RefereshTable(String floor, String tableID, String subTableID,
      String orderID, String orderType, BuildContext context) {
    if (itemGS.length == 0) {
      mRootReference
          .child("TableOrders")
          .child("Floors")
          .child(floor)
          .child("Tables")
          .child(tableID)
          .child("SubTables")
          .child(subTableID)
          .remove()
          .then((value) {
        mRootReference
            .child("Tables")
            .child(floor)
            .child(tableID)
            .child("Order")
            .once()
            .then((DatabaseEvent databaseEvent) {
          if (databaseEvent.snapshot.value != null) {
            Map<dynamic, dynamic> subTableData =
                databaseEvent.snapshot.value as Map;
            subTableData.forEach((key, value) {
              if (value == subTableID) {
                mRootReference
                    .child("Tables")
                    .child(floor)
                    .child(tableID)
                    .child("Order")
                    .child(key.toString())
                    .remove();
                try {
                  mRootReference
                      .child("Tables")
                      .child(floor)
                      .child(tableID)
                      .child("SubTableOrder")
                      .child(subTableID.replaceAll('S', ''))
                      .remove();
                } catch (e) {}
              }
            });
            finish(context);
            finish(context);
          } else {
            finish(context);
            finish(context);
          }
        });
      }).catchError((error, stackTrace) {
        final snackBar = SnackBar(
            backgroundColor: timeout_snack,
            duration: Duration(milliseconds: 5000),
            content: Text(
              'Something went wrong',
              textAlign: TextAlign.center,
              softWrap: true,
              style: snackbarStyle,
            ));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        notifyListeners();
      });

      notifyListeners();
    } else {
      final snackBar = SnackBar(
          backgroundColor: timeout_snack,
          duration: Duration(milliseconds: 3000),
          content: Text(
            'PLEASE DELETE ALL ITEM BEFORE',
            textAlign: TextAlign.center,
            softWrap: true,
            style: snackbarStyle,
          ));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      notifyListeners();
    }
  }

  void funC_RegeneratedBills(BuildContext context) {
    DateTime now = new DateTime.now();
    DateTime date = new DateTime(now.year, now.month, now.day, 0, 0, 0);
    DateTime date2 = new DateTime(now.year, now.month + 1, now.day, 0, 0, 0);
    db
        .collection("Bills")
        .where("Time",
            isGreaterThan: date.millisecondsSinceEpoch,
            isLessThan: date2.millisecondsSinceEpoch)
        .snapshots()
        .listen((QuerySnapshot querySnapshot) {
      daybillGS.clear();
      querySnapshot.docs.forEach((element2) {
        if (element2.get("Status") == "BillPrinted") {
          String stTable = element2.get("Floor") +
              " T: " +
              element2.get("TableNumber") +
              "S : " +
              element2.get("SubTable");

          // var millis = int.tryParse(element2.get("BillingTime").toString())!;
          // var dt = DateTime.fromMillisecondsSinceEpoch(millis);
          //
          // final formatter = DateFormat('MM/dd/yyyy H:m:a');
          // final String timestamp0 = formatter.format(dt);
          final String timestamp0 = element2.get("BillingTime").toString();

          daybillGS.add(new DaybillGS(
              element2.id.toString(),
              element2.get("Status"),
              timestamp0,
              element2.id.toString(),
              stTable,
              element2.get("TotalAmount").toString(),
              "0xFF000000"));
          notifyListeners();
        }
      });
    });
  }

  void funCRegenerate(BuildContext context, String billNo) {
    showLoaderDialog(context);
    mRootReference
        .child("ActiveBills")
        .child(billNo)
        .once()
        .then((DatabaseEvent databaseEvent) {
      DateTime dayNow = DateTime.now();
      String dayNode = dayNow.year.toString() +
          "/" +
          dayNow.month.toString() +
          "/" +
          dayNow.day.toString();
      mRootReference
          .child('ReGeneratedBills')
          .child(dayNode)
          .child(billNo)
          .set(databaseEvent.snapshot.value);
      mRootReference
          .child('ReGeneratedBills')
          .child(dayNode)
          .child(billNo)
          .child('ReGeneratedBy')
          .set(uid);
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;

        final String F = map['Floor'];
        final String T = map['TableNumber'];
        final String S = map['SubTable'];
        int SubTable = 900 + Random().nextInt(99);

        final snackBar = SnackBar(
            backgroundColor: timeout_snack,
            duration: Duration(milliseconds: 1500),
            content: Text(
              'IDENTIFIED SUBTABLE IS + ${SubTable}',
              textAlign: TextAlign.center,
              softWrap: true,
              style: snackbarStyle,
            ));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        mRootReference
            .child("Tables")
            .child(F)
            .child(T)
            .child("SubTableNP")
            .child(SubTable.toString().replaceAll('S', ''))
            .set(map['Steward']);

        DateTime now = new DateTime.now();
        String Today = now.year.toString() +
            "/" +
            now.month.toString() +
            "/" +
            now.day.toString();
        String TempSubTable = 'S' + SubTable.toString();

        mRootReference
            .child("Orders")
            .child(Today)
            .child(F)
            .child(T)
            .child(S.replaceAll("S", ""))
            .once()
            .then((DatabaseEvent databaseEvent) {
          print('dsbssdsdsdsds');
          Map<dynamic, dynamic> OrderMap = databaseEvent.snapshot.value as Map;
          OrderMap.forEach((OrderID, ordervalue) {
            ordervalue.forEach((ID, value2) {
              if (value2['BillNo'].toString() == billNo) {
                mRootReference
                    .child("EditedOrders")
                    .child(Today)
                    .child(F)
                    .child(T)
                    .child(TempSubTable)
                    .child(OrderID.toString())
                    .child(ID.toString())
                    .remove();
                mRootReference
                    .child("Tables")
                    .child(F)
                    .child(T)
                    .child("Order")
                    .child(OrderID.toString())
                    .set(TempSubTable.replaceAll('S', ''));
                mRootReference
                    .child("Tables")
                    .child(F)
                    .child(T)
                    .child("SubTableOrder")
                    .child(TempSubTable.replaceAll('S', ''))
                    .child(OrderID.toString())
                    .set(OrderID.toString());
                mRootReference
                    .child("Orders")
                    .child(Today)
                    .child(F)
                    .child(T)
                    .child(TempSubTable.replaceAll("S", ""))
                    .child(OrderID.toString())
                    .child(ID.toString())
                    .set(value2)
                    .then((value) {
                  if (S != TempSubTable) {
                    mRootReference
                        .child("Orders")
                        .child(Today)
                        .child(F)
                        .child(T)
                        .child(S.replaceAll("S", ""))
                        .child(OrderID.toString())
                        .child(ID.toString())
                        .remove();
                  }
                  Map<String, Object> DataO = HashMap();
                  DataO['Status'] = 'OrderPrinted';
                  DataO['ReGenerated'] = billNo;
                  DataO['SubTable'] = TempSubTable;
                  mRootReference
                      .child("Orders")
                      .child(Today)
                      .child(F)
                      .child(T)
                      .child(TempSubTable.replaceAll("S", ""))
                      .child(OrderID.toString())
                      .child(ID.toString())
                      .update(DataO)
                      .then((value) {
                    Map<String, Object> Data = HashMap();
                    Data['OrderID'] = OrderID.toString();
                    Data['OrderRef'] =
                        'Orders/$Today/${F}/${T}/${TempSubTable.replaceAll('S', '')}/${OrderID.toString()}';
                    mRootReference
                        .child("TableOrders")
                        .child("Floors")
                        .child(F)
                        .child('Tables')
                        .child(T)
                        .child("SubTables")
                        .child(TempSubTable)
                        .child('Orders')
                        .child(OrderID.toString())
                        .update(Data)
                        .then((value) {
                      mRootReference
                          .child("TableOrders")
                          .child("Floors")
                          .child(F)
                          .child('Tables')
                          .child(T)
                          .child("SubTables")
                          .child(TempSubTable)
                          .child('OrderedBy')
                          .child('ID')
                          .set(map['Steward']);
                      finish(context);
                      db.collection("Bills").doc(billNo).delete();
                      db.collection("BillsEdited").doc(billNo).delete();
                      mRootReference
                          .child("ActiveBills")
                          .child(billNo)
                          .remove();
                      SuccessAlert(context, T, TempSubTable, billNo);

                      final snackBar2 = SnackBar(
                          backgroundColor: timeout_snack,
                          duration: Duration(milliseconds: 500),
                          content: Text(
                            "SUCCESS FULLY REGENERATED TO TABLE : ${T} SUBTABLE : ${SubTable} ",
                            textAlign: TextAlign.center,
                            softWrap: true,
                            style: snackbarStyle,
                          ));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar2);
                      final snackBar3 = SnackBar(
                          backgroundColor: timeout_snack,
                          duration: Duration(milliseconds: 500),
                          content: Text(
                            "REACHED  $Today F:  $F T:  $T: S $S",
                            textAlign: TextAlign.center,
                            softWrap: true,
                            style: snackbarStyle,
                          ));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar3);
                      print(
                          ' Today  ${Today}  F  ${F}  T  ${T}   TempSubTable ${TempSubTable}  OrderID  ${OrderID.toString()} ID  ${ID.toString()} ');
                    });
                  });
                });

                // mRootReference
                //     .child("Orders")
                //     .child(Today)
                //     .child(F)
                //     .child(T)
                //     .child(TempSubTable.replaceAll("S", ""))
                //     .child(OrderID.toString())
                //     .child(ID.toString())
                //     .child("Status")
                //     .set("OrderPrinted");
                // mRootReference
                //     .child("Orders")
                //     .child(Today)
                //     .child(F)
                //     .child(T)
                //     .child(TempSubTable.replaceAll("S", ""))
                //     .child(OrderID.toString())
                //     .child(ID.toString())
                //     .child("ReGenerated")
                //     .set(billNo);
                // mRootReference
                //     .child("Orders")
                //     .child(Today)
                //     .child(F)
                //     .child(T)
                //     .child(TempSubTable.replaceAll("S", ""))
                //     .child(OrderID.toString())
                //     .child(ID.toString())
                //     .child("SubTable")
                //     .set(TempSubTable);

                notifyListeners();
              }
            });
          });
        });
      }

      notifyListeners();
    });
  }

  void SuccessAlert(
      BuildContext context, String table, String subtable, String billNo) {
    // set up the buttons

    Widget continueButton = TextButton(
      child: Text("Yes"),
      onPressed: () {
        funC_RegeneratedBills(context);
        finish(context);
        finish(context);
        callNextReplacement(TableListing(), context);

        notifyListeners();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("SUCCESS FULLY REGENERATED"),
      content: Text(
          "SUCCESS FULLY REGENERATED TO TABLE :  ${table}   SUBTABLE : ${subtable}"),
      actions: [
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> fun_finalPrintwithCustomerData(String orderID, String subTableID,
      String tableID, BuildContext context, String uid, String floor) async {
    int count = 1;
    bool isSuccess = false;
    while (count < 50) {
      count++;
      const PaperSize paper = PaperSize.mm80;
      final profile = await CapabilityProfile.load();
      final printer = NetworkPrinter(paper, profile);

      final PosPrintResult res = await printer.connect(FloorIP, port: 9100);
      if (res == PosPrintResult.success) {
        isSuccess = true;
        // List<Map<String, Object>> NITEMS = [];
        // List<String> NITEMCODE = [];
        // int i = 0;
        // itemGS.forEach((element) {
        //   if (element.Qty.toString() != '0') {
        //     if (element.Name.toString() != 'DISCOUNT') {
        //       if (!NITEMCODE.contains(
        //           element.productId.toString() + element.Rate.toString())) {
        //         Map<String, Object> Item = new HashMap();
        //         Item['Item'] = element.Name.toString();
        //         Item['ItemID'] = element.productId.toString();
        //         Item['Quantity'] = element.Qty.toString();
        //         Item['Rate'] = element.Rate.toString();
        //         Item['Total'] = element.Total.toString();
        //         NITEMCODE.add(
        //             element.productId.toString() + element.Rate.toString());
        //
        //         NITEMS.add(Item);
        //       } else {
        //         if (NITEMS.isNotEmpty) {
        //           Map<String, Object> data = new HashMap();
        //           if (NITEMCODE.contains(
        //               element.productId.toString() + element.Rate.toString())) {
        //             int index = NITEMCODE.indexOf(
        //                 element.productId.toString() + element.Rate.toString());
        //
        //             data = NITEMS[NITEMCODE.indexOf(
        //                 element.productId.toString() +
        //                     element.Rate.toString())];
        //
        //             int tempQty = int.tryParse(data["Quantity"].toString())! +
        //                 int.tryParse(element.Qty.toString())!;
        //             data['Quantity'] = tempQty.toString();
        //             double? dQTY = double.tryParse(element.Qty.toString());
        //             double? dRate = double.tryParse(element.Rate.toString());
        //             double dTotal = double.tryParse(data["Total"].toString())! +
        //                 (dQTY! * dRate!);
        //             data['Total'] = dTotal.toStringAsFixed(2);
        //             data['Item'] = element.Name.toString();
        //             data['ItemID'] = element.productId.toString();
        //             data['Rate'] = element.Rate.toString();
        //
        //             NITEMS[index] = data;
        //           }
        //         }
        //       }
        //     } else {
        //       double? dq = double.tryParse(element.Qty.toString());
        //       double? dR = double.tryParse(element.Rate.toString());
        //       dDiscount = (dq! * dR!);
        //     }
        //   }
        //
        //   i++;
        // });
        if (strBillNo != null && strBillNo != 'NIL') {
          await FinalprintReceiptwithCustomer(
              printer, orderID, tableID, subTableID, context, NITEMS);

          billPrintAfterSetDevice(
              orderID, tableID, subTableID, context, uid, floor, res.msg);
          notifyListeners();
          printer.disconnect();
        }
        break;
      }
    }
    if (count == 50) {
      if (!isSuccess) {
        finish(context);
        final snackBar = SnackBar(
            backgroundColor: timeout_snack,
            content: Text(
              'Failed',
              textAlign: TextAlign.center,
              softWrap: true,
              style: snackbarStyle,
            ));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    }

    notifyListeners();
  }

  FinalprintReceiptwithCustomer(
      NetworkPrinter printer,
      String orderID,
      String tableID,
      String subTableID,
      BuildContext context,
      List<Map<String, Object>> NITEMS) async {
    double CGST = (AT * 2.5) / 100;
    double SGST = (AT * 2.5) / 100;
    double dbDiscount = dDiscount;
    final double NewAT = AT;
    final double NewANT = ANT;
    final int NewItemCount = BillItemCountNT + BillITemCountT;
    double dFinalAmt = NewAT + NewANT + CGST + SGST;

    double dAmt = NewAT + NewANT + CGST + SGST;

    // double RoundOf = NewAT + NewANT + CGST + SGST - Math.round(NewAT + NewANT + CGST + SGST);
    double RoundOf = dAmt - dAmt.round();

    // Print image
    final ByteData data = await rootBundle.load('assets/Delicia.jpg');
    final Uint8List bytes = data.buffer.asUint8List();
    var image = decodeImage(bytes);
    printer.rawBytes(printerBytes);
    printer.image(image!);

    printer.rawBytes(printCustom('${strLine1}', 5, 1, 1));
    printer.rawBytes(printCustom('${strLine2}', 5, 1, 0));
    printer.rawBytes(printCustom('${strLine3}', 5, 1, 0));
    printer.rawBytes(printCustom('${strLine4}', 5, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom('${timestamp}', 5, 1, 0));

    printer.rawBytes(printCustom(
        '${FloorName}    TABLE : ${tableID}  SUB ORDER : ${subTableID}',
        1,
        1,
        1));
    printer.rawBytes(printCustom('BILL ID : ${strBillNo}', 5, 1, 0));
    printer.rawBytes(printCustom('STEWARD ID : ${stewardName}', 5, 1, 0));

    printer.hr(ch: '-', linesAfter: 0);

    printer.rawBytes(printCustom('TABLE ITEMS', 5, 1, 1));

    printer.hr(
      ch: '-',
    );
    printer.rawBytes(printerBytesLeft);
    printer.row([
      PosColumn(text: 'PRODUCT', width: 7),
      PosColumn(text: 'QTY', width: 1),
      PosColumn(
          text: 'RATE', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: 'TOTAL', width: 2, styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.hr(
      ch: '=',
    );
    NITEMS.forEach((element) {
      printer.rawBytes(printerBytesLeft);
      printer.row([
        PosColumn(text: element['Item'].toString(), width: 7),
        PosColumn(text: element['Quantity'].toString(), width: 1),
        PosColumn(
            text: element['Rate'].toString(),
            width: 2,
            styles: PosStyles(align: PosAlign.right)),
        PosColumn(
            text: element['Total'].toString(),
            width: 2,
            styles: PosStyles(align: PosAlign.right)),
      ]);
    });

    printer.hr(
      ch: '-',
    );
    if (dbDiscount < 0.0) {
      double Tot = NewAT + dbDiscount.abs();
      printer.rawBytes(printerBytes);
      printer.row([
        PosColumn(
            text: 'TOTAL', width: 6, styles: PosStyles(align: PosAlign.left)),
        PosColumn(
            text: Tot.toStringAsFixed(2),
            width: 6,
            styles: PosStyles(align: PosAlign.right)),
      ]);
      printer.rawBytes(printerBytes);
      printer.row([
        PosColumn(
            text: 'DISCOUNT',
            width: 6,
            styles: PosStyles(align: PosAlign.left)),
        PosColumn(
            text: dbDiscount.toStringAsFixed(2),
            width: 6,
            styles: PosStyles(align: PosAlign.right)),
      ]);
    }
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'TAXABLE ITEM TOTAL',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: NewAT.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'SGST (2.50%)',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: SGST.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'CGST (2.50%)',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: CGST.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);

    printer.hr(ch: '=', linesAfter: 1);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'RoundOf  : ${RoundOf.toStringAsFixed(2)}',
          width: 8,
          styles: PosStyles(align: PosAlign.left, width: PosTextSize.size1)),
      PosColumn(
          text: '',
          width: 4,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    ]);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'Grand Total',
          width: 6,
          styles: PosStyles(align: PosAlign.left, width: PosTextSize.size2)),
      PosColumn(
          text: dFinalAmt.round().toStringAsFixed(2),
          width: 6,
          styles: PosStyles(
              align: PosAlign.right,
              width: PosTextSize.size2,
              height: PosTextSize.size3)),
    ]);
    printer.hr(
      ch: '-',
    );
    printer.rawBytes(printerBytesLeft);

    printer.text(
      'NAME : ${nameController.text} ',
      styles: PosStyles(
        align: PosAlign.left,
        height: PosTextSize.size1,
        width: PosTextSize.size1,
        bold: true,
      ),
    );
    printer.rawBytes(printerBytesLeft);

    printer.text(
      'MOBILE : ${phoneController.text} ',
      styles: PosStyles(
        align: PosAlign.left,
        height: PosTextSize.size1,
        width: PosTextSize.size1,
        bold: true,
      ),
    );
    printer.rawBytes(printerBytesLeft);

    if (gstController.text != '') {
      printer.text(
        'GST : ${gstController.text} ',
        styles: PosStyles(
          align: PosAlign.left,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          bold: true,
        ),
      );
    }
    printer.rawBytes(printerBytesLeft);

    if (addressController.text != '') {
      printer.text(
        'ADDRESS : ${addressController.text} ',
        styles: PosStyles(
          align: PosAlign.left,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          bold: true,
        ),
      );
    }
    printer.rawBytes(printerBytesLeft);

    if (landMarkController.text != '') {
      printer.text(
        'LANDMARK : ${landMarkController.text} ',
        styles: PosStyles(
          align: PosAlign.left,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          bold: true,
        ),
      );
    }
    printer.rawBytes(printerBytesLeft);

    if (notesController.text != '') {
      printer.text(
        'NOTES : ${notesController.text} ',
        styles: PosStyles(
          align: PosAlign.left,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          bold: true,
        ),
      );
    }

    printer.hr(ch: '-');
    printer.feed(1);
    printer.rawBytes(printerBytes);
    printer.text('>>..THANK YOU..<<',
        styles: PosStyles(align: PosAlign.center, bold: true), linesAfter: 1);
    printer.rawBytes(printerBytes);
    printer.rawBytes(printCustom('Powered By : Spine', 5, 1, 0));

    printer.feed(1);
    printer.cut();
  }

  Future<void> fun_finalPrintwithFAS(String orderID, String subTableID,
      String tableID, BuildContext context, String uid, String floor) async {
    int count = 1;
    bool isSuccess = false;
    while (count <= 50) {
      count++;
      const PaperSize paper = PaperSize.mm80;
      final profile = await CapabilityProfile.load();
      final printer = NetworkPrinter(paper, profile);

      final PosPrintResult res = await printer.connect(FloorIP, port: 9100);
      if (res == PosPrintResult.success) {
        isSuccess = true;
        // List<Map<String, Object>> NITEMS = [];
        // List<String> NITEMCODE = [];
        // int i = 0;
        // itemGS.forEach((element) {
        //   if (element.Qty.toString() != '0') {
        //     if (element.Name.toString() != 'DISCOUNT') {
        //       if (!NITEMCODE.contains(
        //           element.productId.toString() + element.Rate.toString())) {
        //         Map<String, Object> Item = new HashMap();
        //         Item['Item'] = element.Name.toString();
        //         Item['ItemID'] = element.productId.toString();
        //         Item['Quantity'] = element.Qty.toString();
        //         Item['Rate'] = element.Rate.toString();
        //         Item['Total'] = element.Total.toString();
        //         NITEMCODE.add(
        //             element.productId.toString() + element.Rate.toString());
        //
        //         NITEMS.add(Item);
        //       } else {
        //         if (NITEMS.isNotEmpty) {
        //           Map<String, Object> data = new HashMap();
        //           if (NITEMCODE.contains(
        //               element.productId.toString() + element.Rate.toString())) {
        //             int index = NITEMCODE.indexOf(
        //                 element.productId.toString() + element.Rate.toString());
        //
        //             data = NITEMS[NITEMCODE.indexOf(
        //                 element.productId.toString() +
        //                     element.Rate.toString())];
        //
        //             int tempQty = int.tryParse(data["Quantity"].toString())! +
        //                 int.tryParse(element.Qty.toString())!;
        //             data['Quantity'] = tempQty.toString();
        //             double? dQTY = double.tryParse(element.Qty.toString());
        //             double? dRate = double.tryParse(element.Rate.toString());
        //             double dTotal = double.tryParse(data["Total"].toString())! +
        //                 (dQTY! * dRate!);
        //             data['Total'] = dTotal.toStringAsFixed(2);
        //             data['Item'] = element.Name.toString();
        //             data['ItemID'] = element.productId.toString();
        //             data['Rate'] = element.Rate.toString();
        //
        //             NITEMS[index] = data;
        //           }
        //         }
        //       }
        //     } else {
        //       double? dq = double.tryParse(element.Qty.toString());
        //       double? dR = double.tryParse(element.Rate.toString());
        //       dDiscount = (dq! * dR!);
        //       notifyListeners();
        //     }
        //   }
        //
        //   i++;
        // });
        if (strBillNo != "" && strBillNo != 'NIL') {
          await FinalprintReceiptwithFADBill(
              printer, orderID, tableID, subTableID, context, NITEMS);

          billPrintAfterSetDevice(
              orderID, tableID, subTableID, context, uid, floor, res.msg);
          notifyListeners();
          printer.disconnect();
        }
        break;
      }
    }
    if (count == 50) {
      if (!isSuccess) {
        finish(context);
        final snackBar = SnackBar(
            backgroundColor: timeout_snack,
            content: Text(
              'Failed',
              textAlign: TextAlign.center,
              softWrap: true,
              style: snackbarStyle,
            ));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    }
    notifyListeners();
  }

  FinalprintReceiptwithFADBill(
      NetworkPrinter printer,
      String orderID,
      String tableID,
      String subTableID,
      BuildContext context,
      List<Map<String, Object>> NITEMS) async {
    double CGST = (AT * 2.5) / 100;
    double SGST = (AT * 2.5) / 100;
    double dbDiscount = dDiscount;
    final double NewAT = AT;
    final double NewANT = ANT;
    final int NewItemCount = BillItemCountNT + BillITemCountT;
    double dFinalAmt = NewAT + NewANT + CGST + SGST;

    double dAmt = NewAT + NewANT + CGST + SGST;

    // double RoundOf = NewAT + NewANT + CGST + SGST - Math.round(NewAT + NewANT + CGST + SGST);
    double RoundOf = dAmt - dAmt.round();

    // Print image
    final ByteData data = await rootBundle.load('assets/Delicia.jpg');
    final Uint8List bytes = data.buffer.asUint8List();
    var image = decodeImage(bytes);
    printer.rawBytes(printerBytes);
    printer.image(image!);

    printer.rawBytes(printCustom('${strLine1}', 5, 1, 1));
    printer.rawBytes(printCustom('${strLine2}', 5, 1, 0));
    printer.rawBytes(printCustom('${strLine3}', 5, 1, 0));
    printer.rawBytes(printCustom('${strLine4}', 5, 1, 0));

    final now = DateTime.now();
    final formatter = DateFormat('MM/dd/yyyy hh:mm a');
    final String timestamp = formatter.format(now);
    printer.rawBytes(printCustom('${timestamp}', 5, 1, 0));

    printer.rawBytes(printCustom(
        '${FloorName}    TABLE : ${tableID}  SUB ORDER : ${subTableID}',
        1,
        1,
        1));
    printer.rawBytes(printCustom('BILL ID : ${strBillNo}', 5, 1, 0));
    printer.rawBytes(printCustom('STEWARD ID : ${stewardName}', 5, 1, 0));

    printer.hr(ch: '-', linesAfter: 0);

    printer.rawBytes(printCustom('TABLE ITEMS', 5, 1, 1));

    printer.hr(
      ch: '-',
    );
    printer.rawBytes(printerBytesLeft);
    printer.row([
      PosColumn(text: 'PRODUCT', width: 7),
      PosColumn(text: 'QTY', width: 1),
      PosColumn(
          text: 'RATE', width: 2, styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: 'TOTAL', width: 2, styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.hr(
      ch: '=',
    );
    for (var element in NITEMS) {
      printer.rawBytes(printerBytesLeft);
      printer.row([
        PosColumn(text: element['Item'].toString(), width: 7),
        PosColumn(text: element['Quantity'].toString(), width: 1),
        PosColumn(
            text: element['Rate'].toString(),
            width: 2,
            styles: PosStyles(align: PosAlign.right)),
        PosColumn(
            text: element['Total'].toString(),
            width: 2,
            styles: PosStyles(align: PosAlign.right)),
      ]);
    }
    printer.rawBytes(printerBytesLeft);
    printer.row([
      PosColumn(text: 'FOOD AND SERVICE', width: 7),
      PosColumn(text: "1", width: 1),
      PosColumn(
          text: NewAT.toStringAsFixed(2),
          width: 2,
          styles: PosStyles(align: PosAlign.right)),
      PosColumn(
          text: NewAT.toStringAsFixed(2),
          width: 2,
          styles: PosStyles(align: PosAlign.right)),
    ]);

    printer.hr(
      ch: '-',
    );
    if (dbDiscount < 0.0) {
      double Tot = NewAT + dbDiscount.abs();
      printer.rawBytes(printerBytes);
      printer.row([
        PosColumn(
            text: 'TOTAL', width: 6, styles: PosStyles(align: PosAlign.left)),
        PosColumn(
            text: Tot.toStringAsFixed(2),
            width: 6,
            styles: PosStyles(align: PosAlign.right)),
      ]);
      printer.rawBytes(printerBytes);
      printer.row([
        PosColumn(
            text: 'DISCOUNT',
            width: 6,
            styles: PosStyles(align: PosAlign.left)),
        PosColumn(
            text: dbDiscount.toStringAsFixed(2),
            width: 6,
            styles: PosStyles(align: PosAlign.right)),
      ]);
    }
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'TAXABLE ITEM TOTAL',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: NewAT.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'SGST (2.50%)',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: SGST.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'CGST (2.50%)',
          width: 6,
          styles: PosStyles(align: PosAlign.left)),
      PosColumn(
          text: CGST.toStringAsFixed(2),
          width: 6,
          styles: PosStyles(align: PosAlign.right)),
    ]);

    printer.hr(ch: '=', linesAfter: 1);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'RoundOf  : ${RoundOf.toStringAsFixed(2)}',
          width: 8,
          styles: PosStyles(align: PosAlign.left, width: PosTextSize.size1)),
      PosColumn(
          text: '',
          width: 4,
          styles: PosStyles(align: PosAlign.right, width: PosTextSize.size2)),
    ]);
    printer.rawBytes(printerBytes);
    printer.row([
      PosColumn(
          text: 'Grand Total',
          width: 6,
          styles: PosStyles(align: PosAlign.left, width: PosTextSize.size2)),
      PosColumn(
          text: dFinalAmt.round().toStringAsFixed(2),
          width: 6,
          styles: PosStyles(
              align: PosAlign.right,
              width: PosTextSize.size2,
              height: PosTextSize.size3)),
    ]);

    printer.hr(ch: '-');
    printer.rawBytes(printerBytesLeft);

    printer.text(
      'NAME : ${nameController.text} ',
      styles: PosStyles(
        align: PosAlign.left,
        height: PosTextSize.size1,
        width: PosTextSize.size1,
        bold: true,
      ),
    );
    printer.rawBytes(printerBytesLeft);

    printer.text(
      'MOBILE : ${phoneController.text} ',
      styles: PosStyles(
        align: PosAlign.left,
        height: PosTextSize.size1,
        width: PosTextSize.size1,
        bold: true,
      ),
    );
    printer.rawBytes(printerBytesLeft);

    if (gstController.text != '') {
      printer.text(
        'GST : ${gstController.text} ',
        styles: PosStyles(
          align: PosAlign.left,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          bold: true,
        ),
      );
    }
    printer.rawBytes(printerBytesLeft);

    if (addressController.text != '') {
      printer.text(
        'ADDRESS : ${addressController.text} ',
        styles: PosStyles(
          align: PosAlign.left,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          bold: true,
        ),
      );
    }
    printer.rawBytes(printerBytesLeft);

    if (landMarkController.text != '') {
      printer.text(
        'LANDMARK : ${landMarkController.text} ',
        styles: PosStyles(
          align: PosAlign.left,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          bold: true,
        ),
      );
    }
    printer.rawBytes(printerBytesLeft);

    if (notesController.text != '') {
      printer.text(
        'NOTES : ${notesController.text} ',
        styles: PosStyles(
          align: PosAlign.left,
          height: PosTextSize.size1,
          width: PosTextSize.size1,
          bold: true,
        ),
      );
    }
    printer.hr(ch: '-');
    printer.feed(1);
    printer.rawBytes(printerBytes);
    printer.text('>>..THANK YOU..<<',
        styles: PosStyles(align: PosAlign.center, bold: true), linesAfter: 1);
    printer.rawBytes(printerBytes);
    printer.rawBytes(printCustom('Powered By : Spine', 5, 1, 0));

    printer.feed(1);
    printer.cut();

    notifyListeners();
  }

  ///Customer more button
  funMoreButtonTrue() {
    moreOpen = true;
    if (costumersPhone.contains(phoneController.text)) {
      mRootReference
          .child("CustomerData")
          .child(phoneController.text)
          .once()
          .then((DatabaseEvent databaseEvent) {
        Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;

        gstController.text = map["CustomerGST"] ?? "";
        addressController.text = map["CustomerAddress"] ?? "";
        landMarkController.text = map["CustomerLandMark"] ?? "";
        if (map["CustomerDOB"] != null && map["CustomerDOB"]["time"] != null) {
          int millis = int.parse(map["CustomerDOB"]["time"].toString());
          var dt = DateTime.fromMillisecondsSinceEpoch(millis);
          var d24 = DateFormat('dd/MM/yyyy').format(dt);
          dateOfBirthController.text = d24.toString();
        }
        notesController.text = map["CustomerNotes"] ?? "";
      });
    } else {
      gstController.text = "";
      addressController.text = "";
      landMarkController.text = "";
      dateOfBirthController.text = "";
      notesController.text = "";
    }
    notifyListeners();
  }

  funMoreButtonFalse() {
    moreOpen = false;
    notifyListeners();
  }

  funAddItemClick() {
    AddItemClick = "";
    notifyListeners();
  }

  funFloorChange(String floorId, String floorName) async {
    SelectedFloor = floorId;
    selectedFloorName = floorName;
    SharedPreferences userPreference = await SharedPreferences.getInstance();

    userPreference.setString("SelectedFloor", floorId);
    userPreference.setString("SelectedFloorName", floorName);
    notifyListeners();
  }

  void floorConsolidated(bool value) {
    isFloorConsolidated = value;
    notifyListeners();
  }

  void floorNIkConsolidated(bool value) {
    isNikalaConsolidated = value;
    notifyListeners();
  }

  void floorNIkSeprated(bool value) {
    isNikalaSeparated = value;
    notifyListeners();
  }

  void kichenConsolidatedP(String type, bool changedvalue, int index) {
    if (type == 'CS') {
      printerGS[index].Consolidated = changedvalue;
    } else {
      printerGS[index].Seprated = changedvalue;
    }
    notifyListeners();
  }

  funLoadReasons() {
    mRootReference
        .child("DeleteReasons")
        .once()
        .then((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;
        reasons.clear();
        map.forEach((key, value) {
          reasons.add(value.toString());
        });
      }
    });
  }

  funDateC(String date) {
    var millis = int.parse(date);
    var dt = DateTime.fromMillisecondsSinceEpoch(millis);
// 12 Hour format:
    var d12 = DateFormat('yyyy/M/d').format(dt);
    // print('d12    ${d12}');
    return d12;
  }

  funGetTokens() {
    DateTime now = new DateTime.now();
    String DayNode = now.year.toString() +
        "/" +
        now.month.toString() +
        "/" +
        now.day.toString();
    mRootReference
        .child("TOKENS")
        .child(DayNode)
        .onValue
        .listen((DatabaseEvent databaseEvent) {
      if (databaseEvent.snapshot.value != null) {
        try {
          Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;
          tokenId = map.keys.length + 1;
        } catch (error) {
          tokenId = 1;
        }
        print("ee     " + tokenId.toString());
      } else {
        tokenId = 1;
      }
      notifyListeners();
    });
  }

  List<DishItemGS> getSuggestions(String query) {
    List<DishItemGS> matches = [];

    if (query != "") {
      matches.addAll(dishesID_Name_list);
      matches.retainWhere((s) => s.dishID.toLowerCase().contains(query.toLowerCase())||s.dishName.toLowerCase().contains(query.toLowerCase()));
      matches.sort((a, b) => int.parse(a.shortNumber).compareTo(int.parse(b.shortNumber)));
      return matches;
    }
    return matches;
  }

  getTakeawaySubTable() {
    DateTime now = DateTime.now();
      String DayNode = now.year.toString() +
          "/" +
          now.month.toString() +
          "/" +
          now.day.toString();
    DateTime date = DateTime(now.year, now.month, now.day, 0, 0, 0);
    DateTime date2 = DateTime(now.year, now.month, now.day + 1, 0, 0, 0);
    // mRootReference.child('Orders').child(DayNode).child("4").onValue.listen((DatabaseEvent databaseEvent) {
    //   for (var tables in databaseEvent.snapshot.children) {
    //     for (var subTables in tables.children) {
    //       Map<dynamic, dynamic> map = subTables.value as Map;
    //
    //       oldTokens.add(OldTokenGs(
    //                       subTables.key.toString(),
    //                       element.get('BillNo'),
    //                       phone,
    //                       element.get('Steward'),
    //                       element.get('TableNumber')));
    //                   oldTokens.sort((a, b) => int.parse(a.subTaleId).compareTo(int.parse(b.subTaleId)));
    //     }
    //   }
    //
    // });
    db
        .collection('Bills')
        // .where("Floor", isEqualTo: "4")
        // .where("TableNumber", isEqualTo: 'TA')
        .where("Time",
            isGreaterThan: date.millisecondsSinceEpoch,
            isLessThan: date2.millisecondsSinceEpoch)
        .snapshots()
        .listen((event) async {
      oldTokens.clear();
      if (event.docs.isNotEmpty) {
        for (var element in event.docs) {
          if(element.get('Floor').toString()=="4"){
            String phone='No Number';
            try {
              if(element.get('CustomerPhone').toString()!='0000000000'){
                phone=element.get('CustomerPhone').toString();
              }
            } catch (error) {

            }

            oldTokens.add(OldTokenGs(
                element.get('SubTable'),
                element.get('BillNo'),
                phone,
                element.get('Steward'),
                element.get('TableNumber')));
            oldTokens.sort((a, b) => int.parse(a.subTaleId).compareTo(int.parse(b.subTaleId)));

          }


          notifyListeners();
        }
      }
      notifyListeners();
    });
    notifyListeners();
  }

  getTABillDetails(String subTableID,String tableId) {
    print("tableID $tableId");
    DateTime now =  DateTime.now();
    String DayNode = now.year.toString() +
        "/" +
        now.month.toString() +
        "/" +
        now.day.toString();
    mRootReference
        .child('Orders')
        .child(DayNode)
        .child('4')
        .child(tableId)
        .child(subTableID)
        .once()
        .then((DatabaseEvent databaseEvent) {
      itemGS.clear();
      dOrderTotal = 0.0;
      dOrderTaxTotal = 0.0;
      ANT = 0;
      BillItemCountNT = 0;
      BillITemCountT = 0;
      AT = 0;
      String BN = '';
      bool ReGenerate = false;
      Regeneate = false;

      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> orderData = databaseEvent.snapshot.value as Map;

        orderData.forEach((kotId, kotValue) {
          Map<dynamic, dynamic> DISHDATA = kotValue;

          DISHDATA.forEach((id, value) {
            if (value["Status"].toString() != "Cancelled") {
              String Id = '';
              String Name = '';
              String Qty = '';
              String Rate = '';
              String Total = '';
              String kitchen = '';
              String Status = '';
              String dbRef = '';
              String floor = '';
              String table = '';
              String subTableId = '';
              String productId = '';
              String kotNumber = '';
              String Notes = '';
              String time = '';

              double? dqty = 0.0;
              double? dRate = 0.0;
              double? dTotal = 0.0;

              if (value['Quantity'] != null) {
                dqty = double.tryParse(value['Quantity']);
              }
              if (value['Rate'] != null) {
                dRate = double.tryParse(value['Rate']);
              }
              if (value['Status'] == 'OrderPrinted' ||
                  value['Status'] == 'OrderTaken') {
                TableItemCoount++;
              }
              if (value['Taxablity'] == 'false') {
                ANT = ANT + dqty! * dRate!;
              } else {
                BillITemCountT++;
                AT = AT + dqty! * dRate!;
              }

              dTotal = dqty * dRate;
              dOrderTotal = dOrderTotal + dTotal;

              String path = '';

              Id = id.toString();
              Name = value['Item'].toString();
              Qty = value['Quantity'].toString();
              Rate = value['Rate'].toString();
              Total = dTotal.toString();
              kitchen = value['Kitchen'].toString();
              Status = value['Status'].toString();
              dbRef = path;
              floor = '4';
              table = 'TA';
              subTableId = subTableID;
              productId = value['ItemID'].toString();
              kotNumber = value['OrderID'].toString();
              Notes = value["Note"].toString();
              time = value["Time"].toString();

              itemGS.add(
                ItemGS(
                  Id,
                  Name,
                  Qty,
                  Rate,
                  Total,
                  kitchen,
                  Status,
                  dbRef,
                  floor,
                  table,
                  subTableId,
                  productId,
                  kotNumber,
                  Notes,
                  time,
                ),
              );
              notifyListeners();
            }
            itemGS.sort((a, b) => a.time.compareTo(b.time));
          });
        });

        double CGST = (dOrderTotal * 2.5) / 100;
        double SGST = (dOrderTotal * 2.5) / 100;
        dOrderTaxTotal = dOrderTotal + CGST + SGST;
        notifyListeners();
      }

      notifyListeners();
    });
  }

  // checkConnection(){
  //   Connectivity().onConnectivityChanged.listen((ConnectivityResult result){
  //
  //     print('ConnectivityResult  eeeeeeeee     ${result.toString()}');
  //
  //
  //   });
  // }

  Future<void> initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
      print('result  ${result}');
    } on PlatformException catch (e) {
      print(e.toString());
    }
    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
      case ConnectivityResult.none:
        _connectionStatus = result.toString();
        break;
      default:
        _connectionStatus = 'Failed to get connectivity.';
        break;
    }
    notifyListeners();

    print('_connectionStatus  ${_connectionStatus}');
  }

  Future<bool> checkConnection(BuildContext context) async {
    bool previousConnection = hasConnection;

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        hasConnection = true;
      } else {
        hasConnection = false;
        fun_Snackbar('No Internet Access', context);
        notifyListeners();
      }
    } on SocketException catch (_) {
      hasConnection = false;
      fun_Snackbar('No Internet Access', context);
      notifyListeners();
    }

    //The connection status changed send out an update to all listeners
    if (previousConnection != hasConnection) {
      connectionChangeController.add(hasConnection);
    }
    print('hasConnection   ${hasConnection}');

    notifyListeners();

    return hasConnection;
  }

  void fun_Snackbar(String msg, BuildContext context) {
    final snackBar = SnackBar(
        backgroundColor: timeout_snack,
        duration: Duration(milliseconds: 5000),
        content: Text(
          '${msg}',
          textAlign: TextAlign.center,
          softWrap: true,
          style: snackbarStyle,
        ));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    notifyListeners();
  }

  FourColumAlighPrint(String str1, String str2, String str3, String str4) {
    if (str1.length > 19) {
      str1 = str1.substring(0, 19);
    }

    List<int> SPEED = [0x1B, 0x21, 0x02];

    List<int> printdata = SPEED;

    SPEED = [0x1B, 0x44, 20, 26, 32, 00];

    printdata = printdata + SPEED;

    printdata = printdata + utf8.encode(str1);

    SPEED = [0x09];

    printdata = printdata + SPEED;

    printdata = printdata + utf8.encode(str2);

    SPEED = [0x09];

    printdata = printdata + SPEED;

    printdata = printdata + utf8.encode(str3);

    SPEED = [0x09];

    printdata = printdata + SPEED;

    printdata = printdata + utf8.encode(str4);

    return printdata;
  }

  IndexColumnPrint(String index, String str2) {
    if (str2.length > 30) {
      str2 = str2.substring(0, 30);
    }
    if (index.length > 5) {
      index = index.substring(0, 5);
    }

    List<int> SPEED = [0x1B, 0x21, 0x02];

    List<int> printdata = SPEED;

    SPEED = [0x1B, 0x44, 3, 7, 00];

    printdata = printdata + SPEED;

    printdata = printdata + printerBytesLeft;

    SPEED = [0x09];

    printdata = printdata + SPEED;

    printdata = printdata + utf8.encode(index);
    printdata = printdata + SPEED;
    printdata = printdata + utf8.encode(str2);
    printdata = printdata + printerFeed;

    return printdata;
  }

  printCustom(
      final String msg, final int size, final int align, final int style) {
    //Print config "mode"

//        new Thread(new Runnable() {
    // @Override
    //   public void run() {
    List<int> printdata = [];

    List<int> cc = [0x1B, 0x21, 0x03]; // 0- normal size text
    // byte[] height = new byte[]{0x1B, 0x21, 0x11};  // 0- normal size text
    //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
    List<int> bb = [0x1B, 0x21, 0x08]; // 1- only bold text
    List<int> bb2 = [0x1B, 0x21, 0x20]; // 2- bold with medium text
    List<int> bb3 = [0x1B, 0x21, 0x10]; // 3- bold with large text
    List<int> bb5 = [0x1B, 0x21, 0x02]; // 3- bold with default text
    //            try {

    switch (size) {
      case 0:
        printdata = printdata + cc;

        //       outputStream.write(cc);

        break;
      case 1:
        printdata = printdata + bb;
        //  outputStream.write(bb);

        break;
      case 2:
        printdata = printdata + bb2;

        //    outputStream.write(bb2);

        break;
      case 3:
        printdata = printdata + bb3;

        //    outputStream.write(bb3);

        break;
      case 4:
        printdata = printdata + bb2;
        printdata = printdata + bb3;

        //    outputStream.write(bb2);
        //    outputStream.write(bb3);

        break;
      case 5:
        printdata = printdata + bb5;

        //    outputStream.write(bb5);

        break;
    }

    switch (align) {
      case 0:
        //left align

        printdata = printdata + printerBytesLeft;

        //    outputStream.write(PrintService.ESC_ALIGN_LEFT);

        break;
      case 1:
        //center align

        printdata = printdata + printerBytes;

        //    outputStream.write(PrintService.ESC_ALIGN_CENTER);
        break;
      case 2:
        //right align
        printdata = printdata + printerBytesRight;

        //    outputStream.write(PrintService.ESC_ALIGN_RIGHT);
        break;
    }

    if (style == 1) {
      printdata = printdata + printerBold;
    }

    printdata = printdata + utf8.encode(msg) + bb5;
    if (style == 1) {
      printdata = printdata + printerCancelBold;
    }
    printdata = printdata + printerFeed;

    return printdata;
  }

  funCalculate(String orderId) {
    kotAmount = 0.0;
    kotNumber = orderId;
    for (var element in itemGS) {
      if (element.kotNumber == orderId) {
        kotAmount = kotAmount +
            (double.parse(element.Qty) * double.parse(element.Rate));
      }
    }
    notifyListeners();
  }

  getItemsList(List<ItemGS> _itemGs, String kot, String kitchen) {
    List<int> printData = [];

    for (var item in _itemGs) {
      if (item.kotNumber == kot) {
        if (item.kitchen == kitchen) {
          printData = printData + IndexColumnPrint(item.Qty, item.Name);
          if (item.Notes != '') {
            printData = printData + IndexColumnPrint('', '( ${item.Notes} )');
          }
        }
      }
    }
    return printData;
  }

  getItemListCon(List<ItemGS> _itemGs, String kot) {
    List<int> printData = [];
    for (var item in _itemGs) {
      if (item.kotNumber == kot) {
        printData = printData + IndexColumnPrint(item.Qty, item.Name);
        if (item.Notes != '') {
          printData = printData + IndexColumnPrint('', '( ${item.Notes} )');
        }
      }
    }
    return printData;
  }

  fetchCategory() {
    mRootReference
        .child('CategoryKitchen')
        .once()
        .then((DatabaseEvent databaseEvent) {
      itemCategory.clear();
      if (databaseEvent.snapshot.value != null) {
        Map<dynamic, dynamic> map = databaseEvent.snapshot.value as Map;
        map.forEach((key, value) {
          itemCategory.add(key.toString());
          notifyListeners();
        });
      }
      notifyListeners();
    });
  }

  void getTokenID() {
    DateTime now = DateTime.now();
    String DayNode = now.year.toString() +
        "/" +
        now.month.toString() +
        "/" +
        now.day.toString();
    mRootReference.child('KotToken').child(strDeviceID).child(DayNode).child('kotID').once().then((DatabaseEvent databaseEvent) {
      if(databaseEvent.snapshot.value!=null){
        kotID=(int.parse(databaseEvent.snapshot.value.toString())+1).toString();
      }else{
        kotID="1";
      }
    });
  }
}
