import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Views/hour_sale_gs.dart';


class ReportProvider with ChangeNotifier{

  final DatabaseReference mRootReference =
  FirebaseDatabase.instance.reference();
  final FirebaseFirestore db = FirebaseFirestore.instance;
  ///total
  int ttlNoBills=0;
  double ttlAmountOfBills=0.0;
  ///settled
  int ttlNoSettledBills=0;
  double ttlAmountSettledBills=0.0;
  double ttlAmountSGSTSettledBills=0.0;
  double ttlAmountCGSTSettledBills=0.0;
  double ttlAmountTaxSettledBills=0.0;
  double ttlAmountWithOutTaxSettledBills=0.0;
  double ttlDiscountSettled=0.0;
  double ttlCashSettled=0.0;
  double ttlCardSettled=0.0;
  double ttlUpiSettled=0.0;
  double ttlCreditSettled=0.0;
  ///pending
  int ttlNoPendingBills=0;
  double ttlAmountPendingBills=0.0;
  double ttlAmountSGSTPendingBills=0.0;
  double ttlAmountCGSTPendingBills=0.0;
  double ttlAmountTaxPendingBills=0.0;
  double ttlAmountWithOutTaxPendingBills=0.0;

  ///hourBaseSale
  List<HourSaleGS> listHourSale=[
    HourSaleGS("0",0.0),
    HourSaleGS("1",0.0),
    HourSaleGS("2",0.0),
    HourSaleGS("3",0.0),
    HourSaleGS("4",0.0),
    HourSaleGS("5",0.0),
    HourSaleGS("6",0.0),
    HourSaleGS("7",0.0),
    HourSaleGS("8",0.0),
    HourSaleGS("9",0.0),
    HourSaleGS("10",0.0),
    HourSaleGS("11",0.0),
    HourSaleGS("12",0.0),
    HourSaleGS("13",0.0),
    HourSaleGS("14",0.0),
    HourSaleGS("15",0.0),
    HourSaleGS("16",0.0),
    HourSaleGS("17",0.0),
    HourSaleGS("18",0.0),
    HourSaleGS("19",0.0),
    HourSaleGS("20",0.0),
    HourSaleGS("21",0.0),
    HourSaleGS("22",0.0),
    HourSaleGS("23",0.0),
  ];

  DateTime fromDate =  DateTime.now();
  DateTime toDate =  DateTime.now();




  Future<void> selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2101));
    if (picked != null) {
      fromDate=picked;
      toDate=DateTime(picked.year, picked.month, picked.day, 23, 59,59);
      funBillDetails(fromDate, toDate);
    }
    notifyListeners();
  }
  funBillDetails(DateTime fFromDate,DateTime fToDate){
    listHourSale=[
      HourSaleGS("0",0.0),
      HourSaleGS("1",0.0),
      HourSaleGS("2",0.0),
      HourSaleGS("3",0.0),
      HourSaleGS("4",0.0),
      HourSaleGS("5",0.0),
      HourSaleGS("6",0.0),
      HourSaleGS("7",0.0),
      HourSaleGS("8",0.0),
      HourSaleGS("9",0.0),
      HourSaleGS("10",0.0),
      HourSaleGS("11",0.0),
      HourSaleGS("12",0.0),
      HourSaleGS("13",0.0),
      HourSaleGS("14",0.0),
      HourSaleGS("15",0.0),
      HourSaleGS("16",0.0),
      HourSaleGS("17",0.0),
      HourSaleGS("18",0.0),
      HourSaleGS("19",0.0),
      HourSaleGS("20",0.0),
      HourSaleGS("21",0.0),
      HourSaleGS("22",0.0),
      HourSaleGS("23",0.0),
    ];
    db.collection("Bills").where("BillingTime", isGreaterThan:fFromDate ).where("BillingTime", isLessThan:fToDate ).orderBy("BillingTime").snapshots().listen((event) async {
      ttlNoBills=0;
      ttlAmountOfBills=0.0;
      ttlNoSettledBills=0;
      ttlAmountSettledBills=0.0;
      ttlAmountTaxSettledBills=0.0;
      ttlAmountWithOutTaxSettledBills=0.0;
       ttlCashSettled=0.0;
       ttlCardSettled=0.0;
       ttlUpiSettled=0.0;
       ttlCreditSettled=0.0;
       ttlAmountSGSTSettledBills=0.0;
       ttlAmountCGSTSettledBills=0.0;
       ttlAmountSGSTPendingBills=0.0;
       ttlAmountCGSTPendingBills=0.0;

      if(event.docs.isNotEmpty){
        ttlNoBills=event.docs.length;

        for (var element in event.docs) {
          if (element.get("TotalAmount") != null) {
            ttlAmountOfBills = ttlAmountOfBills + element.get("TotalAmount");
          }
          if(element.get("Status") !=null){
            if(element.get("Status") =='BillPayed'){
              ttlNoSettledBills++;
              if(element.get("TotalAmount")!=null){
                ttlAmountSettledBills=ttlAmountSettledBills+double.parse(element.get("TotalAmount").toString());
              }
              if(element.get("SGST")!=null){
                ttlAmountSGSTSettledBills=ttlAmountSGSTSettledBills+double.parse(element.get("SGST").toString());
              }
              if(element.get("CGST")!=null){
                ttlAmountCGSTSettledBills=ttlAmountCGSTSettledBills+double.parse(element.get("CGST").toString());
              }
              if(element.get("TaxableAmount")!=null){
                ttlAmountWithOutTaxSettledBills=ttlAmountWithOutTaxSettledBills+double.parse(element.get("TaxableAmount").toString());
              }
              if(element.get("Discount")!=null){
                ttlDiscountSettled=ttlDiscountSettled+double.parse(element.get("Discount").toString());
              }
              if(element.get('Cash')!=null){
                ttlCashSettled=ttlCashSettled+double.parse(element.get("Cash").toString());
              }
              if(element.get('Card')!=null){
                ttlCardSettled=ttlCardSettled+double.parse(element.get("Card").toString());
              }
              if(element.get('UPI')!=null){
                ttlUpiSettled=ttlUpiSettled+double.parse(element.get("UPI").toString());
              }
              if(element.get('Credit')!=null){
                ttlCreditSettled=ttlCreditSettled+double.parse(element.get("Credit").toString());
              }

            }
            if(element.get("Status") =='BillPrinted'){
              ttlNoPendingBills++;
              if(element.get("TotalAmount")!=null){
                ttlAmountPendingBills=ttlAmountPendingBills+double.parse(element.get("TotalAmount").toString());
              }
              if(element.get("SGST")!=null){
                ttlAmountSGSTPendingBills=ttlAmountSGSTPendingBills+double.parse(element.get("SGST").toString().toString());
              }
              if(element.get("CGST")!=null){
                ttlAmountCGSTPendingBills=ttlAmountCGSTPendingBills+double.parse(element.get("CGST").toString());
              }
              if(element.get("TaxableAmount")!=null){
                ttlAmountWithOutTaxPendingBills=ttlAmountWithOutTaxPendingBills+double.parse(element.get("TaxableAmount").toString());
              }
            }


          }
          Timestamp timestamp=element.get('BillingTime');
          DateTime time=DateTime.parse(timestamp.toDate().toString());
          switch (time.hour) {
            case 0:
              double oldAmount=listHourSale[0].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[0].sale=newAmount;
              break;
            case 1:
              double oldAmount=listHourSale[1].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[1].sale=newAmount;
              break;
            case 2:
              double oldAmount=listHourSale[2].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[2].sale=newAmount;
              break;
            case 3:
              double oldAmount=listHourSale[3].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[3].sale=newAmount;
              break;
            case 4:
              double oldAmount=listHourSale[4].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[4].sale=newAmount;
              break;
            case 5:
              double oldAmount=listHourSale[5].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[5].sale=newAmount;
              break;
            case 6:
              double oldAmount=listHourSale[6].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[6].sale=newAmount;
              break;
            case 7:
              double oldAmount=listHourSale[7].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[7].sale=newAmount;
              break;
            case 8:
              double oldAmount=listHourSale[8].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[8].sale=newAmount;
              break;
            case 9:
              double oldAmount=listHourSale[9].sale;
            double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
            listHourSale[9].sale=newAmount;
              break;
            case 10:
              double oldAmount=listHourSale[10].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[10].sale=newAmount;
              break;
            case 11:
              double oldAmount=listHourSale[11].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[11].sale=newAmount;
              break;
            case 12:
              double oldAmount=listHourSale[12].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[12].sale=newAmount;
              break;
            case 13:
              double oldAmount=listHourSale[13].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[13].sale=newAmount;
              break;
            case 14:double oldAmount=listHourSale[14].sale;
            double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
            listHourSale[14].sale=newAmount;
              break;
            case 15:
              double oldAmount=listHourSale[15].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[15].sale=newAmount;
              break;
            case 16:
              double oldAmount=listHourSale[16].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[16].sale=newAmount;
              break;
            case 17:
              double oldAmount=listHourSale[17].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[17].sale=newAmount;
              break;
            case 18:
              double oldAmount=listHourSale[18].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[18].sale=newAmount;
              break;
            case 19:
              double oldAmount=listHourSale[19].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[19].sale=newAmount;
              break;
            case 20:
              double oldAmount=listHourSale[20].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[20].sale=newAmount;
              break;
            case 21:
              double oldAmount=listHourSale[21].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[21].sale=newAmount;
              break;
            case 22:
              double oldAmount=listHourSale[22].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[22].sale=newAmount;
              break;
            case 23:
              double oldAmount=listHourSale[23].sale;
              double newAmount=oldAmount+double.parse(element.get("TotalAmount").toString());
              listHourSale[23].sale=newAmount;
              break;
          }
          notifyListeners();
        }

        ttlAmountTaxSettledBills=ttlAmountSGSTSettledBills+ttlAmountCGSTSettledBills;
        ttlAmountTaxPendingBills=ttlAmountSGSTPendingBills+ttlAmountCGSTPendingBills;
        notifyListeners();

      }
      notifyListeners();

    });
  }


}