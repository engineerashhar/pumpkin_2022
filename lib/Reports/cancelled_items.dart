import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Provider/mainsale_provider.dart';
import 'package:provider/provider.dart';

class CancelledItems extends StatelessWidget {
  const CancelledItems({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<MainSaleProvider>(
          builder: (context,value,child) {
            return value.cancelledGS.isEmpty?Container(
              child: const Center(child: Text('No Cancelled Items'),),
            ):ListView.builder(
                itemCount: value.cancelledGS.length,
                scrollDirection: Axis.vertical,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: (){

                    },
                    child: Card(
                      elevation: 0,
                      child: Container(
                          margin: const EdgeInsets.symmetric(horizontal: 5),
                          padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                          decoration: BoxDecoration(
                              color: my_white
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(value.cancelledGS[index].itemName,style: const TextStyle(
                                  fontSize: 16
                              ),),
                              Text(value.cancelledGS[index].qty,style: const TextStyle(fontSize: 16),),
                            ],
                          )),
                    ),
                  );
                });
          }
      ),
    );
  }
}