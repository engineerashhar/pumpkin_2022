import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_string.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/Provider/counterwise_provider.dart';
import 'package:flutter_pumpkin/Provider/reports_provider.dart';
import 'package:flutter_pumpkin/Views/counter_pie_model.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class CounterWiseReport extends StatelessWidget {
   CounterWiseReport({Key? key}) : super(key: key);
   List<CounterPieGS> pieGS = [];


   @override
  Widget build(BuildContext context) {
     // pieGS.add(CounterPieGS('Cash : ₹520135', 120,  cashColor,"Cash"));
     // pieGS.add(CounterPieGS('UPI : ₹846546', 20,  upiColor,"UPI"));
     // pieGS.add(CounterPieGS('Card : ₹645498', 30, cardColor,"Card"));
     // pieGS.add(CounterPieGS('Credit : ₹541244', 30, creditColor,"Credit"));

     MediaQueryData queryData;
     queryData = MediaQuery.of(context);
     CounterWiseProvider counterWiseProvider = Provider.of<CounterWiseProvider>(context,listen: false);





     return Scaffold(
      backgroundColor: my_white,
       appBar: AppBar(
         backgroundColor: my_white,
         title: Text(
           'Counter Wise Report',
           style: GoogleFonts.redHatDisplay(fontWeight: FontWeight.bold, fontSize: textSize18, color: my_black),
         ),
         leading: InkWell(
             onTap: () {
               finish(context);
             },
             child: const Icon(Icons.arrow_back_ios, color:  my_black,)),
         actions:<Widget> [
           Padding(
             padding: const EdgeInsets.only(right: 8.0),
             child: InkWell(
                 onTap: (){
                   counterWiseProvider.selectDate(context);
                 },
                 child: const Icon(Icons.calendar_today, color: my_black,size: 35,)),
           ),
         ],
       ),

       body: SafeArea(
        maintainBottomViewPadding: true,
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              // Center(child: Text("Counter Wise Report", style: GoogleFonts.redHatDisplay(fontWeight: FontWeight.bold, fontSize: textSize18, color: my_black),))
              // ,
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Consumer<CounterWiseProvider>(
                  builder: (context,value,child){
                    return  Container(
                      width: double.infinity,
                      decoration: const BoxDecoration(
                        color:cardBgColor,
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),

                      child:
                      DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                          value:value.counterDropDownValue,


                          icon: const Icon(Icons.keyboard_arrow_down,color: my_black,),
                          elevation: 16,
                          style: const TextStyle(color: my_black,),
                          underline: Container(
                            height: 2,
                            color: Colors.black,
                          ),
                          onChanged: (newValue) {
                            counterWiseProvider.setDropdown(context,newValue!);
                          },
                          items: value.counterListArray.map((location) {
                            return DropdownMenuItem(
                              child:  Text(location),
                              value: location,
                            );
                          }).toList(),
                        ),
                      ),
                    );

                  },

                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Consumer<CounterWiseProvider>(
                    builder: (context,value,child){
                      return   Text(
                        value.counterDropDownValue=='Select Counter'? 'Sales report':' ${value.counterDropDownValue} Sales report',
                        style: smallHeaderText,
                      );

                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Card(
                  color: cardBgColor,
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: SizedBox(
                      height:queryData.size.height* 0.22 ,
                      child: Consumer<CounterWiseProvider>(
                        builder: (context,value,child){
                          return  Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 12.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Center(child: Text("Sale Amount",style: headerText,))) ,
                                    // Expanded(
                                    //   flex: 2,
                                    //     child: Center(child: Text("Bill Amount",style: headerText,))),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 1,
                                        child: Center(child: Text("₹ ${value.dTotalAmount}",style: amountText,))) ,
                                    // Expanded(
                                    //   flex: 2,
                                    //     child: Center(child: Text("₹ 1502122",style: amountText,))),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 2,
                                        child: Center(child: Text("Taxable",style: taxText,))) ,
                                    // Expanded(
                                    //   flex: 2,
                                    //     child: Center(child: Text("Taxable",style: taxText,))),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 3.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 2,
                                        child: Center(child: Text("₹ ${value.dTaxbleAmount}",style: taxText,))) ,
                                    // Expanded(
                                    //   flex: 2,
                                    //     child: Center(child: Text("₹ 15021",style: taxText,))),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 2,
                                        child: Center(child: Text("Non-Taxable",style: taxText,))) ,
                                    // Expanded(
                                    //   flex: 2,
                                    //     child: Center(child: Text("Non-Taxable",style: taxText,))),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 3.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 2,
                                        child: Center(child: Text("₹ ${value.dNonTaxableAmount}",style: taxText,))) ,
                                    // Expanded(
                                    //   flex: 2,
                                    //     child: Center(child: Text("₹ 15021",style: taxText,))),
                                  ],
                                ),
                              ),
                            ],
                          );

                        },
                      )
                  ),
                ),
              ) ,

              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Counter 1 Payments',
                    style: smallHeaderText,
                  ),
                ),
              ),
              
              Consumer<CounterWiseProvider>(
                builder: (context,value,child){
                  return  
                    Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Card(
                      color: cardBgColor,
                      shape: BeveledRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: SizedBox(
                        height:queryData.size.height* 0.30 ,
                        child: value.counterWisePieGS.isNotEmpty?
                        SfCircularChart(


                          borderWidth: 2,
                          tooltipBehavior: TooltipBehavior(enable: true),
                          legend: Legend(
                              isVisible: true,
                              // textStyle: headerText,
                              position: LegendPosition.right

                          ),
                          // title: ChartTitle(text: 'Counter 1 Payments', textStyle: headerText),
                          annotations: [
                            CircularChartAnnotation(
                              widget: Text(value.dTotalAmount.toStringAsFixed(2), style: annotationText),
                            )
                          ],
                          series: <CircularSeries>[
                            DoughnutSeries<CounterPieGS, String>(
                              innerRadius: '65%',
                              explode: true,
                              dataSource:value.counterWisePieGS,
                              xValueMapper: (CounterPieGS data, _) => data.labelData,
                              yValueMapper: (CounterPieGS data, _) => data.total,
                              dataLabelMapper: (CounterPieGS data, _) => data.type,
                              pointColorMapper: (CounterPieGS data, _) => data.color,
                            )
                          ],
                        ): Center(child: const Text('No counter close'),)
                      ),
                    ),
                  );

                },
              ) ,
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  color: cardBgColor,
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  elevation: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Consumer<CounterWiseProvider>(
                      builder: (context,value,child){
                        return    Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Payment",style: paymentText,) ,
                            Text("₹ ${value.payment}",style: paymentAmountText,),
                          ],
                        );

                      },
                    ),
                  ),

                ),
              ) ,
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  color: cardBgColor,
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  elevation: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Consumer<CounterWiseProvider>(
                      builder: (context,value,child){
                        return   Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text("Reciept",style: paymentText,) ,
                            Text("₹ ${value.rCash}",style: paymentAmountText,),
                          ],
                        );

                      },
                    ),
                  ),

                ),
              ) ,






            ],
          ),
        ),
      ),
    );
  }
}
