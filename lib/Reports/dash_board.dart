import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Provider/reports_provider.dart';
import 'package:flutter_pumpkin/Views/hour_sale_gs.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:charts_flutter/flutter.dart' as charts;


class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ReportProvider  reportProvider= Provider.of<ReportProvider>(context, listen: false);

    List<charts.Series<HourSaleGS, String>> series = [
      charts.Series(
          id: "Subscribers",
          data: reportProvider.listHourSale,
          domainFn: (HourSaleGS series, _) => series.hour.toString(),
          measureFn: (HourSaleGS series, _) => series.sale,
          colorFn: (_, __) => charts.Color.fromHex(code:'#5EBFA4'),
          labelAccessorFn: (HourSaleGS row, _) => row.sale.toString())
    ];
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("",style: GoogleFonts.redHatDisplay()),
        backgroundColor: my_green,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.logout,
              color: Colors.white,
            ),
            onPressed: () {
              reportProvider.selectDate(context);
            },
          )
        ],
      ),
      body: charts.BarChart(
        series, animate: true,

        barRendererDecorator:  charts.BarLabelDecorator<String>(),
        domainAxis:  const charts.OrdinalAxisSpec(),

      ),
    );
  }

}
