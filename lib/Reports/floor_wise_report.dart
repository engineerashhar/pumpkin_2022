import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/Provider/floorwise_provider.dart';
import 'package:flutter_pumpkin/Provider/reports_provider.dart';
import 'package:flutter_pumpkin/Reports/stewardwise_report.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';

import 'itemwise_report.dart';

class FloorWiseReport extends StatelessWidget {
  const FloorWiseReport({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    FloorWiseProvider floorWiseProvider = Provider.of<FloorWiseProvider>(context,listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: my_white,
        title: Text(
          'Floor Wise Report',
          style: GoogleFonts.redHatDisplay(fontWeight: FontWeight.bold, fontSize: textSize18, color: my_black),
        ),
        leading: InkWell(
            onTap: () {
              finish(context);
            },
            child: const Icon(Icons.arrow_back_ios, color:  my_black,)),
        actions:<Widget> [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: InkWell(
                onTap: (){
                  floorWiseProvider.selectDate(context);
                  },
                child: const Icon(Icons.calendar_today, color: my_black,size: 35,)),
          ),
        ],
      ),


      body: SafeArea(
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: <Widget>[
            // Center(child: Text("Floor Wise Report", style: GoogleFonts.redHatDisplay(fontWeight: FontWeight.bold, fontSize: textSize18, color: my_black),)),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Consumer<FloorWiseProvider>(
                builder: (context,value,child){
                  return  Container(
                    width: double.infinity,
                    decoration: const BoxDecoration(
                      color:cardBgColor,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),

                    child:
                    DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value:value.floorDropDownValue,


                        icon: const Icon(Icons.keyboard_arrow_down,color: my_black,),
                        elevation: 16,
                        style: const TextStyle(color: my_black,),
                        underline: Container(
                          height: 2,
                          color: Colors.black,
                        ),
                        onChanged: (newValue) {
                          floorWiseProvider.setDropdown(context,newValue.toString());

                        },
                        items: value.floorListNameArray.map((location) {
                          return DropdownMenuItem(

                            child:  Text(location),

                            value: location,
                          );
                        }).toList(),
                      ),
                    ),
                  );

                },

              ),
            ),

            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  'Floor 1 Orders',
                  style: smallHeaderText,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Card(
                color: cardBgColor,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child:  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                    children: [
                      Consumer<FloorWiseProvider>(
                        builder: (context,value,child){
                          return  CircularPercentIndicator(
                            radius: 100.0,
                            lineWidth: 10.0,
                            percent: 1,
                            addAutomaticKeepAlive: true,
                            animateFromLastPercent: true,
                            animation: true,
                            circularStrokeCap: CircularStrokeCap.round,
                            curve:Curves.linear ,
                            backgroundWidth: -2,
                            center:   Text(value.dTotalCount.toStringAsFixed(0),style: TextStyle(color:progressColor ),),
                            backgroundColor: my_white,
                            progressColor: progressColor,
                            footer:  Padding(
                              padding: const EdgeInsets.only(top: 5.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children:  [
                                  const Text(
                                    "Total",
                                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                  ),
                                  Text(
                                    ' ₹ ${value.dTotalSaleAmount.toString()}',
                                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                  ),
                                ],
                              ),
                            ),
                          );

                        },

                      ),
                      Consumer<FloorWiseProvider>(
                        builder: (context,value,child){
                          return CircularPercentIndicator(
                            radius: 100.0,
                            lineWidth: 10.0,
                            percent: value.dSetteledPerc,
                            addAutomaticKeepAlive: true,
                            animateFromLastPercent: true,
                            animation: true,
                            circularStrokeCap: CircularStrokeCap.round,
                            curve:Curves.linear ,
                            backgroundWidth: -2,



                            center:   Text(value.dSetteledCount.toStringAsFixed(0),style: TextStyle(color:progressColor ),),
                            backgroundColor: my_white,
                            progressColor: progressColor,
                            footer:  Padding(
                              padding: const EdgeInsets.only(top: 5.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children:  [
                                  const Text(
                                    "Settled",
                                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                  ),
                                  Text(
                                    ' ₹ ${value.dTotalSettledAmount.toString()}',
                                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                  ),
                                ],
                              ),
                            ),
                          );

                        },
                      ),
                      Consumer<FloorWiseProvider>(
                        builder: (context,value,child){
                          return CircularPercentIndicator(
                            radius: 100.0,
                            lineWidth: 10.0,
                            percent: value.dPendingPerc,
                            addAutomaticKeepAlive: true,
                            animateFromLastPercent: true,
                            animation: true,
                            circularStrokeCap: CircularStrokeCap.round,
                            curve:Curves.linear ,
                            backgroundWidth: -2,



                            center:   Text((value.dPendingCount).toStringAsFixed(0),style: const TextStyle(color:progressColor),),
                            backgroundColor: my_white,
                            progressColor: progressColor,
                            footer:  Padding(
                              padding: const EdgeInsets.only(top: 5.0,bottom: 5.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children:  [
                                  const Text(
                                    "Pending",
                                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                  ),
                                  Text(
                                    ' ₹ ${(value.dTotalSaleAmount-value.dTotalSettledAmount).toStringAsFixed(1)}',
                                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                  ),
                                ],
                              ),
                            ),
                          );

                        },
                      ),
                    ],

                  ),

                ),
              ),
            ) ,
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Item-wise Report',
                    style: smallHeaderText,
                  ),
                  GestureDetector(
                    onTap: (){
                      callNext( TotalItemWiseReport(from:"Floor"), context);
                    },
                    child: Text(
                      'View All',
                      style: smallHeaderText,
                    ),
                  ),
                ],

              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Card(
                color: cardBgColor,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Consumer<FloorWiseProvider>(
                  builder: (context,value,child){
                    return   value.itemWiseGS.isNotEmpty?
                    ListView.builder(
                        itemCount: value.itemWiseGS.length>5?5:value.itemWiseGS.length,
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),


                        itemBuilder: (context, index) {
                          return Card(
                            color: cardBgColor,
                            child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: Row(
                                  children:  [
                                    const Expanded(
                                      flex:1,
                                      child: CircleAvatar(
                                        radius: 20,
                                        backgroundColor: my_white,
                                      ),
                                    ),
                                    Expanded(
                                        flex:5,
                                        child: Padding(
                                          padding: EdgeInsets.only(left: 8.0),
                                          child: Text(value.itemWiseGS[index].itemName),
                                        )),
                                    Expanded(
                                        flex: 1,
                                        child: Text(value.itemWiseGS[index].itemCount)),
                                  ],
                                )),


                          );

                        }
                    ):const SizedBox(height: 10,);


                  },
                ),
              ),
            ) ,
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Steward-wise Report',
                    style: smallHeaderText,
                  ),
                  GestureDetector(
                    onTap: (){
                      callNext(StewardWiseReport(From:"Floor"), context);
                    },
                    child: Text(
                      'View All',
                      style: smallHeaderText,
                    ),
                  ),

                ],

              ),
            ),

            Consumer<FloorWiseProvider>(
              builder: (context,value,child){
                return value.stewarWiseGS.isNotEmpty?
                ListView.builder(
                    itemCount: value.stewarWiseGS.length>5?5:value.stewarWiseGS.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Card(
                        color: cardBgColor,
                        child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              children:  [
                                const Expanded(
                                  flex:1,
                                  child: CircleAvatar(
                                    radius: 20,
                                    backgroundColor: my_white,
                                  ),
                                ),
                                Expanded(
                                    flex:5,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 8.0),
                                      child: Text(value.stewarWiseGS[index].itemName),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Text(value.stewarWiseGS[index].itemCount)),
                              ],
                            )),
                      );

                    }
                ):const SizedBox(height: 10,);

              },
            )








          ],
        ),
      ),
    ),


    );
  }
}
