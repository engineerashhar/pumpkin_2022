import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/Provider/counterwise_provider.dart';
import 'package:flutter_pumpkin/Provider/floorwise_provider.dart';
import 'package:flutter_pumpkin/Provider/mainsale_provider.dart';
import 'package:flutter_pumpkin/Reports/itemwise_report.dart';
import 'package:flutter_pumpkin/Reports/regenerated_bills.dart';
import 'package:flutter_pumpkin/Reports/stewardwise_report.dart';
import 'package:flutter_pumpkin/Views/barchart_model.dart';
import 'package:flutter_pumpkin/Views/counter_pie_model.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:charts_flutter/flutter.dart' as charts;


import 'cancelled_items.dart';
import 'counterwise_report.dart';
import 'floor_wise_report.dart';


class MainSaleReport extends StatefulWidget {
  const MainSaleReport({Key? key}) : super(key: key);

  @override
  _MainSaleReportState createState() => _MainSaleReportState();
}

class _MainSaleReportState extends State<MainSaleReport> {

  @override
  void initState() {
    Future.delayed(Duration.zero, (){
      MainSaleProvider mainSaleProvider = Provider.of<MainSaleProvider>(context,listen: false);
      DateTime fromDate =  DateTime.now();
      //
      DateTime toDate =  DateTime.now();
      fromDate=DateTime(fromDate.year, fromDate.month, fromDate.day,  1, 59,59);
      toDate=DateTime(toDate.year, toDate.month, toDate.day, 23, 59,59);
      mainSaleProvider.funGetSaleData(context,fromDate,toDate);


    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData;
    queryData = MediaQuery.of(context);
    MainSaleProvider mainSaleProvider = Provider.of<MainSaleProvider>(context,listen: false);
    return Scaffold(
      appBar:AppBar(
        backgroundColor: my_white,
        title: Text(
          'Reports',
          style: GoogleFonts.redHatDisplay(fontWeight: FontWeight.bold, fontSize: textSize18, color: my_black),
        ),
        leading: InkWell(
            onTap: () {
              finish(context);
            },
            child: const Icon(Icons.arrow_back_ios, color:  my_black,)),
        // actions:<Widget> [
        //   Padding(
        //     padding: const EdgeInsets.only(right: 8.0),
        //     child: InkWell(
        //         onTap: (){
        //          mainSaleProvider.selectDate(context);
        //         },
        //         child: const Icon(Icons.calendar_today, color: my_black,size: 35,)),
        //   ),
        // ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10.0,right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Today sale Report',
                      style: smallHeaderText,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: InkWell(
                          onTap: (){
                            mainSaleProvider.selectDate(context);

                          },
                          child: const Icon(Icons.calendar_today, color: my_black,size: 35,)),
                    ),
                  ],

                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Card(
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      height:queryData.size.height* 0.23 ,
                      // decoration: const BoxDecoration(
                      //   gradient: LinearGradient(
                      //     colors: [
                      //       my_white,
                      //       cardlinepinkColor,
                      //     ],
                      //     begin: Alignment.topCenter,
                      //     end: Alignment.bottomCenter,
                      //   ),
                      // ),
                      child: Consumer<MainSaleProvider>(
                        builder: (context,value,child){
                          return Row(
                            children: <Widget>[
                              Expanded(
                                flex:5,
                                child: Container(
                                  decoration:  const BoxDecoration(
                                    borderRadius: BorderRadius.only(topLeft:Radius.circular(8),bottomLeft: Radius.circular(8) ),

                                    gradient: LinearGradient(
                                      colors: [
                                        my_white,
                                        cardlinepinkColor,
                                      ],
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                    ),

                                  ),
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height:queryData.size.height* 0.11 ,
                                        child:  Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Center(
                                              child: Text(
                                                'Sale Amount',
                                                style: headerText,
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Padding(
                                                padding: const EdgeInsets.only(top: 8.0,left: 15.0,right: 15.0,bottom: 8.0),
                                                child: Consumer<MainSaleProvider>(
                                                  builder: (context,value,child){
                                                    return  Text(
                                                      ' ₹ ${value.dTotalSaleAmount.toStringAsFixed(2)}',
                                                      style: headerText,
                                                    );


                                                  },
                                                ),
                                              ),
                                            )


                                          ],
                                        ),


                                      ),
                                      Divider(height:queryData.size.height* 0.01,color:lineColor ,),
                                      SizedBox(
                                        height:queryData.size.height* 0.11 ,
                                        child:  Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Center(
                                              child: Text(
                                                'Bill Amount',
                                                style: headerText,
                                              ),
                                            ),
                                            Consumer<MainSaleProvider>(
                                              builder: (context,value,child){
                                                return   Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(top: 8.0,left: 15.0,right: 15.0,bottom: 8.0),
                                                    child: Text(
                                                      ' ₹ ${value.dTotalSettledAmount.toStringAsFixed(2)}',
                                                      style: headerText,
                                                    ),
                                                  ),
                                                );
                                              },
                                            )


                                          ],
                                        ),


                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex:3,
                                child: Container(
                                  decoration:  const BoxDecoration(
                                    borderRadius: BorderRadius.only(topRight:Radius.circular(8),bottomRight: Radius.circular(8) ),
                                    gradient: LinearGradient(
                                      colors: [
                                        my_white,
                                        cardlinepinkColor,
                                      ],
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0,top: 8.0,right: 8.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                            color: lineColor,
                                          ),
                                          borderRadius: BorderRadius.circular(10),
                                          color: my_white
                                      ),
                                      child: Column(
                                        mainAxisAlignment :MainAxisAlignment.spaceEvenly,
                                        mainAxisSize : MainAxisSize.max,
                                        // crossAxisAlignment : CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          Column(
                                            children: [
                                              Center(
                                                child: Text(
                                                  'Regenerated',
                                                  style: amountText,
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: InkWell(
                                                  onTap:(){
                                                    callNext(RegeneratedBillList(), context);
                                                  },
                                                  child: Card(
                                                    color: regenerartedColor,
                                                    child:  Padding(
                                                      padding: const EdgeInsets.only(top: 8.0,left: 15.0,right: 15.0,bottom: 8.0),
                                                      child: Consumer<MainSaleProvider>(
                                                          builder:(context,value,child){
                                                            return  Text(
                                                              value.dRegeneratedCount.toStringAsFixed(0),
                                                              style: amountText,
                                                            );
                                                          }


                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              )


                                            ],
                                          ),
                                          Column(
                                            children: [
                                              Center(
                                                child: Text(
                                                  'Cancelled',
                                                  style: amountText,
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: InkWell(
                                                  onTap: (){
                                                    callNext(CancelledItems(), context);

                                                  },
                                                  child: Card(
                                                    color:cardlightpinkColor,
                                                    child:  Padding(
                                                      padding: const EdgeInsets.only(top: 8.0,left: 15.0,right: 15.0,bottom: 8.0),
                                                      child: Consumer<MainSaleProvider>(
                                                          builder: (context,value,child) {
                                                            return Text(
                                                              value.cancelledBillsCount.toString(),
                                                              style: amountText,
                                                            );
                                                          }
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              )


                                            ],
                                          ),

                                        ],
                                      ),

                                    ),
                                  ),
                                ),
                              )

                            ],

                          );

                        },
                      )
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Sale Report',
                    style: smallHeaderText,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Card(
                  color: cardBgColor,
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: SizedBox(
                    height:queryData.size.height* 0.22 ,
                    child: Consumer<MainSaleProvider>(
                      builder: (context,value,child){
                        return value.series.isNotEmpty? charts.BarChart(
                          value.series, animate: true,
                          barGroupingType:charts.BarGroupingType.stacked ,
                          barRendererDecorator:  charts.BarLabelDecorator<String>(),
                          domainAxis:  const charts.OrdinalAxisSpec(),
                          defaultRenderer:  charts.BarRendererConfig(
                              cornerStrategy: const charts.ConstCornerStrategy(5)),


                        ):Container(
                          child: const Center(
                            child: Text('Processing'),
                          ),
                        );
                      },

                    ),


                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Orders',
                      style: smallHeaderText,
                    ),
                    GestureDetector(
                      onTap: (){
                        FloorWiseProvider floorWiseProvider = Provider.of<FloorWiseProvider>(context,listen: false);
                        DateTime fromDate =  DateTime.now();
                        DateTime toDate =  DateTime.now();
                        fromDate=DateTime(fromDate.year, fromDate.month, fromDate.day,  1, 59,59);
                        toDate=DateTime(toDate.year, toDate.month, toDate.day, 23, 59,59);
                        floorWiseProvider.funGetConfig(context);
                        floorWiseProvider.funGetFloorData(context,fromDate,toDate);

                        callNext(const FloorWiseReport(), context);
                      },
                      child: Text(
                        'View Floor-wise',
                        style: smallHeaderText,
                      ),
                    ),
                  ],

                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Card(
                  color: cardBgColor,
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child:  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                      children: [
                        Consumer<MainSaleProvider>(
                          builder: (context,value,child){
                            return  CircularPercentIndicator(
                              radius: 100.0,
                              lineWidth: 10.0,
                              percent: 1,
                              addAutomaticKeepAlive: true,
                              animateFromLastPercent: true,
                              animation: true,
                              circularStrokeCap: CircularStrokeCap.round,
                              curve:Curves.linear ,
                              backgroundWidth: -2,
                              center:   Text(value.dTotalCount.toStringAsFixed(0),style: TextStyle(color:progressColor ),),
                              backgroundColor: my_white,
                              progressColor: progressColor,
                              footer:  Padding(
                                padding: const EdgeInsets.only(top: 5.0,bottom: 5.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children:  [
                                    const Text(
                                      "Total",
                                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                    ),
                                    Text(
                                      ' ₹ ${value.dTotalSaleAmount.toString()}',
                                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                    ),
                                  ],
                                ),
                              ),
                            );

                          },

                        ),
                        Consumer<MainSaleProvider>(
                          builder: (context,value,child){
                            print('dSetteledPerc'+value.dSetteledPerc.toString());
                            return CircularPercentIndicator(
                              radius: 100.0,
                              lineWidth: 10.0,
                              percent: value.dSetteledPerc,
                              addAutomaticKeepAlive: true,
                              animateFromLastPercent: true,
                              animation: true,
                              circularStrokeCap: CircularStrokeCap.round,
                              curve:Curves.linear ,
                              backgroundWidth: -2,



                              center:   Text(value.dSetteledCount.toStringAsFixed(0),style: TextStyle(color:progressColor ),),
                              backgroundColor: my_white,
                              progressColor: progressColor,
                              footer:  Padding(
                                padding: const EdgeInsets.only(top: 5.0,bottom: 5.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children:  [
                                    const Text(
                                      "Settled",
                                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                    ),
                                    Text(
                                      ' ₹ ${value.dTotalSettledAmount.toString()}',
                                      style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                    ),
                                  ],
                                ),
                              ),
                            );

                          },
                        ),
                        Consumer<MainSaleProvider>(
                          builder: (context,value,child){
                            return CircularPercentIndicator(
                              radius: 100.0,
                              lineWidth: 10.0,
                              percent: value.dPendingPerc,
                              addAutomaticKeepAlive: true,
                              animateFromLastPercent: true,
                              animation: true,
                              circularStrokeCap: CircularStrokeCap.round,
                              curve:Curves.linear ,
                              backgroundWidth: -2,



                              center:   Text((value.dPendingCount).toStringAsFixed(0),style: const TextStyle(color:progressColor),),
                              backgroundColor: my_white,
                              progressColor: progressColor,
                              footer:  Padding(
                                padding: const EdgeInsets.only(top: 5.0,bottom: 5.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children:  [
                                    const Text(
                                      "Pending",
                                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                    ),
                                    Text(
                                      ' ₹ ${(value.dTotalSaleAmount-value.dTotalSettledAmount).toStringAsFixed(1)}',
                                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0,color: progressColor),
                                    ),
                                  ],
                                ),
                              ),
                            );

                          },
                        ),
                      ],

                    ),

                  ),
                ),
              ) ,
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Payments',
                      style: smallHeaderText,
                    ),
                    GestureDetector(
                      onTap: (){
                        CounterWiseProvider counterWiseProvider = Provider.of<CounterWiseProvider>(context,listen: false);
                        DateTime fromDate =  DateTime.now();
                        DateTime toDate =  DateTime.now();
                        fromDate=DateTime(fromDate.year, fromDate.month, fromDate.day,  1, 59,59);
                        toDate=DateTime(toDate.year, toDate.month, toDate.day, 23, 59,59);
                        counterWiseProvider.funGetConfigCounter(context);
                        counterWiseProvider.funGetCounterData(context,fromDate,toDate);
                        callNext( CounterWiseReport(), context);
                      },
                      child: Text(
                        'View Counter-wise',
                        style: smallHeaderText,
                      ),
                    ),
                  ],

                ),
              ),

              Consumer<MainSaleProvider>(
                builder: (context,value,child){
                  return
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Card(
                        color: cardBgColor,
                        shape: BeveledRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: SizedBox(
                            height:queryData.size.height* 0.30 ,
                            child: value.paymentWisePieGS.isNotEmpty?
                            SfCircularChart(
                              borderWidth: 2,
                              tooltipBehavior: TooltipBehavior(enable: true),
                              legend: Legend(
                                  isVisible: true,
                                  // textStyle: headerText,
                                  position: LegendPosition.right

                              ),
                              // title: ChartTitle(text: 'Counter 1 Payments', textStyle: headerText),
                              annotations: [
                                CircularChartAnnotation(
                                  widget: Text(value.dTotalAmount.toStringAsFixed(2), style: annotationText),
                                )
                              ],
                              series: <CircularSeries>[
                                DoughnutSeries<CounterPieGS, String>(
                                  innerRadius: '65%',
                                  explode: true,
                                  dataSource:value.paymentWisePieGS,
                                  xValueMapper: (CounterPieGS data, _) => data.labelData,
                                  yValueMapper: (CounterPieGS data, _) => data.total,
                                  dataLabelMapper: (CounterPieGS data, _) => data.type,
                                  pointColorMapper: (CounterPieGS data, _) => data.color,
                                )
                              ],
                            ): const Center(child: Text('No counter close'),)
                        ),
                      ),
                    );

                },
              ) ,
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Item-wise Report',
                      style: smallHeaderText,
                    ),
                    GestureDetector(
                      onTap: (){
                        callNext( TotalItemWiseReport(from:"Main"), context);
                      },
                      child: Text(
                        'View All',
                        style: smallHeaderText,
                      ),
                    ),
                  ],

                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Card(
                  color: cardBgColor,
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Consumer<MainSaleProvider>(
                    builder: (context,value,child){
                      return   value.itemWiseMainGS.isNotEmpty?
                      ListView.builder(
                          itemCount: value.itemWiseMainGS.length>5?5:value.itemWiseMainGS.length,
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),


                          itemBuilder: (context, index) {
                            return Card(
                              color: cardBgColor,
                              child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children:  [

                                      Row(
                                        children: [
                                          CircleAvatar(
                                            child: Text(value.itemWiseMainGS[index].itemCount),
                                            radius: 20,
                                            backgroundColor: my_white,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(left: 8.0),
                                            child: Text(value.itemWiseMainGS[index].itemName),
                                          ),
                                        ],
                                      ),
                                      Text(value.itemWiseMainGS[index].itemPrice),
                                    ],
                                  )),
                            );
                          }
                      ):const SizedBox(height: 10,);


                    },
                  ),
                ),
              ) ,
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Steward-wise Report',
                      style: smallHeaderText,
                    ),
                    GestureDetector(
                      onTap: (){
                        callNext(StewardWiseReport(From:"Main"), context);
                      },
                      child: Text(
                        'View All',
                        style: smallHeaderText,
                      ),
                    ),
                  ],

                ),
              ),

              Consumer<MainSaleProvider>(
                builder: (context,value,child){
                  return value.stewardMainWiseGS.isNotEmpty?
                  ListView.builder(
                      itemCount: value.stewardMainWiseGS.length>5?5:value.stewardMainWiseGS.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return Card(
                          color: cardBgColor,
                          child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                children:  [
                                  const Expanded(
                                    flex:1,
                                    child: CircleAvatar(
                                      radius: 20,
                                      backgroundColor: my_white,
                                    ),
                                  ),
                                  Expanded(
                                      flex:5,
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(left: 8.0),
                                            child: Text(value.stewardMainWiseGS[index].itemName),
                                          ),
                                        ],
                                      )),
                                  Expanded(
                                      flex: 1,
                                      child: Text(value.stewardMainWiseGS[index].itemCount)),
                                ],
                              )),
                        );

                      }
                  ):const SizedBox(height: 10,);

                },
              )


            ],
          ),
        ),
      ),
    );
  }
}




class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}


