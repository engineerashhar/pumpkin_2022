import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Provider/mainsale_provider.dart';
import 'package:provider/provider.dart';

class RegeneratedBillList extends StatelessWidget {
  const RegeneratedBillList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<MainSaleProvider>(
        builder: (context,value,child) {
          return value.regeneratedBills.isEmpty?Container(
            child: const Center(child: Text('No Regenerated Bills'),),
          ):ListView.builder(
              itemCount: value.regeneratedBills.length,
              scrollDirection: Axis.vertical,
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  onTap: (){

                  },
                  child: Card(
                    child: Container(
                        margin: const EdgeInsets.symmetric(horizontal: 5,vertical: 5),
                        padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: my_white
                        ),
                        child: Text(value.regeneratedBills[index],style: const TextStyle(
                            fontSize: 20
                        ),)),
                  ),
                );
              });
        }
      ),
    );
  }
}
