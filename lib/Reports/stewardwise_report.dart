import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Constants/my_functions.dart';
import 'package:flutter_pumpkin/Constants/my_text.dart';
import 'package:flutter_pumpkin/Provider/floorwise_provider.dart';
import 'package:flutter_pumpkin/Provider/mainsale_provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class StewardWiseReport extends StatelessWidget {
   String From='';
   StewardWiseReport({Key? key,required this.From}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        backgroundColor: my_white,
        title: Text(
          'Steward-Wise Report',
          style: GoogleFonts.redHatDisplay(fontWeight: FontWeight.bold, fontSize: textSize18, color: my_black),
        ),
        leading: InkWell(
            onTap: () {
              finish(context);
            },
            child: const Icon(Icons.arrow_back_ios, color:  my_black,)),

      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Card(
            color: cardBgColor,
            shape: BeveledRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child:
            From=="Main"?
            Consumer<MainSaleProvider>(
              builder: (context,value,child){
                return value.stewardMainWiseGS.isNotEmpty?
                ListView.builder(
                    itemCount: value.stewardMainWiseGS.length,
                    shrinkWrap: true,
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Card(
                        color: cardBgColor,
                        child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              children:  [
                                const Expanded(
                                  flex:1,
                                  child: CircleAvatar(
                                    radius: 20,
                                    backgroundColor: my_white,
                                  ),
                                ),
                                Expanded(
                                    flex:5,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 8.0),
                                      child: Text(value.stewardMainWiseGS[index].itemName),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Text(value.stewardMainWiseGS[index].itemCount)),
                              ],
                            )),
                      );

                    }
                ):const SizedBox(height: 10,);

              },
            ):Consumer<FloorWiseProvider>(
              builder: (context,value,child){
                return value.stewarWiseGS.isNotEmpty?
                ListView.builder(
                    itemCount: value.stewarWiseGS.length,
                    shrinkWrap: true,
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Card(
                        color: cardBgColor,
                        child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Row(
                              children:  [
                                const Expanded(
                                  flex:1,
                                  child: CircleAvatar(
                                    radius: 20,
                                    backgroundColor: my_white,
                                  ),
                                ),
                                Expanded(
                                    flex:5,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 8.0),
                                      child: Text(value.stewarWiseGS[index].itemName),
                                    )),
                                Expanded(
                                    flex: 1,
                                    child: Text(value.stewarWiseGS[index].itemCount)),
                              ],
                            )),
                      );

                    }
                ):const SizedBox(height: 10,);

              },
            )
          ),
        ),

      ),


    );
  }
}
