import 'package:flutter/foundation.dart';

class ChartGS {
  final String year;
  final double subscribers;
  int hour;
  ChartGS(this.year, this.subscribers,this.hour);
}