class DishGS{
  String dishID;
  String dishName;
  String dishCategory;
  String dishRate;
  String dishStock;
  String dishUnit;
  String dishDepend;
  String dishDependUses;
  String dishKitchen;
  String dishFloor;
  String dishParent;
  DishGS(this.dishID,this.dishName,this.dishCategory,
      this.dishRate,this.dishStock,this.dishUnit,
      this.dishDepend,this.dishDependUses,this.dishKitchen,
      this.dishFloor,this.dishParent);
}