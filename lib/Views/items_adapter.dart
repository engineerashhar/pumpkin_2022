import 'package:flutter/material.dart';
import 'package:flutter_pumpkin/Constants/my_colors.dart';
import 'package:flutter_pumpkin/Views/itemGS.dart';
class ItemsAdapter extends StatelessWidget {
  ItemGS itemGS;
  int index;
  ItemsAdapter(this.itemGS,this.index,BuildContext context);
  final colors = [
    my_red.withOpacity(0.1),
    my_white,
  ];
  @override
  Widget build(BuildContext context) {

    return Container(
      color: colors[index % colors.length],
      child: Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: 10, vertical: 10),
        child: Row(
          children: <Widget>[
            Container(
              height: 15,
              width: 15,
              margin: const EdgeInsets.only(right: 5),
              // decoration: BoxDecoration(
              //     color: itemGS.Status=="OrderPrinted"?my_green:my_red,
              //     borderRadius: BorderRadius.circular(30)
              //
              // ),
              child:itemGS.Status=="OrderPrinted"? Image.asset("assets/kot_success.png"):Image.asset("assets/kot_error.png")
            ),
            Expanded(
                flex: 4,
                child:
                Text(itemGS.Name)),
            Expanded(
                flex: 2,
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                        "${itemGS.Qty}x${itemGS.Rate}"))),
            Expanded(
                flex: 2,
                child: Align(
                  alignment: Alignment.centerRight,
                    child: Text(
                       "₹ "+ itemGS.Total))),
          ],

        ),
      ),
    );
  }
}
