// ignore: file_names
// ignore_for_file: file_names

import 'package:flutter_pumpkin/Views/subtableGS.dart';

class TablesGS{
  String ID;
  String Name;
  String Status;
  String Floor;
  String Count;
  List<SubtableGS> subtableGS=[];
  String sortNumber;


  TablesGS(this.ID,this.Name,this.Status,this.Floor,this.Count,this.subtableGS,this.sortNumber);
}