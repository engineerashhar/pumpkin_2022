import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pumpkin/Provider/counter_close_report_provider.dart';
import 'package:flutter_pumpkin/Provider/counterwise_provider.dart';
import 'package:flutter_pumpkin/Provider/floorwise_provider.dart';
import 'package:flutter_pumpkin/Provider/reports_provider.dart';
import 'package:provider/provider.dart';

import 'GeneralScreens/splash_screen.dart';

import 'Provider/counter_provider.dart';
import 'Provider/main_provider.dart';
import 'Provider/mainsale_provider.dart';
import 'Provider/order_provider.dart';



void main()async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseDatabase database = FirebaseDatabase.instance;
  database.setPersistenceEnabled(true);
  database.setPersistenceCacheSizeBytes(10000000);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  var fsconnect = FirebaseFirestore.instance;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(

      future: Firebase.initializeApp(),
      builder: (context,snapshot){
        if (snapshot.hasError) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: Scaffold(

              backgroundColor: Colors.grey,
              body: Container(child: Center(child: Text("Errorwwwwwwwww")),),
            ),
          );
        }
        if (snapshot.connectionState == ConnectionState.done) {

          return MultiProvider(
            providers:[
              ChangeNotifierProvider(create: (context)=>MainProvider(),),
              ChangeNotifierProvider(create: (context)=>OrderProvider(),),
              ChangeNotifierProvider(create: (context)=>ReportProvider(),),
              ChangeNotifierProvider(create: (context)=>CounterProvider(),),
              ChangeNotifierProvider(create: (context)=>CounterClosingReportProvider(),),
              ChangeNotifierProvider(create: (context)=>FloorWiseProvider(),),
              ChangeNotifierProvider(create: (context)=>CounterWiseProvider(),),
              ChangeNotifierProvider(create: (context)=>MainSaleProvider(),),
            ],
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              title: 'PUMPKIN',
              theme: ThemeData(
                backgroundColor: Colors.white,
                fontFamily: 'Quicksand',
              ),
              home:SplashScreen(),

            ),
          );
        }


        return MaterialApp(
          debugShowCheckedModeBanner: false,

          home: Scaffold(
            // backgroundColor: Colors.white,
            body: Container(

              child: Center(child: Text("Loading",style: TextStyle(color:Colors.white),)),),
          ),
        );

      },



    );
  }
}


